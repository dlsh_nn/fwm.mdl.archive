#include "define.h"

volatile bool fBatMode;
RTC_TBL rtc_tbl;
CE_MDL ce_mdl;
TARIF_VAR trf_var;

LPHeadPack pAnswHeadPack = 0;
uint32_t size_data_AnswPrtcl_, size_AnswHeadPack;

//=============================================================================
// rtc/rtc.c
//=============================================================================
time_t GetTmtBgnSummerWinter_tmt(time_t tmt, bool fSummer){
    DATE_TIME dts;
    
    GetDts_tmt(tmt, &dts);
    return GetTmtBgnSummerWinter(&dts, fSummer);
}
time_t GetTmtBgnSummerWinter(LPDATE_TIME pdst, bool fSummer){
    time_t tmt_Bgn;
    DATE_TIME dts_Bgn;
    
    memset(&dts_Bgn, 0, sizeof(dts_Bgn));
    dts_Bgn.date = 31;
    dts_Bgn.year = pdst->year;
    if(fSummer){
        dts_Bgn.hour = 2;
        dts_Bgn.month = March;
    }else{
        dts_Bgn.hour = 3;
        dts_Bgn.month = October;
    }
    tmt_Bgn = GetTmt_dts(&dts_Bgn);
    GetDts_tmt(tmt_Bgn, &dts_Bgn);
    tmt_Bgn -= (TMT_1DAY * ((uint32_t)(dts_Bgn.day - Sunday)));        
    
    return tmt_Bgn;
}
uint8_t CheckSummerWinter_tmt(time_t tmt){
    time_t tmt_BgnSummer, tmt_BgnWinter;
    
    tmt_BgnSummer = GetTmtBgnSummerWinter_tmt(tmt, true);
    tmt_BgnWinter = GetTmtBgnSummerWinter_tmt(tmt, false);

    if((tmt_BgnSummer <= tmt) && (tmt < tmt_BgnWinter)) return MSK_SUMMER_TIME;
    return 0;
}
uint8_t SetSummerWinter_Check(LPDATE_TIME pdts){
    time_t tmt_BgnSummer, tmt_BgnWinter, tmt;
    
    if(rtc_tbl.rtc_save.fSwitchSummer){
        tmt_BgnSummer = GetTmtBgnSummerWinter(pdts, true);
        tmt_BgnWinter = GetTmtBgnSummerWinter(pdts, false);
        tmt = GetTmt_dts(pdts);
        
        if((tmt_BgnSummer <= tmt) && (tmt < tmt_BgnWinter)) return MSK_SUMMER_TIME;
    }
    return 0;
}
uint8_t SetSummerWinter(void){
    if(rtc_tbl.rtc_save.fSwitchSummer){
        if(rtc_tbl.rtc_save.fSummerTime){
            return MSK_SUMMER_TIME;
        }
    }
    return 0;
}
void CheckValidWinterTime(bool fNeedCorectTime, bool fNeedSavePPR){
    time_t tmt_BgnSummer, tmt_BgnWinter;
    DATE_TIME dts_old;

    if(!rtc_tbl.rtc_save.tmt_BlockCorrectWinter){
        memcpy(&dts_old, &rtc_tbl.crnt_dts, sizeof(dts_old));
        // -- BgnSummer, BgnWinter
        tmt_BgnSummer = GetTmtBgnSummerWinter(&rtc_tbl.crnt_dts, true);
        tmt_BgnWinter = GetTmtBgnSummerWinter(&rtc_tbl.crnt_dts, false);
        // --
        if( (tmt_BgnSummer <= rtc_tbl.crnt_tmt) && 
            ((rtc_tbl.crnt_tmt < tmt_BgnWinter)) && !rtc_tbl.rtc_save.fSummerTime){
            if(fNeedCorectTime){
                if(fNeedSavePPR){
                    arch_var.fSaveArch = true;
                    EventSavePwrProfile(ePrflValid);
                }
                rtc_tbl.crnt_dts.hour++;
                rtc_tbl.crnt_dts.sec = 0;
                //AddRecord_jrnl(eJRNL_SYNC_MTR_HRD, &dts_old, &rtc_tbl.crnt_dts, 0);
            }
            rtc_tbl.rtc_save.fSummerTime = true;
            //SaveRTC_PwrOff();
            if(fNeedCorectTime){
                //write_rtc(&rtc_tbl.crnt_dts);
                rtc_tbl.crnt_tmt = GetTmt_dts(&rtc_tbl.crnt_dts);
            }
        }else if( ( (rtc_tbl.crnt_tmt < tmt_BgnSummer) || (tmt_BgnWinter <= rtc_tbl.crnt_tmt) )
                    && rtc_tbl.rtc_save.fSummerTime ){
            if(fNeedCorectTime){
                if(fNeedSavePPR){
                    arch_var.fSaveArch = true;
                    EventSavePwrProfile(ePrflValid);
                }
                rtc_tbl.crnt_dts.hour--;
                rtc_tbl.crnt_dts.sec = 0;
                //AddRecord_jrnl(eJRNL_SYNC_MTR_HRD, &dts_old, &rtc_tbl.crnt_dts, 0);
                rtc_tbl.rtc_save.tmt_BlockCorrectWinter = tmt_BgnWinter;
            }
            rtc_tbl.rtc_save.fSummerTime = false;
            //SaveRTC_PwrOff();
            if(fNeedCorectTime){
                rtc_tbl.crnt_tmt = GetTmt_dts(&rtc_tbl.crnt_dts);
                //write_rtc(&rtc_tbl.crnt_dts);
            }
        }
    }else if(rtc_tbl.rtc_save.tmt_BlockCorrectWinter < rtc_tbl.crnt_tmt){
        rtc_tbl.rtc_save.tmt_BlockCorrectWinter = 0;
        //SaveRTC_PwrOff();
    }
}
//=============================================================================
// protocol/protocol.c
//=============================================================================
void AnswPrtcl(LPPROTOCOL_DATA prtcl_data, uint16_t size_data){
    prtcl_data->size_data = size_data;
}
void AnswPrtcl_begin(LPPROTOCOL_DATA prtcl_data, uint16_t size_data){
    if(!pAnswHeadPack){
        size_AnswHeadPack = 500000 + size_data;
        pAnswHeadPack = (LPHeadPack)malloc(size_AnswHeadPack);
        
        memcpy(pAnswHeadPack->Data, prtcl_data->pHead->Data, size_data);
        size_data_AnswPrtcl_ = size_data;
    }else{
        // TODO: Bags!!! pAnswBuff here should be ZERO!
        size_data_AnswPrtcl_ = -1;
    }
}
void AnswPrtcl_data(LPPROTOCOL_DATA prtcl_data, uint16_t size_data){
    if( (size_data_AnswPrtcl_ + size_data) > size_AnswHeadPack){
        size_AnswHeadPack = (size_data_AnswPrtcl_ + size_data) + sizeof(HeadPack);
        pAnswHeadPack = (LPHeadPack)realloc(pAnswHeadPack, size_AnswHeadPack);
    }
    memcpy(&pAnswHeadPack->Data[size_data_AnswPrtcl_], prtcl_data->pHead->Data, size_data);
    size_data_AnswPrtcl_ += size_data;
}
void AnswPrtcl_end(LPPROTOCOL_DATA prtcl_data){
    void *pHead = prtcl_data->pHead;
    
    prtcl_data->pHead = pAnswHeadPack;
    prtcl_data->size_data = size_data_AnswPrtcl_;
    // --
    free(pHead);
    pAnswHeadPack = 0;
    size_data_AnswPrtcl_ = 0;
    size_AnswHeadPack = 0;
}