#include "define.h"


void GetDts_tmt(time_t tmt, LPDATE_TIME pdts){
    struct tm stm;
    
    mgmtime(&tmt, &stm);
    pdts->sec = stm.tm_sec;
    pdts->min = stm.tm_min;
    pdts->hour = stm.tm_hour;
    pdts->day = stm.tm_wday + 1;
    pdts->date = stm.tm_mday;
    pdts->month = stm.tm_mon + 1;
    pdts->year = stm.tm_year - 100;
}
void GetDs_tmt(time_t tmt, LPDATE pds){
    struct tm stm;
    
    mgmtime(&tmt, &stm);
    pds->date = stm.tm_mday;
    pds->month = stm.tm_mon + 1;
    pds->year = stm.tm_year - 100;
}
time_t GetTmt_dts(LPCDATE_TIME pdts){
    time_t tmt;
    struct tm stm;
    
    memset(&stm, 0, sizeof(stm));
    stm.tm_sec = pdts->sec;
    stm.tm_min = pdts->min;
    stm.tm_hour = pdts->hour;
    stm.tm_wday = (pdts->day == 0) ? pdts->day : (pdts->day - 1);
    stm.tm_mday = pdts->date;
    stm.tm_mon = pdts->month - 1;
    stm.tm_year = pdts->year + 100;
    tmt = mmktime(&stm);
    
    return tmt;
}
time_t GetTmt_ds(LPCDATE pds){
    time_t tmt;
    struct tm stm;
    
    memset(&stm, 0, sizeof(stm));
    stm.tm_mday = pds->date;
    stm.tm_mon = pds->month - 1;
    stm.tm_year = pds->year + 100;
    tmt = mmktime(&stm);
    
    return tmt;
}
time_t GetTmt_ts(LPTIME pts){
    time_t tmt;
    struct tm stm;
    
    memset(&stm, 0, sizeof(stm));
    stm.tm_sec = pts->sec;
    stm.tm_min = pts->min;
    stm.tm_hour = pts->hour;
    stm.tm_year = 100;
    tmt = mmktime(&stm);
    
    return tmt;
}
bool check_ts(LPTIME ts){
    if((ts->sec > 59) || (ts->min > 59) || (ts->hour > 23)) return false;
    else return true;
}
bool check_dts(LPDATE_TIME dts){
    DATE_TIME dts_check;
    time_t tmt_check;
    
    // Time
    if( (dts->sec > 59) || (dts->min > 59) || (dts->hour > 23) ) return false;
    // -- day
    if( (dts->day < Sunday) || (dts->day > Saturday) ) return false;
    // -- date
    if( (dts->date < 1) || (dts->date > 31) ||
        (dts->month < January) || (dts->month > December) ) return false;
    // -- Check DATE and TIME == day
    tmt_check = GetTmt_dts(dts);
    GetDts_tmt(tmt_check, &dts_check);
    if(dts->day != dts_check.day) return false;
    
    return true;
}
void check_dts_correct(LPDATE_TIME pdts){
    time_t tmt;
    uint8_t zero_array[6] = {0,0,0,0,0,0};

    if(!memcmp(&pdts->min, zero_array, sizeof(*pdts) - offsetof(DATE_TIME, min)) && (pdts->sec != 0) ){
        tmt = rtc_tbl.crnt_tmt + (int8_t)pdts->sec;
        GetDts_tmt(tmt, pdts);
    }
}