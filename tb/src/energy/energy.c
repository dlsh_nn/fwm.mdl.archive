#include "define.h"

#if (_71M6541F || _71M6543 || _71M6541GT)
    ENERGY_DATA enrgDt _at_ 0x10AB;
#else
    ENERGY_DATA enrgDt;
#endif

void ClrEnergy(void){
    memset(&enrgDt, 0, sizeof(enrgDt));
    SaveEnergy();
}
void InitEnergy(void){
    rd_ext_mem(GetAddr(enrg_save), &enrgDt, sizeof(enrgDt));
    // -- Check Energy
    if(!fBatMode){
        Check_enrgDt_valid();
    }
}
void Energy_exec(void){
    uint8_t i;
    
    if(ce_mdl.ce_energy_update){
        ce_mdl.ce_energy_update = false;
        
        if(trf_var.crnt_indx < NMB_TARIF){
            enrgDt.ETarif.enrg[trf_var.crnt_indx].Ap += ce_mdl.enrg_int.Ap;
            enrgDt.ETarif.enrg[trf_var.crnt_indx].An += ce_mdl.enrg_int.An;
            enrgDt.ETarif.enrg[trf_var.crnt_indx].Rp += ce_mdl.enrg_int.Rp;
            enrgDt.ETarif.enrg[trf_var.crnt_indx].Rn += ce_mdl.enrg_int.Rn;
            // --
            enrgDt.EpnTarif.enrg[trf_var.crnt_indx].Ap += (ce_mdl.enrg_int.Ap + ce_mdl.enrg_int.An);
            enrgDt.EpnTarif.enrg[trf_var.crnt_indx].Rp += (ce_mdl.enrg_int.Rp + ce_mdl.enrg_int.Rn);
        }
        memset(&enrgDt.EsumT_Ap, 0, sizeof(enrgDt) - offsetof(ENERGY_DATA, EsumT_Ap));
        for(i = 0; i < NMB_TARIF; i++){
            // -- Ap, An
            enrgDt.EsumT_Ap += enrgDt.ETarif.enrg[i].Ap;
            enrgDt.EsumT_An += enrgDt.ETarif.enrg[i].An;
            // -- Rp, Rn
            enrgDt.EsumT_Rp += enrgDt.ETarif.enrg[i].Rp;
            enrgDt.EsumT_Rn += enrgDt.ETarif.enrg[i].Rn;
        }
        // -- Apn, Rpn
        enrgDt.EsumT_Apn = enrgDt.EsumT_Ap + enrgDt.EsumT_An;
        enrgDt.EsumT_Rpn = enrgDt.EsumT_Rp + enrgDt.EsumT_Rn;
        // -- check overflow
        overflow_check();
        // -- PPR
        PwrProfile_update();
        // -- clr
        memset(&ce_mdl.enrg_int, 0, sizeof(ce_mdl.enrg_int));
    }
}
void overflow_check(void){
    uint32_t *pVal = (uint32_t *)&enrgDt;
    uint16_t i;
    
    for(i = 0; i < (sizeof(enrgDt) / sizeof(uint32_t)); i++, pVal++){
        if(*pVal > WHPMAX){
            *pVal %= WHPMAX;
        }
    }
}
void Check_enrgDt_valid(void){
    
}