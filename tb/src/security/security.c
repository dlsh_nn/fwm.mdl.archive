#include "define.h"

bool pin_INIT = false;

bool CheckPsw(uint8_t *chk_psw, enum enTypePsw type){
    bool r = true;
    uint8_t  MyPsw[PSW_SIZE];
    
    if(!pinAllAccess){
        rd_ext_mem(GetAddr(Password[type]), MyPsw, sizeof(MyPsw));
        if(memcmp(chk_psw, MyPsw, sizeof(MyPsw))) r = false;
    }

    return r;
}
void SetPsw(uint8_t *new_psw, enum enTypePsw type){
    wr_ext_mem(GetAddr(Password[type]), new_psw, PSW_SIZE);
}
void ClrPsw(void){
    set_ext_mem(GetAddr(Password[0]), 0, NMB_TYPE_PSW * PSW_SIZE);
}





