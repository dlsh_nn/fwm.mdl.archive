#include "define.h"

#ifdef __UNIT_TEST__

EXT_MEMORY ExtMemory;
EXT_FLASH ExtFlash;
LPEXT_MEMORY pExtMemory = 0;

/*
 * rd_ext_mem - считывает кусок памяти из EEPROM или FLASH начиная с addr
 * размером size в dst 
 */
void rd_ext_mem(AEMem_t addr, void *dst, uint16_t size){
    // -- Check valid
    if(addr >= (SIZE_EEPROM + SIZE_FLASH)) return;
    // --
    if(addr < SIZE_EEPROM){
        memcpy(dst, &ExtMemory.EEPROM.sect.raw[addr], size);
    }else{
        addr -= SIZE_EEPROM;
        memcpy(dst, &ExtFlash.FLASH.sect.raw[addr], size);
    }
}
void wr_ext_mem(AEMem_t addr, const void *src, uint16_t size){
    // -- Check valid
    if(addr >= (SIZE_EEPROM + SIZE_FLASH)) return;
    // --
    if(addr < SIZE_EEPROM){
        memcpy(&ExtMemory.EEPROM.sect.raw[addr], src, size);
    }else{
        addr -= SIZE_EEPROM;
        // memcpy(&ExtFlash.FLASH.sect.raw[addr], src, size);
        for(uint16_t i = 0; i < size; i++){
            ExtFlash.FLASH.sect.raw[addr + i] &= ((uint8_t*)src)[i];
        }
    }
}
void set_ext_mem(AEMem_t addr, uint8_t val, uint16_t size){
    // -- Check valid
    if(addr >= (SIZE_EEPROM + SIZE_FLASH)) return;
    // --
    if(addr < SIZE_EEPROM){
        memset(&ExtMemory.EEPROM.sect.raw[addr], val, size);
    }else{
        addr -= SIZE_EEPROM;
        //memset(&ExtFlash.FLASH.sect.raw[addr], val, size);
        for(uint16_t i = 0; i < size; i++){
            ExtFlash.FLASH.sect.raw[addr + i] &= val;
        }
    }
}

void ers_at25_all(void){
    memset(&ExtFlash, -1, sizeof(ExtFlash));
}
void ers_at25(uint32_t addr, uint32_t size){
    if( (addr + size) > SIZE_FLASH) goto __error;
    
    memset(&ExtFlash.FLASH.sect.raw[addr], -1, size);
    
    return;
__error:
    // TODO: Error overflow!!!
    return;
}
void unpr_at25_all(void){}
void unpr_at25(uint32_t addr, uint32_t size){}

void ers_ExtMemory(void){
    memset(&ExtMemory, -1, sizeof(ExtMemory));
}

#endif