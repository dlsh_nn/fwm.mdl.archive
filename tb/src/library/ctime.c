/*
 *	Ctime for HI-TECH C - no daylight saving
 */
#include "define.h"

static const uint8r_t moninit[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

static int16_t dylen(int16_t yr){
	if(yr%4 || (yr % 100) == 0 && ((yr+300)%400) != 0)
		return(365);
	return(366);
}
void mgmtime(const time_t * tp, struct tm *ptim){
	union {
		time_t		t;
		struct {
			unsigned char	b, d;
		}	cs;
	}	tc;

	tc.t = *tp;
	ptim->tm_sec = tc.t % 60L;
	tc.t /= 60L;
	ptim->tm_min = tc.t % 60L;
	tc.t /= 60L;
	ptim->tm_hour = tc.t % 24L;
	ptim->tm_mday = tc.t / 24L;
	ptim->tm_wday = (ptim->tm_mday + 4) % 7;
	ptim->tm_year = 70;
	while(ptim->tm_mday >= dylen(ptim->tm_year)) {
		ptim->tm_mday -= dylen(ptim->tm_year);
		ptim->tm_year++;
	}
	/* this value will be 2 for a leap year, 0 otherwise */
	tc.cs.b = (unsigned char)dylen(ptim->tm_year) & 2;
	ptim->tm_yday = ptim->tm_mday;
	ptim->tm_mon = 0;
	while(ptim->tm_mday >= (tc.cs.d = moninit[ptim->tm_mon] | tc.cs.b)) {
		ptim->tm_mday -= tc.cs.d;
		ptim->tm_mon++;
		tc.cs.b >>= 1;
	}
	ptim->tm_mday++;
}
