/****************************************************************************
 * This code and information is provided "as is" without warranty of any
 * kind, either expressed or implied, including but not limited to the
 * implied warranties of merchantability and/or fitness for a particular
 * purpose.
 *
 * Copyright (C) 2008 Maxim Integrated Products, Inc. All Rights Reserved.
 * DESCRIPTION: This file has math functions that are not present in
 * the standard library.
 * 
 * AUTHOR:  RGV
 *
 * HISTORY: See end of file.
 **************************************************************************/
#include "define.h"

#pragma save
#pragma NOAREGS
uint32_t abs_my(int32_t val) small reentrant {
    uint32_t val_ret = val;
    
    val_ret = (val < 0) ? (val * -1L) : val;
    
    return val_ret;
}
#pragma restore
