#include "define.h"

uint8r_t crc8_table [256] = {
     0,    7,   14,    9,   28,   27,   18,   21,   56,   63,   54,   49,   36,   35,   42,   45,
     112,  119,  126,  121,  108,  107,   98,  101,   72,   79,   70,   65,   84,   83,   90,   93,
     224,  231,  238,  233,  252,  251,  242,  245,  216,  223,  214,  209,  196,  195,  202,  205,
     144,  151,  158,  153,  140,  139,  130,  133,  168,  175,  166,  161,  180,  179,  186,  189,
     199,  192,  201,  206,  219,  220,  213,  210,  255,  248,  241,  246,  227,  228,  237,  234,
     183,  176,  185,  190,  171,  172,  165,  162,  143,  136,  129,  134,  147,  148,  157,  154,
      39,   32,   41,   46,   59,   60,   53,   50,   31,   24,   17,   22,    3,    4,   13,   10,
      87,   80,   89,   94,   75,   76,   69,   66,  111,  104,   97,  102,  115,  116,  125,  122,
     137,  142,  135,  128,  149,  146,  155,  156,  177,  182,  191,  184,  173,  170,  163,  164,
     249,  254,  247,  240,  229,  226,  235,  236,  193,  198,  207,  200,  221,  218,  211,  212,
     105,  110,  103,   96,  117,  114,  123,  124,   81,   86,   95,   88,   77,   74,   67,   68,
      25,   30,   23,   16,    5,    2,   11,   12,   33,   38,   47,   40,   61,   58,   51,   52,
      78,   73,   64,   71,   82,   85,   92,   91,  118,  113,  120,  127,  106,  109,  100,   99,
      62,   57,   48,   55,   34,   37,   44,   43,    6,    1,    8,   15,   26,   29,   20,   19,
     174,  169,  160,  167,  178,  181,  188,  187,  150,  145,  152,  159,  138,  141,  132,  131,
     222,  217,  208,  215,  194,  197,  204,  203,  230,  225,  232,  239,  250,  253,  244,  243
};
uint8_t crc_calc(uint8_t crc, void *pData, uint16_t len){
    uint8_t *psrc = (uint8_t *)pData;
    
    while(len--){
        crc = crc8_table[crc ^ *psrc++] & 0xFF;
    }

    return crc;
}

void NextRecord(LPHEAD_LIST pHead, uint16_t NmbRcrd_Cnfg, uint16_t NmbRcrd_Capacity){
    // -- clear sector if begin
    if(!pHead->indx_rcrd && !pHead->nmb_rcrds){
        ers_at25(pHead->Addr_rcrd - SIZE_EEPROM, 1);
        unpr_at25(pHead->Addr_rcrd - SIZE_EEPROM, SIZE_SECTOR);
    }else{
        // -- error head
        if((pHead->indx_rcrd >= NmbRcrd_Capacity) || (pHead->nmb_rcrds > NmbRcrd_Cnfg)){
            pHead->indx_rcrd = 0;
            pHead->nmb_rcrds = 0;
        }else pHead->indx_rcrd++;
    }
    // -- next record
    if(pHead->nmb_rcrds == NmbRcrd_Cnfg){
        if(pHead->indx_rcrd >= NmbRcrd_Capacity) pHead->indx_rcrd = 0;
    }else{
        pHead->nmb_rcrds++;
    }    
}
uint32_t Addr_to_Indx(AEMem_t Addr, LPCLIST_FLASH_MEM plist){
    uint32_t indx;

    // -- Check Valid Addr
    if((Addr < plist->ADDR_BGN) || ((plist->ADDR_BGN + plist->SIZE_SECT) <= Addr)){
        Addr = plist->ADDR_BGN;
    }
    // -- 
    Addr -= plist->ADDR_BGN;
    indx = (Addr / SIZE_SECTOR) * plist->NMB_RCRD_SECT;
    indx += (Addr % SIZE_SECTOR) / plist->SIZE_RECORD;
    
    return indx;
}
AEMem_t Indx_to_Addr(uint32_t indx, LPCLIST_FLASH_MEM plist){
    AEMem_t Addr = plist->ADDR_BGN;

    Addr += (indx / plist->NMB_RCRD_SECT) * SIZE_SECTOR;
    Addr += (indx % plist->NMB_RCRD_SECT) * plist->SIZE_RECORD;
//    AEMem_t Addr;
//    Addr = indx*(SIZE_SECTOR / (plist->NMB_RCRD_SECT + (SIZE_SECTOR-1)*plist->SIZE_RECORD));
//    Addr += plist->ADDR_BGN;
    // -- Check Valid Addr
    if((Addr < plist->ADDR_BGN) || ((plist->ADDR_BGN + plist->SIZE_SECT) <= Addr)){
        Addr = plist->ADDR_BGN;
    }
    
    return Addr;
}

/*
 * Получаем адрес по индексу indx.
 * Необходимо указать адрес первой записи? начала блока? Addr_rcrd
 * и plist (что это?)
 */
AEMem_t GetAddrRecord(int32_t indx, AEMem_t Addr_rcrd, LPCLIST_FLASH_MEM plist){
    int32_t indx_crnt, indx_new;
    AEMem_t Addr_new;
//    AEMem_t Addr_tmp;
//    int32_t indx_tmp;
    
    //получаем первый адрес в блоке 
    indx_crnt = Addr_to_Indx(Addr_rcrd, plist);
//    
//    indx_tmp = Addr_to_Indx(Addr_tmp + 58, plist);
//
//    Addr_tmp = Indx_to_Addr(indx_crnt + 1, plist);
//    indx_tmp = Addr_to_Indx(Addr_tmp, plist);
//    
//    Addr_tmp = Indx_to_Addr(indx_crnt - 1, plist);
//    indx_tmp = Addr_to_Indx(Addr_tmp, plist);
    // --
    indx_new = indx_crnt + indx;
    while(indx_new < 0){
        indx_new += plist->NMB_RCRD_MAX;
    }
    while(indx_new > (plist->NMB_RCRD_MAX - 1)){
        indx_new -= plist->NMB_RCRD_MAX;
    }
    
    // --
    Addr_new = Indx_to_Addr(indx_new, plist);
    
    return Addr_new;
}
