/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   arch_depth_tcls.cpp
 * Author: User
 *
 * Created on 22.06.2017, 11:59:57
 */
#include "arch_depth_tcls.h"
#include "../tb_utils/tb_utils.h"


CPPUNIT_TEST_SUITE_REGISTRATION(arch_depth_tcls);

void arch_depth_tcls::setUp() {
    tbu.Init();
    // -- Init Time
    mtr::rtc_tbl.crnt_tmt = tmt_DTS_TB_begin;
    mtr::GetDts_tmt(mtr::rtc_tbl.crnt_tmt, &mtr::rtc_tbl.crnt_dts);
    mtr::arch_var.fUpdateRTC = true;
    // -- Init Archive
    mtr::InitArch();
    mtr::reset_arch();
}

void arch_depth_tcls::tearDown() {
}

void arch_depth_tcls::PwrOn_and_continuous_work() {
    bool fError = false;
    
    if(fError |= !tbu.TestScenario(nsCNT_WRK::scenario_continuous_work_tb, "scenario_continuous_work_tb")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
