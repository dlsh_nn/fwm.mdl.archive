#include "../tb_utils/tb_utils.h"

namespace nsCNT_WRK{

//=============================================================================
// -- scenario continuous_work and read Archive
//=============================================================================
//// -- Action work 230V, 1A, 45deg 250 day with step_time = 10 sec tmt_end = 1495528200
#if 1 
    // -- Action work /* for 1 day in tmt_step == 1 sec, need 0.25 sec */
    //  for 125 * 2 day tmt_step == 1 sec need 1 min
    //  for 31 year tmt_step == 1 sec need 2828s = 47 min
    #define tmt_TimeWork    250 * 24 * 60 * 60
    #define tb_act_work_250day  { \
                scnrWork, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeWork, 20 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45} } \
            }
#endif
//// -- READ bgn DTS/DS
#if 1 
    // -- Read begin DTS PPR
    #define DTS_TB_begin_alig_halfhour_pls_125day   00, 30, 8, 4, 18, 01, 17 // tmt_125day = 1484728200
    mtr::PACK_ANSW_BGN_DTS_PPR tb_answ_bgn_dts_ppr = {mtr::STS_OK, DTS_TB_begin_alig_halfhour_pls_125day};
    #define tb_rd_bgn_dts_ppr  { scnrNormal, \
                        { mtr::GET_BGN_DTS_PPR, END_PLOAD }, \
                        { mtr::GET_BGN_DTS_PPR, \
                            { {&tb_answ_bgn_dts_ppr, sizeof(tb_answ_bgn_dts_ppr)}, END_PLOAD }, \
                        } \
                    }
    // -- Read begin DS day
    #define DS_TB_begin_alig_day_pls_125day   18, 01, 17 // tmt_125day = 1484728200
    mtr::PACK_GET_BGN_DS_ARCH tb_in_bgn_ds_day = {mtr::eARCH_DAY};
    mtr::PACK_ANSW_BGN_DS_ARCH tb_answ_bgn_ds_day = {mtr::STS_OK, DS_TB_begin_alig_day_pls_125day};
    #define tb_rd_bgn_ds_day  { scnrNormal, \
                        { mtr::GET_BGN_DS_ARCH, \
                            { {&tb_in_bgn_ds_day, sizeof(tb_in_bgn_ds_day)}, END_PLOAD }, \
                        }, \
                        { mtr::GET_BGN_DS_ARCH, \
                            { {&tb_answ_bgn_ds_day, sizeof(tb_answ_bgn_ds_day)}, END_PLOAD }, \
                        } \
                    }
    // -- Read begin DS month
    mtr::PACK_GET_BGN_DS_ARCH tb_in_bgn_ds_month = {mtr::eARCH_MNTH};
    mtr::PACK_ANSW_BGN_DS_ARCH tb_answ_bgn_ds_month = {mtr::STS_OK, DS_TB_begin_alig_month};
    #define tb_rd_bgn_ds_month  { scnrNormal, \
                        { mtr::GET_BGN_DS_ARCH, \
                            { {&tb_in_bgn_ds_month, sizeof(tb_in_bgn_ds_month)}, END_PLOAD }, \
                        }, \
                        { mtr::GET_BGN_DS_ARCH, \
                            { {&tb_answ_bgn_ds_month, sizeof(tb_answ_bgn_ds_month)}, END_PLOAD }, \
                        } \
                    }
#endif
//-----------------------------------------------------------------------------
// -- READ ARCH/PPR
//-----------------------------------------------------------------------------
uint8_t tb_answ_STS_OK = mtr::STS_OK;
#define MTrf_ALL      0xFF
    
    // -- Read PPR *17-01-18Т8:30:00* до *17-05-23Т8:30:00* 
#if 1 
    #define DTS_17_01_18_T_8_30_0    0,  30, 8, 5, 18, 01, 17
    #define DTS_17_5_23_T_8_30_0     0,  30, 8, 5, 23, 05, 17

    mtr::PACK_GET_PWR_PRFL tb_in_ppr_125day_backward = {mtr::enTPPR_NoSkip_PPRv0, {DTS_17_01_18_T_8_30_0}, {DTS_17_5_23_T_8_30_0}};
    #define tb_rd_ppr_125day_backward  { scnrNormal_Check_ConstPQP, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_125day_backward, sizeof(tb_in_ppr_125day_backward)}, END_PLOAD }, }, \
                        { mtr::GET_PWR_PROFILE, END_PLOAD }, \
                        tmt_TimeWork, 15, \
                    }
#endif
    // -- Read ARCH_DAY *16-9-14* до *16-9-16* 
#if 1 
    #define DS_17_01_18    18, 01, 17
    #define DS_17_05_23    23, 05, 17
    
    mtr::PACK_GET_ARCH tb_in_aday_125day_backward = {mtr::enType_NoPass, mtr::eARCH_DAY, MTrf_ALL, {DS_17_01_18}, {DS_17_05_23}};
    #define tb_rd_aday_125day_backward { scnrNormal_Check_ConstPQP, \
                        { mtr::GET_ARCH_MTR, { {&tb_in_aday_125day_backward, sizeof(tb_in_aday_125day_backward)}, END_PLOAD } }, \
                        { mtr::GET_ARCH_MTR, END_PLOAD } \
                      }
                            
#endif
    //// -- Action work 230V, 1A, 45deg 250 day with step_time = 10 sec tmt_end = 1684744200
#if 1 
    #define tmt_TimeWork_6year    6 * 365 * 24 * 60 * 60
    #define tb_act_work_6_year  { \
                scnrWork, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeWork_6year, 20 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45} } \
            }
#endif
#if 1
    // -- Read begin DS month
    #define DS_20_5_1    1, 5, 20
    #define DS_23_5_1    1, 5, 23
    mtr::PACK_ANSW_BGN_DS_ARCH tb_answ_bgn_ds_month_3year = {mtr::STS_OK, DS_20_5_1};
    #define tb_rd_bgn_ds_month_3year  { scnrNormal, \
                        { mtr::GET_BGN_DS_ARCH, \
                            { {&tb_in_bgn_ds_month, sizeof(tb_in_bgn_ds_month)}, END_PLOAD }, \
                        }, \
                        { mtr::GET_BGN_DS_ARCH, \
                            { {&tb_answ_bgn_ds_month_3year, sizeof(tb_answ_bgn_ds_month_3year)}, END_PLOAD }, \
                        } \
                    }
    // -- Read ARCH_MONTH *16-8-1* до *16-10-1* 
    mtr::PACK_GET_ARCH tb_in_amonth_3_year = {mtr::enType_NoPass, mtr::eARCH_MNTH, MTrf_ALL, {DS_20_5_1}, {DS_23_5_1}};
    #define tb_rd_amonth_3_year_backward { scnrNormal_Check_ConstPQP, \
                        { mtr::GET_ARCH_MTR, { {&tb_in_amonth_3_year, sizeof(tb_in_amonth_3_year)}, END_PLOAD } }, \
                        { mtr::GET_ARCH_MTR, END_PLOAD } \
                      }
                            
#endif
//-----------------------------------------------------------------------------
// -- Create SCENARIO_ARCHIVE instance
//-----------------------------------------------------------------------------
    TB_SCENARIO_ARCHIVE scenario_continuous_work_tb[] = {
        //// -- action work
        tb_act_work_250day,
        //// -- READ bgn DTS/DS
        tb_rd_bgn_dts_ppr, tb_rd_bgn_ds_day, tb_rd_bgn_ds_month,
        //// -- READ Arch Day/PPR
        tb_rd_ppr_125day_backward, tb_rd_aday_125day_backward, 
        //// -- action work
        tb_act_work_6_year,
        //// -- READ bgn DS month, READ Arch Month
        tb_rd_bgn_ds_month_3year, tb_rd_amonth_3_year_backward,
        
        { scnrEND }
    };

}