#include "../tb_utils/tb_utils.h"

namespace nsRAT{

//=============================================================================
// -- scenario reset_archive
//=============================================================================
//// -- READ bgn DTS/DS
#if 1 
    // -- Read begin DTS PPR
    mtr::PACK_ANSW_BGN_DTS_PPR tb_answ_bgn_dts_ppr = {mtr::STS_OK, DTS_TB_begin_alig_halfhour};
    #define tb_rd_bgn_dts_ppr  { scnrNormal, \
                        { mtr::GET_BGN_DTS_PPR, END_PLOAD }, \
                        { mtr::GET_BGN_DTS_PPR, \
                            { {&tb_answ_bgn_dts_ppr, sizeof(tb_answ_bgn_dts_ppr)}, END_PLOAD }, \
                        } \
                    }
    // -- Read begin DTS ppr throuth GET_bgn_dts_day
    mtr::PACK_GET_BGN_DS_ARCH tb_in_bgn_ds_ppr = {mtr::eARCH_PPR};
    uint8_t tb_answ_bgn_ds_ppr = mtr::STS_ERR_PRM;
    #define tb_rd_bgn_ds_ppr  { scnrNormal, \
                        { mtr::GET_BGN_DS_ARCH, \
                            { {&tb_in_bgn_ds_ppr, sizeof(tb_in_bgn_ds_ppr)}, END_PLOAD }, \
                        }, \
                        { mtr::GET_BGN_DS_ARCH, \
                            { {&tb_answ_bgn_ds_ppr, sizeof(tb_answ_bgn_ds_ppr)}, END_PLOAD }, \
                        } \
                    }
    // -- Read begin DS day
    mtr::PACK_GET_BGN_DS_ARCH tb_in_bgn_ds_day = {mtr::eARCH_DAY};
    mtr::PACK_ANSW_BGN_DS_ARCH tb_answ_bgn_ds_day = {mtr::STS_OK, DS_TB_begin_alig_day};
    #define tb_rd_bgn_ds_day  { scnrNormal, \
                        { mtr::GET_BGN_DS_ARCH, \
                            { {&tb_in_bgn_ds_day, sizeof(tb_in_bgn_ds_day)}, END_PLOAD }, \
                        }, \
                        { mtr::GET_BGN_DS_ARCH, \
                            { {&tb_answ_bgn_ds_day, sizeof(tb_answ_bgn_ds_day)}, END_PLOAD }, \
                        } \
                    }
    // -- Read begin DS month
    mtr::PACK_GET_BGN_DS_ARCH tb_in_bgn_ds_month = {mtr::eARCH_MNTH};
    mtr::PACK_ANSW_BGN_DS_ARCH tb_answ_bgn_ds_month = {mtr::STS_OK, DS_TB_begin_alig_month};
    #define tb_rd_bgn_ds_month  { scnrNormal, \
                        { mtr::GET_BGN_DS_ARCH, \
                            { {&tb_in_bgn_ds_month, sizeof(tb_in_bgn_ds_month)}, END_PLOAD }, \
                        }, \
                        { mtr::GET_BGN_DS_ARCH, \
                            { {&tb_answ_bgn_ds_month, sizeof(tb_answ_bgn_ds_month)}, END_PLOAD }, \
                        } \
                    }
#endif
//-----------------------------------------------------------------------------
// -- READ ARCH/PPR
//-----------------------------------------------------------------------------
uint8_t tb_answ_STS_OK = mtr::STS_OK;
#define MTrf_ALL      0xFF
    
    // -- Read PPR *16-9-15Т8:30:00* до *16-9-15Т9:00:00* 
#if 1 
    #define DTS_16_9_15_T_8_30_0    0,  30, 8, 5, 15, 9, 16
    #define DTS_16_9_15_T_9_0_0     0,   0, 9, 5, 15, 9, 16

    mtr::PACK_GET_PWR_PRFL tb_in_ppr_8h30_to_9h = {mtr::enTPPR_NoSkip_PPRv0, {DTS_TB_begin_alig_halfhour}, {DTS_16_9_15_T_9_0_0}};
    mtr::PACK_ANSW_PWR_PRFL_V0 tb_answ_ppr_8h30_to_9h[2] = {
        {   {DTS_TB_begin_alig_halfhour}, { {0, 0, 0, 0}, {0, 0, 0, 0}, }, mtr::ePrflCurrent    },
        {   {DTS_16_9_15_T_9_0_0}, { {-1, -1, -1, -1}, {-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde     }
    };
    #define tb_rd_ppr_8h30_to_9h  { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_8h30_to_9h, sizeof(tb_in_ppr_8h30_to_9h)}, END_PLOAD }, }, \
                        { mtr::GET_PWR_PROFILE, \
                            { \
                                {&tb_answ_STS_OK, sizeof(tb_answ_STS_OK)}, \
                                {tb_answ_ppr_8h30_to_9h, sizeof(tb_answ_ppr_8h30_to_9h)}, \
                                END_PLOAD \
                            }, \
                        } \
                    }
#endif
    // -- Read PPR *16-9-15Т7:00:00* до *16-9-15Т9:00:00* 
#if 1 
    #define DTS_16_9_15_T_7_00_0    0,   0, 7, 5, 15, 9, 16
    #define DTS_16_9_15_T_7_30_0    0,  30, 7, 5, 15, 9, 16
    #define DTS_16_9_15_T_8_00_0    0,  00, 8, 5, 15, 9, 16
    #define DTS_16_9_15_T_8_30_0    0,  30, 8, 5, 15, 9, 16
    #define DTS_16_9_15_T_9_00_0    0,   0, 9, 5, 15, 9, 16
    
    mtr::PACK_GET_PWR_PRFL tb_in_ppr_7h_to_9h = {mtr::enTPPR_NoSkip_PPRv0, {DTS_16_9_15_T_7_00_0}, {DTS_16_9_15_T_9_0_0}};
    mtr::PACK_ANSW_PWR_PRFL_V0 tb_answ_ppr_7h_to_9h[5] = {
        {    {DTS_16_9_15_T_7_00_0}, { {-1, -1, -1, -1},{-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde    },
        {    {DTS_16_9_15_T_7_30_0}, { {-1, -1, -1, -1},{-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde    },
        {    {DTS_16_9_15_T_8_00_0}, { {-1, -1, -1, -1},{-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde    },
        {    {DTS_16_9_15_T_8_30_0}, { {0, 0, 0, 0}, {0, 0, 0, 0} }, mtr::ePrflCurrent    },
        {    {DTS_16_9_15_T_9_00_0}, { {-1, -1, -1, -1},{-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde    },
    };
    #define tb_rd_ppr_7h_to_9h { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_7h_to_9h, sizeof(tb_in_ppr_7h_to_9h)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&tb_answ_STS_OK, sizeof(tb_answ_STS_OK)}, \
                              {tb_answ_ppr_7h_to_9h, sizeof(tb_answ_ppr_7h_to_9h)}, END_PLOAD } \
                        } \
                      }
                            
#endif
    // -- Read ARCH_DAY *16-9-14* до *16-9-16* 
#if 1 
    #define DS_16_9_14    14, 9, 16
    #define DS_16_9_15    15, 9, 16
    #define DS_16_9_16    16, 9, 16
    
    mtr::PACK_GET_ARCH tb_in_aday_14_to_15 = {mtr::enType_NoPass, mtr::eARCH_DAY, MTrf_ALL, {DS_16_9_14}, {DS_16_9_16}};
    mtr::PACK_ANSW_ARCH_HEAD tb_answ_aday_14_to_15_head = {mtr::eARCH_DAY, MTrf_ALL};
    mtr::TB_RCRD_ANSW_ARCH_ALLT tb_answ_aday_14_to_15[3] = {
        { {DS_16_9_14}, RCRD_ARCH_NO_ENRG_ALLT },
        { {DS_16_9_15}, RCRD_ARCH_ZERO_ENRG_ALLT },
        { {DS_16_9_16}, RCRD_ARCH_NO_ENRG_ALLT },
    };
    #define tb_rd_aday_14_to_15 { scnrNormal, \
                        { mtr::GET_ARCH_MTR, { {&tb_in_aday_14_to_15, sizeof(tb_in_aday_14_to_15)}, END_PLOAD } }, \
                        { mtr::GET_ARCH_MTR, \
                            { {&tb_answ_STS_OK, sizeof(tb_answ_STS_OK)}, \
                              {&tb_answ_aday_14_to_15_head, sizeof(tb_answ_aday_14_to_15_head)}, \
                              {tb_answ_aday_14_to_15, sizeof(tb_answ_aday_14_to_15)}, END_PLOAD } \
                        } \
                      }
                            
#endif
    // -- Read ARCH_MONTH *16-8-1* до *16-10-1* 
#if 1 
    #define DS_16_8_1    1, 8, 16
    #define DS_16_9_1    1, 9, 16
    #define DS_16_10_1   1, 10, 16
    
    mtr::PACK_GET_ARCH tb_in_amonth_8_to_10 = {mtr::enType_NoPass, mtr::eARCH_MNTH, MTrf_ALL, {DS_16_8_1}, {DS_16_10_1}};
    mtr::PACK_ANSW_ARCH_HEAD tb_answ_month_8_to_10_head = {mtr::eARCH_MNTH, MTrf_ALL};
    mtr::TB_RCRD_ANSW_ARCH_ALLT tb_answ_month_8_to_10[3] = {
        { {DS_16_8_1}, RCRD_ARCH_NO_ENRG_ALLT },
        { {DS_16_9_1}, RCRD_ARCH_ZERO_ENRG_ALLT },
        { {DS_16_10_1}, RCRD_ARCH_NO_ENRG_ALLT },
    };
    #define tb_rd_amonth_8_to_10 { scnrNormal, \
                        { mtr::GET_ARCH_MTR, { {&tb_in_amonth_8_to_10, sizeof(tb_in_amonth_8_to_10)}, END_PLOAD } }, \
                        { mtr::GET_ARCH_MTR, \
                            { {&tb_answ_STS_OK, sizeof(tb_answ_STS_OK)}, \
                              {&tb_answ_month_8_to_10_head, sizeof(tb_answ_month_8_to_10_head)}, \
                              {tb_answ_month_8_to_10, sizeof(tb_answ_month_8_to_10)}, END_PLOAD } \
                        } \
                      }
                            
#endif
//-----------------------------------------------------------------------------
// -- Create SCENARIO_ARCHIVE instance
//-----------------------------------------------------------------------------
    TB_SCENARIO_ARCHIVE scenario_reset_archive_tb[] = {
        // Debug
        
        // ~Debug
        //// -- READ bgn DTS/DS
        tb_rd_bgn_dts_ppr, 
        tb_rd_bgn_ds_ppr, 
        tb_rd_bgn_ds_day, 
        tb_rd_bgn_ds_month,
        //// -- READ arch/ppr
        tb_rd_ppr_8h30_to_9h, tb_rd_ppr_7h_to_9h,   // ppr
        tb_rd_aday_14_to_15,                        // archive->day
        tb_rd_amonth_8_to_10,                       // archive->month
        
        { scnrEND }
    };

}