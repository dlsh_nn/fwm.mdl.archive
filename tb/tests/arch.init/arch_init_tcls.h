/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   arch_init_tcls.h
 * Author: User
 *
 * Created on 07.06.2017, 11:29:38
 */

#ifndef ARCH_INIT_TCLS_H
#define ARCH_INIT_TCLS_H

#include <cppunit/extensions/HelperMacros.h>
#include "../tb_utils/tb_utils.h"

class arch_init_tcls : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(arch_init_tcls);

    CPPUNIT_TEST(reset_arch_tb);
    CPPUNIT_TEST(tst_reset_Duration);

    CPPUNIT_TEST_SUITE_END();

private:
    tb_utils_cls tbu;
    
public:
    void setUp();
    void tearDown();

private:
    void reset_arch_tb();
    void tst_reset_Duration();
};

#endif /* ARCH_INIT_TCLS_H */

