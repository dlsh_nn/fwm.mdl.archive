/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   arch_init_tcls.cpp
 * Author: User
 *
 * Created on 07.06.2017, 11:29:38
 */

#include "arch_init_tcls.h"
#include "../tb_utils/tb_utils.h"


CPPUNIT_TEST_SUITE_REGISTRATION(arch_init_tcls);

void arch_init_tcls::setUp() {
    tbu.Init();
    // -- Init Time
    mtr::rtc_tbl.crnt_tmt = tmt_DTS_TB_begin;
    mtr::GetDts_tmt(mtr::rtc_tbl.crnt_tmt, &mtr::rtc_tbl.crnt_dts);
    // --
    mtr::InitArch();
}

void arch_init_tcls::tearDown() {
}

void arch_init_tcls::reset_arch_tb(){
    bool fError = false;
    
     // -- Set Curent Time
    mtr::rtc_tbl.crnt_tmt = tmt_DTS_TB_begin + 10;
    mtr::GetDts_tmt(mtr::rtc_tbl.crnt_tmt, &mtr::rtc_tbl.crnt_dts);
    // -- 
    mtr::reset_arch();
    
    if(fError |= !tbu.TestScenario(nsRAT::scenario_reset_archive_tb, "scenario_reset_archive_tb")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
void arch_init_tcls::tst_reset_Duration(){
    mtr::arch_var.save.tmt_Duration = -1;
    // -- RESET
    mtr::reset_arch();
    if(mtr::arch_var.save.tmt_Duration != 30 * 60){
        CPPUNIT_FAIL("mtr::arch_var.save.tmt_Duration != 30 * 60 in tst_reset_Duration()"); return;
    }
    // -- Power off/on
    mtr::arch_var.save.tmt_Duration = -1;
    mtr::InitArch();
    if(mtr::arch_var.save.tmt_Duration != 30 * 60){
        CPPUNIT_FAIL("mtr::arch_var.save.tmt_Duration != 30 * 60 in tst_reset_Duration()"); return;
    }
}

