/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   arch_exec_count_tcls.cpp
 * Author: User
 *
 * Created on 18.07.2017, 11:30:52
 */

#include "arch_exec_count_tcls.h"
#include "../../inc/rtc/rtc.h"


CPPUNIT_TEST_SUITE_REGISTRATION(arch_exec_count_tcls);

arch_exec_count_tcls::arch_exec_count_tcls() {
}

arch_exec_count_tcls::~arch_exec_count_tcls() {
}

void arch_exec_count_tcls::setUp() {
    
    tbu.Init();
    
    mtr::rtc_tbl.crnt_tmt = tmt_DTS_TB_begin;
    mtr::GetDts_tmt(mtr::rtc_tbl.crnt_tmt, &mtr::rtc_tbl.crnt_dts);
    
//    uint8_t year = mtr::rtc_tbl.crnt_dts.year;
//    uint8_t month = mtr::rtc_tbl.crnt_dts.month;
//    uint8_t date = mtr::rtc_tbl.crnt_dts.date;
//    uint8_t hours = mtr::rtc_tbl.crnt_dts.hour;
//    uint8_t minutes = mtr::rtc_tbl.crnt_dts.min;
//    uint8_t sec = mtr::rtc_tbl.crnt_dts.sec;
//    
//    
//    time_t crnt_tmt = mtr::rtc_tbl.crnt_tmt;
//    printf("year = %d, month = %d, day = %d, hours = %d, minutes = %d, seconds = %d\n", year, month, date, hours, minutes, sec);
//    printf("crnt_tmt = %d\n", crnt_tmt);
//    
//    crnt_tmt += 6 * 24 * 60 * 60;
//    mtr::GetDts_tmt(crnt_tmt, &mtr::rtc_tbl.crnt_dts);
//    
//    year = mtr::rtc_tbl.crnt_dts.year;
//    month = mtr::rtc_tbl.crnt_dts.month;
//    date = mtr::rtc_tbl.crnt_dts.date;
//    hours = mtr::rtc_tbl.crnt_dts.hour;
//    minutes = mtr::rtc_tbl.crnt_dts.min;
//    sec = mtr::rtc_tbl.crnt_dts.sec;
//    
//    printf("year = %d, month = %d, day = %d, hours = %d, minutes = %d, seconds = %d\n", year, month, date, hours, minutes, sec);
//    printf("crnt_tmt = %d\n", crnt_tmt);
    
    mtr::arch_var.fUpdateRTC = true;
    
    mtr::InitArch();
    
    
}

void arch_exec_count_tcls::tearDown() {
    
}

void arch_exec_count_tcls::testArchExecCount() {
    bool res = tbu.TestScenario(nsAECNT::scenario_checkArchiveExecCount_tb, "scenario_checkArchiveExecCount_tb");
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, res);
}


void arch_exec_count_tcls::testArchExecCountSparseData() {
    bool res = tbu.TestScenario(nsAECNT::scenario_checkArchiveExecCountSparseData_tb, "scenario_checkArchiveExecCountSparseData_tb");
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, res);
}


void arch_exec_count_tcls::testArchExecCountDataWithBreaks() {
    bool res = tbu.TestScenario(nsAECNT::scenario_checkArciveExecCountDataWithBreaks_tb, "scenario_checkArchiveExecCount_tb");
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, res);
}
