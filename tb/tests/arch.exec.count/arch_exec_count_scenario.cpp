#include "../tb_utils/tb_utils.h"

namespace nsAECNT{

//=============================================================================
// -- arch_exec count scenario
//=============================================================================

//// -- Action work 230V, 1A, 45deg 6 years with step_time = 30 min tmt_end = 1663145442
#if 1 
    #define tmt_TimeWork_6_years    6 * 365 * 24 * 60 * 60
    #define tb_act_work_6_years  { \
                scnrWork, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeWork_6_years, 30 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45}, 25 } \
            }

    //#define tb_scnrCheckArchiveExecCount { scnrCheckArchiveExecCount, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD} }
#endif

//-----------------------------------------------------------------------------
// -- READ ARCH/PPR
//-----------------------------------------------------------------------------
// -- Read PPR from the beginning (*22-9-14T8:30:00*)
#if 1 
    #define DTS_22_9_14_T_5_30_0             0,  30, 5, 5, 14, 9, 22//0,  30, 8, 5, 15, 9, 16
    //#define DTS_22_8_1_T_9_0_0              0,  0, 9, 5, 1, 8, 22//0,   0, 9, 5, 15, 9, 16
    

    mtr::PACK_GET_PWR_PRFL tb_in_ppr_bgn = {mtr::enTPPR_NoSkip_PPRv0, {DTS_22_9_14_T_5_30_0}, {DTS_22_9_14_T_5_30_0}};
    
    #define tb_rd_ppr_bgn  { scnrCheckArchiveExecCount/*scnrNormal_Check_ConstPQP*/, \
                    { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_bgn, sizeof(tb_in_ppr_bgn)}, END_PLOAD }, }, \                   
                    { mtr::GET_PWR_PROFILE, END_PLOAD } \
                    }

#endif
    
// -- Read PPR from the middle (*22-8-1T8:30:00*)
#if 1 
    #define DTS_22_8_1_T_8_30_0             0,  30, 8, 5, 1, 8, 22//0,  30, 8, 5, 15, 9, 16
    //#define DTS_22_8_1_T_9_0_0              0,  0, 9, 5, 1, 8, 22//0,   0, 9, 5, 15, 9, 16
    

    mtr::PACK_GET_PWR_PRFL tb_in_ppr_mid = {mtr::enTPPR_NoSkip_PPRv0, {DTS_22_8_1_T_8_30_0}, {DTS_22_8_1_T_8_30_0}};
    
    #define tb_rd_ppr_mid  { scnrCheckArchiveExecCount/*scnrNormal_Check_ConstPQP*/, \
                    { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_mid, sizeof(tb_in_ppr_mid)}, END_PLOAD }, }, \                   
                    { mtr::GET_PWR_PROFILE, END_PLOAD }}

#endif
    
// -- Read PPR from the end (*22-7-14T8:30:00*)
#if 1 
    #define DTS_22_7_14_T_5_30_0             0,  30, 5, 5, 13, 5, 22//0,  30, 8, 5, 15, 9, 16
    //#define DTS_22_8_1_T_9_0_0              0,  0, 9, 5, 1, 8, 22//0,   0, 9, 5, 15, 9, 16
    

    mtr::PACK_GET_PWR_PRFL tb_in_ppr_end = {mtr::enTPPR_NoSkip_PPRv0, {DTS_22_7_14_T_5_30_0}, {DTS_22_7_14_T_5_30_0}};
    
    #define tb_rd_ppr_end  { scnrCheckArchiveExecCount/*scnrNormal_Check_ConstPQP*/, \
                    { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_end, sizeof(tb_in_ppr_end)}, END_PLOAD }, }, \                   
                    { mtr::GET_PWR_PROFILE, END_PLOAD }}

#endif

//-----------------------------------------------------------------------------
// -- Defines for scenario with sparse data
//-----------------------------------------------------------------------------
#if 1 
    #define tmt_TimeWork_with_shift    6 * 365 * 24 * 60 * 60
    #define tb_act_work_with_shift  { \
                scnrWorkWithShift, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeWork_with_shift, 30 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45}, 25 } \
            }

#endif
// -- Read PPR from the sparse data (*35-11-17T8:30:00*)
#if 1 
    #define DTS_35_11_17_T_8_30_0             0,  30, 8, 5, 17, 11, 35//0,  30, 8, 5, 9, 11, 35(Found)//*/ //0, 30, 8, 5, 1, 6, 22//(Not Found)

    

    mtr::PACK_GET_PWR_PRFL tb_in_ppr_in_sparse = {mtr::enTPPR_NoSkip_PPRv0, {DTS_35_11_17_T_8_30_0}, {DTS_35_11_17_T_8_30_0}};
    
    #define tb_rd_ppr_in_sparse  { scnrCheckArchiveExecCount, \
                    { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_in_sparse, sizeof(tb_in_ppr_in_sparse)}, END_PLOAD }, }, \                   
                    { mtr::GET_PWR_PROFILE, END_PLOAD }}

#endif
// -- Read PPR from the sparse data from (*35-11-9T8:30:00*) to (*35-11-18T8:30:00*)                  
#if 1
    #define DTS_35_11_9_T_8_30_0             0,  30, 8, 6, 9, 11, 35
    #define DTS_35_11_18_T_8_30_0             0,  30, 8, 6, 18, 11, 35//0,  30, 8, 5, 9, 11, 35(Found)//*/ //0, 30, 8, 5, 1, 6, 22//(Not Found) 

    mtr::PACK_GET_PWR_PRFL tb_in_ppr_in_sparse_range = {mtr::enTPPR_NoSkip_PPRv0, {DTS_35_11_9_T_8_30_0}, {DTS_35_11_18_T_8_30_0}};
    
    #define tb_rd_ppr_in_sparse_range  { scnrCheckSparseData, \
                    { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_in_sparse_range, sizeof(tb_in_ppr_in_sparse_range)}, END_PLOAD }, }, \                   
                    { mtr::GET_PWR_PROFILE, END_PLOAD }}

#endif 
                    
                    
//-----------------------------------------------------------------------------
// -- Defines for scenario with data with beaks
//-----------------------------------------------------------------------------                    
#if 1 
    #define tmt_TimeWork_1_day    24 * 60 * 60
    #define tb_act_work_1_day  { \
                scnrWork, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeWork_1_day, 30 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45}, 25 } \
            }

#endif
                    
#if 1 
    #define tmt_TimeWork_8_hours    8 * 60 * 60
    #define tb_act_work_8_hours  { \
                scnrWork, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeWork_8_hours, 30 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45}, 25 } \
            }

#endif

#if 1 
    #define tb_act_make_break  { scnrMakeBreak, \
                 {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD} \
            }

#endif
                    
// -- Read PPR from the data with breakes from (*35-11-9T8:30:00*) to (*35-11-18T8:30:00*)                  
#if 1
    #define DTS_20_11_9_T_8_30_0             0,  30, 8, 6, 9, 11, 35
     

    mtr::PACK_GET_PWR_PRFL tb_in_ppr_in_breaks = {mtr::enTPPR_NoSkip_PPRv0, {DTS_20_11_9_T_8_30_0}, {DTS_20_11_9_T_8_30_0}};
    
    #define tb_rd_ppr_in_breaks_range  { scnrCheckDataWhithBreaks, \
                    { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_in_breaks_range, sizeof(tb_in_ppr_in_breaks_range)}, END_PLOAD }, },\                   
                    { mtr::GET_PWR_PROFILE, END_PLOAD }}

#endif 
                    
                    
//-----------------------------------------------------------------------------
// -- Create SCENARIO_ARCHIVE instance
//-----------------------------------------------------------------------------
    TB_SCENARIO_ARCHIVE scenario_checkArchiveExecCount_tb[] = {
        //// -- action work
        tb_act_work_6_years,
        //// -- READ arch/ppr
        tb_rd_ppr_bgn,
        tb_rd_ppr_mid,
        tb_rd_ppr_end,
        { scnrEND }
    };
    
    TB_SCENARIO_ARCHIVE scenario_checkArchiveExecCountSparseData_tb[] = {
        //// -- action work
        tb_act_work_with_shift,
        //// -- READ arch/ppr
        tb_rd_ppr_in_sparse,
        tb_rd_ppr_in_sparse_range,
        { scnrEND }
    };
    
    TB_SCENARIO_ARCHIVE scenario_checkArciveExecCountDataWithBreaks_tb[] = {
        //TODO: action to make data with breaks
//        tb_act_work_8_hours,
//        tb_act_make_break,
//        tb_act_work_1_day,
//        tb_act_make_break,
//        tb_act_work_1_day,
//        tb_act_make_break,
//        tb_act_work_1_day,
//        tb_act_make_break,
        
        { scnrEND }
    };

}