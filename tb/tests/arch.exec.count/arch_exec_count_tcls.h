/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   arch_exec_count_tcls.h
 * Author: User
 *
 * Created on 18.07.2017, 11:30:51
 */

#ifndef ARCH_EXEC_COUNT_TCLS_H
#define ARCH_EXEC_COUNT_TCLS_H

#include <cppunit/extensions/HelperMacros.h>
#include "../tb_utils/tb_utils.h"

class arch_exec_count_tcls : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(arch_exec_count_tcls);

    CPPUNIT_TEST(testArchExecCount);
    CPPUNIT_TEST(testArchExecCountSparseData);
//    CPPUNIT_TEST(testArchExecCountDataWithBreaks);

    CPPUNIT_TEST_SUITE_END();

private:
    tb_utils_cls tbu;
    
public:
    arch_exec_count_tcls();
    virtual ~arch_exec_count_tcls();
    void setUp();
    void tearDown();

private:
    void testArchExecCount();
    void testArchExecCountSparseData();
    void testArchExecCountDataWithBreaks();
};

#endif /* ARCH_EXEC_COUNT_TCLS_H */

