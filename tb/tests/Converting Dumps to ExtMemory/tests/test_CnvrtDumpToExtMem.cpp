/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   test_CnvrtDumpToExtMem.cpp
 * Author: User
 *
 * Created on 7 ���� 2017 �., 7:35
 */

#include <stdlib.h>
#include <iostream>

#include "../../tb_utils/Clock.h"
#include "../CnvrtDumpToExtMem.h"

/*
 * Simple C++ Test Suite
 */
mtr::EXT_MEMORY eeprom_test;
mtr::EXT_FLASH flash_test;

void test_Cnvrt_EEPROM(const Clock &clock) {
    std::cout << "test_Cnvrt_EEPROM test_CnvrtDumpToExtMem" << std::endl;
    
    CnvrtDumpToExtMem cnv("./../tests/Converting Dumps to ExtMemory/tests/dumps/17.08.17 МАЯК 302АРТД read_eeprom.bin", 
                          "./../tests/Converting Dumps to ExtMemory/tests/dumps/17.08.17 МАЯК 302АРТД read_flash_ALL.bin");
    cnv.Read(eeprom_test);
    
    NAME_EEPROM *pName = &eeprom_test.EEPROM.sect.name;
    
    if(!cnv.CheckCompatible_EE(VERSION_EEPROM)) goto __end;
    if(!cnv.CheckKnownVersionDumps()) goto __error;
    
    if(pName->_AddrMeter != 16005626) goto __error;
    if(pName->_enrg_save.EsumT_Rp != 1302754) goto __error;
    if(pName->_arch_head.head_mnth.Addr_rcrd != 0x910E) goto __error;
    if(pName->_pwr_prfl_head.Addr_rcrd != 0xBCB0) goto __error;
    if(pName->_save_Arch.save.tmt_Duration != 1800) goto __error;
    if(pName->_save_ArchMechVar.Mode != mtr::enIdleAM) goto __error;
    if(pName->_save_ArchMechVar.prfl_summ.v2.Umax[1] != 2300) goto __error;

__end:
    return;

__error:
    std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
            " testname=test_Cnvrt_EEPROM (test_CnvrtDumpToExtMem) message=eeprom_test - Not valid Read" << std::endl;
}
void test_Cnvrt_FLASH(const Clock &clock) {
    std::cout << "test_Cnvrt_FLASH test_CnvrtDumpToExtMem" << std::endl;
    
    CnvrtDumpToExtMem cnv("./../tests/Converting Dumps to ExtMemory/tests/dumps/17.08.17 МАЯК 302АРТД read_eeprom.bin", 
                          "./../tests/Converting Dumps to ExtMemory/tests/dumps/17.08.17 МАЯК 302АРТД read_flash_ALL.bin");
    cnv.Read(flash_test);
    
    NAME_FLASH *pName = &flash_test.FLASH.sect.name;

    if(!cnv.CheckCompatible_FLASH(VERSION_FLASH)) goto __end;    
    if(!cnv.CheckKnownVersionDumps()) goto __error;
    
    // -- ArchDay
    if(pName->ArchDay.sect[0].rcrds[3].arch.ds.date != 2) goto __error;
    if(pName->ArchDay.sect[0].rcrds[3].arch.enrg_trf.enrg[2].Ap != 21569) goto __error;
    // -- ArchMonth
    if(pName->ArchMonth.sect[0].rcrds[1].arch.ds.month != 12) goto __error;
    if(pName->ArchMonth.sect[0].rcrds[2].arch.enrg_trf.enrg[1].Rp != 623) goto __error;
    // -- Profile
    if(pName->PwrPrfl.sect[0].rcrds[57].prfl.tmt != 1483324200) goto __error;
    if(pName->PwrPrfl.sect[0].rcrds[57].prfl.dt.v0.enrg.Rp != 131882) goto __error;
    
__end:
    return;
    
__error:
    std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
            " testname=test_Cnvrt_FLASH (test_CnvrtDumpToExtMem) message=flash_test - Not valid Read" << std::endl;
}
void test_Cnvrt_FLASH_21_08_17(const Clock &clock) {
    std::cout << "test_Cnvrt_FLASH_21_08_17 test_CnvrtDumpToExtMem" << std::endl;
    
    CnvrtDumpToExtMem cnv("./../tests/Converting Dumps to ExtMemory/tests/dumps/17.08.17 МАЯК 302АРТД read_eeprom.bin", 
                          "./../tests/Converting Dumps to ExtMemory/tests/dumps/21.08.17 МАЯК 302АРТД read_flash_ALL.bin");
    cnv.Read(flash_test);
    
    NAME_FLASH *pName = &flash_test.FLASH.sect.name;
    
    if(!cnv.CheckCompatible_FLASH(VERSION_FLASH)) goto __end;
    if(!cnv.CheckKnownVersionDumps()) goto __error;
    
    // -- ArchDay
    if(pName->ArchDay.sect[4].rcrds[7].arch.ds.date != 6) goto __error;
    if(pName->ArchDay.sect[4].rcrds[7].arch.enrg_trf.enrg[2].Rp != 9446294) goto __error;
    // -- ArchMonth
    if(pName->ArchMonth.sect[0].rcrds[9].arch.ds.month != 5) goto __error;
    if(pName->ArchMonth.sect[0].rcrds[9].arch.enrg_trf.enrg[2].Rp != 2215834) goto __error;
    // -- Profile
    if(pName->PwrPrfl.sect[82].rcrds[29].prfl.tmt != 1494050400) goto __error;
    if(pName->PwrPrfl.sect[82].rcrds[29].prfl.dt.vs4.Uph_avrg[1] != 1109) goto __error;
    
__end:
    return;
    
__error:
    std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
            " testname=test_Cnvrt_FLASH_21_08_17 (test_CnvrtDumpToExtMem) message=flash_test - Not valid Read" << std::endl;
}
//-----------------------------------------------------------------------------
// -- VER dumps 00.00.01
//-----------------------------------------------------------------------------
void test_Cnvrt_EEPROM_v0_0_1_05_09_17(const Clock &clock) {
    std::cout << "test_Cnvrt_EEPROM_v0_0_1_05_09_17 test_CnvrtDumpToExtMem" << std::endl;
    
    CnvrtDumpToExtMem cnv("./../tests/Converting Dumps to ExtMemory/tests/dumps/05.09.17 METER 300ART read_eeprom.bin", 
                          "./../tests/Converting Dumps to ExtMemory/tests/dumps/05.09.17 METER 300ART read_flash_ALL.bin");
    cnv.Read(eeprom_test);
    
    NAME_EEPROM *pName = &eeprom_test.EEPROM.sect.name;
    
    if(!cnv.CheckCompatible_EE(VERSION_EEPROM)) goto __end;
    if(!cnv.CheckKnownVersionDumps()) goto __error;
    
    if(pName->_AddrMeter != 17000104) goto __error;
    if(pName->_enrg_save.EsumT_Rp != 4638) goto __error;
    if(pName->_arch_head.head_mnth.Addr_rcrd != 0xB77E) goto __error;
    if(pName->_pwr_prfl_head.Addr_rcrd != 0x61B54) goto __error;
    if(pName->_save_Arch.save.tmt_Duration != 1800) goto __error;
    if(pName->_save_ArchMechVar.Mode != mtr::enIdleAM) goto __error;
    if(pName->_save_ArchMechVar.prfl_summ.v2.Umax[1] != -1) goto __error;

__end:
    return;

__error:
    std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
            " testname=test_Cnvrt_EEPROM_v0_0_1_05_09_17 (test_CnvrtDumpToExtMem) message=eeprom_test - Not valid Read" << std::endl;
}
void test_Cnvrt_FLASH_v0_0_1_05_09_17(const Clock &clock) {
    std::cout << "test_Cnvrt_FLASH_v0_0_1_05_09_17 test_CnvrtDumpToExtMem" << std::endl;
    
    CnvrtDumpToExtMem cnv("./../tests/Converting Dumps to ExtMemory/tests/dumps/05.09.17 METER 300ART read_flash_ALL.bin", 
                          "./../tests/Converting Dumps to ExtMemory/tests/dumps/05.09.17 METER 300ART read_flash_ALL.bin");
    cnv.Read(flash_test);
    
    NAME_FLASH *pName = &flash_test.FLASH.sect.name;
    
    if(!cnv.CheckCompatible_FLASH(VERSION_FLASH)) goto __end;    
    if(!cnv.CheckKnownVersionDumps()) goto __error;
    
    // -- ArchDay
    if(pName->ArchDay.sect[4].rcrds[7].arch.ds.date != 29) goto __error;
    if(pName->ArchDay.sect[4].rcrds[7].arch.enrg_trf.enrg[2].Rp != 0) goto __error;
    // -- ArchMonth
    if(pName->ArchMonth.sect[0].rcrds[9].arch.ds.month != 5) goto __error;
    if(pName->ArchMonth.sect[0].rcrds[9].arch.enrg_trf.enrg[2].Rp != 0) goto __error;
    // -- Profile
    if(pName->PwrPrfl.sect[82].rcrds[29].prfl.tmt != 0x59080440) goto __error;
    if(pName->PwrPrfl.sect[82].rcrds[29].prfl.dt.vs4.Uph_avrg[1] != 2300) goto __error;
    //-------------------------------------------------------------------------
    // -- TEST raw
    //-------------------------------------------------------------------------
    {
        uint32_t addr_arch = 0x7780;
        uint32_t tmt = ((uint32_t *)&flash_test.FLASH.sect.raw[addr_arch])[0];
        if(tmt != 0x59067A80) goto __error;
    }
    
__end:
    return;
    
__error:
    std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
            " testname=test_Cnvrt_FLASH_v0_0_1_05_09_17 (test_CnvrtDumpToExtMem) message=flash_test - Not valid Read" << std::endl;
}

int main(int argc, char** argv) {
    Clock time_execute, time_test;
    
    std::cout << "%SUITE_STARTING% test_CnvrtDumpToExtMem" << std::endl;
    std::cout << "%SUITE_STARTED%" << std::endl;

    time_test.start();
    std::cout << "%TEST_STARTED% test_Cnvrt_EEPROM (test_CnvrtDumpToExtMem)" << std::endl;
    test_Cnvrt_EEPROM(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_Cnvrt_EEPROM (test_CnvrtDumpToExtMem)" << std::endl;
    
    time_test.start();
    std::cout << "%TEST_STARTED% test_Cnvrt_FLASH (test_CnvrtDumpToExtMem)" << std::endl;
    test_Cnvrt_FLASH(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_Cnvrt_FLASH (test_CnvrtDumpToExtMem)" << std::endl;
    
    time_test.start();
    std::cout << "%TEST_STARTED% test_Cnvrt_FLASH (test_CnvrtDumpToExtMem)" << std::endl;
    test_Cnvrt_FLASH_21_08_17(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_Cnvrt_FLASH_21_08_17 (test_CnvrtDumpToExtMem)" << std::endl;
    
    time_test.start();
    std::cout << "%TEST_STARTED% test_Cnvrt_EEPROM_v0_0_1_05_09_17 (test_CnvrtDumpToExtMem)" << std::endl;
    test_Cnvrt_EEPROM_v0_0_1_05_09_17(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_Cnvrt_EEPROM_v0_0_1_05_09_17 (test_CnvrtDumpToExtMem)" << std::endl;
    
    time_test.start();
    std::cout << "%TEST_STARTED% test_Cnvrt_FLASH_v0_0_1_05_09_17 (test_CnvrtDumpToExtMem)" << std::endl;
    test_Cnvrt_FLASH_v0_0_1_05_09_17(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_Cnvrt_FLASH_v0_0_1_05_09_17 (test_CnvrtDumpToExtMem)" << std::endl;

    std::cout << "%SUITE_FINISHED% time=0" << std::endl;

    return (EXIT_SUCCESS);
}

