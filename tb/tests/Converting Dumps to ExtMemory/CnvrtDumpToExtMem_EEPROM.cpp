/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "CnvrtDumpToExtMem.h"

uint32_t CnvrtDumpToExtMem::get(uint32_t addr, mtr::ENERGY_DATA& val){ 
    uint32_t addr_shft = addr;
    
    ct_assert(sizeof(val.EsumT_Ap) == 4);
    
    addr_shft += get(addr_shft, val.ETarif);
    addr_shft += get(addr_shft, val.EpnTarif);
    addr_shft += get(addr_shft, val.EsumT_Ap);
    addr_shft += get(addr_shft, val.EsumT_An);
    addr_shft += get(addr_shft, val.EsumT_Apn);
    addr_shft += get(addr_shft, val.EsumT_Rp);
    addr_shft += get(addr_shft, val.EsumT_Rn);
    addr_shft += get(addr_shft, val.EsumT_Rpn);
    
    return (addr_shft - addr);
};
uint32_t CnvrtDumpToExtMem::get(uint32_t addr, mtr::ENERGYS_MTR_TRFS& val){
    uint32_t addr_shft = addr;
    
    for(uint8_t i = 0; i < get_size(val.enrg); i++){
        addr_shft += get(addr_shft, val.enrg[i]);
    }
    
    return (addr_shft - addr);
}
uint32_t CnvrtDumpToExtMem::get(uint32_t addr, mtr::ENERGY& val){
    uint32_t addr_shft = addr;

    ct_assert(sizeof(val.Ap) == 4);
    
    addr_shft += get(addr_shft, val.Ap);
    addr_shft += get(addr_shft, val.An);
    addr_shft += get(addr_shft, val.Rp);
    addr_shft += get(addr_shft, val.Rn);
    
    return (addr_shft - addr);
}
/*
uint32_t CnvrtDumpToExtMem::get(uint32_t addr, mtr::POWER& val){
    uint32_t addr_shft = addr;

    ct_assert(sizeof(val.Ap) == 4);
    
    addr_shft += get(addr_shft, val.Ap);
    addr_shft += get(addr_shft, val.An);
    addr_shft += get(addr_shft, val.Rp);
    addr_shft += get(addr_shft, val.Rn);
    
    return (addr_shft - addr);
}
 */
uint32_t CnvrtDumpToExtMem::get(uint32_t addr, mtr::ARCHIV_HEAD& val){
    uint32_t addr_shft = addr;
    
    addr_shft += get(addr_shft, val.head_day);
    addr_shft += get(addr_shft, val.head_mnth);
    
    return (addr_shft - addr);
}
uint32_t CnvrtDumpToExtMem::get(uint32_t addr, mtr::HEAD_LIST& val){
    uint32_t addr_shft = addr;
    
    ct_assert(sizeof(val.nmb_rcrds) == 2);
    ct_assert(sizeof(val.indx_rcrd) == 2);
    ct_assert(sizeof(val.Addr_rcrd) == 4);
    
    addr_shft += get(addr_shft, val.nmb_rcrds);
    addr_shft += get(addr_shft, val.indx_rcrd);
    addr_shft += get(addr_shft, val.Addr_rcrd);
    
    return (addr_shft - addr);
}
uint32_t CnvrtDumpToExtMem::get(uint32_t addr, mtr::EE_ARCH_SAVE& val){
    uint32_t addr_shft = addr;
    
    addr_shft += get(addr_shft, val.save);
    addr_shft += get(addr_shft, val.crc);
    addr_shft += get(addr_shft, val.reserv);
    
    return (addr_shft - addr);
}
uint32_t CnvrtDumpToExtMem::get(uint32_t addr, mtr::ARCH_SAVE& val){
    uint32_t addr_shft = addr;
    
    ct_assert(sizeof(val.tmt_Duration) == 2);
    
    addr_shft += get(addr_shft, val.tmt_Duration);
    
    return (addr_shft - addr);
}
uint32_t CnvrtDumpToExtMem::get(uint32_t addr, mtr::ARCH_MECH_VAR& val){
    uint32_t addr_shft = addr;
    
    addr_shft += get(addr_shft, val.Mode);
    addr_shft += get(addr_shft, val.fNeedUpdatePPR);
    addr_shft += get(addr_shft, val.tmt_crnt_raw);
    addr_shft += get(addr_shft, val.tmt_new_raw);
    addr_shft += get(addr_shft, val.tmt_crnt);
    addr_shft += get(addr_shft, val.tmt_new);
    addr_shft += get(addr_shft, val.tmt);
    addr_shft += get(addr_shft, val.TypeArch);
    addr_shft += get(addr_shft, val.head);
    addr_shft += get(addr_shft, val.plist);
    addr_shft += get(addr_shft, val.cnt_rcrd);
    addr_shft += get(addr_shft, val.indx_rcrd);
    addr_shft += get(addr_shft, val.prfl_summ);
    addr_shft += get(addr_shft, val.crc);
    addr_shft += get(addr_shft, val.reserv);

    ///< Reset Mode to enIdleAM because val.plist not initialized
    val.Mode = mtr::enIdleAM;
    
    return (addr_shft - addr);
}
uint32_t CnvrtDumpToExtMem::get(uint32_t addr, mtr::enArchMechMode& val){
    uint8_t u8_val;
    
    Little_to_Little_Endian(addr, &u8_val, sizeof(u8_val));
    val = (mtr::enArchMechMode)u8_val;
    
    return sizeof(u8_val);
}
uint32_t CnvrtDumpToExtMem::get(uint32_t addr, mtr::enTypeArch& val){
    uint8_t u8_val;
    
    Little_to_Little_Endian(addr, &u8_val, sizeof(u8_val));
    val = (mtr::enTypeArch)u8_val;
    
    return sizeof(u8_val);
}
uint32_t CnvrtDumpToExtMem::get(uint32_t addr, mtr::LPCLIST_ARCHIV_FLASH_MEM& plist){
    
    /// @todo Необходимо вероятно указать на реальный объект в памяти модели.
    plist = 0;
    
    return 3; ///< for 8051 core sizeof(pointer ) == 3 byres
}
uint32_t CnvrtDumpToExtMem::get(uint32_t addr, mtr::PWR_PRFL_DATA& val){
    uint32_t addr_shft = addr;
    
    addr_shft += get(addr_shft, val.v0);
    addr_shft += get(addr_shft, val.v2);
    addr_shft += get(addr_shft, val.vs4);
    
    return (addr_shft - addr);
}
uint32_t CnvrtDumpToExtMem::get(uint32_t addr, mtr::PWR_PRFL_DATA_v0& val){
    uint32_t addr_shft = addr;
    
    addr_shft += get(addr_shft, val.enrg);
    addr_shft += get(addr_shft, val.pwr_peak);
    
    return (addr_shft - addr);
}
uint32_t CnvrtDumpToExtMem::get(uint32_t addr, mtr::PWR_PRFL_DATA_v2& val){
    uint32_t addr_shft = addr;
    
    for(uint8_t i = 0; i < get_size(val.Umax); i++){
        addr_shft += get(addr_shft, val.Umax[i]);
    }
    for(uint8_t i = 0; i < get_size(val.Umin); i++){
        addr_shft += get(addr_shft, val.Umin[i]);
    }
    
    return (addr_shft - addr);
}
uint32_t CnvrtDumpToExtMem::get(uint32_t addr, mtr::PWR_PRFL_DATA_vs4& val){
    uint32_t addr_shft = addr;
    
    for(uint8_t i = 0; i < get_size(val.Uph_avrg); i++){
        addr_shft += get(addr_shft, val.Uph_avrg[i]);
    }
    addr_shft += get(addr_shft, val.Temp);
    
    return (addr_shft - addr);
}