/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CnvtyDumpToExtMem.h
 * Author: User
 *
 * Created on 17 августа 2017 г., 13:23
 */

#ifndef CNVTYDUMPTOEXTMEM_H
#define CNVTYDUMPTOEXTMEM_H

#include <stdint.h>
#include <string.h>
#include <string>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <ios>
#include <fstream>
#include <unistd.h>
#include <map>
#include <vector>

#include "define.h"
#include "defExtMem_common.h"

typedef mtr::_EXT_MEMORY::_EEPROM::_SECT::_NAME NAME_EEPROM;
// -- FLASH
typedef mtr::_EXT_FLASH::_FLASH::_SECT_FLASH::_NAME_FLASH               NAME_FLASH;
typedef mtr::_EXT_FLASH::_FLASH::_SECT_FLASH::_NAME_FLASH::_DAY_ARCH    DAY_ARCH;
typedef mtr::_EXT_FLASH::_FLASH::_SECT_FLASH::_NAME_FLASH::_MONTH_ARCH  MONTH_ARCH;
typedef mtr::_EXT_FLASH::_FLASH::_SECT_FLASH::_NAME_FLASH::_PWR_PRFL    PWR_PRFL;

class CnvrtDumpToExtMem {
private:
    
    std::string strFileEEPROM, strFileFLASH;
    std::fstream fsFile;
    
    MAP_EEPROM mapEEPROM;
    MAP_FLASH mapFLASH;
    
    uint32_t verEE, verFLASH;
    
public:
    CnvrtDumpToExtMem(std::string sFileEEPROM, std::string sFileFLASH);
    ~CnvrtDumpToExtMem(){ CloseFile(); }
    void Read(mtr::EXT_MEMORY& ext_mem);
    void Read(mtr::EXT_FLASH& ext_flash);
    void Read(mtr::EXT_MEMORY& ext_mem, mtr::EXT_FLASH& ext_flash);

    bool CheckKnownVersionDumps();
    bool CheckCompatible_EE(uint32_t ver_crnt_EE);
    bool CheckCompatible_FLASH(uint32_t ver_crnt_FLASH);
    uint32_t getVerEE(){ return verEE; }
    uint32_t getVerFLASH(){ return verFLASH; }
    
private:
    void OpenFile(std::string& NameFile);
    void CloseFile();
    
    uint32_t Big_to_Little_Endian(uint32_t addr, void *dst_lttl, uint32_t size);
    uint32_t Little_to_Little_Endian(uint32_t addr, void *dst_lttl, uint32_t size);
    template<typename T, std::size_t N> uint32_t get(uint32_t addr, T (&dst)[N]){ return Little_to_Little_Endian(addr, dst, N * sizeof(T)); };
    template<typename T> uint32_t get(uint32_t addr, T& val){ return Big_to_Little_Endian(addr, &val, sizeof(val)); };
    //template<typename T> uint32_t get(uint32_t addr, T *dst){ return Little_to_Little_Endian(addr, dst, sizeof(T)); };
   // -- EEPROM
    uint32_t get(uint32_t addr, mtr::ENERGY_DATA& val);
    uint32_t get(uint32_t addr, mtr::ENERGYS_MTR_TRFS& val);
    uint32_t get(uint32_t addr, mtr::ENERGY& val);
    uint32_t get(uint32_t addr, mtr::ARCHIV_HEAD& val);
    uint32_t get(uint32_t addr, mtr::HEAD_LIST& val);
    uint32_t get(uint32_t addr, mtr::EE_ARCH_SAVE& val);
    uint32_t get(uint32_t addr, mtr::ARCH_SAVE& val);
    uint32_t get(uint32_t addr, mtr::ARCH_MECH_VAR& val);
    uint32_t get(uint32_t addr, mtr::enArchMechMode& val);
    uint32_t get(uint32_t addr, mtr::enTypeArch& val);
    uint32_t get(uint32_t addr, mtr::LPCLIST_ARCHIV_FLASH_MEM& plist);
    uint32_t get(uint32_t addr, mtr::PWR_PRFL_DATA& val);
    uint32_t get(uint32_t addr, mtr::PWR_PRFL_DATA_v0& val);
    uint32_t get(uint32_t addr, mtr::PWR_PRFL_DATA_v2& val);
    uint32_t get(uint32_t addr, mtr::PWR_PRFL_DATA_vs4& val);
    // -- FLASH
    uint32_t get(uint32_t addr, DAY_ARCH& val){ return get_xARCH(addr, val); }
    uint32_t get(uint32_t addr, MONTH_ARCH& val){ return get_xARCH(addr, val); };
    uint32_t get(uint32_t addr, PWR_PRFL& val){ return get_xARCH(addr, val); };
    uint32_t get(uint32_t addr, mtr::RCRD_ARCH& val);
    uint32_t get(uint32_t addr, mtr::RCRD_ARCH_WITH_SHIFT& val);
    uint32_t get(uint32_t addr, mtr::DATE_t& val);
    uint32_t get(uint32_t addr, mtr::RCRD_PWR_PRFL& val);
    uint32_t get(uint32_t addr, mtr::RCRD_PWR_PRFL_WITH_SHIFT& val);
    uint32_t get(uint32_t addr, mtr::PWR_PRFL_STS& val);
       
    template<class T> uint32_t get_xARCH(uint32_t addr, T& val){
        uint32_t addr_shft = addr, i, j;
        uint32_t Nmb_for_i = get_size(val.sect);
        uint32_t Nmb_for_j = get_size(val.sect[0].rcrds);
        
        for(i = 0; i < get_size(val.sect); i++){
            for(j = 0; j < get_size(val.sect[0].rcrds); j++){
                addr_shft += get(addr_shft, val.sect[i].rcrds[j]);
            }
            addr_shft += get(addr_shft, val.sect[i].rsrv);
        }
        
        return (addr_shft - addr);
    }
};

#endif /* CNVTYDUMPTOEXTMEM_H */

