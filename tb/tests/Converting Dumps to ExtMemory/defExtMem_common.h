/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   defExtMem_common.h
 * Author: User
 *
 * Created on 17 августа 2017 г., 14:05
 */

#ifndef DEFEXTMEM_COMMON_H
#define DEFEXTMEM_COMMON_H

struct _EEPROM_ADDR{
    uint32_t ADDR_Password;
    uint32_t ADDR_name_manufacture;
    uint32_t ADDR_AddrMeter;
    uint32_t ADDR_system_mtr;
    uint32_t ADDR_enrg_save;
    uint32_t ADDR_tarif_tbl;
    uint32_t ADDR_arch_head;
    uint32_t ADDR_pwr_prfl_head;
    uint32_t ADDR_save_Arch;
    uint32_t ADDR_save_ArchMechVar;
};
typedef struct _EEPROM_ADDR EEPROM_ADDR, *LPEEPROM_ADDR;
struct _FLASH_ADDR{
    uint32_t ADDR_sect_arch_bgn_day;
    uint32_t ADDR_sect_arch_bgn_mnth;
    uint32_t ADDR_sect_pwr_prfl;
};
typedef struct _FLASH_ADDR FLASH_ADDR, *LPFLASH_ADDR;

typedef std::map<uint32_t, EEPROM_ADDR> MAP_EEPROM;
typedef std::pair<uint32_t, EEPROM_ADDR> PAIR_EEPROM;

typedef std::map<uint32_t, FLASH_ADDR> MAP_FLASH;
typedef std::pair<uint32_t, FLASH_ADDR> PAIR_FLASH;

#define ADDR_Version            0x00000000
//-----------------------------------------------------------------------------
// -- EEPROM
//-----------------------------------------------------------------------------
#define NMB_EEPROM_VER          2
#define EEPROM_0x76C02177 {                     \
    0x0004,     /* ADDR_Password */             \
    0x0016,     /* ADDR_name_manufacture */     \
    0x0056,     /* ADDR_AddrMeter */            \
    0x005A,     /* ADDR_system_mtr */           \
    0x0069,     /* ADDR_enrg_save */            \
    0x0181,     /* ADDR_tarif_tbl */            \
    0x23D0,     /* ADDR_arch_head */            \
    0x23E0,     /* ADDR_pwr_prfl_head */        \
    0x3538,     /* ADDR_save_Arch */            \
    0x357B,     /* ADDR_save_ArchMechVar */     \
}
#define EEPROM_0x01000000 {                     \
    0x0018,     /* ADDR_Password */             \
    0x0042,     /* ADDR_name_manufacture */     \
    0x0092,     /* ADDR_AddrMeter */            \
    0x009E,     /* ADDR_system_mtr */           \
    0x00BD,     /* ADDR_enrg_save */            \
    0x01F5,     /* ADDR_tarif_tbl */            \
    0x2464,     /* ADDR_arch_head */            \
    0x2494,     /* ADDR_pwr_prfl_head */        \
    0x37A0,     /* ADDR_save_Arch */            \
    0x37F3,     /* ADDR_save_ArchMechVar */     \
}
//-----------------------------------------------------------------------------
// -- FLASH
//-----------------------------------------------------------------------------
#define NMB_FLASH_VER          2
#define FLASH_0xFFFFFFFF {                      \
    0x00000,    /* ADDR_sect_arch_bgn_day */    \
    0x05000,    /* ADDR_sect_arch_bgn_mnth */   \
    0x07000     /* ADDR_sect_pwr_prfl */        \
}
#define FLASH_0x01000000 {                      \
    0x01000,    /* ADDR_sect_arch_bgn_day */    \
    0x07000,    /* ADDR_sect_arch_bgn_mnth */   \
    0x0A000,    /* ADDR_sect_pwr_prfl */        \
}

#endif /* DEFEXTMEM_COMMON_H */

