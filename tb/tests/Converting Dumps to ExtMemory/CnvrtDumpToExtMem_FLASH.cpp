/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "CnvrtDumpToExtMem.h"

uint32_t CnvrtDumpToExtMem::get(uint32_t addr, mtr::RCRD_ARCH& val){
    uint32_t addr_shft = addr;
    
    ct_assert(sizeof(val.tmt) == 4);
    
    addr_shft += get(addr_shft, val.tmt);
    addr_shft += get(addr_shft, val.ds);
    addr_shft += get(addr_shft, val.enrg_trf);
    
    return (addr_shft - addr);
}
uint32_t CnvrtDumpToExtMem::get(uint32_t addr, mtr::RCRD_ARCH_WITH_SHIFT& val){
    uint32_t addr_shft = addr;
    
    ct_assert(sizeof(val.shift) == 2);
    
    addr_shft += get(addr_shft, val.shift);
    addr_shft += get(addr_shft, val.arch);
    
    return (addr_shft - addr);
}
uint32_t CnvrtDumpToExtMem::get(uint32_t addr, mtr::DATE_t& val){
    uint32_t addr_shft = addr;
    
    ct_assert(sizeof(val.date) == 1);
    
    addr_shft += get(addr_shft, val.date);
    addr_shft += get(addr_shft, val.month);
    addr_shft += get(addr_shft, val.year);
    
    return (addr_shft - addr);
}
uint32_t CnvrtDumpToExtMem::get(uint32_t addr, mtr::RCRD_PWR_PRFL& val){
    uint32_t addr_shft = addr;
    
    ct_assert(sizeof(val.tmt) == 4);
    
    addr_shft += get(addr_shft, val.tmt);
    addr_shft += get(addr_shft, val.dt);
    addr_shft += get(addr_shft, val.fValid);
    
    return (addr_shft - addr);
}
uint32_t CnvrtDumpToExtMem::get(uint32_t addr, mtr::RCRD_PWR_PRFL_WITH_SHIFT& val){
    uint32_t addr_shft = addr;
    
    ct_assert(sizeof(val.shift) == 2);
    
    addr_shft += get(addr_shft, val.shift);
    addr_shft += get(addr_shft, val.prfl);
    
    return (addr_shft - addr);
}
uint32_t CnvrtDumpToExtMem::get(uint32_t addr, mtr::PWR_PRFL_STS& val){
    ct_assert(sizeof(val.raw) == 1);
    
    get(addr, val.raw);
    
    return sizeof(val.raw);
}