/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CnvtyDumpToExtMem.cpp
 * Author: User
 * 
 * Created on 17 августа 2017 г., 13:23
 */

#include "CnvrtDumpToExtMem.h"

CnvrtDumpToExtMem::CnvrtDumpToExtMem(std::string sFileEEPROM, std::string sFileFLASH){
    strFileEEPROM = sFileEEPROM;
    strFileFLASH = sFileFLASH;
    
    // -- EEPROM
    OpenFile(strFileEEPROM);
    get(ADDR_Version, verEE);
    CloseFile();
    // -- FLASH
    OpenFile(sFileFLASH);
    get(ADDR_Version, verFLASH);
    CloseFile();
    //-------------------------------------------------------------------------
    // -- Setting ADDR map
    //-------------------------------------------------------------------------
    // -- EEPROM
    const int NMB_EEPROM_VER_ALL = NMB_EEPROM_VER + 2;
    EEPROM_ADDR AddrEE[NMB_EEPROM_VER_ALL] = {
        EEPROM_0x76C02177, EEPROM_0x01000000, 
        EEPROM_0x01000000,  EEPROM_0x01000000,
    };
    uint32_t verEE[NMB_EEPROM_VER_ALL] = {
        0x76C02177, 0x01000000, 
        VERSION(0, 0, 1), VERSION(0, 1, 1)
    };
    for(int i = 0; i < NMB_EEPROM_VER_ALL; i++){
        PAIR_EEPROM pairEE(verEE[i], AddrEE[i]);
        mapEEPROM.insert(pairEE);
    }
    // -- FLASH
    const int NMB_FLASH_VER_ALL = NMB_FLASH_VER + 1;
    FLASH_ADDR AddrFLASH[NMB_FLASH_VER_ALL] = {
        FLASH_0xFFFFFFFF, FLASH_0x01000000, FLASH_0x01000000
    };
    uint32_t verFLASH[NMB_FLASH_VER_ALL] = {
        0x76C02177, 0x01000000, VERSION(0, 0, 1)
    };
    for(int i = 0; i < NMB_FLASH_VER_ALL; i++){
        PAIR_FLASH pairFLASH(verFLASH[i], AddrFLASH[i]);
        mapFLASH.insert(pairFLASH);
    }
}
bool CnvrtDumpToExtMem::CheckKnownVersionDumps(){
    return ( (mapEEPROM.find(verEE) != mapEEPROM.end()) &&
             (mapFLASH.find(verEE) != mapFLASH.end()) );
}
bool CnvrtDumpToExtMem::CheckCompatible_EE(uint32_t ver_crnt_EE){
    if( (ver_crnt_EE & ~VERSION(0xFF, 0, 0)) == 
        (verEE & ~VERSION(0xFF, 0, 0)) ) return true;
    // -- Exception
    if( (ver_crnt_EE == VERSION(0, 0, 1)) && (verEE == 0x76C02177) ) return true;
    if( (ver_crnt_EE == VERSION(0, 0, 1)) && (verEE == 0x01000000) ) return true;

    // --
    return false;
}
bool CnvrtDumpToExtMem::CheckCompatible_FLASH(uint32_t ver_crnt_FLASH){
    if(ver_crnt_FLASH == verFLASH) return true;
    // -- Exception
    if( (ver_crnt_FLASH == VERSION(0, 0, 1)) && (verFLASH == 0x01000000) ) return true;
    
    // --
    return false;
}

void CnvrtDumpToExtMem::Read(mtr::EXT_MEMORY& ext_mem){
   OpenFile(strFileEEPROM);
   
   NAME_EEPROM *pName = &ext_mem.EEPROM.sect.name;

   get(mapEEPROM[verEE].ADDR_AddrMeter,      pName->_AddrMeter);
   get(mapEEPROM[verEE].ADDR_Password,       pName->_Password);
   get(mapEEPROM[verEE].ADDR_enrg_save,      pName->_enrg_save);
   get(mapEEPROM[verEE].ADDR_arch_head,      pName->_arch_head);
   get(mapEEPROM[verEE].ADDR_pwr_prfl_head,  pName->_pwr_prfl_head);
   get(mapEEPROM[verEE].ADDR_save_Arch,      pName->_save_Arch);
   get(mapEEPROM[verEE].ADDR_save_ArchMechVar, pName->_save_ArchMechVar);

   CloseFile();
}
void CnvrtDumpToExtMem::Read(mtr::EXT_FLASH& ext_flash){
    OpenFile(strFileFLASH);
    
    NAME_FLASH *pName = &ext_flash.FLASH.sect.name;

    get(mapFLASH[verFLASH].ADDR_sect_arch_bgn_day,     pName->ArchDay);
    get(mapFLASH[verFLASH].ADDR_sect_arch_bgn_mnth,    pName->ArchMonth);
    get(mapFLASH[verFLASH].ADDR_sect_pwr_prfl,         pName->PwrPrfl);

    CloseFile();
}
void CnvrtDumpToExtMem::Read(mtr::EXT_MEMORY& ext_mem, mtr::EXT_FLASH& ext_flash){
    Read(ext_mem);
    Read(ext_flash);
}
void CnvrtDumpToExtMem::OpenFile(std::string& NameFile){
    fsFile.open(NameFile.c_str(), std::ios::binary | std::ios::in);
    if(!fsFile.is_open()) throw std::string("Error! CnvrtDumpToExtMem:: fsFile is not opened");
}
void CnvrtDumpToExtMem::CloseFile(){
    if(fsFile.is_open()) fsFile.close();
}
uint32_t CnvrtDumpToExtMem::Big_to_Little_Endian(uint32_t addr, void *dst_lttl, uint32_t size){
    if(!fsFile.is_open()) throw;
    
    fsFile.seekg(addr, fsFile.beg);
    
    for(uint32_t i = 1; i <= size; i++){
        fsFile.read(&((char *)dst_lttl)[size - i], 1);
        fsFile.seekg(addr + i);
    }
    
    return size;
}
uint32_t CnvrtDumpToExtMem::Little_to_Little_Endian(uint32_t addr, void *dst_lttl, uint32_t size){
    if(!fsFile.is_open()) throw;
    
    fsFile.seekg(addr, fsFile.beg);
    
    for(uint32_t i = 0; i < size; i++){
        fsFile.read(&((char *)dst_lttl)[i], 1);
        fsFile.seekg(addr + i);
    }
    
    return size;
}

        