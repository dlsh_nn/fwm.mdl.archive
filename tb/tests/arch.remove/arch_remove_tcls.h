/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   arch_delete_tcls.h
 * Author: User
 *
 * Created on 02.08.2017, 13:42:00
 */

#ifndef ARCH_DELETE_TCLS_H
#define ARCH_DELETE_TCLS_H

#include <cppunit/extensions/HelperMacros.h>
#include "../tb_utils/tb_utils.h"

class arch_delete_tcls : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(arch_delete_tcls);

    CPPUNIT_TEST(testRemoveFromStart);
    CPPUNIT_TEST(testRemoveFromMiddle);
    CPPUNIT_TEST(testRemoveCaseOne);
    CPPUNIT_TEST(testRemoveCaseTwo);

    CPPUNIT_TEST_SUITE_END();

private:
    tb_utils_cls tbu;
    
public:
    arch_delete_tcls();
    virtual ~arch_delete_tcls();
    void setUp();
    void tearDown();

private:
    void testRemoveFromStart();
    void testRemoveFromMiddle();
    void testRemoveCaseOne();
    void testRemoveCaseTwo();

};

#endif /* ARCH_DELETE_TCLS_H */

