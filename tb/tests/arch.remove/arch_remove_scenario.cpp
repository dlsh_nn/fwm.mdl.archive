#include "../tb_utils/tb_utils.h"

namespace nsAR{

//=============================================================================
// -- scenario archive remove
//=============================================================================
    //// -- Action work 230V, 1A, 45deg 250 day with step_time = 20 min tmt_end = 1663145442
#if 1 
    #define tmt_TimeWork_6_year    6 * 365 * 24 * 60 * 60
    #define tb_act_work_6_year  { \
                scnrWorkDiviation, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeWork_6_year, 30 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45}, 25 } \
            }
    #define tb_CheckArchive_Convergence { scnrCheckArchive_Convergence, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD} }
#endif

#if 1 
    #define tmt_TimeWork_1_day    24 * 60 * 60
    #define tb_act_work_1_day  { \
                scnrWorkDiviation, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeWork_1_day, 30 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45}, 25 } \
            }
#endif

#if 1 
    #define tmt_TimeWork_10_days   10 * 24 * 60 * 60
    #define tb_act_work_10_days  { \
                scnrWorkDiviation, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeWork_10_days, 30 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45}, 25 } \
            }
#endif

#if 1 
    #define tmt_TimeWork_4_hours   4 * 60 * 60
    #define tb_act_work_4_hours  { \
                scnrWorkDiviation, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeWork_4_hours, 30 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45}, 25 } \
            }
#endif
    
//// -- Command to remove some archives
#if 1

    #define tmt_TimeToRemove_1_hour            60 * 60
    #define tmt_TimeToRemove_5_hours           60 * 60
    #define tmt_TimeToRemove_8_hours       8 * 60 * 60
    #define tmt_TimeToRemove_1_day        24 * 60 * 60
    #define tmt_TimeToRemove_2_days   2 * 24 * 60 * 60

    #define tb_act_remove_1_hour { scnrMakeRemove, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeToRemove_1_hour, 30 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45}, 25 } \
             }
    #define tb_act_remove_5_hour { scnrMakeRemove, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeToRemove_5_hours, 30 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45}, 25 } \
             }
    #define tb_act_remove_8_hours { scnrMakeRemove, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeToRemove_8_hours, 30 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45}, 25 } \
             }
    #define tb_act_remove_1_day { scnrMakeRemove, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeToRemove_1_day, 30 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45}, 25 } \
             }
    #define tb_act_remove_2_days { scnrMakeRemove, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeToRemove_2_days, 30 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45}, 25 } \
             }

#endif
//-----------------------------------------------------------------------------
// -- Create SCENARIO_ARCHIVE instance
//-----------------------------------------------------------------------------
    TB_SCENARIO_ARCHIVE scenario_remove_from_start_tb[] = {
        //// -- action work
        tb_act_work_6_year,
        tb_act_remove_8_hours,
        tb_CheckArchive_Convergence,
        { scnrEND }
    };
    
    TB_SCENARIO_ARCHIVE scenario_remove_from_middle_tb[] = {
        //// -- action work
        tb_act_work_6_year,
        tb_act_remove_2_days,
        tb_act_work_1_day,
        tb_CheckArchive_Convergence,
        { scnrEND }
    };
    
    TB_SCENARIO_ARCHIVE scenario_remove_case_one_tb[] = {
        //// -- action work
        tb_act_work_6_year,
        tb_act_remove_2_days,
        tb_act_work_1_day,
        tb_act_work_1_day,
        tb_CheckArchive_Convergence,
        { scnrEND }
    };
    
    TB_SCENARIO_ARCHIVE scenario_remove_case_two_tb[] = {
        //// -- action work
        tb_act_work_1_day,
        tb_act_work_1_day,
        tb_act_remove_1_day,
        tb_act_work_10_days,
        tb_act_remove_2_days,
        tb_act_work_10_days,
        tb_act_remove_2_days,
        tb_CheckArchive_Convergence,
        { scnrEND }
    };

}