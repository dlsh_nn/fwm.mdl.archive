/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   arch_delete_tcls.cpp
 * Author: User
 *
 * Created on 02.08.2017, 13:42:00
 */

#include "arch_remove_tcls.h"


CPPUNIT_TEST_SUITE_REGISTRATION(arch_delete_tcls);

arch_delete_tcls::arch_delete_tcls() {
}

arch_delete_tcls::~arch_delete_tcls() {
}

void arch_delete_tcls::setUp() {
    tbu.Init();
    // -- Init Time
    mtr::rtc_tbl.crnt_tmt = tmt_DTS_TB_begin;
    mtr::GetDts_tmt(mtr::rtc_tbl.crnt_tmt, &mtr::rtc_tbl.crnt_dts);
    mtr::arch_var.fUpdateRTC = true;
    // -- Init Archive
    mtr::InitArch();
    mtr::reset_arch();
}

void arch_delete_tcls::tearDown() {
}

void arch_delete_tcls::testRemoveFromStart() {
    bool res = tbu.TestScenario(nsAR::scenario_remove_from_start_tb, "scenario_remove_tb");
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, res);
}

void arch_delete_tcls::testRemoveFromMiddle() {
    bool res = tbu.TestScenario(nsAR::scenario_remove_from_middle_tb, "scenario_remove_from_middle_tb");
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, res);
}

void arch_delete_tcls::testRemoveCaseOne() {
    bool res = tbu.TestScenario(nsAR::scenario_remove_case_one_tb, "scenario_remove_case_one_tb");
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, res);
}

void arch_delete_tcls::testRemoveCaseTwo() {
    bool res = tbu.TestScenario(nsAR::scenario_remove_case_two_tb, "scenario_remove_case_two_tb");
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, res);
}


