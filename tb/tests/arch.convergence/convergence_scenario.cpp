#include "../tb_utils/tb_utils.h"

namespace nsCNVRG{

//=============================================================================
// -- scenario convergenc
//=============================================================================
    //// -- Action work 230V, 1A, 45deg 250 day with step_time = 20 min tmt_end = 1663145442
#if 1 
    #define tmt_TimeWork_6year    6 * 365 * 24 * 60 * 60
    #define tb_act_work_6_year  { \
                scnrWorkDiviation, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeWork_6year, 20 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45}, 25 } \
            }
    #define tb_CheckArchive_Convergence { scnrCheckArchive_Convergence, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD} }
#endif
//-----------------------------------------------------------------------------
// -- Create SCENARIO_ARCHIVE instance
//-----------------------------------------------------------------------------
    TB_SCENARIO_ARCHIVE scenario_convergence_tb[] = {
        //// -- action work
        tb_act_work_6_year,
        tb_CheckArchive_Convergence,
        { scnrEND }
    };

}