/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   convergence_tcls.h
 * Author: User
 *
 * Created on 06.07.2017, 7:55:46
 */

#ifndef CONVERGENCE_TCLS_H
#define CONVERGENCE_TCLS_H

#include <cppunit/extensions/HelperMacros.h>
#include "../tb_utils/tb_utils.h"

class convergence_tcls : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(convergence_tcls);

    CPPUNIT_TEST(test_convergence);

    CPPUNIT_TEST_SUITE_END();

private:
    tb_utils_cls tbu;
    
public:
    void setUp();
    void tearDown();

private:
    void test_convergence();
    void testFailedMethod();
};

#endif /* CONVERGENCE_TCLS_H */

