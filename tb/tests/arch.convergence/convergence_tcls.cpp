/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   convergence_tcls.cpp
 * Author: User
 *
 * Created on 06.07.2017, 7:55:47
 */

#include "convergence_tcls.h"

CPPUNIT_TEST_SUITE_REGISTRATION(convergence_tcls);


void convergence_tcls::setUp() {
    tbu.Init();
    // -- Init Time
    mtr::rtc_tbl.crnt_tmt = tmt_DTS_TB_begin;
    mtr::GetDts_tmt(mtr::rtc_tbl.crnt_tmt, &mtr::rtc_tbl.crnt_dts);
    mtr::arch_var.fUpdateRTC = true;
    // -- Init Archive
    mtr::InitArch();
    mtr::reset_arch();
}

void convergence_tcls::tearDown() {
}

void convergence_tcls::test_convergence() {
    bool fError = false;
    
    if(fError |= !tbu.TestScenario(nsCNVRG::scenario_convergence_tb, "scenario_convergence_tb")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}


