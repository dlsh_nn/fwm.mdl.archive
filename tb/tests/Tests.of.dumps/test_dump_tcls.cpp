/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   test_dump_tcls.cpp
 * Author: User
 *
 * Created on 17.08.2017, 12:44:02
 */

#include "test_dump_tcls.h"
#include "../../tb_utils/tb_utils.h"


CPPUNIT_TEST_SUITE_REGISTRATION(test_dump_tcls);

void test_dump_tcls::setUp() {
    tbu.Init();
}

bool test_dump_tcls::setUp(time_t tmtBgn, std::string sPathEE, std::string sPathFLASH) {
    // -- Init Time
    mtr::rtc_tbl.crnt_tmt = tmtBgn;
    mtr::GetDts_tmt(mtr::rtc_tbl.crnt_tmt, &mtr::rtc_tbl.crnt_dts);
    mtr::arch_var.fUpdateRTC = true;
    // -- Init Memory by dump
    CnvrtDumpToExtMem cnv((std::string(PATH_ARCHIVE_tests) + sPathEE).c_str(), 
                          (std::string(PATH_ARCHIVE_tests) + sPathFLASH).c_str());
    //Due to binary search task changes
    if( !cnv.CheckCompatible_EE(VERSION_EEPROM) ||
        !cnv.CheckCompatible_FLASH(VERSION_FLASH) ||
        !cnv.CheckKnownVersionDumps() ) return false;
    
    cnv.Read(mtr::ExtMemory, mtr::ExtFlash);
    
    // -- Init Archive
    mtr::InitArch();
   // -- Init Energy
    memset(&mtr::enrgDt, 0, sizeof(mtr::enrgDt));
    // -- Security
    mtr::ClrPsw();
}

void test_dump_tcls::tearDown(){
}
//-----------------------------------------------------------------------------
// Fwm meter 2.1.122
//-----------------------------------------------------------------------------
void test_dump_tcls::test_CmdReadPPR_NmbTotal() {
    bool fError = false;
    
    // Mon, 02 Jan 2017 03:47:17
    if(!setUp(1483328837, "tests/Tests.of.dumps/2.1.122/dumps/17.08.17 МАЯК 302АРТД read_eeprom.bin",
                      "tests/Tests.of.dumps/2.1.122/dumps/17.08.17 МАЯК 302АРТД read_flash_ALL.bin")) return;
    
    if(fError |= !tbu.TestScenario(nsTB_DUMP::scenario_CmdReadPPR_NmbTotal, "scenario_CmdReadPPR_NmbTotal--by Тест для 2.1.122")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
void test_dump_tcls::test_CmdReadPPR_NmbTotal_After_MTestConvergence() {
    bool fError = false;
    
    //  Sat, 06 May 2017 06:48:17
    if(!setUp(1494053297, 
        "tests/Tests.of.dumps/2.1.122/dumps/21.08.17 Тест на сходимость под MTest_5fc6f8e/МАЯК 302АРТД read_eeprom.bin",
        "tests/Tests.of.dumps/2.1.122/dumps/21.08.17 Тест на сходимость под MTest_5fc6f8e/МАЯК 302АРТД read_flash_ALL.bin")) return;
    
    if(fError |= !tbu.TestScenario(nsTB_DUMP::scenario_CmdReadPPR_NmbTotal_21_08_17, 
            "scenario_CmdReadPPR_NmbTotal--by 21.08.17 Тест на сходимость под MTest_5fc6f8e")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
void test_dump_tcls::test_CmdReadPPR_28_08_17_After_MTestConvergence() {
    bool fError = false;
    
    //  Sat, 06 May 2017 10:29:42 GMT
    if(setUp(1494066582, 
        "tests/Tests.of.dumps/2.1.122/dumps/28.08.17 Тест на сходимость под MTest_886ee2c/МАЯК 302АРТД read_eeprom.bin",
        "tests/Tests.of.dumps/2.1.122/dumps/28.08.17 Тест на сходимость под MTest_886ee2c/МАЯК 302АРТД read_flash_ALL.bin")){
        // --
        if(fError |= !tbu.TestScenario(nsTB_DUMP::scenario_CmdReadPPR_28_08_17, 
                "scenario_CmdReadPPR_28_08_17--by 28.08.17 Тест на сходимость под MTest_886ee2c")) goto __exit;
    }
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
//-----------------------------------------------------------------------------
// Fwm meter 4.0.125
//-----------------------------------------------------------------------------
// -- Bags discription https://systems-measure.slack.com/archives/C60R878M6/p1504595370000188
void test_dump_tcls::test_dump_05_09_17_After_MTestConvergence() {
    bool fError = false;
    
    //  Wed, 03 May 2017 23:30:08 GMT
    if(setUp(1493854208, 
        "tests/Tests.of.dumps/4.0.125/dumps/05.09.17 Тест на сходимость/METER 300ART read_eeprom.bin",
        "tests/Tests.of.dumps/4.0.125/dumps/05.09.17 Тест на сходимость/METER 300ART read_flash_ALL.bin")){
        // --
        if(fError |= !tbu.TestScenario(nsTB_DUMP_05_09_17::scenario_ReadNmbPPR_dump_05_09_17, 
                "scenario_ReadNmbPPR_dump_05_09_17--by 05.09.17 Тест на сходимость")) goto __exit;
    }
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
#if 1
void test_dump_tcls::test_dump_05_09_17_After_MTestConvergence_ver_2() {
    bool fError = false;
    int depth_shift = 3;
    
    //  Wed, 03 May 2017 23:30:08 GMT
    if(setUp(1493854208, 
        "tests/Tests.of.dumps/4.0.125/dumps/05.09.17 Тест на сходимость/METER 300ART read_eeprom.bin",
        "tests/Tests.of.dumps/4.0.125/dumps/05.09.17 Тест на сходимость/METER 300ART read_flash_ALL.bin")){
        // --
        NAME_EEPROM *pName = &mtr::ExtMemory.EEPROM.sect.name;
        
        uint16_t indx_rcrd_origin = pName->_pwr_prfl_head.indx_rcrd;
        uint32_t Addr_rcrd_origin = pName->_pwr_prfl_head.Addr_rcrd;
        //for(int i = 0; i < 2 * depth_shift; i++){
        {   int i = 5; depth_shift = 3;  /* 5861 0x61bc8 (0x5dbc8) */
            tbu.indx_line_tab_scnr = 0;
            // -- shift index
            pName->_pwr_prfl_head.indx_rcrd = indx_rcrd_origin + (i - depth_shift);
            //pName->_pwr_prfl_head.Addr_rcrd = Addr_rcrd_origin + ((i - depth_shift) * sizeof(mtr::RCRD_PWR_PRFL_WITH_SHIFT));
            //pName->_pwr_prfl_head.Addr_rcrd = Indx_to_Addr(pName->_pwr_prfl_head.indx_rcrd, &mtr::ListArch[mtr::eARCH_PPR].lfm);
            pName->_pwr_prfl_head.Addr_rcrd = GetAddrRecord(i - depth_shift, Addr_rcrd_origin, &mtr::ListArch[mtr::eARCH_PPR].lfm);
            // -- Init Archive
            mtr::InitArch();
            
            CPPUNIT_NS::stdCOut() << "    - i=" << i << ";\r\n";
            if(fError |= !tbu.TestScenario(nsTB_DUMP_05_09_17::scenario_ReadNmbPPR_dump_05_09_17_v2, 
                    "scenario_ReadNmbPPR_dump_05_09_17--by 05.09.17 Тест на сходимость -> METER 300ART read_eeprom.bin with shift PPR")){
                std::ostringstream oss;
                
                oss << " - i == " << i << "\r\n"
                    << " - pwr_prfl_head.indx_rcrd == " << pName->_pwr_prfl_head.indx_rcrd << "\r\n"
                    << " - pwr_prfl_head.Addr_rcrd == 0x" << std::hex << pName->_pwr_prfl_head.Addr_rcrd 
                    << " (0x" << (pName->_pwr_prfl_head.Addr_rcrd - 0x4000) << ") \r\n"
                    << tbu.mess_err_tb;
                tbu.mess_err_tb = oss.str();
                goto __exit;
            }
        }
    }
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
#endif
void test_dump_tcls::test_dump_05_09_17_ReadNmb_ADAY() {
    bool fError = false;
    
    //  Wed, 03 May 2017 23:30:08 GMT
    if(setUp(1493854208, 
        "tests/Tests.of.dumps/4.0.125/dumps/05.09.17 Тест на сходимость/METER 300ART read_eeprom.bin",
        "tests/Tests.of.dumps/4.0.125/dumps/05.09.17 Тест на сходимость/METER 300ART read_flash_ALL.bin")){
        // --
        if(fError |= !tbu.TestScenario(nsTB_DUMP_05_09_17::scenario_ReadNmbAMONTH_dump_05_09_17, 
                "scenario_ReadNmbAMONTH_dump_05_09_17--by 05.09.17 Тест на сходимость")) goto __exit;
    }
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
