#include "../../tb_utils/tb_utils.h"

namespace nsTB_DUMP{

    uint8_t pout_STS_OK = {mtr::STS_OK};
    uint8_t pout_STS_NOT_FOUND = {mtr::STS_NOT_FOUND};
    
// -- Read Nmb Total Records PPR
#if 1 
    mtr::enTypePPR tb_in_NTotal = mtr::enTPPR_getNmb;
    #define tb_answ_NTotal(nmb) mtr::PACK_ANSW_PPR_NMB_RCRD tb_answ_NTotal_##nmb = { nmb }
    #define tb_rd_NTotal(nmb) { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_NTotal, sizeof(tb_in_NTotal)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                                                  {&tb_answ_NTotal_##nmb, sizeof(tb_answ_NTotal_##nmb)}, END_PLOAD } } \
                      }
    // --
    #define DTS_17_1_d_T_h_m_s(d, h, m, s)      s,   m, h, 1, d, 1, 17
    #define tb_in_ppr_dlms(dB, hB, mB, sB,  dE, hE, mE, sE)     \
            mtr::PACK_GET_PWR_PRFL tb_in_ppr_dlms_##dB##_##hB##_##mB##_##sB##_T_##dE##_##hE##_##mE##_##sE = \
        {mtr::enTPPR_getNmb_from_DTS, {DTS_17_1_d_T_h_m_s(dB, hB, mB, sB)}, {DTS_17_1_d_T_h_m_s(dE, hE, mE, sE)}}
    #define tb_rd_ppr_NmbRcrd(dB, hB, mB, sB,  dE, hE, mE, sE,  nmb) { scnrNormal, \
        { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_dlms_##dB##_##hB##_##mB##_##sB##_T_##dE##_##hE##_##mE##_##sE, \
                                    sizeof(tb_in_ppr_dlms_##dB##_##hB##_##mB##_##sB##_T_##dE##_##hE##_##mE##_##sE)}, END_PLOAD } }, \
        { mtr::GET_PWR_PROFILE, \
            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
              {&tb_answ_ppr_##nmb, sizeof(tb_answ_ppr_##nmb)}, END_PLOAD } \
        } \
      }
    #define tb_answ_ppr(nmb) mtr::PACK_ANSW_PPR_NMB_RCRD tb_answ_ppr_##nmb = { nmb }
    // --
    mtr::PACK_GET_PWR_PRFL tb_in_ppr_Skip_28_07_17 = {mtr::enTPPR_Skip_PPRv0,   {DTS_17_1_d_T_h_m_s(1,0,0xFF,0)}, 
                                                                                {DTS_17_1_d_T_h_m_s(1,0,0xFF,0)}};
    mtr::PACK_ANSW_PWR_PRFL_V0 tb_answ_ppr_Skip_28_07_17[1] = {
            {    {DTS_17_1_d_T_h_m_s(1, 4,15,0)}, { {0, 0, 0, 0}, {0, 0, 0, 0} }, mtr::ePrflIncompl    },
        };
    #define tb_rd_ppr_28_07_17_Skip { scnrNormal, \
        { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_Skip_28_07_17, sizeof(mtr::PACK_GET_PWR_PRFL)}, END_PLOAD } }, \
        { mtr::GET_PWR_PROFILE, \
            { {&pout_STS_NOT_FOUND, sizeof(pout_STS_NOT_FOUND)}, END_PLOAD } \
        } \
      }
#endif
    //=========================================================================
    // -- READ ARCH scenario_cmd_read_arch_NmbRcrd_tb
    //=========================================================================
    #define DS_y_m_d_T(y, m, d)        d, m, y
    #define MTrf_ALL      0xFF
#if 1   // -- Read ARCH mskTYPE=enType_ArchGetNmb_DS(0x42)
    #define tb_in_arch_DAY(yB, mB, dB,  yE, mE, dE)     \
            mtr::PACK_GET_ARCH tb_in_arch_DAY_##yB##_##mB##_##dB##_T_##yE##_##mE##_##dE = \
        {mtr::enType_ArchGetNmb_DS, mtr::eARCH_DAY,  0xFF, {DS_y_m_d_T(yB, mB, dB)}, {DS_y_m_d_T(yE, mE, dE)}}
    #define tb_answ_arch(nmb) mtr::PACK_ANSW_PPR_NMB_RCRD tb_answ_arch_##nmb = { nmb }
    #define rd_NmbRcrd_DAY(yB, mB, dB,  yE, mE, dE,  nmb) { scnrNormal, \
        { mtr::GET_ARCH_MTR, { {&tb_in_arch_DAY_##yB##_##mB##_##dB##_T_##yE##_##mE##_##dE, \
                                    sizeof(mtr::PACK_GET_ARCH)}, END_PLOAD } }, \
        { mtr::GET_ARCH_MTR, \
            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
              {&tb_answ_arch_##nmb, sizeof(mtr::PACK_ANSW_PPR_NMB_RCRD)}, END_PLOAD } \
        } \
      }
    mtr::PACK_GET_ARCH tb_in_aday_28_07_17 = {mtr::enType_Pass, mtr::eARCH_DAY, MTrf_ALL, {1,1,17}, {1,1,17}};
    mtr::PACK_ANSW_ARCH_HEAD tb_answ_aday_28_07_17_head = {mtr::eARCH_DAY, MTrf_ALL};
    mtr::TB_RCRD_ANSW_ARCH_ALLT tb_answ_aday_28_07_17[1] = {
        { {1,1,17}, {  {0, 0, 0, 0}, {583, 0, 498, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, \
                       {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, } },
    };
    #define tb_rd_aday_28_07_17 { scnrNormal, \
                        { mtr::GET_ARCH_MTR, { {&tb_in_aday_28_07_17, sizeof(mtr::PACK_GET_ARCH)}, END_PLOAD } }, \
                        { mtr::GET_ARCH_MTR, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {&tb_answ_aday_28_07_17_head, sizeof(mtr::PACK_ANSW_ARCH_HEAD)}, \
                              {tb_answ_aday_28_07_17, sizeof(mtr::TB_RCRD_ANSW_ARCH_ALLT)}, END_PLOAD } \
                        } \
                      }
#endif
    //-----------------------------------------------------------------------------
    // -- SCENARIO READ Nmb Records TOTAL PPR
    //-----------------------------------------------------------------------------
    tb_answ_NTotal(8);
    TB_SCENARIO_ARCHIVE scenario_CmdReadPPR_NmbTotal[] = {
        tb_rd_NTotal(8),
        
        { scnrEND }
    };
    tb_answ_NTotal(4430);
    TB_SCENARIO_ARCHIVE scenario_CmdReadPPR_NmbTotal_21_08_17[] = {
        tb_rd_NTotal(4430),
        
        { scnrEND }
    };
    tb_in_ppr_dlms(1,0,0xFF,0,  1,0,0xFF,0);  tb_answ_ppr(0);
    tb_in_arch_DAY(17,1,1,  17,1,1);          tb_answ_arch(1);
    TB_SCENARIO_ARCHIVE scenario_CmdReadPPR_28_08_17[] = {
        // -- PPR
        tb_rd_ppr_NmbRcrd(1,0,0xFF,0,  1,0,0xFF,0,  0),
        tb_rd_ppr_28_07_17_Skip,
        // -- Arch DAY
        rd_NmbRcrd_DAY(17,1,1,  17,1,1,  1),
        tb_rd_aday_28_07_17,
        { scnrEND }
    };
}