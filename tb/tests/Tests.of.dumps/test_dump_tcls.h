/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   test_dump_tcls.h
 * Author: User
 *
 * Created on 17.08.2017, 12:44:01
 */

#ifndef TEST_DUMP_TCLS_H
#define TEST_DUMP_TCLS_H

#include "../../tb_utils/tb_utils.h"

class test_dump_tcls : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(test_dump_tcls);

    CPPUNIT_TEST(test_dump_05_09_17_ReadNmb_ADAY);
    CPPUNIT_TEST(test_dump_05_09_17_After_MTestConvergence_ver_2);
    CPPUNIT_TEST(test_dump_05_09_17_After_MTestConvergence);
    CPPUNIT_TEST(test_CmdReadPPR_28_08_17_After_MTestConvergence);
    CPPUNIT_TEST(test_CmdReadPPR_NmbTotal_After_MTestConvergence);
    CPPUNIT_TEST(test_CmdReadPPR_NmbTotal);

    CPPUNIT_TEST_SUITE_END();
private:
    tb_utils_cls tbu;
public:
    void setUp();
    bool setUp(time_t tmtBgn, std::string sPathEE, std::string sPathFLASH);
    void tearDown();

private:
    void test_CmdReadPPR_NmbTotal();
    void test_CmdReadPPR_NmbTotal_After_MTestConvergence();
    void test_CmdReadPPR_28_08_17_After_MTestConvergence();
    void test_dump_05_09_17_After_MTestConvergence();
    void test_dump_05_09_17_After_MTestConvergence_ver_2();
    void test_dump_05_09_17_ReadNmb_ADAY();
};

#endif /* TEST_DUMP_TCLS_H */

