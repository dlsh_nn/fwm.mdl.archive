#include "../../tb_utils/tb_utils.h"

namespace nsTB_DUMP_05_09_17{

    uint8_t pout_STS_OK = {mtr::STS_OK};
    uint8_t pout_STS_NOT_FOUND = {mtr::STS_NOT_FOUND};
    
//=========================================================================
// -- READ ARCH scenario_for_dump_05_09_17
//=========================================================================
#if 1 
    mtr::enTypePPR tb_in_NTotal = mtr::enTPPR_getNmb;
    #define tb_answ_NTotal(nmb) mtr::PACK_ANSW_PPR_NMB_RCRD tb_answ_NTotal_##nmb = { nmb }
    #define tb_rd_NTotal(nmb) { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_NTotal, sizeof(tb_in_NTotal)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                                                  {&tb_answ_NTotal_##nmb, sizeof(tb_answ_NTotal_##nmb)}, END_PLOAD } } \
                      }
    // --
    #define DTS_17_1_d_T_h_m_s(d, h, m, s)      s,   m, h, 1, d, 1, 17
    #define DTS_17_5_d_T_h_m_s(d, h, m, s)      s,   m, h, 1, d, 5, 17
    #define tb_in_ppr_dlms(dB, hB, mB, sB,  dE, hE, mE, sE)     \
            mtr::PACK_GET_PWR_PRFL tb_in_ppr_dlms_##dB##_##hB##_##mB##_##sB##_T_##dE##_##hE##_##mE##_##sE = \
        {mtr::enTPPR_getNmb_from_DTS, {DTS_17_1_d_T_h_m_s(dB, hB, mB, sB)}, {DTS_17_1_d_T_h_m_s(dE, hE, mE, sE)}}
    #define tb_rd_ppr_NmbRcrd(dB, hB, mB, sB,  dE, hE, mE, sE,  nmb) { scnrNormal, \
        { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_dlms_##dB##_##hB##_##mB##_##sB##_T_##dE##_##hE##_##mE##_##sE, \
                                    sizeof(mtr::PACK_GET_PWR_PRFL)}, END_PLOAD } }, \
        { mtr::GET_PWR_PROFILE, \
            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
              {&tb_answ_ppr_##nmb, sizeof(tb_answ_ppr_##nmb)}, END_PLOAD } \
        } \
    }
    //--
    #define tb_in_ppr_dlms_01_to_05(dB, hB, mB, sB,  dE, hE, mE, sE)     \
            mtr::PACK_GET_PWR_PRFL tb_in_ppr_dlms_01_to_05_##dB##_##hB##_##mB##_##sB##_T_##dE##_##hE##_##mE##_##sE = \
        {mtr::enTPPR_getNmb_from_DTS, {DTS_17_1_d_T_h_m_s(dB, hB, mB, sB)}, {DTS_17_5_d_T_h_m_s(dE, hE, mE, sE)}}
    #define tb_rd_ppr_NmbRcrd_01_to_05(dB, hB, mB, sB,  dE, hE, mE, sE,  nmb) { scnrNormal, \
        { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_dlms_01_to_05_##dB##_##hB##_##mB##_##sB##_T_##dE##_##hE##_##mE##_##sE, \
                                    sizeof(mtr::PACK_GET_PWR_PRFL)}, END_PLOAD } }, \
        { mtr::GET_PWR_PROFILE, \
            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
              {&tb_answ_ppr_##nmb, sizeof(tb_answ_ppr_##nmb)}, END_PLOAD } \
        } \
    }
    #define tb_answ_ppr(nmb) mtr::PACK_ANSW_PPR_NMB_RCRD tb_answ_ppr_##nmb = { nmb }
    // --
    mtr::PACK_GET_PWR_PRFL tb_in_ppr_Skip_28_07_17 = {mtr::enTPPR_Skip_PPRv0,   {DTS_17_1_d_T_h_m_s(1,0,0xFF,0)}, 
                                                                                {DTS_17_1_d_T_h_m_s(1,0,0xFF,0)}};
    mtr::PACK_ANSW_PWR_PRFL_V0 tb_answ_ppr_Skip_28_07_17[1] = {
            {    {DTS_17_1_d_T_h_m_s(1, 4,15,0)}, { {0, 0, 0, 0}, {0, 0, 0, 0} }, mtr::ePrflIncompl    },
        };
    #define tb_rd_ppr_28_07_17_Skip { scnrNormal, \
        { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_Skip_28_07_17, sizeof(mtr::PACK_GET_PWR_PRFL)}, END_PLOAD } }, \
        { mtr::GET_PWR_PROFILE, \
            { {&pout_STS_NOT_FOUND, sizeof(pout_STS_NOT_FOUND)}, END_PLOAD } \
        } \
      }
#endif
//=========================================================================
// -- READ ARCH scenario_ReadNmbAMONTH_dump_05_09_17
//=========================================================================
#if 1
    #define DS_y_m_d_T(y, m, d)        d, m, y
    #define tb_in_arch_MONTH(yB, mB, dB,  yE, mE, dE)     \
                mtr::PACK_GET_ARCH tb_in_arch_MONTH_##yB##_##mB##_##dB##_T_##yE##_##mE##_##dE = \
            {mtr::enType_ArchGetNmb_DS, mtr::eARCH_MNTH,  0xFF, {DS_y_m_d_T(yB, mB, dB)}, {DS_y_m_d_T(yE, mE, dE)}}
    #define rd_NmbRcrd_MONTH(yB, mB, dB,  yE, mE, dE,  nmb) { scnrNormal, \
            { mtr::GET_ARCH_MTR, { {&tb_in_arch_MONTH_##yB##_##mB##_##dB##_T_##yE##_##mE##_##dE, \
                                        sizeof(mtr::PACK_GET_ARCH)}, END_PLOAD } }, \
            { mtr::GET_ARCH_MTR, \
                { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                  {&tb_answ_arch_##nmb, sizeof(tb_answ_arch_##nmb)}, END_PLOAD } \
            } \
          }
    #define tb_answ_arch(nmb) mtr::PACK_ANSW_PPR_NMB_RCRD tb_answ_arch_##nmb = { nmb }
#endif
    //-----------------------------------------------------------------------------
    // -- SCENARIO 
    //-----------------------------------------------------------------------------
    tb_in_ppr_dlms_01_to_05(1,0,0,0,  3,23,0,0);    tb_answ_ppr(5903);
    tb_in_ppr_dlms(1,0,0,0,  1,3,30,0);  tb_answ_ppr(8);
    TB_SCENARIO_ARCHIVE scenario_ReadNmbPPR_dump_05_09_17[] = {
        tb_rd_ppr_NmbRcrd_01_to_05(1,0,0,0,  3,23,0,0,  5903),
        tb_rd_ppr_NmbRcrd(1,0,0,0,  1,3,30,0,  8),                   // log_TX_to_Meter.txt-> line 5909
        { scnrEND }
    };
    tb_answ_ppr(5855); tb_answ_ppr(0);
    TB_SCENARIO_ARCHIVE scenario_ReadNmbPPR_dump_05_09_17_v2[] = {
        //---------------------------------------------------------------------
        // Feature dump from 05.09.17. 
        // Initialization of the UE was carried out on version 3.0.124, 
        // and 3.0.125 simply updated the device.
        //---------------------------------------------------------------------
        #if ADDR_FLASH_sect_arch_bgn_day == SIZE_EEPROM
            tb_rd_ppr_NmbRcrd_01_to_05(1,0,0,0,  3,23,0,0,  5855),
            tb_rd_ppr_NmbRcrd(1,0,0,0,  1,3,30,0,  0),                   // log_TX_to_Meter.txt-> line 5909
        #else
            tb_rd_ppr_NmbRcrd_01_to_05(1,0,0,0,  3,23,0,0,  5903),
            tb_rd_ppr_NmbRcrd(1,0,0,0,  1,3,30,0,  8),                   // log_TX_to_Meter.txt-> line 5909
        #endif
        { scnrEND }
    };
    tb_answ_arch(7);
    tb_in_arch_MONTH(17,1,1,  17,5,1);
    TB_SCENARIO_ARCHIVE scenario_ReadNmbAMONTH_dump_05_09_17[] = {
        rd_NmbRcrd_MONTH(17,1,1,  17,5,1,  7),                     // log_TX_to_Meter.txt-> line 7385
        { scnrEND }
    };
}