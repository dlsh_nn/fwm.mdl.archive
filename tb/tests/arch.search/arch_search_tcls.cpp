/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   arch_search_cls.cpp
 * Author: User
 *
 * Created on 17.08.2017, 10:00:25
 */

#include "arch_search_tcls.h"


CPPUNIT_TEST_SUITE_REGISTRATION(arch_search_tcls);

arch_search_tcls::arch_search_tcls() {
}

arch_search_tcls::~arch_search_tcls() {
}

void arch_search_tcls::setUp() {
    tbu.Init();
    // -- Init Time
    mtr::rtc_tbl.crnt_tmt = tmt_DTS_TB_begin;
    mtr::GetDts_tmt(mtr::rtc_tbl.crnt_tmt, &mtr::rtc_tbl.crnt_dts);
    mtr::arch_var.fUpdateRTC = true;
    // -- Init Archive
    mtr::InitArch();
    mtr::reset_arch();
}

void arch_search_tcls::tearDown() {
}

void arch_search_tcls::testSimpleSearch() {
    bool res = tbu.TestScenario(nsAS::scenario_simple_search_tb, "scenario_simple_search_tb");
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, res);
}

void arch_search_tcls::testSearchCase1() {
    bool res = tbu.TestScenario(nsAS::scenario_search_case_1_tb, "scenario_search_case_1_tb");
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, res);
}

void arch_search_tcls::testSearchCase2() {
    bool res = tbu.TestScenario(nsAS::scenario_search_case_2_tb, "scenario_search_case_2_tb");
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, res);
}

void arch_search_tcls::testSearchCase3() {
    bool res = tbu.TestScenario(nsAS::scenario_search_case_3_tb, "scenario_search_case_3_tb");
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, res);
}

void arch_search_tcls::testSearchCase4() {
    bool res = tbu.TestScenario(nsAS::scenario_search_case_4_tb, "scenario_search_case_4_tb");
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, res);
}