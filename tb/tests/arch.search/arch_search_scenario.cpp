#include "../tb_utils/tb_utils.h"

namespace nsAS{

//=============================================================================
// -- scenario archive remove
//=============================================================================
    //// -- Action work 230V, 1A, 45deg 250 day with step_time = 20 min tmt_end = 1663145442
#if 1 
    #define tmt_TimeWork_6_year    6 * 365 * 24 * 60 * 60
    #define tb_act_work_6_year  { \
                scnrWorkDiviation, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeWork_6_year, 20 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45}, 25 } \
            }
    #define tb_CheckArchive_Convergence { scnrCheckArchive_Convergence, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD} }

    #define tmt_TimeWork_1_day    24 * 60 * 60
    #define tb_act_work_1_day  { \
                scnrWorkDiviation, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeWork_1_day, 20 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45}, 25 } \
            }
    #define tmt_TimeWork_10_days   10 * 24 * 60 * 60
    #define tb_act_work_10_days  { \
                scnrWorkDiviation, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeWork_10_days, 20 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45}, 25 } \
            }

    #define tmt_TimeWork_4_hours   4 * 60 * 60
    #define tb_act_work_4_hours  { \
                scnrWorkDiviation, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeWork_4_hours, 20 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45}, 25 } \
            }
#endif
    
//// -- Command to remove some archives
#if 1

    #define tmt_TimeToRemove_1_hour            60 * 60
    #define tmt_TimeToRemove_5_hours           60 * 60
    #define tmt_TimeToRemove_8_hours       8 * 60 * 60
    #define tmt_TimeToRemove_1_day        24 * 60 * 60
    #define tmt_TimeToRemove_2_days   2 * 24 * 60 * 60

    #define tb_act_remove_1_hour { scnrMakeRemove, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeToRemove_1_hour, 20 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45}, 25 } \
             }
    #define tb_act_remove_5_hour { scnrMakeRemove, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeToRemove_5_hours, 20 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45}, 25 } \
             }
    #define tb_act_remove_8_hours { scnrMakeRemove, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeToRemove_8_hours, 20 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45}, 25 } \
             }
    #define tb_act_remove_1_day { scnrMakeRemove, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeToRemove_1_day, 20 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45}, 25 } \
             }
    #define tb_act_remove_2_days { scnrMakeRemove, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeToRemove_2_days, 20 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45}, 25 } \
             }

#endif
#if 1 
    #define DTS_16_9_17_T_0_0_0              0,  0,  0, 6, 17, 9, 16//0,  30, 8, 5, 9, 11, 35(Found)//*/ //0, 30, 8, 5, 1, 6, 22//(Not Found)
    #define DTS_16_9_16_T_16_0_0             0,  0, 16, 5, 16, 9, 16
    #define DTS_16_9_16_T_12_0_0             0,  0,  6, 5, 16, 9, 16    
    #define DTS_16_9_15_T_14_0_0             0,  0, 14, 8, 15, 9, 16
    
    mtr::PACK_GET_PWR_PRFL tb_in_ppr_16_9_17_T_0_0_0 =  {mtr::enTPPR_NoSkip_PPRv0, {DTS_16_9_17_T_0_0_0},  {DTS_16_9_17_T_0_0_0}};
    mtr::PACK_GET_PWR_PRFL tb_in_ppr_16_9_16_T_16_0_0 = {mtr::enTPPR_NoSkip_PPRv0, {DTS_16_9_16_T_16_0_0}, {DTS_16_9_16_T_16_0_0}};
    mtr::PACK_GET_PWR_PRFL tb_in_ppr_16_9_16_T_12_0_0 = {mtr::enTPPR_NoSkip_PPRv0, {DTS_16_9_16_T_12_0_0}, {DTS_16_9_16_T_12_0_0}};
    mtr::PACK_GET_PWR_PRFL tb_in_ppr_16_9_15_T_14_0_0 = {mtr::enTPPR_NoSkip_PPRv0, {DTS_16_9_15_T_14_0_0}, {DTS_16_9_15_T_14_0_0}};
    
    #define tb_find_ppr(date)  { scnrNormal_Check_ConstPQP, \
                    { mtr::GET_PWR_PROFILE, { {&date, sizeof(date)}, END_PLOAD }, }, \                   
                    { mtr::GET_PWR_PROFILE, END_PLOAD }}

#endif
//-----------------------------------------------------------------------------
// -- Create SCENARIO_ARCHIVE instance
//-----------------------------------------------------------------------------
    TB_SCENARIO_ARCHIVE scenario_simple_search_tb[] = {
        //// -- action work
        tb_act_work_1_day,
        tb_act_work_1_day,
        tb_act_remove_1_day,
        tb_act_work_1_day,
        tb_find_ppr(tb_in_ppr_16_9_16_T_12_0_0),
        { scnrEND }
    };
    
    TB_SCENARIO_ARCHIVE scenario_search_case_1_tb[] = {
        //// -- action work
        tb_act_work_1_day,
        tb_act_remove_1_day,
        tb_act_work_1_day,
        tb_act_work_1_day,
        tb_act_remove_1_day,
        tb_act_work_1_day,
        tb_act_work_1_day,
        tb_act_remove_1_day,
        tb_find_ppr(tb_in_ppr_16_9_16_T_12_0_0),
        { scnrEND }
    };
        
    TB_SCENARIO_ARCHIVE scenario_search_case_2_tb[] = {
        //// -- action work
        tb_act_work_1_day,
        tb_act_work_1_day,
        tb_act_remove_1_day,
        tb_act_work_1_day,
        tb_act_work_1_day,
        tb_act_remove_1_day,
        tb_act_work_1_day,
        tb_act_work_1_day,
        tb_find_ppr(tb_in_ppr_16_9_16_T_12_0_0),
        { scnrEND }
    };
            
    TB_SCENARIO_ARCHIVE scenario_search_case_3_tb[] = {
        //// -- action work
        tb_act_work_1_day,
        tb_act_remove_1_day,
        tb_act_work_1_day,
        tb_act_work_1_day,
        tb_find_ppr(tb_in_ppr_16_9_16_T_12_0_0),
        { scnrEND }
    };

    
        TB_SCENARIO_ARCHIVE scenario_search_case_4_tb[] = {
        //// -- action work
        tb_act_work_1_day,
        tb_act_remove_1_day,
        tb_act_work_1_day,
        tb_act_work_1_day,
        tb_find_ppr(tb_in_ppr_16_9_16_T_12_0_0),
        { scnrEND }
    };

}