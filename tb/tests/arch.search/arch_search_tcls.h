/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   arch_search_cls.h
 * Author: User
 *
 * Created on 17.08.2017, 10:00:25
 */

#ifndef ARCH_SEARCH_CLS_H
#define ARCH_SEARCH_CLS_H

#include <cppunit/extensions/HelperMacros.h>
#include "../tb_utils/tb_utils.h"

class arch_search_tcls : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(arch_search_tcls);

    CPPUNIT_TEST(testSimpleSearch);
    CPPUNIT_TEST(testSearchCase1);
    CPPUNIT_TEST(testSearchCase2);
    CPPUNIT_TEST(testSearchCase3);
    CPPUNIT_TEST(testSearchCase4);

    CPPUNIT_TEST_SUITE_END();

private:
    tb_utils_cls tbu;
    
public:
    arch_search_tcls();
    virtual ~arch_search_tcls();
    void setUp();
    void tearDown();

private:
    void testSimpleSearch();
    void testSearchCase1();
    void testSearchCase2();
    void testSearchCase3();
    void testSearchCase4();
};

#endif /* ARCH_SEARCH_CLS_H */

