/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   test_CmdMngPpr_tcls.cpp
 * Author: User
 *
 * Created on 11.07.2017, 7:56:43
 */

#include "test_CmdMngPpr_tcls.h"

CPPUNIT_TEST_SUITE_REGISTRATION(test_CmdMngPpr_tcls);

void test_CmdMngPpr_tcls::setUp() {
    tbu.Init();
    // -- Init Time
    mtr::rtc_tbl.crnt_tmt = tmt_DTS_TB_begin;
    mtr::GetDts_tmt(mtr::rtc_tbl.crnt_tmt, &mtr::rtc_tbl.crnt_dts);
    mtr::arch_var.fUpdateRTC = true;
    // -- Init Archive
    mtr::InitArch();
    mtr::reset_arch();
    // -- Security
    mtr::ClrPsw();
}

void test_CmdMngPpr_tcls::tearDown() {
}

void test_CmdMngPpr_tcls::test_cmd_mng_ppr_type01_PSW() {
    bool fError = false;
    
    if(fError |= !tbu.TestScenario(nsCMPPR::scenario_CMPPR_PSW_tb, "scenario_CMPPК_PSW_tb")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
void test_CmdMngPpr_tcls::test_cmd_mng_ppr_BUSY() {
    bool fError = false;
    
    mtr::arch_mech.Mode = mtr::enFindeArchBgnDTS_Bgn;
    if(fError |= !tbu.TestScenario(nsCMPPR::scenario_CMPPR_BUSY_tb, "scenario_CMPPК_BUSY_tb")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
void test_CmdMngPpr_tcls::test_cmd_mng_ppr_Write() {
    bool fError = false;
    
    if(fError |= !tbu.TestScenario(nsCMPPR::scenario_CMPPR_Write_tb, "scenario_CMPPК_Write_tb")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
void test_CmdMngPpr_tcls::test_cmd_mng_ppr_Write_OnOffOn() {
    bool fError = false;
    
    if(fError |= !tbu.TestScenario(nsCMPPR::scenario_CMPPR_Write_OnOffOn_tb, "scenario_CMPPК_Write_OnOffOn_tb")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
void test_CmdMngPpr_tcls::test_cmd_mng_ppr_Wr_ClrPPR() {
    bool fError = false;
    
    if(fError |= !tbu.TestScenario(nsCMPPR::scenario_CMPPR_Write_ClrPPR_tb, "scenario_CMPPR_Write_ClrPPR_tb")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
void test_CmdMngPpr_tcls::test_cmd_mng_ppr_Wr_ClrPPR_v2() {
    bool fError = false;
    
     if(fError |= !tbu.TestScenario(nsCMPPR::scenario_CMPPR_Write_ClrPPR_v2_tb, "scenario_CMPPR_Write_ClrPPR_v2_tb")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
