/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   test_CmdMngPpr_tcls.h
 * Author: User
 *
 * Created on 11.07.2017, 7:56:43
 */

#ifndef TEST_CMDMNGPPR_TCLS_H
#define TEST_CMDMNGPPR_TCLS_H

#include "../../tb_utils/tb_utils.h"

class test_CmdMngPpr_tcls : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(test_CmdMngPpr_tcls);

    CPPUNIT_TEST(test_cmd_mng_ppr_type01_PSW);
    CPPUNIT_TEST(test_cmd_mng_ppr_BUSY);
    CPPUNIT_TEST(test_cmd_mng_ppr_Write);
    CPPUNIT_TEST(test_cmd_mng_ppr_Write_OnOffOn);
    CPPUNIT_TEST(test_cmd_mng_ppr_Wr_ClrPPR);
    CPPUNIT_TEST(test_cmd_mng_ppr_Wr_ClrPPR_v2);

    CPPUNIT_TEST_SUITE_END();
private:
    tb_utils_cls tbu;
    
public:
    void setUp();
    void tearDown();

private:
    void test_cmd_mng_ppr_type01_PSW();
    void test_cmd_mng_ppr_BUSY();
    void test_cmd_mng_ppr_Write();
    void test_cmd_mng_ppr_Write_OnOffOn();
    void test_cmd_mng_ppr_Wr_ClrPPR();
    void test_cmd_mng_ppr_Wr_ClrPPR_v2();
};

#endif /* TEST_CMDMNGPPR_TCLS_H */

