#include "../../tb_utils/tb_utils.h"

namespace nsCMPPR{

//=============================================================================
// -- scenario cmd_mng_ppr type=0x01
//=============================================================================
    //// -- scenario COMMON
    #define _pin_WrValidPsw(tmt) mtr::PACK_CMD_MNG_PPR pin_WrValidPsw_##tmt = {PSW_DEF, mtr::enMPPR_wrDuration, tmt};
    #define pin_WrValidPsw(tmt) pin_WrValidPsw_##tmt
    #define _pout_Rd_ValidPsw(tmt) mtr::PACK_ANSW_MNG_PPR pout_Rd_ValidPsw_##tmt = {mtr::STS_OK, tmt};
    #define pout_Rd_ValidPsw(tmt) pout_Rd_ValidPsw_##tmt
    
    uint8_t pout_STS_OK = {mtr::STS_OK};
    uint8_t pout_STS_NOT_FOUND = {mtr::STS_NOT_FOUND};
    uint8_t pout_STS_ERR_PRM = {mtr::STS_ERR_PRM};
    uint8_t pout_ANSW_BUSY = {mtr::ANSW_METER_BUSY};
    uint8_t pout_STS_ERR_PSW = {mtr::STS_ERR_PSW};
    //// -- scenario_CMPPR_PSW_tb
#if 1 
    mtr::PACK_CMD_MNG_PPR pin_InvalidPsw = {{0, 0, 0, 1, 0, 0}, mtr::enMPPR_rdDuration};
    #define tb_InvalidPsw(scnr)  { \
                scnr, \
                {mtr::CMD_MNG_PPR, {{&pin_InvalidPsw, sizeof(pin_InvalidPsw)}, END_PLOAD}}, \
                {mtr::CMD_MNG_PPR, {{&pout_STS_ERR_PSW, sizeof(pout_STS_ERR_PSW)}, END_PLOAD}}, \
            }
    mtr::PACK_CMD_MNG_PPR pin_Rd_ValidPsw = {PSW_DEF, mtr::enMPPR_rdDuration};
    mtr::PACK_ANSW_MNG_PPR pout_ValidPsw = {mtr::STS_OK, 30L * 60};
    #define tb_ValidPsw(scnr)  { \
                scnr, \
                {mtr::CMD_MNG_PPR, {{&pin_Rd_ValidPsw, sizeof(pin_Rd_ValidPsw)}, END_PLOAD}}, \
                {mtr::CMD_MNG_PPR, {{&pout_ValidPsw, sizeof(pout_ValidPsw)}, END_PLOAD}}, \
            }
#endif
//// -- scenario_CMPPR_BUSY_tb
#if 1 
    _pin_WrValidPsw(450);
    #define tb_Wr_ValidPsw_BUSY(tmt)  { \
                scnrNormal_NoArchExec, \
                {mtr::CMD_MNG_PPR, {{&pin_WrValidPsw(tmt), sizeof(pin_WrValidPsw(tmt))}, END_PLOAD}}, \
                {mtr::CMD_MNG_PPR, {{&pout_ANSW_BUSY, sizeof(pout_ANSW_BUSY)}, END_PLOAD}}, \
            }
#endif
//// -- scenario_CMPPR_Write_tb
#if 1 
    _pout_Rd_ValidPsw(450);
    _pin_WrValidPsw(50); 
    _pin_WrValidPsw(60); _pin_WrValidPsw(120); _pin_WrValidPsw(180); _pin_WrValidPsw(240); 
    _pin_WrValidPsw(300); _pin_WrValidPsw(360); _pin_WrValidPsw(600); _pin_WrValidPsw(720);
    _pin_WrValidPsw(900); _pin_WrValidPsw(1200); _pin_WrValidPsw(1800); _pin_WrValidPsw(3600); 
    _pin_WrValidPsw(3601); _pin_WrValidPsw(4000); 
    
    #define tb_Wr_ValidPsw(tmt)  { \
                scnrNormal, \
                {mtr::CMD_MNG_PPR, {{&pin_WrValidPsw(tmt), sizeof(pin_WrValidPsw(tmt))}, END_PLOAD}}, \
                {mtr::CMD_MNG_PPR, {{&pout_STS_OK, sizeof(pout_STS_OK)}, END_PLOAD}}, \
            }
    #define tb_Rd_ValidPsw(tmt)  { \
                scnrNormal, \
                {mtr::CMD_MNG_PPR, {{&pin_Rd_ValidPsw, sizeof(pin_Rd_ValidPsw)}, END_PLOAD}}, \
                {mtr::CMD_MNG_PPR, {{&pout_Rd_ValidPsw(tmt), sizeof(pout_Rd_ValidPsw(tmt))}, END_PLOAD}}, \
            }
    #define tb_Wr_ValidPsw_ErrPrm(tmt)  { \
                scnrNormal, \
                {mtr::CMD_MNG_PPR, {{&pin_WrValidPsw(tmt), sizeof(pin_WrValidPsw(tmt))}, END_PLOAD}}, \
                {mtr::CMD_MNG_PPR, {{&pout_STS_ERR_PRM, sizeof(pout_STS_ERR_PRM)}, END_PLOAD}}, \
            }
#endif
//// -- scenario_CMPPR_Write_ClrPPR_tb
#if 1 
#define tmt_TimeWork    250 * 24 * 60 * 60      // End data_tmt = 1495529442
    #define tb_act_work_250day  { \
                scnrWork, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                tmt_TimeWork, 20 * 60, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45} } \
            }
    #define tb_act_work_10min { \
                scnrWork, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                10L * 60, 1, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45} } \
            }
    // -- Read begin DTS PPR
    #define DTS_TB_begin_alig_halfhour_pls_125day   00, 30, 8, 4, 18, 01, 17 // tmt_125day = 1484728200
    mtr::PACK_ANSW_BGN_DTS_PPR tb_answ_bgn_dts_ppr_125day = {mtr::STS_OK, DTS_TB_begin_alig_halfhour_pls_125day};
    #define tb_rd_bgn_dts_ppr_125day  { scnrNormal, \
                        { mtr::GET_BGN_DTS_PPR, END_PLOAD }, \
                        { mtr::GET_BGN_DTS_PPR, \
                            { {&tb_answ_bgn_dts_ppr_125day, sizeof(tb_answ_bgn_dts_ppr_125day)}, END_PLOAD }, \
                        } \
                    }
    // -- Read begin DTS PPR
    #define DTS_TB_begin_alig_halfhour_pls_250day_450   00, 45, 8, 3, 23, 05, 17 // tmt_250day = 
    #define DTS_TB_begin_alig_halfhour_pls_250day_1800  00, 30, 8, 3, 23, 05, 17 // tmt_250day = 1495528200
    #define tb_answ_bgn_dts_ppr_250day(Dur) mtr::PACK_ANSW_BGN_DTS_PPR tb_answ_bgn_dts_ppr_250day_##Dur = {mtr::STS_OK, DTS_TB_begin_alig_halfhour_pls_250day_##Dur};
    #define tb_rd_bgn_dts_ppr_NotFound  { scnrNormal, \
                        { mtr::GET_BGN_DTS_PPR, END_PLOAD }, \
                        { mtr::GET_BGN_DTS_PPR, \
                            { {&pout_STS_NOT_FOUND, sizeof(pout_STS_NOT_FOUND)}, END_PLOAD }, \
                        } \
                    }
    tb_answ_bgn_dts_ppr_250day(450); tb_answ_bgn_dts_ppr_250day(1800);
    #define tb_rd_bgn_dts_ppr_250day_pls10min(Duration)  { scnrNormal, \
                        { mtr::GET_BGN_DTS_PPR, END_PLOAD }, \
                        { mtr::GET_BGN_DTS_PPR, \
                            { {&tb_answ_bgn_dts_ppr_250day_##Duration, sizeof(tb_answ_bgn_dts_ppr_250day_##Duration)}, END_PLOAD }, \
                        } \
                    }
    // -- Read PPR *16-9-15Т8:30:00* до *16-9-15Т9:00:00* 
    #define DTS_17_05_23_T_8_0_0     0,  0, 8, 3, 23, 5, 17
    #define DTS_17_05_23_T_8_30_0    0, 30, 8, 3, 23, 5, 17
    #define DTS_17_05_23_T_9_0_0     0,  0, 9, 3, 23, 5, 17
    #define DTS_17_05_23_T_9_30_0    0, 30, 9, 3, 23, 5, 17
    #define DTS_17_05_23_T(h, m, s) s, m, h, 3, 23, 5, 17

    mtr::PACK_GET_PWR_PRFL tb_in_ppr_8h_to_9h = {mtr::enTPPR_NoSkip_PPRv0, {DTS_17_05_23_T_8_0_0}, {DTS_17_05_23_T_9_30_0}};
    mtr::PACK_ANSW_PWR_PRFL_V0 tb_answ_ppr_8h_to_9h30m[4] = {
        {   {DTS_17_05_23_T_8_0_0},  { {-1, -1, -1, -1}, {-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde    },
        {   {DTS_17_05_23_T_8_30_0}, { {1463950, 0, 1463950, 0}, {487902, 0, 487902, 0}, }, mtr::ePrflSumm    },
        {   {DTS_17_05_23_T_9_0_0},  { {6, 0, 6, 0}, {487902, 0, 487902, 0}, }, mtr::ePrflCurrent    },
        {   {DTS_17_05_23_T_9_30_0}, { {-1, -1, -1, -1}, {-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde     }
    };
    #define tb_rd_ppr_8h_to_9h_D30min  { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_8h_to_9h, sizeof(tb_in_ppr_8h_to_9h)}, END_PLOAD }, }, \
                        { mtr::GET_PWR_PROFILE, \
                            { \
                                {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                                {tb_answ_ppr_8h_to_9h30m, sizeof(tb_answ_ppr_8h_to_9h30m)}, \
                                END_PLOAD \
                            }, \
                        } \
                    }

    mtr::PACK_ANSW_PWR_PRFL_V0 tb_answ_ppr_8h_to_9h30m_D450sec[13] = {
        {   {DTS_17_05_23_T(8, 0, 0)},  { {-1, -1, -1, -1}, {-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde    },
        {   {DTS_17_05_23_T(8, 7, 30)}, { {-1, -1, -1, -1}, {-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde    },
        {   {DTS_17_05_23_T(8, 15, 0)}, { {-1, -1, -1, -1}, {-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde    },
        {   {DTS_17_05_23_T(8, 22, 30)},{ {-1, -1, -1, -1}, {-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde    },
        {   {DTS_17_05_23_T(8, 30, 0)}, { {-1, -1, -1, -1}, {-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde    },
        {   {DTS_17_05_23_T(8, 37, 30)},{ {-1, -1, -1, -1}, {-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde    },
        {   {DTS_17_05_23_T(8, 45, 0)}, { {1463889, 0, 1463889, 0}, {487902, 0, 487902, 0}, }, mtr::ePrflSumm    },
        {   {DTS_17_05_23_T(8, 52, 30)},{ {61, 0, 61, 0}, {487902, 0, 487902, 0}, }, mtr::ePrflValid    },
        {   {DTS_17_05_23_T(9, 0, 0)},  { {6, 0, 6, 0}, {487902, 0, 487902, 0}, }, mtr::ePrflCurrent    },
        {   {DTS_17_05_23_T(9, 7, 30)},  { {-1, -1, -1, -1}, {-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde     },
        {   {DTS_17_05_23_T(9, 15, 00)}, { {-1, -1, -1, -1}, {-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde     },
        {   {DTS_17_05_23_T(9, 22, 30)}, { {-1, -1, -1, -1}, {-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde     },
        {   {DTS_17_05_23_T(9, 30, 00)}, { {-1, -1, -1, -1}, {-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde     },
    };
    #define tb_rd_ppr_8h_to_9h_D450sec  { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_8h_to_9h, sizeof(tb_in_ppr_8h_to_9h)}, END_PLOAD }, }, \
                        { mtr::GET_PWR_PROFILE, \
                            { \
                                {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                                {tb_answ_ppr_8h_to_9h30m_D450sec, sizeof(tb_answ_ppr_8h_to_9h30m_D450sec)}, \
                                END_PLOAD \
                            }, \
                        } \
                    }
#endif
//-----------------------------------------------------------------------------
// -- Create SCENARIO_ARCHIVE 
//-----------------------------------------------------------------------------
    TB_SCENARIO_ARCHIVE scenario_CMPPR_PSW_tb[] = {
        tb_InvalidPsw(scnrNormal), tb_ValidPsw(scnrNormal),
        { scnrEND }
    };
    TB_SCENARIO_ARCHIVE scenario_CMPPR_BUSY_tb[] = {
        tb_InvalidPsw(scnrNormal_NoArchExec), tb_ValidPsw(scnrNormal_NoArchExec), tb_Wr_ValidPsw_BUSY(450),
        { scnrEND }
    };
    TB_SCENARIO_ARCHIVE scenario_CMPPR_Write_tb[] = {
        tb_Wr_ValidPsw(450), tb_Rd_ValidPsw(450),
        tb_Wr_ValidPsw_ErrPrm(50), 
        tb_Wr_ValidPsw(60), tb_Wr_ValidPsw(120), tb_Wr_ValidPsw(180), 
        tb_Wr_ValidPsw(240), tb_Wr_ValidPsw(300), tb_Wr_ValidPsw(360), 
        tb_Wr_ValidPsw(600), tb_Wr_ValidPsw(720), tb_Wr_ValidPsw(900), 
        tb_Wr_ValidPsw(1200), tb_Wr_ValidPsw(1800), tb_Wr_ValidPsw(3600), 
        tb_Wr_ValidPsw_ErrPrm(3601), tb_Wr_ValidPsw_ErrPrm(4000),
        { scnrEND }
    };
    TB_SCENARIO_ARCHIVE scenario_CMPPR_Write_OnOffOn_tb[] = {
        tb_Wr_ValidPsw(450), {scnrAction_OffOn_Mtr}, tb_Rd_ValidPsw(450),
        { scnrEND }
    };
    TB_SCENARIO_ARCHIVE scenario_CMPPR_Write_ClrPPR_tb[] = {
        tb_act_work_250day, tb_rd_bgn_dts_ppr_125day,
        tb_Wr_ValidPsw(450), 
        tb_rd_bgn_dts_ppr_NotFound,
        tb_act_work_10min,
        tb_rd_bgn_dts_ppr_250day_pls10min(450),
        tb_rd_ppr_8h_to_9h_D450sec,
        { scnrEND }
    };
    TB_SCENARIO_ARCHIVE scenario_CMPPR_Write_ClrPPR_v2_tb[] = {
        tb_act_work_250day, tb_rd_bgn_dts_ppr_125day,
        tb_Wr_ValidPsw(1800), 
        tb_rd_bgn_dts_ppr_NotFound,
        tb_act_work_10min,
        tb_rd_bgn_dts_ppr_250day_pls10min(1800),
        tb_rd_ppr_8h_to_9h_D30min,
        { scnrEND }
    };
}