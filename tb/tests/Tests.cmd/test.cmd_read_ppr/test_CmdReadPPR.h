/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   test_CmdReadPPR.h
 * Author: User
 *
 * Created on 24.07.2017, 14:26:49
 */

#ifndef TEST_CMDREADPPR_H
#define TEST_CMDREADPPR_H

#include "../../tb_utils/tb_utils.h"

class test_CmdReadPPR : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(test_CmdReadPPR);
    ///// DEBUG //////
    CPPUNIT_TEST(test_cmd_read_ppr_PPRvs04_season_StW);
    ///////////////
    CPPUNIT_TEST(test_cmd_read_ppr_PPRvs04_ByIndex);
    CPPUNIT_TEST(test_cmd_read_ppr_NmbRcrds_Total);
    CPPUNIT_TEST(test_cmd_read_ppr_PPRvs04_season_StW_D60s);
    CPPUNIT_TEST(test_cmd_read_ppr_PPRvs04_season_StW);
    CPPUNIT_TEST(test_cmd_read_ppr_PPRvs04_season_WtS);
    CPPUNIT_TEST(test_cmd_read_ppr_PPRvs04_season);
    CPPUNIT_TEST(test_cmd_read_ppr_AllType);
    CPPUNIT_TEST(test_cmd_read_ppr_variousAlignDTS);
    CPPUNIT_TEST(test_cmd_read_ppr_PPRvs04);
    CPPUNIT_TEST(test_cmd_read_ppr_PPRv2);
    CPPUNIT_TEST(test_cmd_read_ppr_NmbRcrd);
    CPPUNIT_TEST(test_cmd_read_ppr_NmbRcrd_season_WtS);
    CPPUNIT_TEST(test_cmd_read_ppr_NmbRcrd_season_StW);
    CPPUNIT_TEST(test_cmd_read_ppr_NmbRcrd_season_StW_D60s);

    CPPUNIT_TEST_SUITE_END();
private:
    tb_utils_cls tbu;
    
public:
    void setUp();
    void setUp(time_t tmtBgn);
    void tearDown();

private:
    void test_cmd_read_ppr_AllType();
    void test_cmd_read_ppr_variousAlignDTS();
    void test_cmd_read_ppr_PPRv2();
    void test_cmd_read_ppr_PPRvs04();
    void test_cmd_read_ppr_NmbRcrd();
    void test_cmd_read_ppr_NmbRcrd_season_WtS();
    void test_cmd_read_ppr_NmbRcrd_season_StW();
    void test_cmd_read_ppr_NmbRcrd_season_StW_D60s();
    void test_cmd_read_ppr_PPRvs04_season();
    void test_cmd_read_ppr_PPRvs04_season_WtS();
    void test_cmd_read_ppr_PPRvs04_season_StW();
    void test_cmd_read_ppr_PPRvs04_season_StW_D60s();
    void test_cmd_read_ppr_NmbRcrds_Total();
    void test_cmd_read_ppr_PPRvs04_ByIndex();
};

#endif /* TEST_CMDREADPPR_H */

