#include "../../tb_utils/tb_utils.h"

namespace nsCRPPR_SEASON{

//=============================================================================
// -- scenario cmd_read_ppr with Season
//=============================================================================
#if 1
    //// -- scenario COMMON
    #define _pin_WrValidPsw(tmt) mtr::PACK_CMD_MNG_PPR pin_WrValidPsw_##tmt = {PSW_DEF, mtr::enMPPR_wrDuration, tmt};
    #define pin_WrValidPsw(tmt) pin_WrValidPsw_##tmt
    #define _pout_Rd_ValidPsw(tmt) mtr::PACK_ANSW_MNG_PPR pout_Rd_ValidPsw_##tmt = {mtr::STS_OK, tmt};
    #define pout_Rd_ValidPsw(tmt) pout_Rd_ValidPsw_##tmt
    
    uint8_t pout_STS_OK = {mtr::STS_OK};
    uint8_t pout_STS_NOT_FOUND = {mtr::STS_NOT_FOUND};
    uint8_t pout_STS_ERR_PRM = {mtr::STS_ERR_PRM};
    uint8_t pout_ANSW_BUSY = {mtr::ANSW_METER_BUSY};
    uint8_t pout_STS_ERR_PSW = {mtr::STS_ERR_PSW};
    
    // -- Action
#if 1
    #define tb_act_work_min(val) { \
            scnrWork, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
            ((val) * 60L), 1, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45} }, \
            {UseOtherPrm, TEMP_DEF} \
        }
#endif
#endif 
    #define DTS_16_d_m_TSSN_(d,mnth,h,m, fssn)  0,m,h, (5 | fssn), d,mnth,16
    #define DTS_16_d_m_TSSN_wd_(d,mnth,wd,h,m, fssn)  0,m,h, (wd | fssn), d,mnth,16
    #define DTS_16_9_15_TSSN_(h, m, fssn)       DTS_16_d_m_TSSN_(15,9, h,m,fssn)
    
    //=============================================================================
    // scenario_cmd_read_ppr_season_tb
    //=============================================================================
#if 1 // -- Read PPR mskTYPE=enTPPR_NoSkipSeason_PPR_vs4(0x44) NO_SKIP *16-9-15Т7:00:00* до *16-9-15Т11:00:00* 
    #define _in_vs4_ssn_(dB,mnthB,hB,mB,fssnB,  dE,mnthE,hE,mE,fssnE) \
        in_vs4_ssn_PPRvs4__##dB##_##mnthB##_##hB##_##mB##_##fssnB##__to__##dE##_##mnthE##_##hE##_##mE##_##fssnE
    #define in_vs4_ssn(dB,mnthB,hB,mB,fssnB,  dE,mnthE,hE,mE,fssnE) mtr::PACK_GET_PWR_PRFL \
        in_vs4_ssn_PPRvs4__##dB##_##mnthB##_##hB##_##mB##_##fssnB##__to__##dE##_##mnthE##_##hE##_##mE##_##fssnE = \
        {mtr::enTPPR_NoSkipSeason_PPR_vs4, {DTS_16_d_m_TSSN_(dB,mnthB,hB,mB, fssnB)}, {DTS_16_d_m_TSSN_(dE,mnthE,hE,mE, fssnE)}}
    mtr::PACK_ANSW_PWR_PRFL_VS4 answ_NoSkipSsnOnPPRvs4_7h_to_11h[9] = {
        { {DTS_16_9_15_TSSN_(7,  0,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_TSSN_(7, 30,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_TSSN_(8,  0,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_TSSN_(8, 30,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_TSSN_(9, 00,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_TSSN_(9, 30,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_TSSN_(10, 0,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_TSSN_(10,30,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_TSSN_(11, 0,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
    };
    mtr::PACK_ANSW_PWR_PRFL_VS4 answ_NoSkipSsnOffPPRvs4_7h_to_11h[9] = {
        { {DTS_16_9_15_TSSN_(7,  0,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_TSSN_(7, 30,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_TSSN_(8,  0,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_TSSN_(8, 30,  0)}, {0, 0, 0, 0}, { {0, 0, 0}, 0 }, mtr::ePrflIncompl },
        { {DTS_16_9_15_TSSN_(9,  0,  0)}, {319, 0, 319, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_9_15_TSSN_(9, 30,  0)}, {168, 0, 168, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflCurrent },
        { {DTS_16_9_15_TSSN_(10, 0,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_TSSN_(10,30,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_TSSN_(11, 0,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
    };
    in_vs4_ssn(15,9,7,0,MSK_SUMMER_TIME,  15,9,11,0,MSK_SUMMER_TIME);
    in_vs4_ssn(15,9,7,0,0,  15,9,11,0,0);
    #define rd_vs4_7h_to_11h_fSsnS { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&_in_vs4_ssn_(15,9,7,0,MSK_SUMMER_TIME,  15,9,11,0,MSK_SUMMER_TIME), \
                            sizeof(_in_vs4_ssn_(15,9,7,0,MSK_SUMMER_TIME,  15,9,11,0,MSK_SUMMER_TIME))}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {answ_NoSkipSsnOnPPRvs4_7h_to_11h, sizeof(answ_NoSkipSsnOnPPRvs4_7h_to_11h)}, END_PLOAD } \
                        } \
                      }
    #define rd_vs4_7h_to_11h_fSsnW { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&_in_vs4_ssn_(15,9,7,0,0,  15,9,11,0,0), \
                            sizeof(_in_vs4_ssn_(15,9,7,0,0,  15,9,11,0,0))}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {answ_NoSkipSsnOffPPRvs4_7h_to_11h, sizeof(answ_NoSkipSsnOffPPRvs4_7h_to_11h)}, END_PLOAD } \
                        } \
                      }                            
    #define rd_vs4_ERR(dB,mnthB,hB,mB,fssnB,  dE,mnthE,hE,mE,fssnE) { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&in_vs4_ssn_PPRvs4__##dB##_##mnthB##_##hB##_##mB##_##fssnB##__to__##dE##_##mnthE##_##hE##_##mE##_##fssnE, \
                            sizeof(mtr::PACK_GET_PWR_PRFL)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_ERR_PRM, sizeof(pout_STS_ERR_PRM)}, END_PLOAD } \
                        } \
                      }
#endif
    //=============================================================================
    // scenario_cmd_read_ppr_season_WtS_tb
    //=============================================================================
#if 1 // -- Read PPR mskTYPE=enTPPR_NoSkipSeason_PPR_vs4(0x44) NO_SKIP
    #define DTS_16_3_27_TSSN_(h, m, fssn)       DTS_16_d_m_TSSN_wd_(27,3,1, h,m,fssn)
    mtr::PACK_ANSW_PWR_PRFL_VS4 answ_WtS_0h_to_4h_fS[9] = {
        { {DTS_16_3_27_TSSN_(0,  0,  0)}, {0, 0, 0, 0}, { {0, 0, 0}, 0 }, mtr::ePrflIncompl },
        { {DTS_16_3_27_TSSN_(0, 30,  0)}, {345, 0, 345, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
        { {DTS_16_3_27_TSSN_(1,  0,  0)}, {244, 0, 244, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
        { {DTS_16_3_27_TSSN_(1, 30,  0)}, {243, 0, 243, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
        { {DTS_16_3_27_TSSN_(2, 00,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_3_27_TSSN_(2, 30,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_3_27_TSSN_(3, 0,  MSK_SUMMER_TIME)}, {245, 0, 245, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
        { {DTS_16_3_27_TSSN_(3,30,  MSK_SUMMER_TIME)}, {243, 0, 243, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
        { {DTS_16_3_27_TSSN_(4, 0,  MSK_SUMMER_TIME)}, {143, 0, 143, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflCurrent },
    };
    mtr::PACK_ANSW_PWR_PRFL_VS4 answ_WtS_0h_to_4h_fW[9] = {
        { {DTS_16_3_27_TSSN_(0,  0,  0)}, {0, 0, 0, 0}, { {0, 0, 0}, 0 }, mtr::ePrflIncompl },
        { {DTS_16_3_27_TSSN_(0, 30,  0)}, {345, 0, 345, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
        { {DTS_16_3_27_TSSN_(1,  0,  0)}, {244, 0, 244, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
        { {DTS_16_3_27_TSSN_(1, 30,  0)}, {243, 0, 243, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
        { {DTS_16_3_27_TSSN_(2,  0,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_3_27_TSSN_(2, 30,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_3_27_TSSN_(3,  0,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_3_27_TSSN_(3, 30,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_3_27_TSSN_(4,  0,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
    };
    in_vs4_ssn(27,3,0,0,0,  27,3,4,0,MSK_SUMMER_TIME);
    in_vs4_ssn(27,3,0,0,0,  27,3,4,0,0);
    #define rd_WtS_0h_to_4h_fSsnS { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&_in_vs4_ssn_(27,3,0,0,0,  27,3,4,0,MSK_SUMMER_TIME), \
                            sizeof(mtr::PACK_GET_PWR_PRFL)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {answ_WtS_0h_to_4h_fS, sizeof(answ_WtS_0h_to_4h_fS)}, END_PLOAD } \
                        } \
                      }
    #define rd_WtS_0h_to_4h_fSsnW { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&_in_vs4_ssn_(27,3,0,0,0,  27,3,4,0,0), \
                            sizeof(mtr::PACK_GET_PWR_PRFL)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {answ_WtS_0h_to_4h_fW, sizeof(answ_WtS_0h_to_4h_fW)}, END_PLOAD } \
                        } \
                      }
#endif
    //=============================================================================
    // scenario_cmd_read_ppr_season_StW_tb
    //=============================================================================
#if 1 // -- Read PPR mskTYPE=enTPPR_NoSkipSeason_PPR_vs4(0x44) NO_SKIP
    #define DTS_16_10_30_TSSN_(h, m, fssn)       DTS_16_d_m_TSSN_wd_(30,10,1, h,m,fssn)
    mtr::PACK_ANSW_PWR_PRFL_VS4 answ_StW_0h_to_4h_fSW[11] = {
        { {DTS_16_10_30_TSSN_(0,  0,  MSK_SUMMER_TIME)}, {0, 0, 0, 0}, { {0, 0, 0}, 0 }, mtr::ePrflIncompl },
        { {DTS_16_10_30_TSSN_(0, 30,  MSK_SUMMER_TIME)}, {345, 0, 345, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
        { {DTS_16_10_30_TSSN_(1,  0,  MSK_SUMMER_TIME)}, {244, 0, 244, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
        { {DTS_16_10_30_TSSN_(1, 30,  MSK_SUMMER_TIME)}, {244, 0, 244, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
        { {DTS_16_10_30_TSSN_(2, 00,  MSK_SUMMER_TIME)}, {244, 0, 244, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
        { {DTS_16_10_30_TSSN_(2, 30,  MSK_SUMMER_TIME)}, {243, 0, 243, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
        { {DTS_16_10_30_TSSN_(2, 00,  0)}, {143, 0, 143, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflCurrent },
        { {DTS_16_10_30_TSSN_(2, 30,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(3,  0,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(3, 30,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(4,  0,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
    };
    mtr::PACK_ANSW_PWR_PRFL_VS4 answ_StW_0h_to_4h_fWW[9] = {
        { {DTS_16_10_30_TSSN_(0, 00,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(0, 30,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(1, 00,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(1, 30,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(2, 00,  0)}, {143, 0, 143, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflCurrent },
        { {DTS_16_10_30_TSSN_(2, 30,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(3,  0,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(3, 30,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(4,  0,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
    };
    
    in_vs4_ssn(30,10,0,0,MSK_SUMMER_TIME,  30,10,4,0,0);
    in_vs4_ssn(30,10,0,0,0,  30,10,4,0,0);
    #define rd_StW_0h_to_4h_fSsnSW { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&_in_vs4_ssn_(30,10,0,0,MSK_SUMMER_TIME,  30,10,4,0,0), \
                            sizeof(mtr::PACK_GET_PWR_PRFL)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {answ_StW_0h_to_4h_fSW, sizeof(answ_StW_0h_to_4h_fSW)}, END_PLOAD } \
                        } \
                      }
    #define rd_StW_0h_to_4h_fSsnWW { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&_in_vs4_ssn_(30,10,0,0,0,  30,10,4,0,0), \
                            sizeof(mtr::PACK_GET_PWR_PRFL)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {answ_StW_0h_to_4h_fWW, sizeof(answ_StW_0h_to_4h_fWW)}, END_PLOAD } \
                        } \
                      }
#if 1 // rd_StW_2hS_to_2h30S
    in_vs4_ssn(30,10,2,0,MSK_SUMMER_TIME,  30,10,2,30,MSK_SUMMER_TIME);
    mtr::PACK_ANSW_PWR_PRFL_VS4 answ_StW_2hS_to_2h30S[2] = {
        { {DTS_16_10_30_TSSN_(2, 00,  MSK_SUMMER_TIME)}, {244, 0, 244, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
        { {DTS_16_10_30_TSSN_(2, 30,  MSK_SUMMER_TIME)}, {243, 0, 243, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
    };
    #define rd_StW_2hS_to_2h30S { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&_in_vs4_ssn_(30,10,2,0,MSK_SUMMER_TIME,  30,10,2,30,MSK_SUMMER_TIME), \
                            sizeof(mtr::PACK_GET_PWR_PRFL)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {answ_StW_2hS_to_2h30S, sizeof(answ_StW_2hS_to_2h30S)}, END_PLOAD } \
                        } \
                      }
#endif 
#if 1 // rd_StW_2h30S_to_3h0W
    in_vs4_ssn(30,10,2,30,MSK_SUMMER_TIME,  30,10,2,0,0);
    mtr::PACK_ANSW_PWR_PRFL_VS4 answ_StW_2h30S_to_2h0W[2] = {
        { {DTS_16_10_30_TSSN_(2, 30,  MSK_SUMMER_TIME)}, {243, 0, 243, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
        { {DTS_16_10_30_TSSN_(2, 00,  0)}, {143, 0, 143, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflCurrent },
    };
    #define rd_StW_2h30S_to_2h0W { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&_in_vs4_ssn_(30,10,2,30,MSK_SUMMER_TIME,  30,10,2,0,0), \
                            sizeof(mtr::PACK_GET_PWR_PRFL)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {answ_StW_2h30S_to_2h0W, sizeof(answ_StW_2h30S_to_2h0W)}, END_PLOAD } \
                        } \
                      }
#endif 
#if 1 // rd_StW_2hW_to_2h30W
    in_vs4_ssn(30,10,2,0,0,  30,10,2,30,0);
    mtr::PACK_ANSW_PWR_PRFL_VS4 answ_StW_2hW_to_2h30W[2] = {
        { {DTS_16_10_30_TSSN_(2, 00,  0)}, {143, 0, 143, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflCurrent },
        { {DTS_16_10_30_TSSN_(2, 30,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
    };
    #define rd_StW_2hW_to_2h30W { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&_in_vs4_ssn_(30,10,2,0,0,  30,10,2,30,0), \
                            sizeof(mtr::PACK_GET_PWR_PRFL)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {answ_StW_2hW_to_2h30W, sizeof(answ_StW_2hW_to_2h30W)}, END_PLOAD } \
                        } \
                      }
#endif 

#endif
//-----------------------------------------------------------------------------
// -- SCENARIO READ_PPR SEASON
//-----------------------------------------------------------------------------
    in_vs4_ssn(27,3,1,30,MSK_SUMMER_TIME,  27,3,2,0,0);
    in_vs4_ssn(27,3,0,00,MSK_SUMMER_TIME,  27,3,2,0,0);
    in_vs4_ssn(30,10,3,00,MSK_SUMMER_TIME,  30,10,3,00,0);
    in_vs4_ssn(30,10,16,00,MSK_SUMMER_TIME,  30,10,23,00,0);
    in_vs4_ssn(30,10,2,00,0,  30,10,0,0,MSK_SUMMER_TIME);
 
    TB_SCENARIO_ARCHIVE scenario_cmd_read_ppr_season_tb[] = {
        tb_act_work_min(60),
        // -- Debug

        // -- ~Debug
        rd_vs4_7h_to_11h_fSsnS, rd_vs4_7h_to_11h_fSsnW, 
        // -- Read with Erorr parameters
        rd_vs4_ERR(27,3,1,30,MSK_SUMMER_TIME,  27,3,2,0,0),         // Err can not be 27d,3m T 1h,30m with set MSK_SUMMER_TIME
        rd_vs4_ERR(27,3,0,00,MSK_SUMMER_TIME,  27,3,2,0,0),         // Err can not be 27d,3m T 00h,00m with set MSK_SUMMER_TIME
        rd_vs4_ERR(30,10,3,00,MSK_SUMMER_TIME,  30,10,3,00,0),      // Err can not be 30d,10m T 3h,00m with set MSK_SUMMER_TIME
        rd_vs4_ERR(30,10,16,00,MSK_SUMMER_TIME,  30,10,23,00,0),    // Err can not be 30d,10m T 16h,00m with set MSK_SUMMER_TIME
        rd_vs4_ERR(30,10,2,00,0,  30,10,0,0,MSK_SUMMER_TIME),       // Err DTS_bgn > DTS_end
        // --
        { scnrEND }
    };
    TB_SCENARIO_ARCHIVE scenario_cmd_read_ppr_season_WtS_tb[] = {
        tb_act_work_min(3 * 60L),
        // --
        rd_WtS_0h_to_4h_fSsnS, rd_WtS_0h_to_4h_fSsnW,
        // --
        { scnrEND }
    };
    TB_SCENARIO_ARCHIVE scenario_cmd_read_ppr_season_StW_tb[] = {
        tb_act_work_min(3 * 60L),
        // Debug
        
        // ~Debug
        // --
        rd_StW_0h_to_4h_fSsnSW, rd_StW_0h_to_4h_fSsnWW,
        rd_StW_2hS_to_2h30S, rd_StW_2h30S_to_2h0W, rd_StW_2hW_to_2h30W, 
        // --
        { scnrEND }
    };
 }