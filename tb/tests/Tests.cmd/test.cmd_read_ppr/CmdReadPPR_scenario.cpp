#include "../../tb_utils/tb_utils.h"

namespace nsCRPPR{

//=============================================================================
// -- scenario cmd_read_ppr type=0x00 - PPRv0 no skip
//=============================================================================
#if 1
    //// -- scenario COMMON
    #define _pin_WrValidPsw(tmt) mtr::PACK_CMD_MNG_PPR pin_WrValidPsw_##tmt = {PSW_DEF, mtr::enMPPR_wrDuration, tmt};
    #define pin_WrValidPsw(tmt) pin_WrValidPsw_##tmt
    #define _pout_Rd_ValidPsw(tmt) mtr::PACK_ANSW_MNG_PPR pout_Rd_ValidPsw_##tmt = {mtr::STS_OK, tmt};
    #define pout_Rd_ValidPsw(tmt) pout_Rd_ValidPsw_##tmt
    
    uint8_t pout_STS_OK = {mtr::STS_OK};
    uint8_t pout_STS_NOT_FOUND = {mtr::STS_NOT_FOUND};
    uint8_t tb_answ_STS_ERR_PRM = {mtr::STS_ERR_PRM};
    uint8_t pout_ANSW_BUSY = {mtr::ANSW_METER_BUSY};
    uint8_t pout_STS_ERR_PSW = {mtr::STS_ERR_PSW};
    
    // -- Action
#if 1
    #define tb_act_work_min(val) { \
            scnrWork, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
            ((val) * 60L), 1, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45} }, \
            {UseOtherPrm, TEMP_DEF} \
        }
    #define tb_act_work_day(val) { \
            scnrWork, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
            ((val) * 24 * 60 * 60L), 20 * 60L, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45} }, \
            {UseOtherPrm, TEMP_DEF} \
        }
    #define tb_act_remove_day(val) { scnrMakeRemove, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
            ((val) * 24 * 60 * 60L), 20 * 60L, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45} } \
         }
    #define tb_act_PwrOn_day(val) { scnrAction_On_Mtr, {0}, {0}, ((val) * 24 * 60 * 60L) }
    #define tb_act_PwrOn_min(val) { scnrAction_On_Mtr, {0}, {0}, ((val) * 60L) }
#endif
#endif /* scenario cmd_read_ppr type=0x00 - PPRv0 no skip */
    //=========================================================================
    // -- READ PPR scenario_cmd_read_ppr_type_tb
    //=========================================================================
#if 1
    #define DTS_16_9_15_T_(h, m)     0,   m, h, 5, 15, 9, 16
    #define DTS_16_9_15_TSSN_(h, m, ssn)     0,   m, h, (5 | ssn), 15, 9, 16
    // -- Read PPR mskTYPE=0x00 NO_SKIP *16-9-15Т7:00:00* до *16-9-15Т10:30:00* 
#if 1 
    mtr::PACK_GET_PWR_PRFL tb_in_ppr_NoSkip_7h_to_9h = {mtr::enTPPR_NoSkip_PPRv0, {DTS_16_9_15_T_(7, 0)}, {DTS_16_9_15_T_(11, 00)}};
    mtr::PACK_ANSW_PWR_PRFL_V0 tb_answ_ppr_NoSkip_7h_to_10h30[9] = {
        {    {DTS_16_9_15_T_(7,  0)}, { {-1, -1, -1, -1},{-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde    },
        {    {DTS_16_9_15_T_(7, 30)}, { {-1, -1, -1, -1},{-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde    },
        {    {DTS_16_9_15_T_(8,  0)}, { {-1, -1, -1, -1},{-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde    },
        {    {DTS_16_9_15_T_(8, 30)}, { {0, 0, 0, 0}, {0, 0, 0, 0} }, mtr::ePrflIncompl    },
        {    {DTS_16_9_15_T_(9, 0)}, { {319, 0, 319, 0}, {487902, 0, 487902, 0} }, mtr::ePrflValid    },
        {    {DTS_16_9_15_T_(9, 30)}, { {168, 0, 168, 0}, {487902, 0, 487902, 0} }, mtr::ePrflCurrent    },
        {    {DTS_16_9_15_T_(10, 0)}, { {-1, -1, -1, -1},{-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde    },
        {    {DTS_16_9_15_T_(10, 30)}, { {-1, -1, -1, -1},{-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde    },
        {    {DTS_16_9_15_T_(11,  0)}, { {-1, -1, -1, -1},{-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde    },
    };
    #define tb_rd_ppr_7h_to_10h30_NoSkip { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_NoSkip_7h_to_9h, sizeof(tb_in_ppr_NoSkip_7h_to_9h)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {tb_answ_ppr_NoSkip_7h_to_10h30, sizeof(tb_answ_ppr_NoSkip_7h_to_10h30)}, END_PLOAD } \
                        } \
                      }
                            
#endif
    // -- Read PPR mskTYPE=0x01 whifth SKIP *16-9-15Т7:00:00* до *16-9-15Т10:30:00* 
#if 1 
    mtr::PACK_GET_PWR_PRFL tb_in_ppr_Skip_7h_to_9h = {mtr::enTPPR_Skip_PPRv0, {DTS_16_9_15_T_(7, 0)}, {DTS_16_9_15_T_(11, 00)}};
    mtr::PACK_ANSW_PWR_PRFL_V0 tb_answ_ppr_Skip_7h_to_10h30[3] = {
        {    {DTS_16_9_15_T_(8, 30)}, { {0, 0, 0, 0}, {0, 0, 0, 0} }, mtr::ePrflIncompl    },
        {    {DTS_16_9_15_T_(9, 0)}, { {319, 0, 319, 0}, {487902, 0, 487902, 0} }, mtr::ePrflValid    },
        {    {DTS_16_9_15_T_(9, 30)}, { {168, 0, 168, 0}, {487902, 0, 487902, 0} }, mtr::ePrflCurrent    },
    };
    #define tb_rd_ppr_7h_to_10h30_Skip { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_Skip_7h_to_9h, sizeof(tb_in_ppr_Skip_7h_to_9h)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {tb_answ_ppr_Skip_7h_to_10h30, sizeof(tb_answ_ppr_Skip_7h_to_10h30)}, END_PLOAD } \
                        } \
                      }
                            
#endif
    // -- Read PPR NoDef mskTYPE
#if 1 
    #define tb_in_ppr_Type(val) mtr::PACK_GET_PWR_PRFL tb_in_ppr_Type_##val = {(mtr::enTypePPR)val, {DTS_16_9_15_T_(7, 0)}, {DTS_16_9_15_T_(11, 00)}};
    #define RdNoDef_mskType(val) { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_Type_##val, sizeof(tb_in_ppr_Type_##val)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, { {&tb_answ_STS_ERR_PRM, sizeof(tb_answ_STS_ERR_PRM)}, END_PLOAD } } \
                        }
    tb_in_ppr_Type(0x02); tb_in_ppr_Type(0x03); tb_in_ppr_Type(0x10); tb_in_ppr_Type(0x30);
    tb_in_ppr_Type(0x80); tb_in_ppr_Type(0x81); tb_in_ppr_Type(0x82); tb_in_ppr_Type(0x83); 
    tb_in_ppr_Type(0x90); tb_in_ppr_Type(0xB0); tb_in_ppr_Type(0xC0); tb_in_ppr_Type(0xC1); 
#endif
#endif /* READ PPR scenario_cmd_read_ppr_type_tb */
    //=========================================================================
    // -- READ PPR scenario_cmd_read_ppr_type_tb
    //=========================================================================
#if 1 
    //#define DTS_16_9_15_T(h, m)     0,   m, h, 5, 15, 9, 16
    #define tb_in_ppr_VarDTS(m_bgn, m_end) mtr::PACK_GET_PWR_PRFL tb_in_ppr_VarDTS_##m_bgn##_##m_end = \
                                               {mtr::enTPPR_NoSkip_PPRv0, {DTS_16_9_15_T_(9, m_bgn)}, {DTS_16_9_15_T_(10, m_end)}};
    #define RdPPR_VarDTS(m_bgn, m_end) { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_VarDTS_##m_bgn##_##m_end, sizeof(tb_in_ppr_VarDTS_##m_bgn##_##m_end)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, { {&tb_answ_STS_ERR_PRM, sizeof(tb_answ_STS_ERR_PRM)}, END_PLOAD } } \
                        }
    tb_in_ppr_VarDTS(3, 0); tb_in_ppr_VarDTS(0, 3); tb_in_ppr_VarDTS(3, 3); tb_in_ppr_VarDTS(42, 6);

    mtr::PACK_ANSW_PWR_PRFL_V0 tb_answ_ppr_NoSkip_9h_to_10h[9] = {
        {    {DTS_16_9_15_T_(9, 42)}, { {-1, -1, -1, -1},{-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde    },
        {    {DTS_16_9_15_T_(9, 45)}, { {-1, -1, -1, -1},{-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde    },
        {    {DTS_16_9_15_T_(9, 48)}, { {490, 0, 490, 0}, {487902, 0, 487902, 0} }, mtr::ePrflSumm    },
        {    {DTS_16_9_15_T_(9, 51)}, { {24, 0, 24, 0}, {487902, 0, 487902, 0} }, mtr::ePrflValid    },
        {    {DTS_16_9_15_T_(9, 54)}, { {25, 0, 25, 0}, {487902, 0, 487902, 0} }, mtr::ePrflValid    },
        {    {DTS_16_9_15_T_(9, 57)}, { {24, 0, 24, 0}, {487902, 0, 487902, 0} }, mtr::ePrflValid    },
        {    {DTS_16_9_15_T_(10, 0)}, { {6, 0, 6, 0}, {487902, 0, 487902, 0} }, mtr::ePrflCurrent    },
        {    {DTS_16_9_15_T_(10, 3)}, { {-1, -1, -1, -1},{-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde    },
        {    {DTS_16_9_15_T_(10, 6)}, { {-1, -1, -1, -1},{-1, -1, -1, -1} }, mtr::ePrflNoValid_NoFinde    },
    };
    #define RdPPR_VarDTS_OK(m_bgn, m_end) { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_VarDTS_##m_bgn##_##m_end, sizeof(tb_in_ppr_VarDTS_##m_bgn##_##m_end)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                                    {&tb_answ_ppr_NoSkip_9h_to_10h, sizeof(tb_answ_ppr_NoSkip_9h_to_10h)}, END_PLOAD } } \
                        }
   
    _pin_WrValidPsw(180);    
    #define tb_Wr_ValidPsw(tmt)  { \
                scnrNormal, \
                {mtr::CMD_MNG_PPR, {{&pin_WrValidPsw(tmt), sizeof(pin_WrValidPsw(tmt))}, END_PLOAD}}, \
                {mtr::CMD_MNG_PPR, {{&pout_STS_OK, sizeof(pout_STS_OK)}, END_PLOAD}}, \
                }
#endif
    //=========================================================================
    // -- READ PPR scenario_cmd_read_ppr_PPRv2_tb
    //=========================================================================
#if 1
    // -- Read PPR mskTYPE=0x20 NO_SKIP *16-9-15Т7:00:00* до *16-9-15Т11:00:00* 
#if 1 
    mtr::PACK_GET_PWR_PRFL tb_in_ppr_NoSkipPPRv2_7h_to_11h = {mtr::enTPPR_NoSkip_PPRv2, {DTS_16_9_15_T_(7, 0)}, {DTS_16_9_15_T_(11, 00)}};
    mtr::PACK_ANSW_PWR_PRFL_V2 tb_answ_ppr_NoSkipPPRv2_7h_to_11h[9] = {
        { {DTS_16_9_15_T_(7,  0)}, { {-1, -1, -1, -1},{-1, -1, -1, -1} }, { {-1, -1, -1}, {-1, -1, -1} }, mtr::ePrflNoValid_NoFinde_v02 },
        { {DTS_16_9_15_T_(7, 30)}, { {-1, -1, -1, -1},{-1, -1, -1, -1} }, { {-1, -1, -1}, {-1, -1, -1} }, mtr::ePrflNoValid_NoFinde_v02 },
        { {DTS_16_9_15_T_(8,  0)}, { {-1, -1, -1, -1},{-1, -1, -1, -1} }, { {-1, -1, -1}, {-1, -1, -1} }, mtr::ePrflNoValid_NoFinde_v02 },
        { {DTS_16_9_15_T_(8, 30)}, { {0, 0, 0, 0}, {0, 0, 0, 0} }, { {0, 0, 0}, {0, 0, 0} }, mtr::ePrflIncompl },
        { {DTS_16_9_15_T_(9,  0)}, { {319, 0, 319, 0}, {487902, 0, 487902, 0} }, { {2300, 2300, 2300}, {2300, 2300, 2300} }, mtr::ePrflValid  },
        { {DTS_16_9_15_T_(9, 30)}, { {168, 0, 168, 0}, {487902, 0, 487902, 0} }, { {2300, 2300, 2300}, {2300, 2300, 2300} }, mtr::ePrflCurrent },
        { {DTS_16_9_15_T_(10, 0)}, { {-1, -1, -1, -1},{-1, -1, -1, -1} }, { {-1, -1, -1}, {-1, -1, -1} }, mtr::ePrflNoValid_NoFinde_v02 },
        { {DTS_16_9_15_T_(10,30)}, { {-1, -1, -1, -1},{-1, -1, -1, -1} }, { {-1, -1, -1}, {-1, -1, -1} }, mtr::ePrflNoValid_NoFinde_v02 },
        { {DTS_16_9_15_T_(11, 0)}, { {-1, -1, -1, -1},{-1, -1, -1, -1} }, { {-1, -1, -1}, {-1, -1, -1} }, mtr::ePrflNoValid_NoFinde_v02 },
    };
    #define tb_rd_ppr_7h_to_11h_NoSkip_PPRv2 { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_NoSkipPPRv2_7h_to_11h, sizeof(tb_in_ppr_NoSkipPPRv2_7h_to_11h)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {tb_answ_ppr_NoSkipPPRv2_7h_to_11h, sizeof(tb_answ_ppr_NoSkipPPRv2_7h_to_11h)}, END_PLOAD } \
                        } \
                      }
                            
#endif
    // -- Read PPR mskTYPE=0x21 SKIP *16-9-15Т7:00:00* до *16-9-15Т11:00:00* 
#if 1 
    mtr::PACK_GET_PWR_PRFL tb_in_ppr_SkipPPRv2_7h_to_11h = {mtr::enTPPR_Skip_PPRv2, {DTS_16_9_15_T_(7, 0)}, {DTS_16_9_15_T_(11, 00)}};
    mtr::PACK_ANSW_PWR_PRFL_V2 tb_answ_ppr_SkipPPRv2_7h_to_11h[3] = {
        { {DTS_16_9_15_T_(8, 30)}, { {0, 0, 0, 0}, {0, 0, 0, 0} }, { {0, 0, 0}, {0, 0, 0} }, mtr::ePrflIncompl },
        { {DTS_16_9_15_T_(9,  0)}, { {319, 0, 319, 0}, {487902, 0, 487902, 0} }, { {2300, 2300, 2300}, {2300, 2300, 2300} }, mtr::ePrflValid  },
        { {DTS_16_9_15_T_(9, 30)}, { {168, 0, 168, 0}, {487902, 0, 487902, 0} }, { {2300, 2300, 2300}, {2300, 2300, 2300} }, mtr::ePrflCurrent },
    };
    #define tb_rd_ppr_7h_to_11h_Skip_PPRv2 { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_SkipPPRv2_7h_to_11h, sizeof(tb_in_ppr_SkipPPRv2_7h_to_11h)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {tb_answ_ppr_SkipPPRv2_7h_to_11h, sizeof(tb_answ_ppr_SkipPPRv2_7h_to_11h)}, END_PLOAD } \
                        } \
                      }
                            
#endif
#endif /* READ PPR scenario_cmd_read_ppr_PPRv2_tb */
    //=========================================================================
    // -- READ PPR scenario_cmd_read_ppr_PPRvs4_tb
    //=========================================================================
#if 1
    // -- Read PPR mskTYPE=0x44 NO_SKIP *16-9-15Т7:00:00* до *16-9-15Т11:00:00* 
#if 1 
    mtr::PACK_GET_PWR_PRFL tb_in_ppr_NoSkipPPRvs4_7h_to_11h = {mtr::enTPPR_NoSkip_PPR_vs4, {DTS_16_9_15_T_(7, 0)}, {DTS_16_9_15_T_(11, 00)}};
    mtr::PACK_ANSW_PWR_PRFL_VS4 tb_answ_ppr_NoSkipPPRvs4_7h_to_11h[9] = {
        { {DTS_16_9_15_T_(7,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(7, 30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(8,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(8, 30)}, {0, 0, 0, 0}, { {0, 0, 0}, 0 }, mtr::ePrflIncompl },
        { {DTS_16_9_15_T_(9,  0)}, {319, 0, 319, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_9_15_T_(9, 30)}, {168, 0, 168, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflCurrent },
        { {DTS_16_9_15_T_(10, 0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(10,30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(11, 0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
    };
    #define tb_rd_ppr_7h_to_11h_NoSkip_PPRvs4 { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_NoSkipPPRvs4_7h_to_11h, sizeof(tb_in_ppr_NoSkipPPRvs4_7h_to_11h)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {tb_answ_ppr_NoSkipPPRvs4_7h_to_11h, sizeof(tb_answ_ppr_NoSkipPPRvs4_7h_to_11h)}, END_PLOAD } \
                        } \
                      }
                            
#endif
#endif /* READ PPR scenario_cmd_read_ppr_PPRvs4_tb */
    //=========================================================================
    // -- READ PPR scenario_cmd_read_ppr_PPRvs04_ByIndex_tb
    //=========================================================================
#if 1
    // -- Read PPR mskTYPE=TypePPR_forINDX 0x46 
#if 1
    mtr::PACK_ANSW_PWR_PRFL_VS4 tb_answ_byIndex_PPRvs4_6_to_0[7] = {
        { {DTS_16_9_15_T_(6, 30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(7,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(7, 30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(8,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(8, 30)}, {0, 0, 0, 0}, { {0, 0, 0}, 0 }, mtr::ePrflIncompl },
        { {DTS_16_9_15_T_(9,  0)}, {319, 0, 319, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_9_15_T_(9, 30)}, {168, 0, 168, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflCurrent },
    };
    #define tb_rd_ByIndx_6_to_0_PPRvs4 { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_byIndex_6_0, sizeof(mtr::PACK_GET_PPR_by_INDX)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {tb_answ_byIndex_PPRvs4_6_to_0, sizeof(tb_answ_byIndex_PPRvs4_6_to_0)}, END_PLOAD } \
                        } \
                      }
    mtr::PACK_ANSW_PWR_PRFL_VS4 tb_answ_byIndex_PPRvs4_0_to_6[7] = {
        { {DTS_16_9_15_T_(9, 30)}, {168, 0, 168, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflCurrent },
        { {DTS_16_9_15_T_(9,  0)}, {319, 0, 319, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_9_15_T_(8, 30)}, {0, 0, 0, 0}, { {0, 0, 0}, 0 }, mtr::ePrflIncompl },
        { {DTS_16_9_15_T_(8,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(7, 30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(7,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(6, 30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
    };
    #define tb_rd_ByIndx_0_to_6_PPRvs4 { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_byIndex_0_6, sizeof(mtr::PACK_GET_PPR_by_INDX)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {tb_answ_byIndex_PPRvs4_0_to_6, sizeof(tb_answ_byIndex_PPRvs4_0_to_6)}, END_PLOAD } \
                        } \
                      }
    mtr::PACK_ANSW_PWR_PRFL_VS4 tb_answ_byIndex_PPRvs4_0_to_m1[20] = {
        { {DTS_16_9_15_T_(9, 30)}, {168, 0, 168, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflCurrent },
        { {DTS_16_9_15_T_(9,  0)}, {319, 0, 319, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_9_15_T_(8, 30)}, {0, 0, 0, 0}, { {0, 0, 0}, 0 }, mtr::ePrflIncompl },
        { {DTS_16_9_15_T_(8,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(7, 30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(7,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(6, 30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(6, 00)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(5, 30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(5, 00)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(4, 30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(4, 00)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(3, 30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(3, 00)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(2, 30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(2, 00)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(1, 30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(1, 00)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(0, 30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(0, 00)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
    };
    mtr::PACK_GET_PPR_by_INDX tb_in_byIndex_0_m1 = {mtr::enTPPR_PPRvs4_by_INDX, 0, -1};
    #define tb_rd_ByIndx_0_to_m1_PPRvs4 { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_byIndex_0_m1, sizeof(mtr::PACK_GET_PPR_by_INDX)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {tb_answ_byIndex_PPRvs4_0_to_m1, sizeof(tb_answ_byIndex_PPRvs4_0_to_m1)}, END_PLOAD } \
                        } \
                      }
    mtr::PACK_ANSW_PWR_PRFL_VS4 tb_answ_byIndex_PPRvs4_m1_to_0[20] = {
        { {DTS_16_9_15_T_(0, 00)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(0, 30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(1, 00)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(1, 30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(2, 00)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(2, 30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(3, 00)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(3, 30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(4, 00)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(4, 30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(5, 00)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(5, 30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(6, 00)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(6, 30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(7,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(7, 30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(8,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_9_15_T_(8, 30)}, {0, 0, 0, 0}, { {0, 0, 0}, 0 }, mtr::ePrflIncompl },
        { {DTS_16_9_15_T_(9,  0)}, {319, 0, 319, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_9_15_T_(9, 30)}, {168, 0, 168, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflCurrent },
    };
    mtr::PACK_GET_PPR_by_INDX tb_in_byIndex_m1_0 = {mtr::enTPPR_PPRvs4_by_INDX, -1, 0};
    #define tb_rd_ByIndx_m1_to_0_PPRvs4 { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_byIndex_m1_0, sizeof(mtr::PACK_GET_PPR_by_INDX)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {tb_answ_byIndex_PPRvs4_m1_to_0, sizeof(tb_answ_byIndex_PPRvs4_m1_to_0)}, END_PLOAD } \
                        } \
                      }
    mtr::PACK_ANSW_PWR_PRFL_VS4 tb_answ_byIndex_PPRvs4_1_to_2[2] = {
        { {DTS_16_9_15_T_(9,  0)}, {319, 0, 319, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_9_15_T_(8, 30)}, {0, 0, 0, 0}, { {0, 0, 0}, 0 }, mtr::ePrflIncompl },
    };
    #define tb_rd_ByIndx_1_to_2_PPRvs4 { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_byIndex_1_2, sizeof(mtr::PACK_GET_PPR_by_INDX)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {tb_answ_byIndex_PPRvs4_1_to_2, sizeof(tb_answ_byIndex_PPRvs4_1_to_2)}, END_PLOAD } \
                        } \
                      }
    mtr::PACK_ANSW_PWR_PRFL_VS4 tb_answ_byIndex_PPRvs4_2_to_1[2] = {
        { {DTS_16_9_15_T_(8, 30)}, {0, 0, 0, 0}, { {0, 0, 0}, 0 }, mtr::ePrflIncompl },
        { {DTS_16_9_15_T_(9,  0)}, {319, 0, 319, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
    };
    #define tb_rd_ByIndx_2_to_1_PPRvs4 { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_byIndex_2_1, sizeof(mtr::PACK_GET_PPR_by_INDX)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {tb_answ_byIndex_PPRvs4_2_to_1, sizeof(tb_answ_byIndex_PPRvs4_2_to_1)}, END_PLOAD } \
                        } \
                      }
    /////////////////////////////////////////////
    ///////////// After action
    /////////////////////////////////////////////
    #define DTS_17_5_20_T_(h, m)     0,   m, h, 7, 20, 5, 17
    #define DTS_17_5_19_T_(h, m)     0,   m, h, 6, 19, 5, 17
    #define DTS_17_1_18_T_(h, m)     0,   m, h, 4, 18, 1, 17
    
    mtr::PACK_ANSW_PWR_PRFL_VS4 tb_answ_byIndex_PPRvs4_AfterAct_0_to_6[7] = {
        { {DTS_17_5_20_T_(11, 30)}, {168, 0, 168, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflCurrent },
        { {DTS_17_5_20_T_(11,  0)}, {244, 0, 244, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_17_5_20_T_(10, 30)}, {244, 0, 244, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
        { {DTS_17_5_20_T_(10,  0)}, {244, 0, 244, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
        { {DTS_17_5_20_T_(9, 30)}, {35373, 0, 35373, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflSumm },
        { {DTS_17_5_20_T_(9,  0)}, {244, 0, 244, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
        { {DTS_17_5_20_T_(8, 30)}, {244, 0, 244, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
    };
    #define tb_rd_ByIndx_AfterAct_0_to_6_PPRvs4 { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_byIndex_0_6, sizeof(mtr::PACK_GET_PPR_by_INDX)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {tb_answ_byIndex_PPRvs4_AfterAct_0_to_6, sizeof(tb_answ_byIndex_PPRvs4_AfterAct_0_to_6)}, END_PLOAD } \
                        } \
                      }
    mtr::PACK_ANSW_PWR_PRFL_VS4 tb_answ_byIndex_PPRvs4_AfterAct_22_to_28[7] = {
        { {DTS_17_5_20_T_(0, 30)}, {244, 0, 244, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
        { {DTS_17_5_20_T_(0,  0)}, {244, 0, 244, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_17_5_19_T_(23, 30)}, {244, 0, 244, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
        { {DTS_17_5_19_T_(23,  0)}, {244, 0, 244, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
        { {DTS_17_5_19_T_(22, 30)}, {244, 0, 244, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
        { {DTS_17_5_19_T_(22,  0)}, {244, 0, 244, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
        { {DTS_17_5_19_T_(21, 30)}, {244, 0, 244, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
    };
    #define tb_rd_ByIndx_AfterAct_22_to_28_PPRvs4 { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_byIndex_22_28, sizeof(mtr::PACK_GET_PPR_by_INDX)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {tb_answ_byIndex_PPRvs4_AfterAct_22_to_28, sizeof(tb_answ_byIndex_PPRvs4_AfterAct_22_to_28)}, END_PLOAD } \
                        } \
                      }
    mtr::PACK_ANSW_PWR_PRFL_VS4 tb_answ_byIndex_PPRvs4_AfterAct_m1_to_5879[1] = {
        { {DTS_17_1_18_T_(0, 00)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
    };
    mtr::PACK_GET_PPR_by_INDX tb_in_byIndex_m1_5879 = {mtr::enTPPR_PPRvs4_by_INDX, -1, 5879};
    #define tb_rd_ByIndx_AfterAct_m1_to_5879_PPRvs4 { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_byIndex_m1_5879, sizeof(mtr::PACK_GET_PPR_by_INDX)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {tb_answ_byIndex_PPRvs4_AfterAct_m1_to_5879, sizeof(tb_answ_byIndex_PPRvs4_AfterAct_m1_to_5879)}, END_PLOAD } \
                        } \
                      }
    /////////////////////////////////////////////
    ///////////// After action 2
    /////////////////////////////////////////////
    #define DTS_17_5_22_T_(h, m)     0,   m, h, 2, 22, 5, 17
    mtr::PACK_ANSW_PWR_PRFL_VS4 tb_answ_byIndex_PPRvs4_AfterAct2_24_to_30[7] = {
        { {DTS_17_5_22_T_(0,  30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_17_5_22_T_(0,   0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_17_5_20_T_(23, 30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_17_5_20_T_(23,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_17_5_20_T_(22, 30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_17_5_20_T_(22,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_17_5_20_T_(21, 30)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
    };
    #define tb_rd_ByIndx_AfterAct2_24_to_30_PPRvs4 { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_byIndex_24_30, sizeof(mtr::PACK_GET_PPR_by_INDX)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {tb_answ_byIndex_PPRvs4_AfterAct2_24_to_30, sizeof(tb_answ_byIndex_PPRvs4_AfterAct2_24_to_30)}, END_PLOAD } \
                        } \
                      }
    ////////////////////////////////////////////////
    ////// -- rd_ByIndx_ERR
    ////////////////////////////////////////////////
    #define tb_in_byIndex(iBgn, iEnd) mtr::PACK_GET_PPR_by_INDX tb_in_byIndex_##iBgn##_##iEnd = {mtr::enTPPR_PPRvs4_by_INDX, iBgn, iEnd};
    #define tb_rd_ByIndx_ERR(iBgn, iEnd) { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_byIndex_##iBgn##_##iEnd, sizeof(mtr::PACK_GET_PPR_by_INDX)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&tb_answ_STS_ERR_PRM, sizeof(tb_answ_STS_ERR_PRM)}, END_PLOAD } \
                        } \
                      }
#endif
    mtr::enTypePPR tb_in_NTotal = mtr::enTPPR_getNmb;
    #define tb_answ_NTotal(nmb) mtr::PACK_ANSW_PPR_NMB_RCRD tb_answ_NTotal_##nmb = { nmb }
    #define tb_rd_NTotal(nmb) { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_NTotal, sizeof(tb_in_NTotal)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                                                  {&tb_answ_NTotal_##nmb, sizeof(tb_answ_NTotal_##nmb)}, END_PLOAD } } \
                      }
#endif /* Read PPR mskTYPE=TypePPR_forINDX 0x46  */
//-----------------------------------------------------------------------------
// -- SCENARIO READ_PPR various  TYPE
//-----------------------------------------------------------------------------
    TB_SCENARIO_ARCHIVE scenario_cmd_read_ppr_type_tb[] = {
        tb_act_work_min(60),
        tb_rd_ppr_7h_to_10h30_NoSkip, tb_rd_ppr_7h_to_10h30_Skip, 
        // Read PPR NoDef mskTYPE
        RdNoDef_mskType(0x02), RdNoDef_mskType(0x03), RdNoDef_mskType(0x10), RdNoDef_mskType(0x30),
        RdNoDef_mskType(0x80), RdNoDef_mskType(0x81), RdNoDef_mskType(0x82), RdNoDef_mskType(0x83),
        RdNoDef_mskType(0x90), RdNoDef_mskType(0xB0), RdNoDef_mskType(0xC0), RdNoDef_mskType(0xC1),
        // --
        { scnrEND }
    };
    TB_SCENARIO_ARCHIVE scenario_cmd_read_ppr_VariousAlignDTS_tb[] = {
        tb_act_work_min(60),
        RdPPR_VarDTS(3, 0), RdPPR_VarDTS(0, 3), RdPPR_VarDTS(3, 3), RdPPR_VarDTS(42, 6),
        // --
        tb_Wr_ValidPsw(180), tb_act_work_min(10),
        // --
        RdPPR_VarDTS_OK(42, 6), 
        // --
        { scnrEND }
    };
    TB_SCENARIO_ARCHIVE scenario_cmd_read_ppr_PPRv2_tb[] = {
        tb_act_work_min(60),
        tb_rd_ppr_7h_to_11h_NoSkip_PPRv2, tb_rd_ppr_7h_to_11h_Skip_PPRv2,
        // --
        { scnrEND }
    };
    TB_SCENARIO_ARCHIVE scenario_cmd_read_ppr_PPRvs04_tb[] = {
        tb_act_work_min(60),
        tb_rd_ppr_7h_to_11h_NoSkip_PPRvs4,
        // --
        { scnrEND }
    };
    tb_answ_NTotal(20); tb_answ_NTotal(6020); tb_answ_NTotal(5876);  tb_answ_NTotal(5880); 
    tb_answ_NTotal(5930); 
    tb_in_byIndex(20, 0); tb_in_byIndex(0, 20); tb_in_byIndex(6, 0); tb_in_byIndex(0, 6);
    tb_in_byIndex(1, 2); tb_in_byIndex(2, 1); tb_in_byIndex(22, 28);
    tb_in_byIndex(24, 30);
    TB_SCENARIO_ARCHIVE scenario_cmd_read_ppr_PPRvs04_ByIndex_tb[] = {
                                ///   1473929442  Thu, 15 Sep 2016 08:50:42
        tb_act_work_min(60),    ///<  1473933042  Thu, 15 Sep 2016 09:50:42
        // Debug
        
        //~Debug
        tb_rd_NTotal(20),
        tb_rd_ByIndx_6_to_0_PPRvs4, tb_rd_ByIndx_0_to_6_PPRvs4,
        tb_rd_ByIndx_0_to_m1_PPRvs4, tb_rd_ByIndx_m1_to_0_PPRvs4,
        tb_rd_ByIndx_1_to_2_PPRvs4, tb_rd_ByIndx_2_to_1_PPRvs4,
        // --
        tb_rd_ByIndx_ERR(20, 0), tb_rd_ByIndx_ERR(0, 20), 
        ////////////// action
        tb_act_work_day(250),       ///<    1495533042  Tue, 23 May 2017 09:50:42
        tb_rd_NTotal(6020), 
        tb_act_remove_day(3),       ///<    1495273842  Sat, 20 May 2017 09:50:42
        tb_rd_NTotal(5876), 
        tb_act_work_min(2 * 60),    ///<    1495281042  Sat, 20 May 2017 11:50:42
        tb_rd_NTotal(5880),
        ////////////// read profile by index
        tb_rd_ByIndx_AfterAct_0_to_6_PPRvs4, tb_rd_ByIndx_AfterAct_22_to_28_PPRvs4,
        tb_rd_ByIndx_AfterAct_m1_to_5879_PPRvs4,
        ////////////// action
        {scnrAction_Off_Mtr}, tb_act_PwrOn_min(0), {scnrAction_Off_Mtr}, tb_act_PwrOn_min(0),
        tb_act_PwrOn_day(2),        ///< 1495453842  Mon, 22 May 2017 11:50:42 
        tb_act_work_min(1 * 60),    ///< 1495457442  Mon, 22 May 2017 12:50:42
        ////////////// read profile by index
        tb_rd_NTotal(5930),
        tb_rd_ByIndx_AfterAct2_24_to_30_PPRvs4,
        // --
        { scnrEND }
    };
    
}