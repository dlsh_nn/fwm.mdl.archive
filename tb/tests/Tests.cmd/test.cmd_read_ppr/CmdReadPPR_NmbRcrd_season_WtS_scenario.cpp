#include "../../tb_utils/tb_utils.h"

namespace nsCRPPR_NMB_RCRD_season_WtS{

//=============================================================================
// -- scenario cmd_read_ppr NmskType=enTPPR_getNmb_from_DTS(0x42)
//=============================================================================
#if 1
    //// -- scenario COMMON
    #define _pin_WrValidPsw(tmt) mtr::PACK_CMD_MNG_PPR pin_WrValidPsw_##tmt = {PSW_DEF, mtr::enMPPR_wrDuration, tmt};
    #define pin_WrValidPsw(tmt) pin_WrValidPsw_##tmt
    #define _pout_Rd_ValidPsw(tmt) mtr::PACK_ANSW_MNG_PPR pout_Rd_ValidPsw_##tmt = {mtr::STS_OK, tmt};
    #define pout_Rd_ValidPsw(tmt) pout_Rd_ValidPsw_##tmt
    
    uint8_t pout_STS_OK = {mtr::STS_OK};
    uint8_t pout_STS_NOT_FOUND = {mtr::STS_NOT_FOUND};
    uint8_t tb_answ_STS_ERR_PRM = {mtr::STS_ERR_PRM};
    uint8_t pout_ANSW_BUSY = {mtr::ANSW_METER_BUSY};
    uint8_t pout_STS_ERR_PSW = {mtr::STS_ERR_PSW};
    
    // -- Action
#if 1 /* scenario cmd_read_ppr mskType=enTPPR_getNmb_from_DTS(0x42) */
    #define tb_act_work_min(val) { \
            scnrWork, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
            (val) * 60L, 1, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45} }, \
            {UseOtherPrm, TEMP_DEF} \
        }
#endif
#endif 
    //=========================================================================
    // -- READ PPR scenario_cmd_read_ppr_NmbRcrd_tb
    //=========================================================================
    #define DTS_16_03_27_T_(h, m)                0,   m, h, 5, 27, 03, 16
    #define DTS_16_03_d_T_h_0_00(d, h)            0,   0, h, 5, d, 03, 16
    #define DTS_16_03_d_T_h_m_s(d, h, m, s)      s,   m, h, 5, d, 03, 16
    // -- Read PPR mskTYPE=enTPPR_getNmb_from_DTS(0x42) of hours
#if 1 
    #define tb_in_ppr(h_bgn, h_end) mtr::PACK_GET_PWR_PRFL tb_in_ppr_##h_bgn##_##h_end = \
            {mtr::enTPPR_getNmb_from_DTS, {DTS_16_03_27_T_(h_bgn, 0)}, {DTS_16_03_27_T_(h_end, 00)}}
    #define tb_answ_ppr(nmb) mtr::PACK_ANSW_PPR_NMB_RCRD tb_answ_ppr_##nmb = { nmb }
    #define tb_rd_ppr_NmbRcrd_h(h_bgn, h_end, nmb) { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_##h_bgn##_##h_end, sizeof(tb_in_ppr_##h_bgn##_##h_end)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {&tb_answ_ppr_##nmb, sizeof(tb_answ_ppr_##nmb)}, END_PLOAD } \
                        } \
                      }
#endif
// -- Read Nmb Records PPR
#if 1 
    #define tb_in_ppr_dhms(dB, hB, mB, sB,  dE, hE, mE, sE)     \
                mtr::PACK_GET_PWR_PRFL tb_in_ppr_dhms_##dB##_##hB##_##mB##_##sB##_T_##dE##_##hE##_##mE##_##sE = \
            {mtr::enTPPR_getNmb_from_DTS, {DTS_16_03_d_T_h_m_s(dB, hB, mB, sB)}, {DTS_16_03_d_T_h_m_s(dE, hE, mE, sE)} }
    #define tb_rd_ppr_NmbRcrd(dB, hB, mB, sB,  dE, hE, mE, sE,  nmb) { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_dhms_##dB##_##hB##_##mB##_##sB##_T_##dE##_##hE##_##mE##_##sE, \
                                                    sizeof(tb_in_ppr_dhms_##dB##_##hB##_##mB##_##sB##_T_##dE##_##hE##_##mE##_##sE)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {&tb_answ_ppr_##nmb, sizeof(tb_answ_ppr_##nmb)}, END_PLOAD } \
                        } \
                      }
    #define tb_rd_ppr_NmbRcrd_ERR(dB, hB, mB, sB,  dE, hE, mE, sE) { scnrNormal, \
                        { mtr::GET_PWR_PROFILE, { {&tb_in_ppr_dhms_##dB##_##hB##_##mB##_##sB##_T_##dE##_##hE##_##mE##_##sE, \
                                                    sizeof(tb_in_ppr_dhms_##dB##_##hB##_##mB##_##sB##_T_##dE##_##hE##_##mE##_##sE)}, END_PLOAD } }, \
                        { mtr::GET_PWR_PROFILE, \
                            { {&tb_answ_STS_ERR_PRM, sizeof(tb_answ_STS_ERR_PRM)}, END_PLOAD } \
                        } \
                      }
#endif
    #define _pin_WrValidPsw(tmt) mtr::PACK_CMD_MNG_PPR pin_WrValidPsw_##tmt = {PSW_DEF, mtr::enMPPR_wrDuration, tmt};
    #define pin_WrValidPsw(tmt) pin_WrValidPsw_##tmt
    #define tb_Wr_ValidPsw(tmt)  { \
                scnrNormal, \
                {mtr::CMD_MNG_PPR, {{&pin_WrValidPsw(tmt), sizeof(pin_WrValidPsw(tmt))}, END_PLOAD}}, \
                {mtr::CMD_MNG_PPR, {{&pout_STS_OK, sizeof(pout_STS_OK)}, END_PLOAD}}, \
                }
//-----------------------------------------------------------------------------
// -- SCENARIO READ Nmb PPR
//-----------------------------------------------------------------------------
    tb_answ_ppr(0); tb_answ_ppr(1); tb_answ_ppr(2); tb_answ_ppr(3); tb_answ_ppr(4);
    tb_answ_ppr(9); 
    
    tb_in_ppr(0, 4); tb_in_ppr(2, 2); tb_in_ppr(2, 3);
    
    tb_in_ppr_dhms(27,2,30,0,  28,0,0,0);
    tb_in_ppr_dhms(27,2,00,0,  27,2,30,0);
    TB_SCENARIO_ARCHIVE scenario_cmd_read_ppr_NmbRcrd_season_WtS_tb[] = {
        tb_act_work_min(3 * 60L),
        // -- Debug
        
        // -- ~Debug
        tb_rd_ppr_NmbRcrd_h(0, 4,  9), tb_rd_ppr_NmbRcrd_h(2, 3,  3), 
        tb_rd_ppr_NmbRcrd_h(2, 2,  1), 
        tb_rd_ppr_NmbRcrd(27,2,30,0,  28,0,0,0,  4), 
        tb_rd_ppr_NmbRcrd(27,2,30,0,  28,0,0,0,  4), 
        tb_rd_ppr_NmbRcrd(27,2,00,0,  27,2,30,0,  2), 
        { scnrEND }
    };
}