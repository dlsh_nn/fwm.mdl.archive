#include "../../tb_utils/tb_utils.h"

namespace nsCRPPR_SEASON_D60s{

//=============================================================================
// -- scenario cmd_read_ppr with Season
//=============================================================================
#if 1
    //// -- scenario COMMON
    #define _pin_WrValidPsw(tmt) mtr::PACK_CMD_MNG_PPR pin_WrValidPsw_##tmt = {PSW_DEF, mtr::enMPPR_wrDuration, tmt};
    #define pin_WrValidPsw(tmt) pin_WrValidPsw_##tmt
    #define tb_Wr_ValidPsw(tmt)  { \
                scnrNormal, \
                {mtr::CMD_MNG_PPR, {{&pin_WrValidPsw(tmt), sizeof(pin_WrValidPsw(tmt))}, END_PLOAD}}, \
                {mtr::CMD_MNG_PPR, {{&pout_STS_OK, sizeof(pout_STS_OK)}, END_PLOAD}}, \
                }

    
    uint8_t pout_STS_OK = {mtr::STS_OK};
    uint8_t pout_STS_NOT_FOUND = {mtr::STS_NOT_FOUND};
    uint8_t pout_STS_ERR_PRM = {mtr::STS_ERR_PRM};
    uint8_t pout_ANSW_BUSY = {mtr::ANSW_METER_BUSY};
    uint8_t pout_STS_ERR_PSW = {mtr::STS_ERR_PSW};
    
    // -- Action
#if 1
    #define tb_act_work_min(val) { \
            scnrWork, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
            ((val) * 60L), 1, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45} }, \
            {UseOtherPrm, TEMP_DEF} \
        }
#endif
#endif 
    #define DTS_16_d_m_TSSN_(d,mnth,h,m, fssn)  0,m,h, (5 | fssn), d,mnth,16
    #define DTS_16_d_m_TSSN_wd_(d,mnth,wd,h,m, fssn)  0,m,h, (wd | fssn), d,mnth,16
    #define DTS_16_9_15_TSSN_(h, m, fssn)       DTS_16_d_m_TSSN_(15,9, h,m,fssn)
    #define DTS_16_10_30_TSSN_(h, m, fssn)       DTS_16_d_m_TSSN_wd_(30,10,1, h,m,fssn)
    //=============================================================================
    // scenario_cmd_read_ppr_season_StW_D60s_tb
    //=============================================================================
#if 1 // -- Read PPR
    #define _in_vs4_ssn_(dB,mnthB,hB,mB,fssnB,  dE,mnthE,hE,mE,fssnE) \
        in_vs4_ssn_PPRvs4__##dB##_##mnthB##_##hB##_##mB##_##fssnB##__to__##dE##_##mnthE##_##hE##_##mE##_##fssnE
    #define in_vs4_ssn(dB,mnthB,hB,mB,fssnB,  dE,mnthE,hE,mE,fssnE) mtr::PACK_GET_PWR_PRFL \
        in_vs4_ssn_PPRvs4__##dB##_##mnthB##_##hB##_##mB##_##fssnB##__to__##dE##_##mnthE##_##hE##_##mE##_##fssnE = \
        {mtr::enTPPR_NoSkipSeason_PPR_vs4, {DTS_16_d_m_TSSN_(dB,mnthB,hB,mB, fssnB)}, {DTS_16_d_m_TSSN_(dE,mnthE,hE,mE, fssnE)}}
    
    mtr::PACK_ANSW_PWR_PRFL_VS4 answ_0hS_to_0h28S[29] = {
        { {DTS_16_10_30_TSSN_(0, 0,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(0, 1,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(0, 2,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(0, 3,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(0, 4,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(0, 5,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(0, 6,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(0, 7,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(0, 8,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(0, 9,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(0, 10,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(0, 11,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(0, 12,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(0, 13,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(0, 14,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(0, 15,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(0, 16,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(0, 17,  MSK_SUMMER_TIME)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
        { {DTS_16_10_30_TSSN_(0, 18,  MSK_SUMMER_TIME)}, {11, 0, 11, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid },
        { {DTS_16_10_30_TSSN_(0, 19,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 20,  MSK_SUMMER_TIME)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 21,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 22,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 23,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 24,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 25,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 26,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 27,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 28,  MSK_SUMMER_TIME)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
    };
    in_vs4_ssn(30,10,0,0,MSK_SUMMER_TIME,  30,10,0,28,MSK_SUMMER_TIME);
    #define rd_0hS_to_0h29S { scnrNormal, \
        { mtr::GET_PWR_PROFILE, { {&_in_vs4_ssn_(30,10,0,0,MSK_SUMMER_TIME,  30,10,0,28,MSK_SUMMER_TIME), \
            sizeof(mtr::PACK_GET_PWR_PRFL)}, END_PLOAD } }, \
        { mtr::GET_PWR_PROFILE, { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
            {answ_0hS_to_0h28S, sizeof(answ_0hS_to_0h28S)}, END_PLOAD } } \
    }

    mtr::PACK_ANSW_PWR_PRFL_VS4 answ_0h29S_to_0h57S[29] = {
        { {DTS_16_10_30_TSSN_(0, 29,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 30,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 31,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 32,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 33,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 34,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 35,  MSK_SUMMER_TIME)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 36,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 37,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 38,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 39,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 40,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 41,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 42,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 43,  MSK_SUMMER_TIME)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 44,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 45,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 46,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 47,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 48,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 49,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 50,  MSK_SUMMER_TIME)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 51,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 52,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 53,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 54,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 55,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 56,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 57,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
    };
    in_vs4_ssn(30,10,0,29,MSK_SUMMER_TIME,  30,10,0,57,MSK_SUMMER_TIME);
    #define rd_0h29S_to_0h57S { scnrNormal, \
        { mtr::GET_PWR_PROFILE, { {&_in_vs4_ssn_(30,10,0,29,MSK_SUMMER_TIME,  30,10,0,57,MSK_SUMMER_TIME), \
            sizeof(mtr::PACK_GET_PWR_PRFL)}, END_PLOAD } }, \
        { mtr::GET_PWR_PROFILE, { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
            {answ_0h29S_to_0h57S, sizeof(answ_0h29S_to_0h57S)}, END_PLOAD } } \
    }

    mtr::PACK_ANSW_PWR_PRFL_VS4 answ_0h58S_to_1h26S[29] = {
        { {DTS_16_10_30_TSSN_(0, 58,  MSK_SUMMER_TIME)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(0, 59,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 0,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 1,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 2,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 3,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 4,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 5,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 6,  MSK_SUMMER_TIME)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 7,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 8,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 9,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 10,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 11,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 12,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 13,  MSK_SUMMER_TIME)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 14,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 15,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 16,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 17,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 18,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 19,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 20,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 21,  MSK_SUMMER_TIME)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 22,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 23,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 24,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 25,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 26,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
    };
    in_vs4_ssn(30,10,0,58,MSK_SUMMER_TIME,  30,10,1,26,MSK_SUMMER_TIME);
    #define rd_0h58S_to_1h26S { scnrNormal, \
        { mtr::GET_PWR_PROFILE, { {&_in_vs4_ssn_(30,10,0,58,MSK_SUMMER_TIME,  30,10,1,26,MSK_SUMMER_TIME), \
            sizeof(mtr::PACK_GET_PWR_PRFL)}, END_PLOAD } }, \
        { mtr::GET_PWR_PROFILE, { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
            {answ_0h58S_to_1h26S, sizeof(answ_0h58S_to_1h26S)}, END_PLOAD } } \
    }

    mtr::PACK_ANSW_PWR_PRFL_VS4 answ_1h27S_to_1h55S[29] = {
        { {DTS_16_10_30_TSSN_(1, 27,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 28,  MSK_SUMMER_TIME)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 29,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 30,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 31,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 32,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 33,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 34,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 35,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 36,  MSK_SUMMER_TIME)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 37,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 38,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 39,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 40,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 41,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 42,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 43,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 44,  MSK_SUMMER_TIME)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 45,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 46,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 47,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 48,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 49,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 50,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 51,  MSK_SUMMER_TIME)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 52,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 53,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 54,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 55,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
    };
    in_vs4_ssn(30,10,1,27,MSK_SUMMER_TIME,  30,10,1,55,MSK_SUMMER_TIME);
    #define rd_1h27S_to_1h55S { scnrNormal, \
        { mtr::GET_PWR_PROFILE, { {&_in_vs4_ssn_(30,10,1,27,MSK_SUMMER_TIME,  30,10,1,55,MSK_SUMMER_TIME), \
            sizeof(mtr::PACK_GET_PWR_PRFL)}, END_PLOAD } }, \
        { mtr::GET_PWR_PROFILE, { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
            {answ_1h27S_to_1h55S, sizeof(answ_1h27S_to_1h55S)}, END_PLOAD } } \
    }

    mtr::PACK_ANSW_PWR_PRFL_VS4 answ_1h56S_to_2h24S[29] = {
        { {DTS_16_10_30_TSSN_(1, 56,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 57,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 58,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(1, 59,  MSK_SUMMER_TIME)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 0,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 1,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 2,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 3,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 4,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 5,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 6,  MSK_SUMMER_TIME)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 7,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 8,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 9,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 10,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 11,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 12,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 13,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 14,  MSK_SUMMER_TIME)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 15,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 16,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 17,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 18,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 19,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 20,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 21,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 22,  MSK_SUMMER_TIME)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 23,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 24,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
    };
    in_vs4_ssn(30,10,1,56,MSK_SUMMER_TIME,  30,10,2,24,MSK_SUMMER_TIME);
    #define rd_1h56S_to_2h24S { scnrNormal, \
        { mtr::GET_PWR_PROFILE, { {&_in_vs4_ssn_(30,10,1,56,MSK_SUMMER_TIME,  30,10,2,24,MSK_SUMMER_TIME), \
            sizeof(mtr::PACK_GET_PWR_PRFL)}, END_PLOAD } }, \
        { mtr::GET_PWR_PROFILE, { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
            {answ_1h56S_to_2h24S, sizeof(answ_1h56S_to_2h24S)}, END_PLOAD } } \
    }

    mtr::PACK_ANSW_PWR_PRFL_VS4 answ_2h25S_to_2h53S[29] = {
        { {DTS_16_10_30_TSSN_(2, 25,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 26,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 27,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 28,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 29,  MSK_SUMMER_TIME)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 30,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 31,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 32,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 33,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 34,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 35,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 36,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 37,  MSK_SUMMER_TIME)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 38,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 39,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 40,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 41,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 42,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 43,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 44,  MSK_SUMMER_TIME)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 45,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 46,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 47,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 48,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 49,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 50,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 51,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 52,  MSK_SUMMER_TIME)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 53,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
    };
    in_vs4_ssn(30,10,2,25,MSK_SUMMER_TIME,  30,10,2,53,MSK_SUMMER_TIME);
    #define rd_2h25S_to_2h53S { scnrNormal, \
        { mtr::GET_PWR_PROFILE, { {&_in_vs4_ssn_(30,10,2,25,MSK_SUMMER_TIME,  30,10,2,53,MSK_SUMMER_TIME), \
            sizeof(mtr::PACK_GET_PWR_PRFL)}, END_PLOAD } }, \
        { mtr::GET_PWR_PROFILE, { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
            {answ_2h25S_to_2h53S, sizeof(answ_2h25S_to_2h53S)}, END_PLOAD } } \
    }

    mtr::PACK_ANSW_PWR_PRFL_VS4 answ_2h54S_to_2h22W[29] = {
        { {DTS_16_10_30_TSSN_(2, 54,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 55,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 56,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 57,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 58,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 59,  MSK_SUMMER_TIME)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 0,  0)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 1,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 2,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 3,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 4,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 5,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 6,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 7,  0)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 8,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 9,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 10,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 11,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 12,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 13,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 14,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 15,  0)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 16,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 17,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 18,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 19,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 20,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 21,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 22,  0)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
    };
    in_vs4_ssn(30,10,2,54,MSK_SUMMER_TIME,  30,10,2,22,0);
    //=========================================================================
    // Summer to Winter
    //=========================================================================
    #define rd_2h54S_to_2h22W { scnrNormal, \
        { mtr::GET_PWR_PROFILE, { {&_in_vs4_ssn_(30,10,2,54,MSK_SUMMER_TIME,  30,10,2,22,0), \
            sizeof(mtr::PACK_GET_PWR_PRFL)}, END_PLOAD } }, \
        { mtr::GET_PWR_PROFILE, { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
            {answ_2h54S_to_2h22W, sizeof(answ_2h54S_to_2h22W)}, END_PLOAD } } \
    }

    mtr::PACK_ANSW_PWR_PRFL_VS4 answ_2h23W_to_2h51W[29] = {
        { {DTS_16_10_30_TSSN_(2, 23,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 24,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 25,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 26,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 27,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 28,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 29,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 30,  0)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 31,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 32,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 33,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 34,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 35,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 36,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 37,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 38,  0)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 39,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 40,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 41,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 42,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 43,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 44,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 45,  0)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 46,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 47,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 48,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 49,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 50,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 51,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
    };
    in_vs4_ssn(30,10,2,23,0,  30,10,2,51,0);
    #define rd_2h23W_to_2h51W { scnrNormal, \
        { mtr::GET_PWR_PROFILE, { {&_in_vs4_ssn_(30,10,2,23,0,  30,10,2,51,0), \
            sizeof(mtr::PACK_GET_PWR_PRFL)}, END_PLOAD } }, \
        { mtr::GET_PWR_PROFILE, { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
            {answ_2h23W_to_2h51W, sizeof(answ_2h23W_to_2h51W)}, END_PLOAD } } \
    }

mtr::PACK_ANSW_PWR_PRFL_VS4 answ_2h52W_to_3h20W[29] = {
        { {DTS_16_10_30_TSSN_(2, 52,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 53,  0)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 54,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 55,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 56,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 57,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 58,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(2, 59,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 0,  0)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 1,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 2,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 3,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 4,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 5,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 6,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 7,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 8,  0)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 9,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 10,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 11,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 12,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 13,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 14,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 15,  0)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 16,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 17,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 18,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 19,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 20,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
    };
    in_vs4_ssn(30,10,2,52,0,  30,10,3,20,0);
    #define rd_2h52W_to_3h20W { scnrNormal, \
        { mtr::GET_PWR_PROFILE, { {&_in_vs4_ssn_(30,10,2,52,0,  30,10,3,20,0), \
            sizeof(mtr::PACK_GET_PWR_PRFL)}, END_PLOAD } }, \
        { mtr::GET_PWR_PROFILE, { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
            {answ_2h52W_to_3h20W, sizeof(answ_2h52W_to_3h20W)}, END_PLOAD } } \
    }

mtr::PACK_ANSW_PWR_PRFL_VS4 answ_3h21W_to_3h49W[29] = {
        { {DTS_16_10_30_TSSN_(3, 21,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 22,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 23,  0)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 24,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 25,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 26,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 27,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 28,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 29,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 30,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 31,  0)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 32,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 33,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 34,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 35,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 36,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 37,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 38,  0)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 39,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 40,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 41,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 42,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 43,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 44,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 45,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 46,  0)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 47,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 48,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 49,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
    };
    in_vs4_ssn(30,10,3,21,0,  30,10,3,49,0);
    #define rd_3h21W_to_3h49W { scnrNormal, \
        { mtr::GET_PWR_PROFILE, { {&_in_vs4_ssn_(30,10,3,21,0,  30,10,3,49,0), \
            sizeof(mtr::PACK_GET_PWR_PRFL)}, END_PLOAD } }, \
        { mtr::GET_PWR_PROFILE, { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
            {answ_3h21W_to_3h49W, sizeof(answ_3h21W_to_3h49W)}, END_PLOAD } } \
    }

mtr::PACK_ANSW_PWR_PRFL_VS4 answ_3h50W_to_4h18W[29] = {
        { {DTS_16_10_30_TSSN_(3, 50,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 51,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 52,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 53,  0)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 54,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 55,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 56,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 57,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 58,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(3, 59,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(4, 0,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(4, 1,  0)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(4, 2,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(4, 3,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(4, 4,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(4, 5,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(4, 6,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(4, 7,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(4, 8,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(4, 9,  0)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(4, 10,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(4, 11,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(4, 12,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(4, 13,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(4, 14,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(4, 15,  0)}, {8, 0, 8, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(4, 16,  0)}, {9, 0, 9, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflValid  },
        { {DTS_16_10_30_TSSN_(4, 17,  0)}, {4, 0, 4, 0}, { {Uph_DEF}, TEMP_DEF / 100 }, mtr::ePrflCurrent  },
        { {DTS_16_10_30_TSSN_(4, 18,  0)}, {-1, -1, -1, -1}, { {-1, -1, -1}, -1 }, mtr::ePrflNoValid_NoFinde },
    };
    in_vs4_ssn(30,10,3,50,0,  30,10,4,18,0);
    #define rd_3h50W_to_4h18W { scnrNormal, \
        { mtr::GET_PWR_PROFILE, { {&_in_vs4_ssn_(30,10,3,50,0,  30,10,4,18,0), \
            sizeof(mtr::PACK_GET_PWR_PRFL)}, END_PLOAD } }, \
        { mtr::GET_PWR_PROFILE, { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
            {answ_3h50W_to_4h18W, sizeof(answ_3h50W_to_4h18W)}, END_PLOAD } } \
    }

#endif
//-----------------------------------------------------------------------------
// -- SCENARIO READ_PPR SEASON Duration = 60 sec
//-----------------------------------------------------------------------------
    _pin_WrValidPsw(60);
    
    TB_SCENARIO_ARCHIVE scenario_cmd_read_ppr_season_StW_D60s_tb[] = {
        // -- action
        tb_Wr_ValidPsw(60), tb_act_work_min(5 * 60),
        // -- Debug
        
        // -- ~Debug
        rd_0hS_to_0h29S, rd_0h29S_to_0h57S, rd_0h58S_to_1h26S,
        rd_0h58S_to_1h26S, rd_1h27S_to_1h55S, rd_1h56S_to_2h24S,
        rd_2h25S_to_2h53S,
        // --
        rd_2h54S_to_2h22W, rd_2h23W_to_2h51W, rd_2h52W_to_3h20W,
        rd_3h21W_to_3h49W, rd_3h50W_to_4h18W, 
        // --
        { scnrEND }
    };
}