/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   test_CmdReadPPR.cpp
 * Author: User
 *
 * Created on 24.07.2017, 14:26:49
 */

#include "test_CmdReadPPR.h"


CPPUNIT_TEST_SUITE_REGISTRATION(test_CmdReadPPR);

void test_CmdReadPPR::setUp() {
    tbu.Init();
    setUp(tmt_DTS_TB_begin);
}
void test_CmdReadPPR::setUp(time_t tmtBgn) {
    // -- Init Time
    mtr::rtc_tbl.crnt_tmt = tmtBgn;
    mtr::GetDts_tmt(mtr::rtc_tbl.crnt_tmt, &mtr::rtc_tbl.crnt_dts);
    mtr::arch_var.fUpdateRTC = true;
    // -- Init Archive
    mtr::InitArch();
    mtr::ers_at25_all();
    mtr::reset_arch();
    // -- Security
    mtr::ClrPsw();
}

void test_CmdReadPPR::tearDown() {
}

void test_CmdReadPPR::test_cmd_read_ppr_AllType() {
    bool fError = false;
    
    if(fError |= !tbu.TestScenario(nsCRPPR::scenario_cmd_read_ppr_type_tb, "scenario_cmd_read_ppr_type_tb")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
void test_CmdReadPPR::test_cmd_read_ppr_variousAlignDTS() {
    bool fError = false;
    
    if(fError |= !tbu.TestScenario(nsCRPPR::scenario_cmd_read_ppr_VariousAlignDTS_tb, "scenario_cmd_read_ppr_VariousAlignDTS_tb")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
void test_CmdReadPPR::test_cmd_read_ppr_PPRv2() {
    bool fError = false;
    
    if(fError |= !tbu.TestScenario(nsCRPPR::scenario_cmd_read_ppr_PPRv2_tb, "scenario_cmd_read_ppr_PPRv2_tb")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
void test_CmdReadPPR::test_cmd_read_ppr_PPRvs04() {
    bool fError = false;
    
    if(fError |= !tbu.TestScenario(nsCRPPR::scenario_cmd_read_ppr_PPRvs04_tb, "scenario_cmd_read_ppr_PPRvs04_tb")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
void test_CmdReadPPR::test_cmd_read_ppr_PPRvs04_ByIndex() {
    bool fError = false;
    
    if(fError |= !tbu.TestScenario(nsCRPPR::scenario_cmd_read_ppr_PPRvs04_ByIndex_tb, "scenario_cmd_read_ppr_PPRvs04_ByIndex_tb")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
void test_CmdReadPPR::test_cmd_read_ppr_NmbRcrd() {
    bool fError = false;
    
    if(fError |= !tbu.TestScenario(nsCRPPR_NMB_RCRD::scenario_cmd_read_ppr_NmbRcrd_tb, "scenario_cmd_read_ppr_NmbRcrd_tb")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
void test_CmdReadPPR::test_cmd_read_ppr_NmbRcrd_season_WtS() {
    bool fError = false;
    
    mtr::rtc_tbl.rtc_save.fSwitchSummer = true;
    setUp(tmt_DTS_LastSunMarch);
    if(fError |= !tbu.TestScenario(nsCRPPR_NMB_RCRD_season_WtS::scenario_cmd_read_ppr_NmbRcrd_season_WtS_tb, "scenario_cmd_read_ppr_NmbRcrd_season_WtS_tb")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
void test_CmdReadPPR::test_cmd_read_ppr_NmbRcrd_season_StW() {
    bool fError = false;
    
    mtr::rtc_tbl.rtc_save.fSwitchSummer = true;
    mtr::rtc_tbl.rtc_save.fSummerTime = true;
    setUp(tmt_DTS_LastSunOctober);
    // --
    if(fError |= !tbu.TestScenario(nsCRPPR_NMB_RCRD_season_StW::scenario_cmd_read_ppr_NmbRcrd_season_StW_tb, 
            "scenario_cmd_read_ppr_NmbRcrd_season_StW_tb")) goto __exit;

__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
void test_CmdReadPPR::test_cmd_read_ppr_NmbRcrd_season_StW_D60s() {
    bool fError = false;
    
    mtr::rtc_tbl.rtc_save.fSwitchSummer = true;
    mtr::rtc_tbl.rtc_save.fSummerTime = true;
    setUp(tmt_DTS_LastSunOctober);
    // --
    if(fError |= !tbu.TestScenario(nsCRPPR_NMB_RCRD_season_StW::scenario_cmd_read_ppr_NmbRcrd_season_StW_D60s_tb, 
            "scenario_cmd_read_ppr_NmbRcrd_season_StW_D60s_tb")) goto __exit;
    
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
void test_CmdReadPPR::test_cmd_read_ppr_PPRvs04_season() {
    bool fError = false;
    
    mtr::rtc_tbl.rtc_save.fSwitchSummer = false;
    if(fError |= !tbu.TestScenario(nsCRPPR_SEASON::scenario_cmd_read_ppr_season_tb, "scenario_cmd_read_ppr_season_tb")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
void test_CmdReadPPR::test_cmd_read_ppr_PPRvs04_season_WtS() {
    bool fError = false;
    
    mtr::rtc_tbl.rtc_save.fSwitchSummer = true;
    setUp(tmt_DTS_LastSunMarch);
    
    if(fError |= !tbu.TestScenario(nsCRPPR_SEASON::scenario_cmd_read_ppr_season_WtS_tb, "scenario_cmd_read_ppr_season_WtS_tb")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
void test_CmdReadPPR::test_cmd_read_ppr_PPRvs04_season_StW() {
    bool fError = false;
    
    mtr::rtc_tbl.rtc_save.fSwitchSummer = true;
    mtr::rtc_tbl.rtc_save.fSummerTime = true;
    setUp(tmt_DTS_LastSunOctober);
    
    if(fError |= !tbu.TestScenario(nsCRPPR_SEASON::scenario_cmd_read_ppr_season_StW_tb, "scenario_cmd_read_ppr_season_StW_tb")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
void test_CmdReadPPR::test_cmd_read_ppr_PPRvs04_season_StW_D60s() {
    bool fError = false;
    
    mtr::rtc_tbl.rtc_save.fSwitchSummer = true;
    mtr::rtc_tbl.rtc_save.fSummerTime = true;
    setUp(tmt_DTS_LastSunOctober);
    
    if(fError |= !tbu.TestScenario(nsCRPPR_SEASON_D60s::scenario_cmd_read_ppr_season_StW_D60s_tb, "scenario_cmd_read_ppr_season_StW_D60s_tb")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
void test_CmdReadPPR::test_cmd_read_ppr_NmbRcrds_Total() {
    bool fError = false;
    
    if(fError |= !tbu.TestScenario(nsCRPPR_NMB_RCRD::scenario_cmd_read_ppr_NmbRcrd_Total_tb, "scenario_cmd_read_ppr_NmbRcrd_Total_tb")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
