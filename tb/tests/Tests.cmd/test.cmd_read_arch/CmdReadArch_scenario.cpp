#include "../../tb_utils/tb_utils.h"

namespace nsCRA{

//=============================================================================
// -- scenario cmd_read_arch mskType=enType_ArchGetNmb(0x42)
//=============================================================================
#if 1
    //// -- scenario COMMON
    uint8_t pout_STS_OK = {mtr::STS_OK};
    uint8_t pout_STS_NOT_FOUND = {mtr::STS_NOT_FOUND};
    uint8_t pout_STS_ERR_PRM = {mtr::STS_ERR_PRM};
    uint8_t pout_ANSW_BUSY = {mtr::ANSW_METER_BUSY};
    uint8_t pout_STS_ERR_PSW = {mtr::STS_ERR_PSW};
    
    // -- Action
#if 1 /* scenario cmd_read_ppr mskType=enTPPR_getNmb_from_DTS(0x42) */
    #define tb_act_work_day(val) { \
            scnrWork, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
            ((val) * 24 * 60 * 60L), 15 * 60L, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45} }, \
            {UseOtherPrm, TEMP_DEF} \
        }
    #define tb_act_remove_day(NmbDay) { scnrMakeRemove, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                                        ((NmbDay) * 24 * 60 * 60L), 20 * 60 }
    #define tb_act_PwrOn_min(val) { scnrAction_On_Mtr, {0}, {0}, ((val) * 60L) }
    #define tb_act_PwrOn_day(val) { scnrAction_On_Mtr, {0}, {0}, ((val) * 24 * 60 * 60L) }
#endif
#endif 
    //=========================================================================
    // -- READ ARCH NmbRcrd Total
    //=========================================================================
    #define DS_y_m_d_T(y, m, d)        d, m, y
#if 1   // -- Read ARCH mskTYPE=enType_ArchGetNmb(0x40)
    mtr::PACK_GET_NMB_ARCH tb_in_arch_DAY_Total = {mtr::enType_ArchGetNmb, mtr::eARCH_DAY};
    mtr::PACK_GET_NMB_ARCH tb_in_arch_MONTH_Total = {mtr::enType_ArchGetNmb, mtr::eARCH_MNTH};
    #define tb_answ_NTotal(nmb) mtr::PACK_ANSW_PPR_NMB_RCRD tb_answ_NTotal_##nmb = { nmb }
    #define rd_NmbRcrd_DAY_Total(nmb) { scnrNormal, \
                        { mtr::GET_ARCH_MTR, { {&tb_in_arch_DAY_Total, sizeof(tb_in_arch_DAY_Total)}, END_PLOAD } }, \
                        { mtr::GET_ARCH_MTR, { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                                                {&tb_answ_NTotal_##nmb, sizeof(tb_answ_NTotal_##nmb)}, END_PLOAD } \
                        } \
                      }
    #define rd_NmbRcrd_MONTH_Total(nmb) { scnrNormal, \
                        { mtr::GET_ARCH_MTR, { {&tb_in_arch_MONTH_Total, sizeof(tb_in_arch_MONTH_Total)}, END_PLOAD } }, \
                        { mtr::GET_ARCH_MTR, {  {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                                                {&tb_answ_NTotal_##nmb, sizeof(tb_answ_NTotal_##nmb)}, END_PLOAD } \
                        } \
                      }
#endif
    //=========================================================================
    // -- READ ARCH scenario COMMON defined
    //=========================================================================
    #define DS_y_m_d_T(y, m, d)        d, m, y
    #define M1  -1    
    #define MTrf_ALL      0xFF
    //=========================================================================
    // -- READ ARCH scenario_rdArchDAY_ByIndex_tb
    //=========================================================================
#if 1   // -- Read ARCH mskTYPE=enType_Pass_by_INDX(0x46)
    #define tb_in_aday(iBgn, iEnd)   mtr::PACK_GET_ARCH_by_INDX tb_in_aday_##iBgn##_to_##iEnd = {mtr::enType_Pass_by_INDX, mtr::eARCH_DAY, MTrf_ALL, iBgn, iEnd};

    mtr::PACK_ANSW_ARCH_HEAD tb_answ_aday_head = {mtr::eARCH_DAY, MTrf_ALL};
    #define rdADAY_ByIndex(iBgn, iEnd, comment)  { scnrNormal, \
            { mtr::GET_ARCH_MTR, { {&tb_in_aday_##iBgn##_to_##iEnd, sizeof(mtr::PACK_GET_ARCH_by_INDX)}, END_PLOAD } }, \
            { mtr::GET_ARCH_MTR, \
                { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                  {&tb_answ_aday_head, sizeof(tb_answ_aday_head)}, \
                  {tb_answ_aday_##iBgn##_to_##iEnd##_##comment, sizeof(tb_answ_aday_##iBgn##_to_##iEnd##_##comment)}, END_PLOAD } \
            } \
          }
    #define rdADAY_ByIndex_NameAnsw(iBgn, iEnd, NameAnsw)  { scnrNormal, \
            { mtr::GET_ARCH_MTR, { {&tb_in_aday_##iBgn##_to_##iEnd, sizeof(mtr::PACK_GET_ARCH_by_INDX)}, END_PLOAD } }, \
            { mtr::GET_ARCH_MTR, \
                { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                  {&tb_answ_aday_head, sizeof(tb_answ_aday_head)}, \
                  {NameAnsw, sizeof(NameAnsw)}, END_PLOAD } \
            } \
          }
    #define rdADAY_ByIndexDAY_ERR(iBgn, iEnd)  { scnrNormal, \
            { mtr::GET_ARCH_MTR, { {&tb_in_aday_##iBgn##_to_##iEnd, sizeof(mtr::PACK_GET_ARCH_by_INDX)}, END_PLOAD } }, \
            { mtr::GET_ARCH_MTR, \
                { {&pout_STS_ERR_PRM, sizeof(pout_STS_ERR_PRM)}, END_PLOAD } \
            } \
          }
    mtr::TB_RCRD_ANSW_ARCH_ALLT tb_answ_aday_0_to_0_DT15_9_16[1] = {
        { {15, 9, 16}, RCRD_ARCH_ZERO_ENRG_ALLT },
    };
    mtr::TB_RCRD_ANSW_ARCH_ALLT tb_answ_aday_0_to_2_DT24_12_16[3] = {
        { {24, 12, 16}, RCRD_ARCH_ENRG_ALLT(1166649, 1166649) },
        { {23, 12, 16}, RCRD_ARCH_ENRG_ALLT(1154939, 1154939) },
        { {22, 12, 16}, RCRD_ARCH_ENRG_ALLT(1143230, 1143230) },
    };
    mtr::TB_RCRD_ANSW_ARCH_ALLT tb_answ_aday_2_to_0_DT24_12_16[3] = {
        { {22, 12, 16}, RCRD_ARCH_ENRG_ALLT(1143230, 1143230) },
        { {23, 12, 16}, RCRD_ARCH_ENRG_ALLT(1154939, 1154939) },
        { {24, 12, 16}, RCRD_ARCH_ENRG_ALLT(1166649, 1166649) },
    };
    mtr::TB_RCRD_ANSW_ARCH_ALLT tb_answ_aday_M1_to_98_DT24_12_16[3] = {
        { {15, 9, 16}, RCRD_ARCH_ZERO_ENRG_ALLT },
        { {16, 9, 16}, RCRD_ARCH_ENRG_ALLT(7394, 7394) },
        { {17, 9, 16}, RCRD_ARCH_ENRG_ALLT(19103, 19103) },
    };
    mtr::TB_RCRD_ANSW_ARCH_ALLT tb_answ_aday_98_to_M1_DT24_12_16[3] = {
        { {17, 9, 16}, RCRD_ARCH_ENRG_ALLT(19103, 19103) },
        { {16, 9, 16}, RCRD_ARCH_ENRG_ALLT(7394, 7394) },
        { {15, 9, 16}, RCRD_ARCH_ZERO_ENRG_ALLT },
    };
    mtr::TB_RCRD_ANSW_ARCH_ALLT tb_answ_aday_0_to_2_DT25_12_16[3] = {
        { {25, 12, 16}, RCRD_ARCH_ENRG_ALLT(1178115, 1178115) },
        { {24, 12, 16}, RCRD_ARCH_ENRG_ALLT(1166649, 1166649) },
        { {23, 12, 16}, RCRD_ARCH_ENRG_ALLT(1154939, 1154939) },
    };
    mtr::TB_RCRD_ANSW_ARCH_ALLT tb_answ_aday_0_to_2_DT28_12_16[3] = {
        { {28, 12, 16}, RCRD_ARCH_ENRG_ALLT(1182674, 1182674) },
        { {25, 12, 16}, RCRD_ARCH_ENRG_ALLT(1178115, 1178115) },
        { {24, 12, 16}, RCRD_ARCH_ENRG_ALLT(1166649, 1166649) },
    };
    mtr::TB_RCRD_ANSW_ARCH_ALLT tb_answ_aday_10_to_12_DT28_12_16[3] = {
        { {16, 12, 16}, RCRD_ARCH_ENRG_ALLT(1072972, 1072972) },
        { {15, 12, 16}, RCRD_ARCH_ENRG_ALLT(1061262, 1061262) },
        { {14, 12, 16}, RCRD_ARCH_ENRG_ALLT(1049552, 1049552) },
    };
    mtr::TB_RCRD_ANSW_ARCH_ALLT tb_answ_aday_0_to_3_DT15_12_16_remove[4] = {
        { {16, 12, 16}, RCRD_ARCH_ENRG_ALLT(1189824, 1189824) },
        { {15, 12, 16}, RCRD_ARCH_ENRG_ALLT(1061262, 1061262) },
        { {14, 12, 16}, RCRD_ARCH_ENRG_ALLT(1049552, 1049552) },
        { {13, 12, 16}, RCRD_ARCH_ENRG_ALLT(1037843, 1037843) },
    };
    mtr::TB_RCRD_ANSW_ARCH_ALLT tb_answ_aday_0_to_0_DT14_9_16_remove[1] = {
        { {14, 9, 16}, RCRD_ARCH_ENRG_ALLT(1194384, 1194384) },
    };
#endif
    //=========================================================================
    // -- READ ARCH scenario_rdArchMONTH_ByIndex_tb
    //=========================================================================
#if 1   // -- Read ARCH mskTYPE=enType_Pass_by_INDX(0x46)
    #define tb_in_amonth(iBgn, iEnd) mtr::PACK_GET_ARCH_by_INDX tb_in_amonth_##iBgn##_to_##iEnd = {mtr::enType_Pass_by_INDX, mtr::eARCH_MNTH, MTrf_ALL, iBgn, iEnd};

    mtr::PACK_ANSW_ARCH_HEAD tb_answ_amonth_head = {mtr::eARCH_MNTH, MTrf_ALL};
    #define rdAMONTH_ByIndex(iBgn, iEnd, comment)  { scnrNormal, \
            { mtr::GET_ARCH_MTR, { {&tb_in_amonth_##iBgn##_to_##iEnd, sizeof(mtr::PACK_GET_ARCH_by_INDX)}, END_PLOAD } }, \
            { mtr::GET_ARCH_MTR, \
                { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                  {&tb_answ_amonth_head, sizeof(tb_answ_amonth_head)}, \
                  {tb_answ_amonth_##iBgn##_to_##iEnd##_##comment, sizeof(tb_answ_amonth_##iBgn##_to_##iEnd##_##comment)}, END_PLOAD } \
            } \
          }
    #define rdAMONTH_ByIndex_NameAnsw(iBgn, iEnd, NameAnsw)  { scnrNormal, \
            { mtr::GET_ARCH_MTR, { {&tb_in_amonth_##iBgn##_to_##iEnd, sizeof(mtr::PACK_GET_ARCH_by_INDX)}, END_PLOAD } }, \
            { mtr::GET_ARCH_MTR, \
                { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                  {&tb_answ_amonth_head, sizeof(tb_answ_amonth_head)}, \
                  {NameAnsw, sizeof(NameAnsw)}, END_PLOAD } \
            } \
          }
    #define rdAMONTH_ByIndex_ERR(iBgn, iEnd)  { scnrNormal, \
            { mtr::GET_ARCH_MTR, { {&tb_in_amonth_##iBgn##_to_##iEnd, sizeof(mtr::PACK_GET_ARCH_by_INDX)}, END_PLOAD } }, \
            { mtr::GET_ARCH_MTR, \
                { {&pout_STS_ERR_PRM, sizeof(pout_STS_ERR_PRM)}, END_PLOAD } \
            } \
          }
    mtr::TB_RCRD_ANSW_ARCH_ALLT tb_answ_amonth_0_to_0_DT1_9_16[1] = {
        { {1, 9, 16}, RCRD_ARCH_ZERO_ENRG_ALLT },
    };
    
    mtr::TB_RCRD_ANSW_ARCH_ALLT tb_answ_amonth_0_to_2_DT24_12_16[3] = {
        { {1, 12, 16}, RCRD_ARCH_ENRG_ALLT(897327, 897327) },
        { {1, 11, 16}, RCRD_ARCH_ENRG_ALLT(546037, 546037) },
        { {1, 10, 16}, RCRD_ARCH_ENRG_ALLT(183038, 183038) },
    };
    mtr::TB_RCRD_ANSW_ARCH_ALLT tb_answ_amonth_2_to_0_DT24_12_16[3] = {
        { {1, 10, 16}, RCRD_ARCH_ENRG_ALLT(183038, 183038) },
        { {1, 11, 16}, RCRD_ARCH_ENRG_ALLT(546037, 546037) },
        { {1, 12, 16}, RCRD_ARCH_ENRG_ALLT(897327, 897327) },
    };
    mtr::TB_RCRD_ANSW_ARCH_ALLT tb_answ_amonth_M1_to_1_DT24_12_16[3] = {
        { {1, 9, 16}, RCRD_ARCH_ZERO_ENRG_ALLT },
        { {1, 10, 16}, RCRD_ARCH_ENRG_ALLT(183038, 183038) },
        { {1, 11, 16}, RCRD_ARCH_ENRG_ALLT(546037, 546037) },
    };
    mtr::TB_RCRD_ANSW_ARCH_ALLT tb_answ_amonth_1_to_M1_DT24_12_16[3] = {
        { {1, 11, 16}, RCRD_ARCH_ENRG_ALLT(546037, 546037) },
        { {1, 10, 16}, RCRD_ARCH_ENRG_ALLT(183038, 183038) },
        { {1, 9, 16}, RCRD_ARCH_ZERO_ENRG_ALLT },
    };
    mtr::TB_RCRD_ANSW_ARCH_ALLT tb_answ_amonth_0_to_2_DT24_01_17[3] = {
        { {1, 01, 17}, RCRD_ARCH_ENRG_ALLT(1260082, 1260082) },
        { {1, 12, 16}, RCRD_ARCH_ENRG_ALLT(897327, 897327) },
        { {1, 11, 16}, RCRD_ARCH_ENRG_ALLT(546037, 546037) },
    };
    mtr::TB_RCRD_ANSW_ARCH_ALLT tb_answ_amonth_0_to_2_DT27_03_17[3] = {
        { {1, 03, 17}, RCRD_ARCH_ENRG_ALLT(1533963, 1533963) },
        { {1, 01, 17}, RCRD_ARCH_ENRG_ALLT(1260082, 1260082) },
        { {1, 12, 16}, RCRD_ARCH_ENRG_ALLT(897327, 897327) },
    };
    mtr::TB_RCRD_ANSW_ARCH_ALLT tb_answ_amonth_0_to_3_DT24_02_17_remove[4] = {
        { {1, 02, 17}, RCRD_ARCH_ENRG_ALLT(1569092, 1569092) },
        { {1, 01, 17}, RCRD_ARCH_ENRG_ALLT(1260082, 1260082) },
        { {1, 12, 16}, RCRD_ARCH_ENRG_ALLT(897327, 897327) },
        { {1, 11, 16}, RCRD_ARCH_ENRG_ALLT(546037, 546037) },
    };
    
#endif
//-----------------------------------------------------------------------------
// -- SCENARIO READ Arch DAY
//-----------------------------------------------------------------------------
    tb_in_aday(0, 0); tb_in_aday(0, 1); tb_in_aday(0, M1); tb_in_aday(M1, 0); tb_in_aday(M1, M1); 
    tb_in_aday(0, 2); tb_in_aday(0, 3); tb_in_aday(2, 0); tb_in_aday(10, 12);
    tb_in_aday(M1, 98); tb_in_aday(98, M1);
    tb_in_aday(0, 93); tb_in_aday(93, 0); tb_in_aday(93, M1); tb_in_aday(M1, 93);
    
    tb_answ_NTotal(1); tb_answ_NTotal(93);
    tb_answ_NTotal(101); tb_answ_NTotal(126); tb_answ_NTotal(37);
    
    TB_SCENARIO_ARCHIVE scenario_rdArchDAY_ByIndex_tb[] = {
        
        rd_NmbRcrd_DAY_Total(1), rd_NmbRcrd_MONTH_Total(1),
        rdADAY_ByIndex(0,0, DT15_9_16),
        rdADAY_ByIndex_NameAnsw(0, M1, tb_answ_aday_0_to_0_DT15_9_16),
        rdADAY_ByIndex_NameAnsw(M1, 0, tb_answ_aday_0_to_0_DT15_9_16),
        rdADAY_ByIndex_NameAnsw(M1, M1, tb_answ_aday_0_to_0_DT15_9_16),
        ////////////// action
        tb_act_work_day(100),   ///< tmt_End = 1482569442  Sat, 24 Dec 2016 08:50:42 
        // -- Read archive DAY
        rd_NmbRcrd_DAY_Total(101),
        rdADAY_ByIndex(0,2, DT24_12_16), rdADAY_ByIndex(2,0, DT24_12_16),
        rdADAY_ByIndex(M1,98, DT24_12_16), rdADAY_ByIndex(98,M1, DT24_12_16), 
        rdADAY_ByIndex_NameAnsw(98, M1, tb_answ_aday_98_to_M1_DT24_12_16),
        ////////////// action On/Off
        {scnrAction_Off_Mtr}, tb_act_PwrOn_min(15),  ///< tmtEnd = 1482570342   Sat, 24 Dec 2016 09:05:42
        {scnrAction_Off_Mtr}, tb_act_PwrOn_min(15),  ///< tmtEnd = 1482571242   Sat, 24 Dec 2016 09:20:42
        tb_act_work_day(1),                          ///< tmtEnd = 1482657642   Sun, 25 Dec 2016 09:20:42
        // -- Read archive DAY
        rdADAY_ByIndex(0,2, DT25_12_16),
        ////////////// action
        {scnrAction_Off_Mtr}, tb_act_PwrOn_day(3),  ///< tmtEnd = 1482916842   Wed, 28 Dec 2016 09:20:42
        // -- Read archive DAY
        rdADAY_ByIndex(0,2, DT28_12_16),
        rdADAY_ByIndex(10,12, DT28_12_16),        
        ////////////// action REMOVE
        tb_act_remove_day(13),                      ///< tmtEnd = 1481793642   Thu, 15 Dec 2016 09:20:42
        tb_act_work_day(1),                         ///< tmtEnd = 1481880042   Fri, 16 Dec 2016 09:20:42
        // -- Read archive DAY
        rdADAY_ByIndex(0,3, DT15_12_16_remove),
        //=====================================================================
        // STS_ERR_PRM
        //=====================================================================
        rd_NmbRcrd_DAY_Total(93),
        rdADAY_ByIndexDAY_ERR(0, 93), rdADAY_ByIndexDAY_ERR(93, 0),
        rdADAY_ByIndexDAY_ERR(93, M1), rdADAY_ByIndexDAY_ERR(M1, 93),
        ////////////// action REMOVE
        tb_act_remove_day(93),                     ///< tmtEnd = 1473844842   Wed, 14 Sep 2016 09:20:42
        // -- Read archive DAY
        rd_NmbRcrd_DAY_Total(1),
        rdADAY_ByIndexDAY_ERR(0, 1), rdADAY_ByIndex(0, 0, DT14_9_16_remove),
        // --
        { scnrEND }
    };
    //-----------------------------------------------------------------------------
    // -- SCENARIO READ Arch MONTH
    //-----------------------------------------------------------------------------
    tb_in_amonth(0, 0); tb_in_amonth(0, 2); tb_in_amonth(0, 3); tb_in_amonth(2, 0);
    tb_in_amonth(0, M1); tb_in_amonth(M1, 0); tb_in_amonth(M1, M1); 
    tb_in_amonth(M1, 1); tb_in_amonth(1, M1);
    tb_in_amonth(0, 6); tb_in_amonth(6, 0); tb_in_amonth(6, M1);
    tb_in_amonth(M1, 6);
    
    tb_answ_NTotal(4); tb_answ_NTotal(6);
    
    TB_SCENARIO_ARCHIVE scenario_rdArchMONTH_ByIndex_tb[] = {
        rd_NmbRcrd_MONTH_Total(1),
        rdAMONTH_ByIndex_NameAnsw(0, M1, tb_answ_amonth_0_to_0_DT1_9_16),
        rdAMONTH_ByIndex_NameAnsw(M1, 0, tb_answ_amonth_0_to_0_DT1_9_16),
        rdAMONTH_ByIndex_NameAnsw(M1, M1, tb_answ_amonth_0_to_0_DT1_9_16),
        ////////////// action
        tb_act_work_day(100),                       ///< tmt_End = 1482569442  Sat, 24 Dec 2016 08:50:42 
        // -- Read archive MONTH
        rd_NmbRcrd_MONTH_Total(4),
        rdAMONTH_ByIndex(0,2, DT24_12_16), rdAMONTH_ByIndex(2,0, DT24_12_16),
        rdAMONTH_ByIndex(M1,1, DT24_12_16), rdAMONTH_ByIndex(1,M1, DT24_12_16), 
        rdAMONTH_ByIndex_NameAnsw(1, M1, tb_answ_amonth_1_to_M1_DT24_12_16),
        ////////////// action On/Off
        {scnrAction_Off_Mtr}, tb_act_PwrOn_min(15),  ///< tmtEnd = 1482570342   Sat, 24 Dec 2016 09:05:42 
        {scnrAction_Off_Mtr}, tb_act_PwrOn_min(15),  ///< tmtEnd = 1482571242   Sat, 24 Dec 2016 09:20:42
        tb_act_work_day(31),                         ///< tmtEnd = 1485249642   Tue, 24 Jan 2017 09:20:42
        // -- Read archive MONTH
        rdAMONTH_ByIndex(0,2, DT24_01_17),
        ////////////// action On/Off
        {scnrAction_Off_Mtr}, tb_act_PwrOn_day(62),  ///< tmtEnd = 1490606442   Mon, 27 Mar 2017 09:20:42
        // -- Read archive MONTH
        rdAMONTH_ByIndex(0,2, DT27_03_17),
        ////////////// action REMOVE
        tb_act_work_day(3),                          ///< tmtEnd = 1490865642   Thu, 30 Mar 2017 09:20:42
        tb_act_remove_day(31),                       ///< tmtEnd = 1488187242   Mon, 27 Feb 2017 09:20:42
        // -- Read archive MONTH
        rdAMONTH_ByIndex(0,3, DT24_02_17_remove),
        //=====================================================================
        // STS_ERR_PRM
        //=====================================================================
        rd_NmbRcrd_MONTH_Total(6),
        rdAMONTH_ByIndex_ERR(0, 6), rdAMONTH_ByIndex_ERR(6, 0),
        rdAMONTH_ByIndex_ERR(6, M1), rdAMONTH_ByIndex_ERR(M1, 6),
        // --
        { scnrEND }
    };
}