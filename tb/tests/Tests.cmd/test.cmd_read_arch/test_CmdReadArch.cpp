/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   test_CmdReadArch.cpp
 * Author: User
 *
 * Created on 02.08.2017, 16:14:01
 */

#include "test_CmdReadArch.h"


CPPUNIT_TEST_SUITE_REGISTRATION(test_CmdReadArch);

void test_CmdReadArch::setUp() {
    tbu.Init();
    setUp(tmt_DTS_TB_begin);
}
void test_CmdReadArch::setUp(time_t tmtBgn) {
    // -- Init Time
    mtr::rtc_tbl.crnt_tmt = tmtBgn;
    mtr::GetDts_tmt(mtr::rtc_tbl.crnt_tmt, &mtr::rtc_tbl.crnt_dts);
    mtr::arch_var.fUpdateRTC = true;
    // -- Init Archive
    mtr::InitArch();
    mtr::reset_arch();
    // -- Init Energy
    memset(&mtr::enrgDt, 0, sizeof(mtr::enrgDt));
    // -- Security
    mtr::ClrPsw();
}

void test_CmdReadArch::tearDown() {
}

void test_CmdReadArch::test_cmd_read_arch_NmbRcrd() {
    bool fError = false;
    
    if(fError |= !tbu.TestScenario(nsCRA_NMB_RCRD::scenario_cmd_read_arch_NmbRcrd_tb, "scenario_cmd_read_arch_NmbRcrd_tb")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
void test_CmdReadArch::test_cmd_read_arch_ReadByIndex_DAY() {
    bool fError = false;
    
    if(fError |= !tbu.TestScenario(nsCRA::scenario_rdArchDAY_ByIndex_tb, "scenario_rdArchDAY_ByIndex_tb")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}
void test_CmdReadArch::test_cmd_read_arch_ReadByIndex_MONTH() {
    bool fError = false;
    
    if(fError |= !tbu.TestScenario(nsCRA::scenario_rdArchMONTH_ByIndex_tb, "scenario_rdArchMONTH_ByIndex_tb")) goto __exit;
__exit:
    CPPUNIT_ASSERT_MESSAGE(tbu.mess_err_tb, !fError);
}