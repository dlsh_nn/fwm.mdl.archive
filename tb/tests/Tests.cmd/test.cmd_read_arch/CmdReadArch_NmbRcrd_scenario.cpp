#include "../../tb_utils/tb_utils.h"

namespace nsCRA_NMB_RCRD{

//=============================================================================
// -- scenario cmd_read_arch mskType=enType_ArchGetNmb(0x42)
//=============================================================================
#if 1
    //// -- scenario COMMON
    uint8_t pout_STS_OK = {mtr::STS_OK};
    uint8_t pout_STS_NOT_FOUND = {mtr::STS_NOT_FOUND};
    uint8_t tb_answ_STS_ERR_PRM = {mtr::STS_ERR_PRM};
    uint8_t pout_ANSW_BUSY = {mtr::ANSW_METER_BUSY};
    uint8_t pout_STS_ERR_PSW = {mtr::STS_ERR_PSW};
    
    // -- Action
#if 1 /* scenario cmd_read_ppr mskType=enTPPR_getNmb_from_DTS(0x42) */
    #define tb_act_work_day(val) { \
            scnrWork, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
            ((val) * 24 * 60 * 60L), 15 * 60L, { {230000, 230000, 230000}, {1000, 1000, 1000}, {45, 45, 45} }, \
            {UseOtherPrm, TEMP_DEF} \
        }
#endif
#endif 
    //=========================================================================
    // -- READ ARCH scenario_cmd_read_arch_NmbRcrd_tb
    //=========================================================================
    #define DS_y_m_d_T(y, m, d)        d, m, y
#if 1   // -- Read ARCH mskTYPE=enType_ArchGetNmb_DS(0x42)
    #define tb_in_arch_DAY(yB, mB, dB,  yE, mE, dE)     \
                mtr::PACK_GET_ARCH tb_in_arch_DAY_##yB##_##mB##_##dB##_T_##yE##_##mE##_##dE = \
            {mtr::enType_ArchGetNmb_DS, mtr::eARCH_DAY,  0xFF, {DS_y_m_d_T(yB, mB, dB)}, {DS_y_m_d_T(yE, mE, dE)}}
    mtr::PACK_GET_NMB_ARCH tb_in_arch_DAY_Total = {mtr::enType_ArchGetNmb, mtr::eARCH_DAY};
    #define tb_in_arch_MONTH(yB, mB, dB,  yE, mE, dE)     \
                mtr::PACK_GET_ARCH tb_in_arch_MONTH_##yB##_##mB##_##dB##_T_##yE##_##mE##_##dE = \
            {mtr::enType_ArchGetNmb_DS, mtr::eARCH_MNTH,  0xFF, {DS_y_m_d_T(yB, mB, dB)}, {DS_y_m_d_T(yE, mE, dE)}}
    mtr::PACK_GET_NMB_ARCH tb_in_arch_MONTH_Total = {mtr::enType_ArchGetNmb, mtr::eARCH_MNTH};
    #define tb_answ_arch(nmb) mtr::PACK_ANSW_PPR_NMB_RCRD tb_answ_arch_##nmb = { nmb }
    #define tb_answ_NTotal(nmb) mtr::PACK_ANSW_PPR_NMB_RCRD tb_answ_NTotal_##nmb = { nmb }
    #define rd_NmbRcrd_DAY(yB, mB, dB,  yE, mE, dE,  nmb) { scnrNormal, \
                        { mtr::GET_ARCH_MTR, { {&tb_in_arch_DAY_##yB##_##mB##_##dB##_T_##yE##_##mE##_##dE, \
                                                    sizeof(tb_in_arch_DAY_##yB##_##mB##_##dB##_T_##yE##_##mE##_##dE)}, END_PLOAD } }, \
                        { mtr::GET_ARCH_MTR, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {&tb_answ_arch_##nmb, sizeof(tb_answ_arch_##nmb)}, END_PLOAD } \
                        } \
                      }
    #define rd_NmbRcrd_DAY_Total(nmb) { scnrNormal, \
                        { mtr::GET_ARCH_MTR, { {&tb_in_arch_DAY_Total, sizeof(tb_in_arch_DAY_Total)}, END_PLOAD } }, \
                        { mtr::GET_ARCH_MTR, { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                                                {&tb_answ_NTotal_##nmb, sizeof(tb_answ_NTotal_##nmb)}, END_PLOAD } \
                        } \
                      }
    #define rd_NmbRcrd_MONTH(yB, mB, dB,  yE, mE, dE,  nmb) { scnrNormal, \
                        { mtr::GET_ARCH_MTR, { {&tb_in_arch_MONTH_##yB##_##mB##_##dB##_T_##yE##_##mE##_##dE, \
                                                    sizeof(tb_in_arch_MONTH_##yB##_##mB##_##dB##_T_##yE##_##mE##_##dE)}, END_PLOAD } }, \
                        { mtr::GET_ARCH_MTR, \
                            { {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                              {&tb_answ_arch_##nmb, sizeof(tb_answ_arch_##nmb)}, END_PLOAD } \
                        } \
                      }
    #define rd_NmbRcrd_MONTH_Total(nmb) { scnrNormal, \
                        { mtr::GET_ARCH_MTR, { {&tb_in_arch_MONTH_Total, sizeof(tb_in_arch_MONTH_Total)}, END_PLOAD } }, \
                        { mtr::GET_ARCH_MTR, {  {&pout_STS_OK, sizeof(pout_STS_OK)}, \
                                                {&tb_answ_NTotal_##nmb, sizeof(tb_answ_NTotal_##nmb)}, END_PLOAD } \
                        } \
                      }
    #define rd_NmbRcrd_DAY_ERR(yB, mB, dB,  yE, mE, dE) { scnrNormal, \
                        { mtr::GET_ARCH_MTR, { {&tb_in_arch_DAY_##yB##_##mB##_##dB##_T_##yE##_##mE##_##dE, \
                                                    sizeof(tb_in_arch_DAY_##yB##_##mB##_##dB##_T_##yE##_##mE##_##dE)}, END_PLOAD } }, \
                        { mtr::GET_ARCH_MTR, \
                            { {&tb_answ_STS_ERR_PRM, sizeof(tb_answ_STS_ERR_PRM)}, END_PLOAD } \
                        } \
                    }
    #define rd_NmbRcrd_MONTH_ERR(yB, mB, dB,  yE, mE, dE) { scnrNormal, \
                        { mtr::GET_ARCH_MTR, { {&tb_in_arch_MONTH_##yB##_##mB##_##dB##_T_##yE##_##mE##_##dE, \
                                                    sizeof(tb_in_arch_DAY_##yB##_##mB##_##dB##_T_##yE##_##mE##_##dE)}, END_PLOAD } }, \
                        { mtr::GET_ARCH_MTR, \
                            { {&tb_answ_STS_ERR_PRM, sizeof(tb_answ_STS_ERR_PRM)}, END_PLOAD } \
                        } \
                    }
    #define tb_act_remove_day(NmbDay) { scnrMakeRemove, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD}, \
                                        ((NmbDay) * 24 * 60 * 60L), 20 * 60 }
//#define tb_CheckArchive_Convergence { scnrCheckArchive_Convergence, {mtr::CMD_NON, END_PLOAD}, {mtr::CMD_NON, END_PLOAD} }
#endif
//-----------------------------------------------------------------------------
// -- SCENARIO READ Nmb Arch
//-----------------------------------------------------------------------------
    tb_answ_arch(0); tb_answ_arch(1);  tb_answ_arch(2); tb_answ_arch(98); 
    tb_in_arch_DAY(16,9,14,  16,9,14);
    tb_in_arch_DAY(16,9,15,  16,9,15);
    tb_in_arch_DAY(16,9,1,  16,9,16);
    tb_in_arch_DAY(16,9,16,  16,12,23);
    tb_in_arch_DAY(16,9,17,  16,12,23);
    
    tb_answ_arch(4); tb_answ_arch(37);
    tb_in_arch_MONTH(16,9,1,  16,12,1);
    tb_in_arch_MONTH(16,11,1,  16,11,1);
    tb_in_arch_MONTH(16,4,1,  16,7,1);
    tb_in_arch_MONTH(16,1,1,  16,12,1);
    tb_in_arch_MONTH(19,12,1,  22,12,1);
    // --
    tb_in_arch_DAY(19,12,2,  22,12,1);
    tb_in_arch_MONTH(19,12,2,  22,12,1);
    
    tb_answ_NTotal(1);
    tb_answ_NTotal(101); tb_answ_NTotal(126); tb_answ_NTotal(37);
    TB_SCENARIO_ARCHIVE scenario_cmd_read_arch_NmbRcrd_tb[] = {
        // Debug
 
        // ~Debug
        rd_NmbRcrd_DAY_Total(1), rd_NmbRcrd_MONTH_Total(1),
        // -- tmt_End = 1482569442  Sat, 24 Dec 2016 08:50:42 
        tb_act_work_day(100),
        // -- Debug
        
        // -- ~Debug
        // -- DAY
        rd_NmbRcrd_DAY_Total(101),
        rd_NmbRcrd_DAY(16,9,14,  16,9,14,  0), 
        rd_NmbRcrd_DAY(16,9,15,  16,9,15,  1), 
        rd_NmbRcrd_DAY(16,9,1,  16,9,16,  2), 
        rd_NmbRcrd_DAY(16,9,1,  16,9,16,  2), 
        rd_NmbRcrd_DAY(16,9,17,  16,12,23,  98), 
        // -- MONTH
        rd_NmbRcrd_MONTH(16,9,1,  16,12,1,  4), 
        rd_NmbRcrd_MONTH(16,11,1,  16,11,1,  1), 
        rd_NmbRcrd_MONTH(16,4,1,  16,7,1,  0), 
        rd_NmbRcrd_MONTH(16,1,1,  16,12,1,  4), 
        // --
        tb_act_work_day(6 * 365L),
        // -- MONTH
//        tb_CheckArchive_Convergence,
        rd_NmbRcrd_DAY_Total(126),
        rd_NmbRcrd_MONTH_Total(37),
        rd_NmbRcrd_MONTH(19,12,1,  22,12,1,  37), 
        // --
        rd_NmbRcrd_DAY_ERR(19,12,2,  22,12,1),
        rd_NmbRcrd_MONTH_ERR(19,12,2,  22,12,1),
        // --
        tb_act_remove_day(7 * 365),
        rd_NmbRcrd_DAY_Total(1),
        rd_NmbRcrd_MONTH_Total(1),
        // --
        { scnrEND }
    };
}