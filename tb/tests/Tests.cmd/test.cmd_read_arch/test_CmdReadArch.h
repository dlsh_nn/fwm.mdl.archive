/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   test_CmdReadArch.h
 * Author: User
 *
 * Created on 02.08.2017, 16:14:01
 */

#ifndef TEST_CMDREADARCH_H
#define TEST_CMDREADARCH_H

#include "../../tb_utils/tb_utils.h"

class test_CmdReadArch : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(test_CmdReadArch);

    CPPUNIT_TEST(test_cmd_read_arch_ReadByIndex_MONTH);
    CPPUNIT_TEST(test_cmd_read_arch_ReadByIndex_DAY);
    CPPUNIT_TEST(test_cmd_read_arch_NmbRcrd);

    CPPUNIT_TEST_SUITE_END();
private:
    tb_utils_cls tbu;
    
public:
    void setUp();
    void setUp(time_t tmtBgn);
    void tearDown();

private:
    void test_cmd_read_arch_NmbRcrd();
    void test_cmd_read_arch_ReadByIndex_DAY();
    void test_cmd_read_arch_ReadByIndex_MONTH();
};

#endif /* TEST_CMDREADARCH_H */

