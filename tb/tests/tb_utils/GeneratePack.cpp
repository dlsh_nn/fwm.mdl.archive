/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   GeneratePack.cpp
 * Author: User
 * 
 * Created on 1 июля 2017 г., 18:16
 */
#include <list>

#include "GeneratePack.h"


GeneratePack::GeneratePack(){
    memset(pay_load_generate, 0, sizeof(pay_load_generate));
    for(int i = 0; i < 2; i++) pay_load_generate[i].size_data = -1;
}
GeneratePack::~GeneratePack(){
    free_pay_load();
}
void GeneratePack::free_pay_load(){
    LPPAY_LOAD_RAW pload = pay_load_generate;
    // -- Check need delete
    if(pload->pData){
        for(int i = 0; i < 2; i++) if(pload[i].pData){
            delete[] (uint8_t *)pload[i].pData;
            pload[i].pData = 0;
            pload[i].size_data = -1;
        }
    }
}
#define NMB_RCRD_PPR_V0     (125L * 24 * 60 / 30)
LPCPAY_LOAD_RAW GeneratePack::PackConstPQP(LPCTB_SCENARIO_ARCHIVE pscnr){
    LPPAY_LOAD_RAW pload = pay_load_generate;
    
    free_pay_load();
    
    if(pscnr->pack_valid.cmd == mtr::GET_PWR_PROFILE){
        pload = GenPack(pload, new PackPPR, pscnr);
    }else if(pscnr->pack_valid.cmd == mtr::GET_ARCH_MTR){
        mtr::LPPACK_GET_ARCH pack_in = (mtr::LPPACK_GET_ARCH)pscnr->pack_tested.pload->pData;
        if(pack_in->Type == mtr::enType_NoPass){
            if(pack_in->TypeArch == mtr::eARCH_DAY){
                pload = GenPack(pload, new PackADAY, pscnr);
            }else if(pack_in->TypeArch == mtr::eARCH_MNTH){
                pload = GenPack(pload, new PackAMONTH, pscnr);
            }else throw "ERROR!!! pack_in->TypeArch - Not implemented in GeneratePack::Generate_PackValid_ConstPQP.";
        }else throw "ERROR!!! pack_in->Type - Not implemented in GeneratePack::Generate_PackValid_ConstPQP.";
    }else throw "ERROR!!! pscnr->pack_valid.cmd - Not implemented in GeneratePack::Generate_PackValid_ConstPQP.";

    return pload;
}
LPPAY_LOAD_RAW GeneratePack::GenPack(LPPAY_LOAD_RAW pload, PackArchive* packa, LPCTB_SCENARIO_ARCHIVE pscnr){
    time_t tmt_work = pscnr->tmt_work;
    time_t tmt_ppr_crnt, tmt_ppr_bgn, tmt_ppr_end, tmt_step;
    Electrical electrical(mtr::ce_mdl);
    Energy enrg;
    
    packa->Init(pscnr->pack_tested.pload->pData);
    if(packa->GetTypeAnsw() == mtr::enType_NoPass){
        tmt_ppr_bgn = tmt_ppr_crnt = mtr::GetTmtArchPPR_align(packa->Get_tmt_bgn());
        tmt_ppr_end = mtr::GetTmtArchPPR_align(packa->Get_tmt_end());
        enrg.Calculate(tmt_ppr_crnt - tmt_DTS_TB_begin, electrical);
        while(tmt_ppr_crnt != tmt_ppr_end){
            if(packa->GetTypeMark() == PackArchive::enMark_End){
                packa->add_rcrd(tmt_ppr_crnt, enrg, electrical);
            }
            // --
            tmt_step = packa->next_step(tmt_ppr_crnt, tmt_ppr_end);
            enrg.rst_eint();
            enrg.Calculate(tmt_step, electrical);
            // --
            if(packa->GetTypeMark() == PackArchive::enMark_Begin){
                packa->add_rcrd(tmt_ppr_crnt, enrg, electrical);
            }
            //--
            tmt_ppr_crnt += tmt_step;
        }
        packa->serializ(pload);
    }else throw "ERROR!!! pack_in->Type - Not implemented in GeneratePack::Generate_PackValid_ConstPQP.";
    
    return pload;
}


LPCPAY_LOAD_RAW GeneratePack::PackConstPQPSparseData(LPCTB_SCENARIO_ARCHIVE pscnr, std::list<time_t> tmt_of_shifts, time_t tmt_shift){
    LPPAY_LOAD_RAW pload = pay_load_generate;
    
    free_pay_load();
    
    if(pscnr->pack_valid.cmd == mtr::GET_PWR_PROFILE){
        pload = GenSparsePack(pload, new PackPPR, pscnr, tmt_of_shifts, tmt_shift);
    }else if(pscnr->pack_valid.cmd == mtr::GET_ARCH_MTR){
        mtr::LPPACK_GET_ARCH pack_in = (mtr::LPPACK_GET_ARCH)pscnr->pack_tested.pload->pData;
        if(pack_in->Type == mtr::enType_NoPass){
            if(pack_in->TypeArch == mtr::eARCH_DAY){
                pload = GenSparsePack(pload, new PackADAY, pscnr, tmt_of_shifts, tmt_shift);
            }else if(pack_in->TypeArch == mtr::eARCH_MNTH){
                pload = GenSparsePack(pload, new PackAMONTH, pscnr, tmt_of_shifts, tmt_shift);
            }else throw "ERROR!!! pack_in->TypeArch - Not implemented in GeneratePack::Generate_PackValid_ConstPQP.";
        }else throw "ERROR!!! pack_in->Type - Not implemented in GeneratePack::Generate_PackValid_ConstPQP.";
    }else throw "ERROR!!! pscnr->pack_valid.cmd - Not implemented in GeneratePack::Generate_PackValid_ConstPQP.";

    return pload;
}
LPPAY_LOAD_RAW GeneratePack::GenSparsePack(LPPAY_LOAD_RAW pload, PackArchive* packa, LPCTB_SCENARIO_ARCHIVE pscnr, std::list<time_t> tmt_of_shifts, time_t tmt_shift){
    time_t tmt_work = pscnr->tmt_work;
    time_t tmt_ppr_crnt, tmt_ppr_bgn, tmt_ppr_end, tmt_step;
    Electrical electrical(mtr::ce_mdl);
    Energy enrg;
    
    packa->Init(pscnr->pack_tested.pload->pData);
    if(packa->GetTypeAnsw() == mtr::enType_NoPass){
        tmt_ppr_bgn = tmt_ppr_crnt = mtr::GetTmtArchPPR_align(packa->Get_tmt_bgn());
        tmt_ppr_end = mtr::GetTmtArchPPR_align(packa->Get_tmt_end());
        enrg.Calculate(tmt_ppr_crnt - tmt_DTS_TB_begin, electrical);
        while(tmt_ppr_crnt > tmt_of_shifts.back()) {
            tmt_of_shifts.pop_back();
        }
        //while(tmt_ppr_crnt != tmt_ppr_end){
        while(tmt_ppr_crnt <= tmt_ppr_end){
            if(tmt_ppr_crnt != tmt_of_shifts.back()) {
                if(packa->GetTypeMark() == PackArchive::enMark_End){
                    packa->add_rcrd(tmt_ppr_crnt, enrg, electrical);
                }
                // --
                tmt_step = packa->next_step(tmt_ppr_crnt, tmt_ppr_end);
                enrg.rst_eint();
                enrg.Calculate(tmt_step, electrical);
                // --
                if(packa->GetTypeMark() == PackArchive::enMark_Begin){
                    packa->add_rcrd(tmt_ppr_crnt, enrg, electrical);
                }
                //--
                tmt_ppr_crnt += tmt_step;
            } else {
                //tmt_shift должен быть кратен tmt_step

                time_t tmt_end_of_shift = tmt_ppr_crnt + tmt_shift;
                while(tmt_ppr_crnt+tmt_step < tmt_end_of_shift) {
                    if(packa->GetTypeMark() == PackArchive::enMark_End){
                        packa->add_corrupt_rcrd(tmt_ppr_crnt);
                        //packa->add_rcrd(tmt_ppr_crnt, enrg, electrical);
                    }
                    //enrg.setCorrupt();
                    //TODO: здесь должно производиться добавление некорректных записей
                    // --
                    //tmt_step = packa->next_step(tmt_ppr_crnt, tmt_ppr_end);
                    //enrg.rst_eint();
                    //enrg.Calculate(tmt_step, electrical);
                    // --
//                    if(packa->GetTypeMark() == PackArchive::enMark_Begin){
//                        packa->add_rcrd(tmt_ppr_crnt, enrg, electrical);
//                    }
                    //--
                    tmt_ppr_crnt += tmt_step;
                }
                tmt_of_shifts.pop_back();
            }
        }
        packa->serializ(pload);
    }else throw "ERROR!!! pack_in->Type - Not implemented in GeneratePack::Generate_PackValid_ConstPQP.";
    
    return pload;
}

