/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Convergence.cpp
 * Author: User
 * 
 * Created on 6 ���� 2017 �., 10:12
 */

#include "Convergence.h"
#include "Energy.h"

Convergence::Convergence() {
    
}
bool Convergence::CheckArchive(const PackPPR& appr, const PackADAY& aday, const PackAMONTH& amonth){
    if(!check_convergence(appr, aday)) goto __error;
    if(!check_convergence(aday, amonth)) goto __error;
    
    return true;
__error:
    return false;
}
 
bool Convergence::check_convergence(const PackPPR& appr, const PackADAY& aday){
    std::vector<RCRD_ANSW_ARCH> rcrds_aday(aday.get_rcrds());
    std::vector<RCRD_ANSW_ARCH>::iterator it_rcrd_aday;
    std::vector<mtr::PACK_ANSW_PWR_PRFL_V0> rcrds_appr(appr.get_rcrds());
    std::vector<mtr::PACK_ANSW_PWR_PRFL_V0>::iterator it_rcrd_appr;
    
    if(rcrds_aday.size() < 2) {
        return true;
    }
    // -- 
    for(it_rcrd_aday = rcrds_aday.begin(); it_rcrd_aday != rcrds_aday.end(); it_rcrd_aday++){
        for(it_rcrd_appr = rcrds_appr.begin(); it_rcrd_appr != rcrds_appr.end(); it_rcrd_appr++){
            if(it_rcrd_aday->ds == it_rcrd_appr->dts) break;
        }
        if(it_rcrd_aday->ds == it_rcrd_appr->dts) break;
    }
    if(it_rcrd_aday >= rcrds_aday.end()) goto __error;
    // --
    mtr::ENERGY sumE_ppr_day;
    while((it_rcrd_aday + 1) != rcrds_aday.end()){
        it_rcrd_aday++;
        
        sumE_ppr_day = appr.sum((it_rcrd_aday - 1)->ds, it_rcrd_aday->ds);
        if( sumE_ppr_day != (it_rcrd_aday->enrg - (it_rcrd_aday - 1)->enrg) ){
            goto __error;
        }
    }
    return true;
__error:
    return false;
}
bool Convergence::check_convergence(const PackADAY& aday, const PackAMONTH& amonth){
    std::vector<RCRD_ANSW_ARCH> rcrds_aday(aday.get_rcrds()), rcrds_month(amonth.get_rcrds());
    std::vector<RCRD_ANSW_ARCH>::iterator it_rcrd_aday, it_rcrd_month;

    // -- 
    if(rcrds_month.size() < 2) {
        return true;
    } 
        
    for(it_rcrd_month = rcrds_month.begin(); it_rcrd_month != rcrds_month.end(); it_rcrd_month++){
        for(it_rcrd_aday = rcrds_aday.begin(); it_rcrd_aday != rcrds_aday.end(); it_rcrd_aday++){
            if(it_rcrd_month->ds == it_rcrd_aday->ds) break;
        }
        if(it_rcrd_month->ds == it_rcrd_aday->ds) break;
    }
    if(it_rcrd_month >= rcrds_month.end()) goto __error;
    // --
    mtr::ENERGY sumE_ppr_day;
    while((it_rcrd_month + 1) != rcrds_month.end()){
        it_rcrd_month++;
        
        sumE_ppr_day = aday.sum((it_rcrd_month - 1)->ds, it_rcrd_month->ds);
        if( sumE_ppr_day != (it_rcrd_month->enrg - (it_rcrd_month - 1)->enrg) ){
            goto __error;
        }
    }
    return true;
__error:
    return false;
}

