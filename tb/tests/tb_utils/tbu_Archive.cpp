/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "tb_utils.h"
#include "Convergence.h"

void tb_utils_cls::read_bgn_archive(mtr::LPPACK_ANSW_BGN_DTS_PPR pbgn_ppr, 
                                    mtr::LPPACK_ANSW_BGN_DS_ARCH pbgn_day, 
                                    mtr::LPPACK_ANSW_BGN_DS_ARCH pbgn_month ){
    mtr::PACK_GET_BGN_DS_ARCH tb_in_bgn_ds_day = {mtr::eARCH_DAY};
    mtr::PACK_GET_BGN_DS_ARCH tb_in_bgn_ds_month = {mtr::eARCH_MNTH};
    
    #define rd_bgn_ppr  { scnrNormal, { mtr::GET_BGN_DTS_PPR, END_PLOAD } }
    #define rd_bgn_day   { scnrNormal, { mtr::GET_BGN_DS_ARCH, { {&tb_in_bgn_ds_day, sizeof(tb_in_bgn_ds_day)}, END_PLOAD } }}
    #define rd_bgn_month   { scnrNormal, { mtr::GET_BGN_DS_ARCH, { {&tb_in_bgn_ds_month, sizeof(tb_in_bgn_ds_month)}, END_PLOAD } }}
    
    TB_SCENARIO_ARCHIVE scnr[] = {
        rd_bgn_ppr, rd_bgn_day, rd_bgn_month
    };
    //-------------------------------------------------------------------------
    // READ BGN DTS/DS
    //-------------------------------------------------------------------------
    // -- rd_bgn_ppr
    test_action(&scnr[0]);
    memcpy(pbgn_ppr, prtcl_dt.pHead->Data, sizeof(*pbgn_ppr));
    reInit_prtcl_dt();
    // -- rd_bgn_day
    test_action(&scnr[1]);
    memcpy(pbgn_day, prtcl_dt.pHead->Data, sizeof(*pbgn_day));
    reInit_prtcl_dt();
    // -- rd_bgn_month
    test_action(&scnr[2]);
    memcpy(pbgn_month, prtcl_dt.pHead->Data, sizeof(*pbgn_month));
    reInit_prtcl_dt();
}
void tb_utils_cls::read_archive(const mtr::PACK_ANSW_BGN_DTS_PPR& bgn_ppr, PackPPR* pappr){
    mtr::PACK_GET_PWR_PRFL tb_in_ppr = {mtr::enTPPR_NoSkip_PPRv0, {-1}, {-1}};
    TB_SCENARIO_ARCHIVE scnr = { scnrNormal, { mtr::GET_PWR_PROFILE, { {&tb_in_ppr, sizeof(tb_in_ppr)}, END_PLOAD } } };
    
    tb_in_ppr.dts_bgn = bgn_ppr.dts_bgn;
    mtr::GetDts_tmt(mtr::GetTmtArchPPR_align(mtr::rtc_tbl.crnt_tmt), &tb_in_ppr.dts_end);
    // -- rd_ppr
    test_action(&scnr);
    pappr->deserializ(prtcl_dt.pHead->Data, prtcl_dt.size_data);
    reInit_prtcl_dt();
}
void tb_utils_cls::read_archive(const mtr::PACK_ANSW_BGN_DS_ARCH& bgn, PackADAY* paday){
    #define MTrf_ALL      0xFF
    mtr::PACK_GET_ARCH tb_in = {mtr::enType_NoPass, mtr::eARCH_DAY, MTrf_ALL, {-1}, {-1}};
    TB_SCENARIO_ARCHIVE scnr = { scnrNormal, { mtr::GET_ARCH_MTR, { {&tb_in, sizeof(tb_in)}, END_PLOAD } } };
    
    tb_in.ds_bgn = bgn.ds;
    mtr::GetDs_tmt(mtr::GetTmtArchDay_align(mtr::rtc_tbl.crnt_tmt), &tb_in.ds_end);
    // -- rd_archive
    test_action(&scnr);
    paday->deserializ(prtcl_dt.pHead->Data, prtcl_dt.size_data);
    reInit_prtcl_dt();
}
void tb_utils_cls::read_archive(const mtr::PACK_ANSW_BGN_DS_ARCH& bgn, PackAMONTH* pamonth){
    #define MTrf_ALL      0xFF
    mtr::PACK_GET_ARCH tb_in = {mtr::enType_NoPass, mtr::eARCH_MNTH, MTrf_ALL, {-1}, {-1}};
    TB_SCENARIO_ARCHIVE scnr = { scnrNormal, { mtr::GET_ARCH_MTR, { {&tb_in, sizeof(tb_in)}, END_PLOAD } } };
    
    tb_in.ds_bgn = bgn.ds;
    mtr::GetDs_tmt(mtr::GetTmtArchDay_align(mtr::rtc_tbl.crnt_tmt), &tb_in.ds_end);
    // -- rd_archive
    test_action(&scnr);
    pamonth->deserializ(prtcl_dt.pHead->Data, prtcl_dt.size_data);
    reInit_prtcl_dt();
}
bool tb_utils_cls::CheckArchive_Convergence(const TB_SCENARIO_ARCHIVE *pscnr){
    mtr::PACK_ANSW_BGN_DS_ARCH bgn_day, bgn_month;
    mtr::PACK_ANSW_BGN_DTS_PPR bgn_ppr;
    PackPPR appr;
    PackADAY aday;
    PackAMONTH amonth;
    Convergence cnvrg;
    
    read_bgn_archive(&bgn_ppr, &bgn_day, &bgn_month);
    // --
    read_archive(bgn_ppr, &appr);
    read_archive(bgn_day, &aday);
    read_archive(bgn_month, &amonth);
    
    appr.OutputFile("appr.csv");
    aday.OutputFile("aday.csv");
    amonth.OutputFile("amonth.csv");
    
    return cnvrg.CheckArchive(appr, aday, amonth);
    return true;
}

bool tb_utils_cls::CheckArchiveExecCount(const TB_SCENARIO_ARCHIVE *pscnr, LPCPAY_LOAD_RAW pload){
    const uint32_t max_cnt = 13;
    test_action(pscnr);
    pload = gen_pack.PackConstPQP(pscnr);
    return cnt_pass_Arch_exec < max_cnt;
}
void tb_utils_cls::RunArchExec(LPCTB_SCENARIO_ARCHIVE pscnr, int NMB_RunForcibly){
    int Interface;
    for(cnt_pass_Arch_exec = 0; cnt_pass_Arch_exec < 1e+9; cnt_pass_Arch_exec++){
        // -- Check Need
        if(pscnr->TypeTest == scnrNormal_NoArchExec) break;
        // -- Check END Arch_exec
        for(Interface = 0; Interface < mtr::NMB_Interface; Interface++){
            if(mtr::arch_var.pAnswVar[Interface] != 0) break;
        }
        if((Interface >= mtr::NMB_Interface) && 
           (mtr::arch_mech.Mode == mtr::enIdleAM)){
            break;
        }
        // --
        mtr::Arch_exec();
    }
    if(cnt_pass_Arch_exec){
        float TimeWork = (float)cnt_pass_Arch_exec * 5.48;
        int Tsec = (TimeWork / 1000);
        int Tmin = Tsec / 60;
        int Tms = (Tmin || Tsec) ? (int)TimeWork % ((Tmin * 1000 * 60) + Tsec * 1000) :
                                     (int)TimeWork;
        
        CPPUNIT_NS::stdCOut() << "    - scnr_line=" << indx_line_tab_scnr 
            << "; cnt_pass_Arch_exec = " << cnt_pass_Arch_exec 
            << "; Time work Meter = " << Tmin << ":" << Tsec << ":" << Tms
            << "; (min:sec:ms)\r\n";
    }
}
