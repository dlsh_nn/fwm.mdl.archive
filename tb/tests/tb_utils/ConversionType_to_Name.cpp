/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ConversionType_to_Name.cpp
 * Author: User
 * 
 * Created on 1 июля 2017 г., 16:46
 */

#include "ConversionType_to_Name.h"

const TB_CONVERSION_TypeMtr_to_Name tb_cnvr_cmd_mtr[] = {
    // -- PROFILE
    {mtr::GET_PWR_PROFILE,          "GET_PWR_PROFILE"},
    {mtr::CMD_MNG_PPR,              "CMD_MNG_PPR"},
    {mtr::GET_BGN_DTS_PPR,          "GET_BGN_DTS_PPR"},
    // -- Arch
    {mtr::GET_BGN_DS_ARCH,          "GET_BGN_DS_ARCH"},
    {mtr::GET_ARCH_MTR,             "GET_ARCH_MTR"},
    
    {mtr::CMD_NON,                  "CMD_NON"}
};
const TB_CONVERSION_TypeMtr_to_Name tb_cnvr_Status_mtr[] = {
    {mtr::STS_OK,                "STS_OK"},
    {mtr::STS_UNKNOWN_CMD,       "STS_UNKNOWN_CMD"},
    {mtr::STS_ERR_FRMT,          "STS_ERR_FRMT"},
    {mtr::STS_NOT_FOUND,         "STS_NOT_FOUND"},
    {mtr::STS_ERR_PSW,           "STS_ERR_PSW"},
    {mtr::STS_ERR_OTHER,         "STS_ERR_OTHER"},
    {mtr::STS_ERR_TIME,          "STS_ERR_TIME"},
    {mtr::STS_ERR_CORECT_TIME,   "STS_ERR_CORECT_TIME"},
    {mtr::STS_ERR_LEN,           "STS_ERR_LEN"},
    {mtr::STS_ERR_PRM,           "STS_ERR_PRM"},
    {mtr::STS_ERR_pin_INIT,      "STS_ERR_pin_INIT"},
    {mtr::STS_ERR_PROHIBITED,    "STS_ERR_PROHIBITED"},
    {mtr::STS_ERR_METR_RELOAD,   "STS_ERR_METR_RELOAD"},
    {mtr::ANSW_METER_WAIT_TIMEOUT,   "ANSW_METER_WAIT_TIMEOUT"},
    {mtr::ANSW_METER_BUSY,       "ANSW_METER_BUSY"},
    {mtr::STS_ERR_MODEM,         "STS_ERR_MODEM"},
    
    // -- Service STATUS
    {mtr::STS_NEED_CHECKED_EXTERN,   "STS_NEED_CHECKED_EXTERN"},
    {mtr::STS_NOT_DEFINED,           "STS_NOT_DEFINED"}
};
const TB_CONVERSION_TypeMtr_to_Name tb_cnvr_TypeArch[] = {
    {mtr::eARCH_DAY,        "eARCH_DAY"},
    {mtr::eARCH_MNTH,       "eARCH_MNTH"},
    {mtr::eARCH_PPR,        "eARCH_PPR"},
   
    {mtr::eARCH_NOT_DEFINED,    "eARCH_NOT_DEFINED"}
};

LPCTB_CONVERSION_TypeMtr_to_Name FindItem(LPCTB_CONVERSION_TypeMtr_to_Name pitem, int32_t val, int32_t NotDefined){
    while( (val != pitem->ValType) && (pitem->ValType != NotDefined) ){
        pitem++;
    }
    return pitem;
}

std::ostream& operator<< (std::ostream& out, const mtr::enStatusPrtcl &sts){
    LPCTB_CONVERSION_TypeMtr_to_Name pitem = tb_cnvr_Status_mtr;
    
    pitem = FindItem(pitem, sts, mtr::STS_NOT_DEFINED);
    out << pitem->name << "(0x" << (uint16_t)sts << ")";
}
std::ostream& operator<< (std::ostream& out, const mtr::enCmdPrtcl &cmd){
    LPCTB_CONVERSION_TypeMtr_to_Name pitem = tb_cnvr_cmd_mtr;
    
    pitem = FindItem(pitem, cmd, mtr::CMD_NON);
    out << pitem->name << " (" << (uint16_t)cmd << ")";
}
std::ostream& operator<< (std::ostream& out, const mtr::enTypeArch &TypeArch){
    LPCTB_CONVERSION_TypeMtr_to_Name pitem = tb_cnvr_TypeArch;
    
    pitem = FindItem(pitem, TypeArch, mtr::eARCH_NOT_DEFINED);
    out << pitem->name << "(0x" << (uint16_t)TypeArch << ")";
}