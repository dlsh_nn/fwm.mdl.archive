/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   test_Convergence.cpp
 * Author: User
 *
 * Created on 7 ���� 2017 �., 10:46
 */

#include <stdlib.h>
#include <iostream>

#include "../../tb_utils/Clock.h"
#include "../Convergence.h"

/*
 * Simple C++ Test Suite
 */
void test_CheckArchive(const Clock &clock) {
    std::cout << "test_Convergence test_CheckArchive" << std::endl;
    
    time_t tmt_bgn = 1483228800; // Sun, 01 Jan 2017 00:00:00 GMT GMT -> https://www.cy-pr.com/tools/time/
    PackPPR appr;
    PackADAY aday;
    PackAMONTH amonth;
    Convergence cnvrg;

    appr.Init(PackArchive::enInit_1_year, tmt_bgn);
    aday.Init(PackArchive::enInit_1_year, tmt_bgn);
    amonth.Init(PackArchive::enInit_1_year, tmt_bgn);
    if(!cnvrg.CheckArchive(appr, aday, amonth)) goto __error;
    
    return;
__error:
    std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
            " testname=test_CheckArchive (test_Convergence) message=cnvrg.CheckArchive(appr, aday, amonth) - false!!" << std::endl;
}

int main(int argc, char** argv) {
    Clock time_execute, time_test;
    
    std::cout << "%SUITE_STARTING% test_Convergence" << std::endl;
    std::cout << "%SUITE_STARTED%" << std::endl;

    try{
        time_test.start();
        std::cout << "%TEST_STARTED% test_CheckArchive (test_Convergence)" << std::endl;
        test_CheckArchive(time_test);
        std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_CheckArchive (test_Convergence)" << std::endl;
    }
    catch(const char* err){
        std::cout << "%TEST_FAILED% time=0" << " testname=-------- (test_Convergence) message=throw " << std::string(err) << std::endl;
    }

    std::cout << "%SUITE_FINISHED% time=0" << std::endl;

    return (EXIT_SUCCESS);
}

