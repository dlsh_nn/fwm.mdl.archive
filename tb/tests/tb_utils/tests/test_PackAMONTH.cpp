/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   test_PackAMONTH.cpp
 * Author: User
 *
 * Created on 4 ���� 2017 �., 13:27
 */

#include <stdlib.h>
#include <iostream>
#include <time.h>

#include "../../tb_utils/Clock.h"
#include "../PackAMONTH.h"

/*
 * Simple C++ Test Suite
 */

void test_next_step(const Clock &clock) {
    std::cout << "test_PackAMONTH test_serializ" << std::endl;
    
    PackAMONTH amonth;
    
    if(amonth.next_step(1684745442) != (1685577600 - 1684745442)) goto __error;
    if(amonth.next_step(1684745442, 1716281442) != (1685577600 - 1684745442)) goto __error;
    if(amonth.next_step(1684745442, 1653209442) != (1684745442 - 1685577600)) goto __error;
    if(amonth.next_step(1684745442, 1684745442) != 0) goto __error;
    
    return;
__error:
    std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
            " testname=test_next_step (test_PackAMONTH) message=amonth.next_step(1684745442) != (1672963200 - 1684745442)" << std::endl;
}

int main(int argc, char** argv) {
    Clock time_execute, time_test;
    std::cout << "%SUITE_STARTING% test_PackAMONTH" << std::endl;
    std::cout << "%SUITE_STARTED%" << std::endl;

    time_test.start();
    std::cout << "%TEST_STARTED% test_next_step (test_PackAMONTH)" << std::endl;
    test_next_step(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_next_step (test_PackAMONTH)" << std::endl;

    std::cout << "%SUITE_FINISHED% time=0" << std::endl;

    return (EXIT_SUCCESS);
}

