/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   test_PackPPR.cpp
 * Author: User
 *
 * Created on 6 ���� 2017 �., 13:40
 */

#include <stdlib.h>
#include <iostream>

#include "../../tb_utils/Clock.h"
#include "../PackPPR.h"

/*
 * Simple C++ Test Suite
 */

void test_serializ(const Clock &clock) {
    std::cout << "test_PackPPR test_serializ" << std::endl;
    bool r = false;
    
    mtr::CE_MDL ce_mdl_init = {
        {0, 0, 0}, {0, 0, 0}, {0, 0, 0, 0}, 
        {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, 
        // -- energy_int
        {1, 2, 3, 4}
    };
    mtr::PACK_GET_ARCH pack_in = {
        mtr::enType_NoPass, mtr::eARCH_DAY, 0xFF,
        {-1, -1, -1}, {-1, -1, -1}
    };
    struct _PACK_OUT{
        mtr::enStatusPrtcl sts;
        mtr::PACK_ANSW_PWR_PRFL_V0 rcrds[5];
    } pack_valid = {
        mtr::STS_OK, 
        { {   {0,  0, 0, 2, 3, 7, 17}, { {244, 2, 246, 4}, {487902, 0, 487902, 0} }, mtr::ePrflValid   },
          {   {0, 30, 0, 2, 3, 7, 17}, { {244, 0, 244, 0}, {487902, 0, 487902, 0} }, mtr::ePrflValid   },
          {   {0,  0, 1, 2, 3, 7, 17}, { {244, 0, 244, 0}, {487902, 0, 487902, 0} }, mtr::ePrflValid   },
          {   {0, 30, 1, 2, 3, 7, 17}, { {244, 0, 244, 0}, {487902, 0, 487902, 0} }, mtr::ePrflValid   },
          {   {0,  0, 2, 2, 3, 7, 17}, { {244, 0, 244, 0}, {487902, 0, 487902, 0} }, mtr::ePrflValid   } }
    };
    
    Energy enrgy(ce_mdl_init);
    Electrical electrical(Electrical::enInit_230V_1A_45deg);
    time_t tmt = 1499040000;    // 17/7/3T00:00:00
    PAY_LOAD_RAW pload[2] = {{0, -1}, {0, -1}};
    
    PackPPR appr(&pack_in);
    for(int i = 0; i < 5; i++, tmt += 30 * 60){
        enrgy.Calculate(30 * 60, electrical);
        appr.add_rcrd(tmt, enrgy, electrical);
        enrgy.rst_eint();
    }
    appr.serializ(pload);
    if(r = (pack_valid.sts == ((uint8_t*)pload->pData)[0])){
        r = !memcmp(pack_valid.rcrds, &((uint8_t*)pload->pData)[1], pload->size_data - 1);
    }
    if(pload->pData) delete[] (uint8_t*)pload->pData;
    
    if(!r) goto __error;
    return;
__error:
    std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
            " testname=test_serializ (test_PackPPR) message=pack_valid != appr.serializ(pload)" << std::endl;
}
void test_next_step(const Clock &clock) {
    std::cout << "test_PackPPR test_next_step" << std::endl;
    
    PackPPR appr;
    
    if(appr.next_step(1684745442) != (1684747242 - 1684745442)) goto __error;
    if(appr.next_step(1684745442, 1700815842) != (1684747242 - 1684745442)) goto __error;
    if(appr.next_step(1684745442, 1671353442) != (1684745442 - 1684747242)) goto __error;
    if(appr.next_step(1684745442, 1684745442) != 0) goto __error;
    
    return;
__error:
    std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
            " testname=test_next_step (test_PackPPR) message=appr.next_step - Not valid" << std::endl;
}
void test_deserializ(const Clock &clock) {
    std::cout << "test_PackPPR test_deserializ" << std::endl;
    bool r = true;
    
    mtr::CE_MDL ce_mdl_init = {
        {0, 0, 0}, {0, 0, 0}, {0, 0, 0, 0}, 
        {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0},
        // -- energy_int
        {1, 2, 3, 4}
    };
    
    Energy enrgy(ce_mdl_init);
    Electrical electrical(Electrical::enInit_230V_1A_45deg);
    time_t tmt = 1499040000;    // 17/7/3T00:00:00
    PAY_LOAD_RAW pload[2] = {{0, -1}, {0, -1}};
    
    PackPPR appr;
    for(int i = 0; i < 5; i++, tmt += 30L * 60){
        appr.add_rcrd(tmt, enrgy, electrical);
    }
    appr.serializ(pload);
    // --
    PackPPR appr_test;
    appr_test.deserializ(pload);
    
    r = (appr != appr_test);
    if(pload->pData) delete[] (uint8_t*)pload->pData;
    
    if(r) goto __error;
    return;
__error:
    std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
            " testname=test_deserializ (test_PackPPR) message=appr != appr_test" << std::endl;
}

int main(int argc, char** argv) {
    Clock time_execute, time_test;
    
    std::cout << "%SUITE_STARTING% test_PackPPR" << std::endl;
    std::cout << "%SUITE_STARTED%" << std::endl;

    
    time_test.start();
    std::cout << "%TEST_STARTED% test_serializ (test_PackPPR)" << std::endl;
    test_serializ(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_serializ (test_PackPPR)" << std::endl;
    
    time_test.start();
    std::cout << "%TEST_STARTED% test_next_step (test_PackPPR)" << std::endl;
    test_next_step(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_next_step (test_PackPPR)" << std::endl;

    time_test.start();
    std::cout << "%TEST_STARTED% test_deserializ (test_PackPPR)" << std::endl;
    test_deserializ(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_deserializ (test_PackPPR)" << std::endl;
    
    std::cout << "%SUITE_FINISHED% time=0" << std::endl;

    return (EXIT_SUCCESS);
}

