/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   test_PackADAY.cpp
 * Author: User
 *
 * Created on 3 ���� 2017 �., 14:15
 */

#include <stdlib.h>
#include <iostream>
#include <time.h>

#include "../../tb_utils/Clock.h"
#include "../PackADAY.h"
#include "../Energy.h"
#include "../archive_scenario.h"

/*
 * Simple C++ Test Suite
 */

void test_serializ(const Clock &clock) {
    std::cout << "test_PackADAY test_serializ" << std::endl;
    bool r = false;
    
    /*
    struct _PACK_GET_ARCH{
        enum enTypeGetArch Type;
        enum enTypeArch TypeArch;
        uint8_t MskTarif;
        DATE ds_bgn, ds_end;
    };
     */
    mtr::ENERGYS_MTR_TRFS enrgy_init = {
        {
        {1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}, 
        {17, 18, 19, 20}, {21, 22, 23, 24}, {25, 26, 27, 28}, {29, 30, 31, 32}
        }
    };
    mtr::PACK_GET_ARCH pack_in = {
        mtr::enType_NoPass, mtr::eARCH_DAY, 0xFF,
        {-1, -1, -1}, {-1, -1, -1}
    };
    struct _PACK_OUT{
        mtr::enStatusPrtcl sts;
        mtr::PACK_ANSW_ARCH_HEAD head;
        mtr::TB_RCRD_ANSW_ARCH_ALLT rcrds[5];
    } pack_valid = {
        mtr::STS_OK, 
        {mtr::eARCH_DAY, 0xFF},
        {
            {{3, 7, 17}, 
            {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}, 
             {17, 18, 19, 20}, {21, 22, 23, 24}, {25, 26, 27, 28}, {29, 30, 31, 32}} },
            {{4, 7, 17}, 
            {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}, 
             {17, 18, 19, 20}, {21, 22, 23, 24}, {25, 26, 27, 28}, {29, 30, 31, 32}} },
            {{5, 7, 17}, 
            {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}, 
             {17, 18, 19, 20}, {21, 22, 23, 24}, {25, 26, 27, 28}, {29, 30, 31, 32}} },
            {{6, 7, 17}, 
            {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}, 
             {17, 18, 19, 20}, {21, 22, 23, 24}, {25, 26, 27, 28}, {29, 30, 31, 32}} },
            {{7, 7, 17}, 
            {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}, 
             {17, 18, 19, 20}, {21, 22, 23, 24}, {25, 26, 27, 28}, {29, 30, 31, 32}} },
        }
    };
    
    Energy enrgy(enrgy_init);
    time_t tmt = 1499040000;    // 17/7/3T00:00:00
    PAY_LOAD_RAW pload[2] = {{0, -1}, {0, -1}};
    
    PackADAY aday(&pack_in);
    for(int i = 0; i < 5; i++, tmt += 24L * 60 * 60){
        aday.add_rcrd(tmt, enrgy);
    }
    aday.serializ(pload);
    if(r = (pack_valid.sts == ((uint8_t*)pload->pData)[0])){
        r = !memcmp(&pack_valid.head, &((uint8_t*)pload->pData)[1], pload->size_data - 1);
    }
    if(pload->pData) delete[] (uint8_t*)pload->pData;
    
    if(!r) goto __error;
    return;
__error:
    std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
            " testname=test_serializ (test_PackADAY) message=pack_valid != aday.serializ(pload)" << std::endl;
}
void test_next_step(const Clock &clock) {
    std::cout << "test_PackAMONTH test_serializ" << std::endl;
    
    PackADAY aday;
    
    if(aday.next_step(1684745442) != (1684831842 - 1684745442)) goto __error;
    if(aday.next_step(1684745442, 1685577600) != (1684831842 - 1684745442)) goto __error;
    if(aday.next_step(1684745442, 1682067042) != (1684745442 - 1684831842)) goto __error;
    if(aday.next_step(1684745442, 1684745442) != 0) goto __error;
    
    return;
__error:
    std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
            " testname=test_next_step (test_PackADAY) message=aday.next_step - Not valid" << std::endl;
}
void test_operator_noteq_aday(const Clock &clock) {
    std::cout << "test_PackADAY test_operator_noteq_aday" << std::endl;
    
    time_t tmt = 1499040000;    // 17/7/3T00:00:00
    PackADAY aday_1, aday_2, aday_3;

    aday_1.Init(PackArchive::enInit_1_year, tmt);
    aday_2.Init(PackArchive::enInit_1_year, tmt);
    aday_3.Init(PackArchive::enInit_1_year, tmt + 24L * 60 * 60);
    
    if(aday_1 != aday_2) goto __error;
    if(aday_1 == aday_3) goto __error;
   
    return;
__error:
    std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
            " testname=test_operator_noteq_aday (test_PackADAY) message=aday != aday_test" << std::endl;
}
void test_deserializ(const Clock &clock) {
    std::cout << "test_PackADAY test_deserializ" << std::endl;
    bool r = true;
    
    mtr::ENERGYS_MTR_TRFS enrgy_init = {
        {
        {1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}, 
        {17, 18, 19, 20}, {21, 22, 23, 24}, {25, 26, 27, 28}, {29, 30, 31, 32}
        }
    };
    mtr::PACK_GET_ARCH pack_in = {
        mtr::enType_NoPass, mtr::eARCH_DAY, 0xFF,
        {-1, -1, -1}, {-1, -1, -1}
    };
    
    Energy enrgy(enrgy_init);
    time_t tmt = 1499040000;    // 17/7/3T00:00:00
    PAY_LOAD_RAW pload[2] = {{0, -1}, {0, -1}};
    
    PackADAY aday(&pack_in);
    for(int i = 0; i < 5; i++, tmt += 24L * 60 * 60){
        aday.add_rcrd(tmt, enrgy);
    }
    aday.serializ(pload);
    // --
    PackADAY aday_test;
    aday_test.deserializ(pload);
    
    r = (aday != aday_test);
    if(pload->pData) delete[] (uint8_t*)pload->pData;
    
    if(r) goto __error;
    return;
__error:
    std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
            " testname=test_deserializ (test_PackADAY) message=aday != aday_test" << std::endl;
}

int main(int argc, char** argv) {
    Clock time_execute, time_test;
    std::cout << "%SUITE_STARTING% test_PackADAY" << std::endl;
    std::cout << "%SUITE_STARTED%" << std::endl;

    time_test.start();
    std::cout << "%TEST_STARTED% test_serializ (test_PackADAY)" << std::endl;
    test_serializ(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_serializ (test_PackADAY)" << std::endl;
    
    time_test.start();
    std::cout << "%TEST_STARTED% test_serializ (test_PackADAY)" << std::endl;
    test_next_step(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_serializ (test_PackADAY)" << std::endl;

    time_test.start();
    std::cout << "%TEST_STARTED% test_operator_noteq_aday (test_PackADAY)" << std::endl;
    test_operator_noteq_aday(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_operator_noteq_aday (test_PackADAY)" << std::endl;
    
    time_test.start();
    std::cout << "%TEST_STARTED% test_deserializ (test_PackADAY)" << std::endl;
    test_deserializ(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_deserializ (test_PackADAY)" << std::endl;
    
    std::cout << "%SUITE_FINISHED% time=0" << std::endl;

    return (EXIT_SUCCESS);
}

