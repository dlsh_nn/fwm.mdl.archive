/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   test_Electrical.cpp
 * Author: User
 *
 * Created on 27 ���� 2017 �., 10:46
 */

#include <stdlib.h>
#include <iostream>
#include <string.h>

#include "../../tb_utils/Clock.h"
#include "../Energy.h"
#include "../Electrical.h"

/*
 * Simple C++ Test Suite
 */

bool operator!=(const mtr::CE_MDL &rigth, const mtr::CE_MDL &left){
    return memcmp(&rigth, &left, sizeof(mtr::CE_MDL));
}
void test_constr_Electrical(const Clock &clock) {
    std::cout << "test_Electrical test_constr_Electrical" << std::endl;
    
    /* mtr::CE_MDL
    int32_t Vmv[NMB_PHASE], Ima[NMB_PHASE], CosF[NMB_PHASE + 1];
    // -- 
    int32_t Pa[NMB_PHASE + 1], Pr[NMB_PHASE + 1], Ps[NMB_PHASE + 1];
    //int32_t sumPa, sumPr, sumPs;
    ENERGY  enrg_int;
    */
    mtr::CE_MDL ce_mdl_init = {
        // -- V, I, Cos
        {230000, 230000, 230000},       // Vmv[NMB_PHASE]
        {1000,   1000,   1000},         // Ima[NMB_PHASE]
        {707,     707,     707,    -1},    // CosF[NMB_PHASE + 1]
        // -- P
        {-1,     -1,     -1,    -1},    // Pa[NMB_PHASE + 1]
        {-1,     -1,     -1,    -1},    // Pr[NMB_PHASE + 1]
        {-1,     -1,     -1,    -1},    // Ps[NMB_PHASE + 1]
        // -- Energy
        {-1,     -1,     -1,    -1}     // ENERGY{Ap, An. Rp, Rn}
    };
    
    mtr::CE_MDL ce_mdl_valid = {
        // -- V, I, Cos
        {230000, 230000, 230000},       // Vmv[NMB_PHASE]
        {1000,   1000,   1000},         // Ima[NMB_PHASE]
        {707,     707,     707,    707},    // CosF[NMB_PHASE + 1]
        // -- P
        {162634, 162634, 162634, 487902},    // Pa[NMB_PHASE + 1]
        {162634, 162634, 162634, 487902},    // Pr[NMB_PHASE + 1]
        {-1,         -1,     -1, 689997},    // Ps[NMB_PHASE + 1]
        // -- Energy
        {0,     0,     0,    0}      // ENERGY{Ap, An. Rp, Rn}
    };
    mtr::CE_MDL ce_mdl_out;
    
    Electrical electro(ce_mdl_init);
    // -- 
    ce_mdl_out = electro.GetCE();
    if(ce_mdl_out != ce_mdl_valid) goto __error;
    // --
    ce_mdl_out = electro.GetCE(ce_mdl_init);
    ce_mdl_valid.enrg_int = ce_mdl_init.enrg_int;
    if(ce_mdl_out != ce_mdl_valid) goto __error;
        
    return;
__error:
        std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
                " testname=test_constr_Electrical (test_Electrical) message=ce_mdl_out != ce_mdl_valid" << std::endl;
}
void test_constr_Electrical_enTypeInit(const Clock &clock) {
    std::cout << "test_constr_Electrical_enTypeInit test_constr_Electrical" << std::endl;
    
    /* mtr::CE_MDL
    int32_t Vmv[NMB_PHASE], Ima[NMB_PHASE], CosF[NMB_PHASE + 1];
    // -- 
    int32_t Pa[NMB_PHASE + 1], Pr[NMB_PHASE + 1], Ps[NMB_PHASE + 1];
    //int32_t sumPa, sumPr, sumPs;
    ENERGY  enrg_int;
    */
    mtr::CE_MDL ce_mdl_valid = {
        // -- V, I, Cos
        {230000, 230000, 230000},       // Vmv[NMB_PHASE]
        {1000,   1000,   1000},         // Ima[NMB_PHASE]
        {707,     707,     707,    707},    // CosF[NMB_PHASE + 1]
        // -- P
        {162634, 162634, 162634, 487902},    // Pa[NMB_PHASE + 1]
        {162634, 162634, 162634, 487902},    // Pr[NMB_PHASE + 1]
        {-1,         -1,     -1, 689997},    // Ps[NMB_PHASE + 1]
        // -- Energy
        {0,     0,     0,    0}     // ENERGY{Ap, An. Rp, Rn}
    };
    mtr::CE_MDL ce_mdl_out;
    
    Electrical electro(Electrical::enInit_230V_1A_45deg);
    // -- 
    ce_mdl_out = electro.GetCE();
    if(ce_mdl_out != ce_mdl_valid) goto __error;
        
    return;
__error:
        std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
                " testname=test_constr_Electrical (test_Electrical) message=ce_mdl_out != ce_mdl_valid" << std::endl;
}
void test_Electrical_SetAll(const Clock &clock) {
    std::cout << "test_Electrical_SetAll test_constr_Electrical" << std::endl;
    
    Electrical electro(Electrical::enInit_230V_1A_45deg);
    mtr::CE_MDL ce_mdl_out;
    //------------------------------------------------------------------------
    // electro.SetCosF
    //------------------------------------------------------------------------
    {
        mtr::CE_MDL ce_mdl_valid = {
            // -- V, I, Cos
            {230000, 230000, 230000},       // Vmv[NMB_PHASE]
            {1000,   1000,   1000},         // Ima[NMB_PHASE]
            {707,     707,     707,    707},    // CosF[NMB_PHASE + 1]
            // -- P
            {162634, 162634, 162634, 487902},    // Pa[NMB_PHASE + 1]
            {-162634, -162634, -162634, -487902},    // Pr[NMB_PHASE + 1]
            {-1,         -1,     -1, 689997},    // Ps[NMB_PHASE + 1]
            // -- Energy
            {0,     0,     0,    0}     // ENERGY{Ap, An. Rp, Rn}
        };
        // -- 
        electro.SetCosF(Electrical::phAll, -45);
        ce_mdl_out = electro.GetCE();
        if(ce_mdl_out != ce_mdl_valid) goto __error;
    }
    //------------------------------------------------------------------------
    // electro.SetVmv
    //------------------------------------------------------------------------
    {
        mtr::CE_MDL ce_mdl_valid = {
            // -- V, I, Cos
            {23000, 23000, 23000},       // Vmv[NMB_PHASE]
            {1000,   1000,   1000},         // Ima[NMB_PHASE]
            {707,     707,     707,    707},    // CosF[NMB_PHASE + 1]
            // -- P
            {16263, 16263, 16263, 48789},    // Pa[NMB_PHASE + 1]
            {-16263, -16263, -16263, -48789},    // Pr[NMB_PHASE + 1]
            {-1,         -1,     -1, 68997},    // Ps[NMB_PHASE + 1]
            // -- Energy
            {0,     0,     0,    0}     // ENERGY{Ap, An. Rp, Rn}
        };
        electro.SetV(Electrical::phAll, 23000);
        ce_mdl_out = electro.GetCE();
        if(ce_mdl_out != ce_mdl_valid) goto __error;
    }//------------------------------------------------------------------------
    // electro.SetI
    //------------------------------------------------------------------------
    {
        mtr::CE_MDL ce_mdl_valid = {
            // -- V, I, Cos
            {23000, 23000, 23000},       // Vmv[NMB_PHASE]
            {100,   100,   100},         // Ima[NMB_PHASE]
            {707,     707,     707,    707},    // CosF[NMB_PHASE + 1]
            // -- P
            {1626, 1626, 1626, 4878},    // Pa[NMB_PHASE + 1]
            {-1626, -1626, -1626, -4878},    // Pr[NMB_PHASE + 1]
            {-1,         -1,     -1, 6897},    // Ps[NMB_PHASE + 1]
            // -- Energy
            {0,     0,     0,    0}     // ENERGY{Ap, An. Rp, Rn}
        };
        electro.SetI(Electrical::phAll, 100);
        ce_mdl_out = electro.GetCE();
        if(ce_mdl_out != ce_mdl_valid) goto __error;
    }
    
    
    return;
__error:
        std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
                " testname=test_Electrical_SetAll (test_Electrical) message=ce_mdl_out != ce_mdl_valid" << std::endl;
}
void test_Electrical_GetPOWER(const Clock &clock) {
    std::cout << "test_Electrical test_Electrical_GetPOWER" << std::endl;
    
    /* mtr::CE_MDL
    int32_t Vmv[NMB_PHASE], Ima[NMB_PHASE], CosF[NMB_PHASE + 1];
    // -- 
    int32_t Pa[NMB_PHASE + 1], Pr[NMB_PHASE + 1], Ps[NMB_PHASE + 1];
    //int32_t sumPa, sumPr, sumPs;
    ENERGY  enrg_int;
    */
    mtr::CE_MDL ce_mdl_init = {
        // -- V, I, Cos
        {230000, 230000, 230000},       // Vmv[NMB_PHASE]
        {1000,   1000,   1000},         // Ima[NMB_PHASE]
        {707,     707,     707,    -1},    // CosF[NMB_PHASE + 1]
        // -- P
        {-1,     -1,     -1,    -1},    // Pa[NMB_PHASE + 1]
        {-1,     -1,     -1,    -1},    // Pr[NMB_PHASE + 1]
        {-1,     -1,     -1,    -1},    // Ps[NMB_PHASE + 1]
        // -- Energy
        {-1,     -1,     -1,    -1}     // ENERGY{Ap, An. Rp, Rn}
    };
    
    mtr::POWER pwr_valid = {487902, 0, 487902, 0};
    
    Electrical electro(ce_mdl_init);
    // -- 
    if(pwr_valid != electro.GetPOWER()) goto __error;
        
    return;
__error:
        std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
                " testname=test_constr_Electrical (test_Electrical) message=ce_mdl_out != ce_mdl_valid" << std::endl;
}
void test_Electrical_Cnstr_ELECTRO_SCNR(const Clock &clock) {
    std::cout << "test_constr_Electrical_enTypeInit test_constr_Electrical" << std::endl;
    
    // struct ELECTRO_SCNR int32_t Vmv[NMB_PHASE], Ima[NMB_PHASE], angl[NMB_PHASE];
    ELECTRO_SCNR prm_ElNet_init = {
        {230000, 230000, 230000},       // Vmv[NMB_PHASE]
        {1000,   1000,   1000},         // Ima[NMB_PHASE]
        {45,     45,     45,},          // CosF[NMB_PHASE + 1]
    };
    mtr::CE_MDL ce_mdl_valid = {
        // -- V, I, Cos
        {230000, 230000, 230000},       // Vmv[NMB_PHASE]
        {1000,   1000,   1000},         // Ima[NMB_PHASE]
        {707,     707,     707,    707},    // CosF[NMB_PHASE + 1]
        // -- P
        {162634, 162634, 162634, 487902},    // Pa[NMB_PHASE + 1]
        {162634, 162634, 162634, 487902},    // Pr[NMB_PHASE + 1]
        {-1,         -1,     -1, 689997},    // Ps[NMB_PHASE + 1]
        // -- Energy
        {0,     0,     0,    0}     // ENERGY{Ap, An. Rp, Rn}
    };
    Electrical electro(prm_ElNet_init);
    // -- 
    if(ce_mdl_valid != electro.GetCE()) goto __error;
        
    return;
__error:
        std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
                " testname=test_constr_Electrical (test_Electrical) message=ce_mdl_out != ce_mdl_valid" << std::endl;
}
void check_valid_diviation_less(int32_t *pval, int32_t *pval_nom, int32_t *pflasg, int NMB = NMB_PHASE){
    for(int i = 0; i < NMB; i++){
        if((pval[i] < pval_nom[i]) &&
           (labs(pval[i] - pval_nom[i]) <= (pval_nom[i] / 10)) ){
            pflasg[i] = 0;
        }
        
    }
}
void check_valid_diviation_more(int32_t *pval, int32_t *pval_nom, int32_t *pflasg, int NMB = NMB_PHASE){
    for(int i = 0; i < NMB; i++){
        if((pval[i] > pval_nom[i]) &&
           (labs(pval[i] - pval_nom[i]) <= (pval_nom[i] / 10)) ){
            pflasg[i] = 0;
        }
        
    }
}
void test_Electrical_random_val(const Clock &clock) {
    std::cout << "test_Electrical_random_val test_constr_Electrical" << std::endl;
    
    mtr::CE_MDL ce_mdl_nom = {
        // -- V, I, Cos
        {230000, 230000, 230000},       // Vmv[NMB_PHASE]
        {1000,   1000,   1000},         // Ima[NMB_PHASE]
        {707,     707,     707,    707},    // CosF[NMB_PHASE + 1]
        // -- P
        {162634, 162634, 162634, 487902},    // Pa[NMB_PHASE + 1]
        {162634, 162634, 162634, 487902},    // Pr[NMB_PHASE + 1]
        {-1,         -1,     -1, 689997},    // Ps[NMB_PHASE + 1]
    };
    Electrical electro(Electrical::enInit_230V_1A_45deg);
    mtr::CE_MDL fvalid, fless_nom, fmore_nom, ce_mdl_out;
    
    memset(&fvalid, 0, sizeof(fvalid));
    memset(&fless_nom, 0, sizeof(fless_nom));
    memset(&fless_nom, -1, offsetof(mtr::CE_MDL, CosF[NMB_PHASE]));
    memset(&fmore_nom, 0, sizeof(fmore_nom));
    memset(&fmore_nom, -1, offsetof(mtr::CE_MDL, CosF[NMB_PHASE]));
    
    for(int i = 0; i < 1000; i++){
        electro.Generate(10);
        ce_mdl_out = electro.GetCE();
        // -- Less
        check_valid_diviation_less(ce_mdl_out.Vmv, ce_mdl_nom.Vmv, fless_nom.Vmv);
        check_valid_diviation_less(ce_mdl_out.Ima, ce_mdl_nom.Ima, fless_nom.Ima);
        check_valid_diviation_less(ce_mdl_out.CosF, ce_mdl_nom.CosF, fless_nom.CosF);
        // -- More
        check_valid_diviation_more(ce_mdl_out.Vmv, ce_mdl_nom.Vmv, fmore_nom.Vmv);
        check_valid_diviation_more(ce_mdl_out.Ima, ce_mdl_nom.Ima, fmore_nom.Ima);
        check_valid_diviation_more(ce_mdl_out.CosF, ce_mdl_nom.CosF, fmore_nom.CosF);
    }
    if(memcmp(&fless_nom, &fvalid, sizeof(fless_nom)) || memcmp(&fmore_nom, &fvalid, sizeof(fless_nom))){
        goto __error;
    }
        
    return;
__error:
        std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
                " testname=test_Electrical_random_val (test_Electrical) message=Algorithm random_val ERROR" << std::endl;
}

int main(int argc, char** argv) {
    Clock time_execute, time_test;
    
    std::cout << "%SUITE_STARTING% test_Electrical" << std::endl;
    std::cout << "%SUITE_STARTED%" << std::endl;

    time_test.start();
    std::cout << "%TEST_STARTED% test_constr_Electrical (test_Electrical)" << std::endl;
    test_constr_Electrical(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_constr_Electrical (test_Electrical)" << std::endl;
    
    time_test.start();
    std::cout << "%TEST_STARTED% test_constr_Electrical_enTypeInit (test_Electrical)" << std::endl;
    test_constr_Electrical_enTypeInit(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_constr_Electrical_enTypeInit (test_Electrical)" << std::endl;
    
    time_test.start();
    std::cout << "%TEST_STARTED% test_Electrical_SetAll (test_Electrical)" << std::endl;
    test_Electrical_SetAll(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_Electrical_SetAll (test_Electrical)" << std::endl;
    
    time_test.start();
    std::cout << "%TEST_STARTED% test_Electrical_GetPOWER (test_Electrical)" << std::endl;
    test_Electrical_GetPOWER(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_Electrical_GetPOWER (test_Electrical)" << std::endl;
    
    time_test.start();
    std::cout << "%TEST_STARTED% test_Electrical_Cnstr_ELECTRO_SCNR (test_Electrical)" << std::endl;
    test_Electrical_Cnstr_ELECTRO_SCNR(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_Electrical_Cnstr_ELECTRO_SCNR (test_Electrical)" << std::endl;
    
    time_test.start();
    std::cout << "%TEST_STARTED% test_Electrical_random_val (test_Electrical)" << std::endl;
    test_Electrical_random_val(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_Electrical_random_val (test_Electrical)" << std::endl;

    std::cout << "%SUITE_FINISHED% time=" << time_execute.end() << std::endl;

    return (EXIT_SUCCESS);
}

