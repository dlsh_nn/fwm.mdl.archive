/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   test_tb_utils.cpp
 * Author: User
 *
 * Created on 7 ���� 2017 �., 7:35
 */

#include <stdlib.h>
#include <iostream>

#include "../../tb_utils/Clock.h"
#include "../tb_utils.h"

/*
 * Simple C++ Test Suite
 */

void test_read_archive_MONTH(const Clock &clock) {
    std::cout << "test_PackAMONTH test_tb_utils" << std::endl;
    
    mtr::PACK_ANSW_BGN_DS_ARCH bgn = {mtr::STS_OK, {}};
    
    PackAMONTH amonth;
    //------------------------------------------------------------------------
    // -- tb_utils
    //------------------------------------------------------------------------
    tb_utils_cls tbu;
    // -- Init Time
    mtr::rtc_tbl.crnt_tmt = tmt_DTS_TB_begin;
    mtr::GetDts_tmt(mtr::rtc_tbl.crnt_tmt, &mtr::rtc_tbl.crnt_dts);
    // --
    mtr::InitArch();

    // TODO: very very difficult, leave.
    
    return;
__error:
    std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
            " testname=test_read_archive_MONTH (test_tb_utils) message= - Not valid" << std::endl;
}

int main(int argc, char** argv) {
    Clock time_execute, time_test;
    
    std::cout << "%SUITE_STARTING% test_tb_utils" << std::endl;
    std::cout << "%SUITE_STARTED%" << std::endl;

    time_test.start();
    std::cout << "%TEST_STARTED% test_serializ (test_tb_utils)" << std::endl;
    test_read_archive_MONTH(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_read_archive_MONTH (test_tb_utils)" << std::endl;

    std::cout << "%SUITE_FINISHED% time=0" << std::endl;

    return (EXIT_SUCCESS);
}

