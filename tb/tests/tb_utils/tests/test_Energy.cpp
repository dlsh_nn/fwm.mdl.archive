/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   test_Energy.cpp
 * Author: User
 *
 * Created on 28 ���� 2017 �., 16:15
 */

#include <stdlib.h>
#include <iostream>
#include <time.h>

#include "../../tb_utils/Clock.h"
#include "../Electrical.h"
#include "../Energy.h"

/*
 * Simple C++ Test Suite
 */
void test_Calculate(const Clock &clock) {
    std::cout << "test_Energy test_Calculate" << std::endl;
    
    Electrical electro(Electrical::enInit_230V_1A_45deg);
    Energy enrgy;
    /* Ap, An, Rp, Rn */
    mtr::ENERGY enrgy_valid = {2, 0, 2, 0};
    
    enrgy.Calculate(15, electro);
    if(enrgy_valid != enrgy.Get()) goto __error;
    
    enrgy.Calculate(60 * 60, electro);
    enrgy_valid.Ap = enrgy_valid.Rp = 489;
    if(enrgy_valid != enrgy.Get()) goto __error;
    // -- +90 deg
    electro.SetCosF(Electrical::phAll, 135);
    enrgy.Calculate(60 * 60, electro);
    enrgy_valid.An = 487;
    enrgy_valid.Rp = 977;
    if(enrgy_valid != enrgy.Get()) goto __error;
    // -- +90 deg
    electro.SetCosF(Electrical::phAll, 225);
    enrgy.Calculate(60 * 60, electro);
    enrgy_valid.An = 975;
    enrgy_valid.Rn = 487;
    if(enrgy_valid != enrgy.Get()) goto __error;
    // -- +90 deg
    electro.SetCosF(Electrical::phAll, 315);
    enrgy.Calculate(60 * 60, electro);
    enrgy_valid.Rp = 977;
    enrgy_valid.Rn = 975;
    enrgy_valid.Ap = 977;
    if(enrgy_valid != enrgy.Get()) goto __error;
    
    return;
__error:
    std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
            " testname=test_Calculate (test_Energy) message=enrgy_valid != enrgy.Get()" << std::endl;
}
void test_Calculate_return_ENERGY(const Clock &clock) {
    std::cout << "test_Energy test_Calculate_return_ENERGY" << std::endl;
    
    Electrical electro(Electrical::enInit_230V_1A_45deg);
    Energy enrgy;
    /* Ap, An, Rp, Rn */
    mtr::ENERGY enrgy_valid = {2, 0, 2, 0};
    
    if(enrgy_valid != enrgy.Calculate(15, electro)) goto __error;
   
    return;
__error:
    std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
            " testname=test_Calculate_return_ENERGY (test_Energy) message=enrgy_valid != enrgy.Get()" << std::endl;
}
void test_constructor_CE_MDL(const Clock &clock) {
    std::cout << "test_Energy test_constructor_CE_MDL" << std::endl;
    
    mtr::CE_MDL ce_mdl_init = {
        {0, 0, 0}, {0, 0, 0}, {0, 0, 0, 0}, 
        {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, 
        // -- energy_int
        {1, 2, 3, 4}
    };
    
    Energy enrgy(ce_mdl_init);
    if(ce_mdl_init.enrg_int != enrgy.Get()) goto __error;
    
    return;
__error:
    std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
            " testname=test_constructor_CE_MDL (test_Energy) message=enrgy_valid != enrgy.Get()" << std::endl;
}
void test_Get_indxTarif(const Clock &clock) {
    std::cout << "test_Energy test_Get_indxTarif" << std::endl;
    
    Electrical electro(Electrical::enInit_230V_1A_45deg);
    Energy enrgy;
    /* Ap, An, Rp, Rn */
    mtr::ENERGY enrgy_valid[NMB_TARIF] = {
        {4, 0, 4, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, 
        {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0} 
    };
    
    enrgy.Calculate(15, electro);
    enrgy.rst_eint();
    enrgy.Calculate(15, electro);
    for(int i = 0; i < NMB_TARIF; i++){
        if(enrgy_valid[i] != enrgy.Get(i)) goto __error;
    }
   
    return;
__error:
    std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
            " testname=test_Get_indxTarif (test_Energy) message=enrgy_valid != enrgy.Get(indxTarif)" << std::endl;
}
void test_construct_ETarif(const Clock &clock) {
    std::cout << "test_Energy test_construct_ETarif" << std::endl;
    
    Electrical electro(Electrical::enInit_230V_1A_45deg);
    
    mtr::ENERGYS_MTR_TRFS enrgy_init = {
        {
        {1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}, 
        {17, 18, 19, 20}, {21, 22, 23, 24}, {25, 26, 27, 28}, {29, 30, 31, 32}
        }
    };
    mtr::ENERGYS_MTR_TRFS enrgy_valid = {
        {
        {5, 2, 7, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}, 
        {17, 18, 19, 20}, {21, 22, 23, 24}, {25, 26, 27, 28}, {29, 30, 31, 32} 
        }
    };
    Energy enrgy(enrgy_init);
    enrgy.Calculate(15, electro);
    enrgy.rst_eint();
    enrgy.Calculate(15, electro);
    for(int i = 0; i < NMB_TARIF; i++){
        if(enrgy_valid.enrg[i] != enrgy.Get(i)) goto __error;
    }
   
    return;
__error:
    std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
            " testname=test_construct_ETarif (test_Energy) message=enrgy_valid != enrgy.Get(indxTarif)" << std::endl;
}
void test_Calculate_Long(const Clock &clock) {
    std::cout << "test_Energy test_Calculate_Long" << std::endl;
    
    Electrical electro(Electrical::enInit_230V_1A_45deg);
    Energy enrgy_short, enrgy_long;
    time_t Tlong = 6 * 365 * 24 * 60 * 60;
    time_t Tstep = 15 * 60;
    
    enrgy_long.Calculate(Tlong, electro);
    for(time_t tmt = 0; tmt < Tlong; tmt += Tstep){
        enrgy_short.Calculate(Tstep, electro);
        enrgy_short.rst_eint();
    }
    
    if(enrgy_short.Get(0) != enrgy_long.Get(0)) goto __error;
   
    return;
__error:
    std::cout << "%TEST_FAILED% time=" << clock.crnt_elapsedTiem() << 
            " testname=test_Calculate_return_ENERGY (test_Energy) message=enrgy_valid != enrgy.Get()" << std::endl;
}
int main(int argc, char** argv) {
    Clock time_execute, time_test;
    
    std::cout << "%SUITE_STARTING% test_Electrical" << std::endl;
    std::cout << "%SUITE_STARTED%" << std::endl;

    time_test.start();
    std::cout << "%TEST_STARTED% test_constr_Electrical (test_Electrical)" << std::endl;
    test_Calculate(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_constr_Electrical (test_Electrical)" << std::endl;
    
    time_test.start();
    std::cout << "%TEST_STARTED% test_Calculate_return_ENERGY (test_Electrical)" << std::endl;
    test_Calculate_return_ENERGY(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_Calculate_return_ENERGY (test_Electrical)" << std::endl;
    
    time_test.start();
    std::cout << "%TEST_STARTED% test_constructor_CE_MDL (test_Electrical)" << std::endl;
    test_constructor_CE_MDL(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_constructor_CE_MDL (test_Electrical)" << std::endl;

    time_test.start();
    std::cout << "%TEST_STARTED% test_Get_indxTarif (test_Electrical)" << std::endl;
    test_Get_indxTarif(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_Get_indxTarif (test_Electrical)" << std::endl;
    
    time_test.start();
    std::cout << "%TEST_STARTED% test_construct_ETarif (test_Electrical)" << std::endl;
    test_construct_ETarif(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_construct_ETarif (test_Electrical)" << std::endl;
    
    time_test.start();
    std::cout << "%TEST_STARTED% test_Calculate_Long (test_Electrical)" << std::endl;
    test_Calculate_Long(time_test);
    std::cout << "%TEST_FINISHED% time=" << time_test.end() << " test_Calculate_Long (test_Electrical)" << std::endl;
    
    std::cout << "%SUITE_FINISHED% time=" << time_execute.end() << std::endl;

    return (EXIT_SUCCESS);
}

