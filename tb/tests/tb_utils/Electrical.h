/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Electrical.h
 * Author: User
 *
 * Created on 27 июня 2017 г., 8:07
 */

#ifndef ELECTRICAL_H
#define ELECTRICAL_H

#include <stdint.h>
#include <string.h>
#include <string>
#include <stdlib.h>
#include <math.h>

#include "define.h"
#include "archive_scenario.h"

class Electrical{

#define NMB_PHASE 3
    
public:
    enum enTypeInit{ enInit_230V_1A_45deg };
    enum enPhase{phA, phB, phC, phAll};
private:
    struct _ELECTR_PRM{
        int32_t Vmv[NMB_PHASE], Ima[NMB_PHASE], CosF[NMB_PHASE + 1];
        int32_t Pa[NMB_PHASE + 1], Pr[NMB_PHASE + 1], Ps[NMB_PHASE + 1];
        // --
        int32_t anglPh[NMB_PHASE + 1];
    }prm, prm_random;
    bool fRandom;
    
public:
    Electrical();
    Electrical(enTypeInit type);
    Electrical(const mtr::CE_MDL &ce_mdl);
    Electrical(const ELECTRO_SCNR &prm_ElNet);
    Electrical(const Electrical& origin);
    
    void Generate(float diviation);
    void Set(const mtr::CE_MDL &ce_mdl);
    void Set(const ELECTRO_SCNR &prm_ElNet);
    // -- Set
    void SetV(enPhase ph, int32_t val){SetV((uint8_t)ph, val);}
    void SetV(uint8_t i, int32_t val){
        SetPrm(i, prm.Vmv, val, "(i > NMB_PHASE) in SetV class Electrical");
    }
    void SetI(enPhase ph, int32_t val){SetI((uint8_t)ph, val);}
    void SetI(uint8_t i, int32_t val){
        SetPrm(i, prm.Ima, val, "(i > NMB_PHASE) in SetI class Electrical");
    }
    void SetCosF(enPhase ph, int32_t val){SetCosF((uint8_t)ph, val);}
    void SetCosF(uint8_t i, int32_t val){
        SetPrm(i, prm.anglPh, val, "(i > NMB_PHASE) in SetCosF class Electrical");
    }
    // -- Get
    int32_t GetV(uint8_t i) const{return (i < NMB_PHASE) ? prm.Vmv[i] : 
        throw std::string("(i >= NMB_PHASE) in GetV class Electrical"); }
    int32_t GetI(uint8_t i) const{return (i < NMB_PHASE) ? prm.Ima[i] : 
        throw std::string("(i >= NMB_PHASE) in GetI class Electrical"); }
    int32_t GetCosF(uint8_t i) const{return (i <= NMB_PHASE) ? prm.CosF[i] : 
        throw std::string("(i > NMB_PHASE + 1) in GetCosF class Electrical"); }
    int32_t GetPa(uint8_t i) const{return (i <= NMB_PHASE) ? prm.Pa[i] : 
        throw std::string("(i >= NMB_PHASE) in GetPa class Electrical"); }
    int32_t GetPr(uint8_t i) const{return (i <= NMB_PHASE) ? prm.Pr[i] : 
        throw std::string("(i >= NMB_PHASE) in GetPr class Electrical"); }
    int32_t GetPs(uint8_t i){return (i <= NMB_PHASE) ? prm.Ps[i] : 
        throw std::string("(i >= NMB_PHASE) in GetPs class Electrical"); }
    mtr::CE_MDL GetCE();
    mtr::CE_MDL GetCE(const mtr::CE_MDL &ce_mdl);
    mtr::POWER GetPOWER() const;
    
private:
    void CalclElectricalPrm();
    void SetPrm(uint8_t i, int32_t* pprm_val, int32_t val, std::string mess_throw, bool fCalcl = true);
    int32_t random_val(int32_t val, int32_t diviation);
};

#endif /* ELECTRICAL_H */

