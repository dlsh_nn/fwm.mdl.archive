/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PackADAY.h
 * Author: User
 *
 * Created on 1 июля 2017 г., 19:15
 */

#ifndef PACKADAY_H
#define PACKADAY_H

#include "PackArchive.h"

class PackADAY : public PackArchive{
public:
    PackADAY() : PackArchive::PackArchive(){  };
    PackADAY(mtr::LPCPACK_GET_ARCH pack_in) : PackArchive::PackArchive(pack_in){};
    
    void Init(enTypeInitArchive typeInit, time_t tmt_bgn){PackArchive::Init(typeInit, tmt_bgn);}
    void deserializ(const void* pData, uint32_t size_data){
        PackArchive::deserializ(pData, size_data);
    }
    void deserializ(LPCPAY_LOAD_RAW pload){PackArchive::deserializ(pload);}
    time_t next_step(time_t tmt_crnt){return 24 * 60 * 60L;};
    time_t next_step(time_t tmt_crnt, time_t tmt_direction){
        return PackArchive::next_step(tmt_crnt, tmt_direction);
    }
};

#endif /* PACKADAY_H */

