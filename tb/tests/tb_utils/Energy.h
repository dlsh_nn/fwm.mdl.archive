/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Energy.h
 * Author: User
 *
 * Created on 27 ���� 2017 �., 8:01
 */

#ifndef ENERGY_H
#define ENERGY_H

#include <stdint.h>
#include <string.h> 
#include <time.h>
#include <stdlib.h>

#include "define.h"
#include "Electrical.h"
#include <vector>

typedef std::vector<mtr::ENERGY> vENERGY;

class Energy {
public:
    Energy();
    Energy(const mtr::CE_MDL &ce_mdl);
    Energy(const mtr::ENERGYS_MTR_TRFS &enrg_trfs);

    mtr::ENERGY Calculate(time_t Time, const Electrical& electro);
    mtr::ENERGY Get()const{return enrg_int;};
    mtr::ENERGY Get(uint8_t indxTarif) const;
    void reset(){
        memset(&enrg_int, 0, sizeof(enrg_int));
        memset(&enrg_fract, 0, sizeof(enrg_fract));
        memset(&ETarif, 0, sizeof(ETarif));
    }
    
    void setCorrupt () {
       memset(&enrg_int, -1, sizeof(enrg_int));
        memset(&enrg_fract, -1, sizeof(enrg_fract));
        memset(&ETarif, -1, sizeof(ETarif)); 
    }
    
    void rst_eint(){
        memset(&enrg_int, 0, sizeof(enrg_int));
    }
private:
    void CalclEnrg(const time_t& Tperiod, const int32_t P, int32_t& E_int, int32_t& E_fract);
    
private:
    /*struct _ENRG{
        int32_t Ap, An;
        int32_t Rp, Rn;
    };*/
    mtr::ENERGY enrg_int, enrg_fract;
    mtr::ENERGYS_MTR_TRFS ETarif;
};

extern mtr::ENERGY& operator+=(mtr::ENERGY& left, const mtr::ENERGY& rigth);
extern mtr::ENERGY& operator+=(mtr::ENERGY& left, const vENERGY& rigth);
extern mtr::ENERGY operator-(const mtr::ENERGY& left, const mtr::ENERGY& rigth);
extern mtr::ENERGY operator-(const vENERGY& left, const vENERGY& rigth);
extern bool operator!=(const mtr::ENERGY &left, const mtr::ENERGY &rigth);
extern bool operator==(const mtr::ENERGY &left, const mtr::ENERGY &rigth);
extern mtr::ENERGY sum(const vENERGY& venrgy);

#endif /* ENERGY_H */

