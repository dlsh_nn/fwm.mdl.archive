/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Clock.h
 * Author: User
 * SRC URL: https://ru.stackoverflow.com/a/75438
 * Created on 23 июня 2017 г., 8:00
 */

#ifndef CLOCK_H
#define CLOCK_H

#ifndef _OPENMP
    #warning "Компилировать с опцией -fopenmp (g++ -fopenmp ...)!!!"
#endif

#include <omp.h>    // Компилировать с опцией -fopenmp (g++ -fopenmp ...)
#include <algorithm>
#include <string>

class Clock {
private: 
    double t_start, t_end;
public:
    Clock(){t_start = omp_get_wtime();}
    void start(){t_start = omp_get_wtime(); t_end = 0;}
    double end(){
        t_end = omp_get_wtime();
        return get_delta();
    }
    double elapsedTime() {return get_delta();}
    double crnt_elapsedTiem() const{
        if(t_end) throw std::string("ERROR!!!  t_end != 0 in Clock::crnt_elapsedTiem()");
        return get_delta(omp_get_wtime());
    }
private:
    double get_delta(){return std::max(t_end - t_start, 0.01);}
    double get_delta(double t_end) const{return std::max(t_end - t_start, 0.01);}
};

#endif /* CLOCK_H */

