/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ConversionType_to_Name.h
 * Author: User
 *
 * Created on 1 июля 2017 г., 16:46
 */

#ifndef CONVERSIONTYPE_TO_NAME_H
#define CONVERSIONTYPE_TO_NAME_H

#include <stdint.h>
#include <string>
#include <iostream>
#include "define.h"

struct _TB_CONVERSION_TypeMtr_to_Name{
    int32_t ValType;
    std::string name;
};
typedef _TB_CONVERSION_TypeMtr_to_Name TB_CONVERSION_TypeMtr_to_Name;
typedef const TB_CONVERSION_TypeMtr_to_Name *LPCTB_CONVERSION_TypeMtr_to_Name;

class ConversionType_to_Name {
public:

private:
    
};
    std::ostream& operator<< (std::ostream& out, const mtr::enStatusPrtcl &sts);
    std::ostream& operator<< (std::ostream& out, const mtr::enCmdPrtcl &cmd);
    std::ostream& operator<< (std::ostream& out, const mtr::enTypeArch &TypeArch);
    
#endif /* CONVERSIONTYPE_TO_NAME_H */

