/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PackArchive.cpp
 * Author: User
 * 
 * Created on 3 июля 2017 г., 17:38
 */

#include <iostream>
#include <fstream>
#include "PackArchive.h"

PackArchive::PackArchive(mtr::LPCPACK_GET_ARCH pack_in){
    Init(pack_in);
}
void PackArchive::reset(){
    memset(&head, 0, sizeof(head));
    // -- default
    sts = mtr::STS_OK;
    head.MskTarif = 0xFF;
    // --
    reset_rcrds();
}
void PackArchive::reset_rcrds(){
    vARCH::iterator it_arch;
    for(it_arch = rcrds.begin(); it_arch != rcrds.end(); it_arch++){
        it_arch->enrg.clear();        
    }
    rcrds.clear();
}
void PackArchive::Init(enTypeInitArchive typeInit, time_t tmt_bgn){
    if(typeInit == enInit_1_year){
        Electrical electrical(Electrical::enInit_230V_1A_45deg);
        Energy enrgy;
        
        for(time_t tmt = tmt_bgn; tmt < tmt_bgn + 1L * 365 * 24 * 60 * 60; tmt += next_step(tmt)){
            if(GetTypeMark() == enMark_End) add_rcrd(tmt, enrgy, electrical);
            enrgy.Calculate(next_step(tmt), electrical);
            if(GetTypeMark() == enMark_Begin) add_rcrd(tmt, enrgy, electrical);
            enrgy.rst_eint();
        }
    }else throw "ERROR!!! Value typeInit in PackPPR::PackPPR(enTypeInitArchive typeInit, time_t tmt_bgn)";
}
void PackArchive::Init(const void* ppack_in_raw){
    mtr::LPCPACK_GET_ARCH pack_in = (mtr::LPCPACK_GET_ARCH)ppack_in_raw;
    // --
    sts = mtr::STS_OK;
    head.TypeArch = pack_in->TypeArch;
    head.MskTarif = pack_in->MskTarif;
    // --
    TypeAnswer = pack_in->Type;
    tmt_bgn = mtr::GetTmt_ds(&pack_in->ds_bgn); 
    tmt_end = mtr::GetTmt_ds(&pack_in->ds_end);
}
void PackArchive::add_rcrd(time_t tmt, const Energy& enrg){
    RCRD_ANSW_ARCH rcrd;
    uint8_t msk = 0x01;
    
    mtr::GetDs_tmt(tmt, &rcrd.ds);
    for(int i = 0; i < NMB_TARIF; i++, msk <<= 1){
        if(msk & head.MskTarif){
            rcrd.enrg.push_back(enrg.Get(i));
        }
    }
    rcrds.push_back(rcrd);
}

void PackArchive::add_corrupt_rcrd(time_t tmt) {
    RCRD_ANSW_ARCH rcrd;
    uint8_t msk = 0x01;

    Energy enrg;
    enrg.setCorrupt();
    
    mtr::GetDs_tmt(tmt, &rcrd.ds);
    for(int i = 0; i < NMB_TARIF; i++, msk <<= 1){
        if(msk & head.MskTarif){
            rcrd.enrg.push_back(enrg.Get(i));
        }
    }
    rcrds.push_back(rcrd);
}

void PackArchive::serializ(LPPAY_LOAD_RAW pload){
    pload->size_data = getSize();
    
    if(pload->pData || (pload + 1)->pData) throw "ERROR!!! pload[xx].pData != 0 in PackArchive::serializ";
    
    pload->pData = new uint8_t[pload->size_data];
    (pload + 1)->pData = 0, (pload + 1)->size_data = -1;
    // --
    serializ_children(pload);
}
void PackArchive::deserializ(LPCPAY_LOAD_RAW pload){
    RCRD_ANSW_ARCH rcrd_arch;
    mtr::ENERGY rcrd_enrgy;
    int32_t size = pload->size_data - (1 + sizeof(head));
    uint8_t* pData = (uint8_t*)pload->pData + 1;
    uint8_t* pData_bgn_rcrd;
    
    if(!pload->pData) throw "ERROR!!! pload->pData == 0 in PackPPR::deserializ.";
    
    reset_rcrds();
    memcpy(&head, pData, sizeof(head)), pData += sizeof(head);
    while(size > 0){
        rcrd_arch.enrg.clear();
        // --
        pData_bgn_rcrd = pData;
        memcpy(&rcrd_arch.ds, pData, sizeof(rcrd_arch.ds)), pData += sizeof(rcrd_arch.ds);
        for(int i = 0; i < NMB_TARIF; i++){
            memcpy(&rcrd_enrgy, pData, sizeof(rcrd_enrgy)), pData += sizeof(rcrd_enrgy);
            rcrd_arch.enrg.push_back(rcrd_enrgy);
        }
        rcrds.push_back(rcrd_arch);
        size -= (pData - pData_bgn_rcrd);
    }
}
void PackArchive::serializ_children(LPPAY_LOAD_RAW pload){
    // -- 
    uint8_t *pData = (uint8_t *)pload->pData;
    memcpy(pData, &sts, 1), pData += 1;
    memcpy(pData, &head, sizeof(head)), pData += sizeof(head);
    {
        std::vector<RCRD_ANSW_ARCH>::iterator it_rcrd;
        std::vector<mtr::ENERGY>::iterator it_enrg;
        
        for(it_rcrd = rcrds.begin(); it_rcrd != rcrds.end(); it_rcrd++){
            memcpy(pData, &it_rcrd->ds, sizeof(it_rcrd->ds)), pData += sizeof(it_rcrd->ds);
            for(it_enrg = it_rcrd->enrg.begin(); it_enrg != it_rcrd->enrg.end(); it_enrg++){
                memcpy(pData, it_enrg.base(), sizeof(*it_enrg.base())), pData += sizeof(*it_enrg.base());
            }
        }
    }    
}
int32_t PackArchive::getSize(){
    int32_t size = 0;
    
    size = 1 + sizeof(head);
    if(rcrds.size()){
        size += rcrds.size() * (sizeof(mtr::DATE_t) + rcrds.begin()->enrg.size() * sizeof(mtr::ENERGY));
    }
    return size;
}
mtr::ENERGY PackArchive::sum(const mtr::DATE_t& bgn, const mtr::DATE_t& end)const{
    std::vector<RCRD_ANSW_ARCH>::const_iterator it, it_bgn = rcrds.end(), it_end = rcrds.end();
    mtr::ENERGY sunE = {0};
    
    // -- finde bgn and end iterators
    for(it = rcrds.begin(); it != rcrds.end(); it++){
        if(it->ds == bgn) it_bgn = it;
        if(it->ds == end) it_end = it;
        if((it_bgn != rcrds.end()) && (it_end != rcrds.end())) break;
    }
    // --
    while(it_bgn != it_end){
        sunE += (it_bgn + 1)->enrg - it_bgn->enrg;
        it_bgn++;
    }
    return sunE;
}
std::ofstream& operator<<(std::ofstream& ofs, mtr::DATE_t& ds){
    
    return ofs;
}
std::ofstream& operator<<(std::ofstream& ofs, vENERGY& enrg){
    
    return ofs;
}
void PackArchive::OutputFile(std::string sFileName){
    using namespace std;
    ofstream outputFile;
    string SP = ";";
    
    outputFile.open(sFileName.c_str());
    
    // write the file headers
    outputFile << "TIME" << SP << "Energy" << std::endl;
    std::vector<RCRD_ANSW_ARCH>::iterator it;
    vENERGY::iterator ite;
    
    for(it = rcrds.begin(); it < rcrds.end(); it++){
        outputFile << (int)it->ds.date << "-" << (int)it->ds.month << "-" << (int)it->ds.year <<"_T_00:00:00" << SP;
        for(ite = it->enrg.begin(); ite < it->enrg.end(); ite++){
            outputFile << ite->Ap << SP << ite->An << SP << ite->Rp << SP << ite->Rn << SP << std::endl;
        }
    }
    
    outputFile.close();
}
bool PackArchive::operator==(const PackArchive& val){
    return !(*this != val);
}
bool PackArchive::operator!=(const PackArchive& val){
    vARCH::const_iterator itA, itVA = val.rcrds.begin();
    vENERGY::const_iterator itE, itVE;
    
    if(rcrds.size() != val.rcrds.size()) return true;
    for(itA = rcrds.begin(); itA != rcrds.end(); itA++, itVA++){
        if(itVA == val.rcrds.end()) return true;
        if(itA->ds != itVA->ds) return true;
        if(itA->enrg.size() != itVA->enrg.size()) return true;
        itVE = itVA->enrg.begin();
        for(itE = itA->enrg.begin(); itE != itA->enrg.end(); itE++, itVE++){
            if(itVE == itVA->enrg.end()) return true;;
            if(*itE != *itVE) return true;
        }
    }
    return false;
}
/*
bool operator==(const RCRD_ANSW_ARCH& left, const RCRD_ANSW_ARCH& right){
    if(left.ds != right.ds) return false;
    return (left.enrg == right.enrg);
}
*/
bool operator!=(const mtr::DATE_t& left, const mtr::DATE_t& right){
    return !(left == right);
}
bool operator==(const mtr::DATE_t& left, const mtr::DATE_t& right){
    return (mtr::GetTmt_ds(&left) == mtr::GetTmt_ds(&right));
}
bool operator==(const mtr::DATE_TIME& left, const mtr::DATE_t& right){
    return (mtr::GetTmt_dts(&left) == mtr::GetTmt_ds(&right));
}
bool operator==(const mtr::DATE_t& left, const mtr::DATE_TIME& right){
    return (mtr::GetTmt_ds(&left) == mtr::GetTmt_dts(&right));
}
bool operator==(const mtr::DATE_TIME& left, const mtr::DATE_TIME& right){
    return (mtr::GetTmt_dts(&left) == mtr::GetTmt_dts(&right));
}
