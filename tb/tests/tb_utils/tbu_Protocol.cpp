#include "tb_utils.h"

namespace TBU{
    mtr::LPHeadPack pAnswHeadPack = 0;
    uint32_t size_data_AnswPrtcl_, size_AnswHeadPack;
    // UART_DATA
    mtr::enUART_STATE state = mtr::ust_IDLE;
    uint32_t cnt;
    uint8_t crc, fStaffMask;
    
    void reset_MPro_pareser(void){ 
        state = mtr::ust_GET_DATA;
        crc = 0;
        cnt = 0;
        fStaffMask = false;
    }
    void tbuAnswPrtcl_begin(mtr::LPPROTOCOL_DATA prtcl_data, uint16_t size_data){
        if(!pAnswHeadPack){
            size_AnswHeadPack = 500000 + size_data;
            pAnswHeadPack = (mtr::LPHeadPack)malloc(size_AnswHeadPack);

            memcpy(pAnswHeadPack, prtcl_data->pHead, sizeof(mtr::LPHeadPack));
            memcpy(pAnswHeadPack->Data, prtcl_data->pHead->Data, size_data);
            size_data_AnswPrtcl_ = size_data;
        }else{
            // TODO: Bags!!! pAnswBuff here should be ZERO!
            size_data_AnswPrtcl_ = -1;
        }
    }
    void tbuAnswPrtcl_data(mtr::LPPROTOCOL_DATA prtcl_data, uint16_t size_data){
        if( (size_data_AnswPrtcl_ + size_data) > size_AnswHeadPack){
            size_AnswHeadPack = (size_data_AnswPrtcl_ + size_data) + sizeof(mtr::HeadPack);
            pAnswHeadPack = (mtr::LPHeadPack)realloc(pAnswHeadPack, size_AnswHeadPack);
        }
        memcpy(&pAnswHeadPack->Data[size_data_AnswPrtcl_], prtcl_data->pHead->Data, size_data);
        size_data_AnswPrtcl_ += size_data;
    }
    void tbuAnswPrtcl_end(mtr::LPPROTOCOL_DATA prtcl_data){
        void *pHead = prtcl_data->pHead;

        prtcl_data->pHead = pAnswHeadPack;
        prtcl_data->size_data = size_data_AnswPrtcl_ - 1; ///< skip CRC
        // --
        free(pHead);
        pAnswHeadPack = 0;
        size_data_AnswPrtcl_ = 0;
        size_AnswHeadPack = 0;
    }
    void isr_MPro_parser(mtr::LPPROTOCOL_DATA prtcl_data, uint8_t rx_chr){
        switch(state){
        case mtr::ust_IDLE:
            if(rx_chr == PACKET_FLG){
                reset_MPro_pareser();
                tbuAnswPrtcl_begin(prtcl_data, 0);
            }
            break;
        case mtr::ust_GET_DATA:
            if(rx_chr == STAFF_BYTE){
                fStaffMask = true;
                break;
            }
            if(rx_chr == PACKET_FLG){
                state = mtr::ust_IDLE;
                // --
                tbuAnswPrtcl_end(prtcl_data);
            }else{
                if(fStaffMask){
                    fStaffMask = false;
                    rx_chr ^= STAFF_MASK;
                }
                crc ^= rx_chr;
                // -- Skip ADDR and CMD
                if(cnt++ >= offsetof(mtr::HeadPack, Data)){
                    prtcl_data->pHead->Data[0] = rx_chr;
                    tbuAnswPrtcl_data(prtcl_data, 1);
                }
            }
            break;
        }
    }
}
void tb_utils_cls::UartWrByte_wrapper(uint8_t byte){
    using namespace TBU;
    
    isr_MPro_parser(&prtcl_dt, byte);
}