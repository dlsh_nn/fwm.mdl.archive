/* 
 * File:   pc_interface_utils.h
 * Author: ShaginDL
 *
 * Created on 21 Март 2016 г., 13:57
 */

#ifndef TB_UTILS_H
#define	TB_UTILS_H

#include <cppunit/extensions/HelperMacros.h>
#include <string.h>
#include <algorithm>
#include <iostream>
//#include <boost/circular_buffer.hpp>

#include "define.h"

#include "archive_scenario.h"
#include "Electrical.h"
#include "Energy.h"
#include "ConversionType_to_Name.h"
#include "GeneratePack.h"
#include "../Converting Dumps to ExtMemory/CnvrtDumpToExtMem.h"

#include "rtc/rtc.h"

class tb_utils_cls {
public:
    std::string mess_err_tb;
    uint16_t indx_line_tab_scnr;
private:
    enum enTypeMessErr{eTMErr_All, eTMErr_OnlSett};
    
    static mtr::DRIVER driver;
    static mtr::UART_DATA ud_tested;
    static mtr::PROTOCOL_DATA prtcl_dt;
    uint32_t cnt_pass_Arch_exec;
    GeneratePack gen_pack;
    Energy energy;
    
    std::list<time_t> tmt_of_shifts;
    
public:
    tb_utils_cls();
    ~tb_utils_cls();
    void Init();
    
    bool TestScenario(const TB_SCENARIO_ARCHIVE *pscnr, std::string name_scnr);
private:
    void reInit_prtcl_dt();
    void isr_protocl_parser(uint8_t rx_chr);
    static void UartWrByte_wrapper(uint8_t byte);
    static void UartManagerRec_wrapper(enum mtr::enDrvMngRec flg){}
    static uint8_t UartStatus_wrapper(void){return mtr::mdm_READY_sts; }
    
    std::string MessageErrTest(const TB_SCENARIO_ARCHIVE *pscnr, enTypeMessErr typeMErr);
    std::string MessageErrTest(const TB_SCENARIO_ARCHIVE *pscnr, LPCPAY_LOAD_RAW pload_valid, enTypeMessErr typeMErr);
    
    void test_action(const TB_SCENARIO_ARCHIVE *pscnr);
    void test_action_work(const TB_SCENARIO_ARCHIVE *pscnr);
    void test_action_make_break(time_t break_time);
    void test_action_work_with_time_shift(const TB_SCENARIO_ARCHIVE *pscnr, time_t tmt_shift);
    void test_action_remove(const time_t &seconds_to_remove);
    time_t CalclTmtStep_Action(time_t* ptmt_work, time_t* ptmt_step, const TB_SCENARIO_ARCHIVE *pscnr);
    uint32_t copy_pload(void *dst, LPCPAY_LOAD_RAW pload);
    bool check_pload(LPCTB_SCENARIO_ARCHIVE pscnr, mtr::LPHeadPack pHead, LPCPAY_LOAD_RAW pload);
    
    bool CheckArchive_Convergence(const TB_SCENARIO_ARCHIVE *pscnr);
    bool CheckArchiveExecCount(const TB_SCENARIO_ARCHIVE *pscnr, LPCPAY_LOAD_RAW pload);
    
    void read_bgn_archive(mtr::LPPACK_ANSW_BGN_DTS_PPR pbgn_ppr, 
                          mtr::LPPACK_ANSW_BGN_DS_ARCH pbgn_day, 
                          mtr::LPPACK_ANSW_BGN_DS_ARCH pbgn_month );
    void read_archive(const mtr::PACK_ANSW_BGN_DTS_PPR& bgn_ppr, PackPPR* pappr);
    void read_archive(const mtr::PACK_ANSW_BGN_DS_ARCH& bgn, PackADAY* paday);
    void read_archive(const mtr::PACK_ANSW_BGN_DS_ARCH& bgn, PackAMONTH* pamonth);
    
    std::string ArrayToStr(const void *pSrc, uint32_t size);
    std::string ArrayToStr(mtr::LPPROTOCOL_DATA prtcl);
    std::string ArrayToStr(LPCPAY_LOAD_RAW ppack_pload);
    std::string ArrayToStr(LPCPACK_TB ppack);
    uint32_t GetSize(LPCPAY_LOAD_RAW ppack_pload);
    uint32_t GetSize(LPCPACK_TB ppack){return GetSize(ppack->pload);}
    
    void RunArchExec(LPCTB_SCENARIO_ARCHIVE pscnr, int NMB_RunForcibly = 0);
};

#endif

