/* 
 * File:   set_get_addr_scenario.h
 * Author: ShaginDL
 *
 * Created on 21 Март 2016 г., 14:03
 */

#ifndef ARCHIVE_SCENARIO_H
#define	ARCHIVE_SCENARIO_H

#define tmt_DTS_LastSunMarch        1459037853L // Sun, 27 Mar 2016 00:17:33
#define tmt_DTS_LastSunOctober      1477786653L // Sun, 30 Oct 2016 00:17:33 GMT

#define tmt_DTS_TB_begin            1473929442L
#define DTS_TB_begin                42, 50, 8, 5, 15, 9, 16
#define DTS_TB_begin_plus10s        52, 50, 8, 5, 15, 9, 16
#define tmt_TB_begin_alig_halfhour  1473928200
#define DTS_TB_begin_alig_halfhour  0,  30, 8, 5, 15, 9, 16
#define DS_TB_begin_alig_day                      15, 9, 16
#define DS_TB_begin_alig_month                     1, 9, 16

#define RCRD_ARCH_NO_ENRG_ALLT   {  {-1, -1, -1, -1}, {-1, -1, -1, -1}, {-1, -1, -1, -1}, {-1, -1, -1, -1}, \
                                    {-1, -1, -1, -1}, {-1, -1, -1, -1}, {-1, -1, -1, -1}, {-1, -1, -1, -1}, }
#define RCRD_ARCH_ZERO_ENRG_ALLT {  {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, \
                                    {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, }
#define RCRD_ARCH_ENRG_ALLT(eA, eR) {  {eA, 0, eR, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, \
                                    {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, }

#define PSW_DEF     {0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
#define PSW_ERR_DEF {0x00, 0x00, 0x00, 0x00, 0x01, 0x00}
#define END_PLOAD   {0, -1}

#define Uvm_DEF     230000, 230000, 230000
#define Uph_DEF     2300, 2300, 2300
#define TEMP_DEF    -1312

enum enTypeScenario{ scnrNormal, scnrNormal_Check_ConstPQP,
		scnrCheckSparseData, scnrCheckDataWhithBreaks,
        scnrNormal_NoArchExec, 
        scnrIgnor, scnrWork, scnrWorkDiviation, scnrMakeRemove,
		scnrWorkWithShift,		
        scnrCheckArchive_Convergence,
        scnrAction_OffOn_Mtr, scnrAction_Off_Mtr, scnrAction_On_Mtr,
        scnrCheckArchiveExecCount,
        scnrEND 
};

struct _PAY_LOAD_RAW{
    void*       pData;
    int32_t     size_data;
};
typedef struct _PAY_LOAD_RAW PAY_LOAD_RAW, *LPPAY_LOAD_RAW;
typedef const PAY_LOAD_RAW* LPCPAY_LOAD_RAW;
struct _PACK_TB{
    uint16_t    cmd;
    PAY_LOAD_RAW pload[4];
};
typedef struct _PACK_TB PACK_TB, *LPPACK_TB;
typedef const PACK_TB* LPCPACK_TB;
struct ELECTRO_SCNR{
    int32_t Vmv[NMB_PHASE], Ima[NMB_PHASE], angl[NMB_PHASE];
    int32_t diviation;    // percent for all parameters
    //private:
        uint32_t reserv[16];
};
#define UseOtherPrm 123321
struct OTHER_PRM_SCNR{
    uint32_t MagicUSE;
    int16_t Temp;
    //private:
        uint32_t reserv[64];
};

struct _TB_SCENARIO_ARCHIVE{
    // -- 
    enum enTypeScenario TypeTest;
    PACK_TB pack_tested, pack_valid;
    // -- action
    time_t tmt_work, tmt_step_work;
    ELECTRO_SCNR prm_ElNet;
    OTHER_PRM_SCNR prm_Other;
};
typedef struct _TB_SCENARIO_ARCHIVE TB_SCENARIO_ARCHIVE, *LPTB_SCENARIO_ARCHIVE;
typedef const TB_SCENARIO_ARCHIVE* LPCTB_SCENARIO_ARCHIVE;

namespace nsRAT{ 
    extern TB_SCENARIO_ARCHIVE scenario_reset_archive_tb[];
}
namespace nsCNT_WRK{ 
    extern TB_SCENARIO_ARCHIVE scenario_continuous_work_tb[];
}
namespace nsCNVRG{ 
    extern TB_SCENARIO_ARCHIVE scenario_convergence_tb[];
}

namespace nsAECNT {
    extern TB_SCENARIO_ARCHIVE scenario_checkArchiveExecCount_tb[];
    extern TB_SCENARIO_ARCHIVE scenario_checkArchiveExecCountSparseData_tb[];
    extern TB_SCENARIO_ARCHIVE scenario_checkArciveExecCountDataWithBreaks_tb[];
}

namespace nsAR{
    extern TB_SCENARIO_ARCHIVE scenario_remove_from_start_tb[];
    extern TB_SCENARIO_ARCHIVE scenario_remove_from_middle_tb[];
    extern TB_SCENARIO_ARCHIVE scenario_remove_case_one_tb[];
    extern TB_SCENARIO_ARCHIVE scenario_remove_case_two_tb[];
}

namespace nsAS{
    extern TB_SCENARIO_ARCHIVE scenario_simple_search_tb[];
    extern TB_SCENARIO_ARCHIVE scenario_search_case_1_tb[];
    extern TB_SCENARIO_ARCHIVE scenario_search_case_2_tb[];
    extern TB_SCENARIO_ARCHIVE scenario_search_case_3_tb[];
    extern TB_SCENARIO_ARCHIVE scenario_search_case_4_tb[];
}

namespace nsCMPPR{ 
    extern TB_SCENARIO_ARCHIVE scenario_CMPPR_PSW_tb[];
    extern TB_SCENARIO_ARCHIVE scenario_CMPPR_BUSY_tb[];
    extern TB_SCENARIO_ARCHIVE scenario_CMPPR_Write_tb[];
    extern TB_SCENARIO_ARCHIVE scenario_CMPPR_Write_OnOffOn_tb[];
    extern TB_SCENARIO_ARCHIVE scenario_CMPPR_Write_ClrPPR_tb[];
    extern TB_SCENARIO_ARCHIVE scenario_CMPPR_Write_ClrPPR_v2_tb[];
}
namespace nsCRPPR{
    extern TB_SCENARIO_ARCHIVE scenario_cmd_read_ppr_type_tb[];
    extern TB_SCENARIO_ARCHIVE scenario_cmd_read_ppr_VariousAlignDTS_tb[];
    extern TB_SCENARIO_ARCHIVE scenario_cmd_read_ppr_PPRv2_tb[];
    extern TB_SCENARIO_ARCHIVE scenario_cmd_read_ppr_PPRvs04_tb[];
    extern TB_SCENARIO_ARCHIVE scenario_cmd_read_ppr_PPRvs04_ByIndex_tb[];
}
namespace nsCRPPR_SEASON{
    extern TB_SCENARIO_ARCHIVE scenario_cmd_read_ppr_season_tb[];
    extern TB_SCENARIO_ARCHIVE scenario_cmd_read_ppr_season_WtS_tb[];
    extern TB_SCENARIO_ARCHIVE scenario_cmd_read_ppr_season_StW_tb[];
}
namespace nsCRPPR_NMB_RCRD{
    extern TB_SCENARIO_ARCHIVE scenario_cmd_read_ppr_NmbRcrd_tb[];
    extern TB_SCENARIO_ARCHIVE scenario_cmd_read_ppr_NmbRcrd_Total_tb[];
}
namespace nsCRPPR_NMB_RCRD_season_WtS{
    extern TB_SCENARIO_ARCHIVE scenario_cmd_read_ppr_NmbRcrd_season_WtS_tb[];
}
namespace nsCRPPR_NMB_RCRD_season_StW{
    extern TB_SCENARIO_ARCHIVE scenario_cmd_read_ppr_NmbRcrd_season_StW_tb[];
    extern TB_SCENARIO_ARCHIVE scenario_cmd_read_ppr_NmbRcrd_season_StW_D60s_tb[];
}
namespace nsTB_DUMP{
    extern TB_SCENARIO_ARCHIVE scenario_CmdReadPPR_NmbTotal[];
    extern TB_SCENARIO_ARCHIVE scenario_CmdReadPPR_NmbTotal_21_08_17[];
    extern TB_SCENARIO_ARCHIVE scenario_CmdReadPPR_28_08_17[];
}
namespace nsTB_DUMP_05_09_17{
    extern TB_SCENARIO_ARCHIVE scenario_ReadNmbPPR_dump_05_09_17[];
    extern TB_SCENARIO_ARCHIVE scenario_ReadNmbPPR_dump_05_09_17_v2[];
    extern TB_SCENARIO_ARCHIVE scenario_ReadNmbAMONTH_dump_05_09_17[];
}
namespace nsCRA_NMB_RCRD{
    extern TB_SCENARIO_ARCHIVE scenario_cmd_read_arch_NmbRcrd_tb[];
}
namespace nsCRA{
    extern TB_SCENARIO_ARCHIVE scenario_rdArchDAY_ByIndex_tb[];
    extern TB_SCENARIO_ARCHIVE scenario_rdArchMONTH_ByIndex_tb[];
}

namespace nsCRPPR_SEASON_D60s{
    extern TB_SCENARIO_ARCHIVE scenario_cmd_read_ppr_season_StW_D60s_tb[];
}

#endif	/* PC_INTERFACE_SCENARIO_H */

