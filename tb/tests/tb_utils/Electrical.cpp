/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Electrical.cpp
 * Author: User
 * 
 * Created on 27 июня 2017 г., 8:07
 */

#include "Electrical.h"

Electrical::Electrical(enTypeInit type){
    if(type == enInit_230V_1A_45deg){
        _ELECTR_PRM prm_init = {
            // -- V, I, Cos
            {230000, 230000, 230000},       // Vmv[NMB_PHASE]
            {1000,   1000,   1000},         // Ima[NMB_PHASE]
            {707,     707,     707,    707},    // CosF[NMB_PHASE + 1]
            // -- P
            {162634, 162634, 162634, 487902},    // Pa[NMB_PHASE + 1]
            {162634, 162634, 162634, 487902},    // Pr[NMB_PHASE + 1]
            {-1,        -1,     -1,  689997},    // Ps[NMB_PHASE + 1]
            {45,        45,     45,      45}
        };
        fRandom = false;
        prm = prm_init;
    }else throw "ERROR!!! Electrical::Electrical(enTypeInit type) not know type";
}
Electrical::Electrical(){
    fRandom = false;
    memset(&prm, 0, sizeof(prm));
    
    CalclElectricalPrm();
}
Electrical::Electrical(const mtr::CE_MDL &ce_mdl){
    Set(ce_mdl);
}
Electrical::Electrical(const ELECTRO_SCNR &prm_ElNet){
    Set(prm_ElNet);
}
void Electrical::Set(const mtr::CE_MDL &ce_mdl){
    fRandom = false;
    memset(&prm, 0, sizeof(prm));
    memcpy(&prm, &ce_mdl, offsetof(_ELECTR_PRM, Pa));
    for(int i = 0; i < NMB_PHASE; i++){
        prm.anglPh[i] = acos(ce_mdl.CosF[i] / 1000.0) * 180 / M_PI;
    }
    
    CalclElectricalPrm();
}
void Electrical::Set(const ELECTRO_SCNR &prm_ElNet){
    fRandom = false;
    memset(&prm, 0, sizeof(prm));
    memcpy(prm.Vmv, prm_ElNet.Vmv, sizeof(prm.Vmv));
    memcpy(prm.Ima, prm_ElNet.Ima, sizeof(prm.Ima));
    memcpy(&prm.anglPh, &prm_ElNet.angl, sizeof(prm.anglPh));
    
    CalclElectricalPrm();
}
Electrical::Electrical(const Electrical& origin){
    
}
mtr::CE_MDL Electrical::GetCE(const mtr::CE_MDL &ce_mdl){
    mtr::CE_MDL ce_mdl_out;
    
    ce_mdl_out = GetCE();
    memcpy(&ce_mdl_out.enrg_int, &ce_mdl.enrg_int, sizeof(ce_mdl) - offsetof(mtr::CE_MDL, enrg_int));
    
    return ce_mdl_out;
}
mtr::CE_MDL Electrical::GetCE(){
    mtr::CE_MDL ce_mdl;
    
    memset(&ce_mdl, 0, sizeof(ce_mdl));
    for(int i = 0; i < NMB_PHASE + 1; i++){
        
        if(i < NMB_PHASE){
            ce_mdl.Vmv[i] = GetV(i);
            ce_mdl.Ima[i] = GetI(i);
        }
        ce_mdl.CosF[i] = GetCosF(i);
        // -- 
        ce_mdl.Pa[i] = GetPa(i);
        ce_mdl.Pr[i] = GetPr(i);
        ce_mdl.Ps[i] = GetPs(i);
        // --
        //ENERGY  enrg_int;
    }
    return ce_mdl;
}
void Electrical::CalclElectricalPrm(){
    const int32_t *pPa = prm.Pa;
    const int32_t *pPr = prm.Pr;
    const int32_t *pPs = prm.Ps;
    _ELECTR_PRM *pprm = &prm;
    
    // -- Calculated dependent parameters
    prm.Pa[NMB_PHASE] = prm.Pr[NMB_PHASE] = prm.Ps[NMB_PHASE] = 0;
    for(int i = 0; i < NMB_PHASE; i++){
        prm.Vmv[i] = pprm->Vmv[i];
        prm.Ima[i] = pprm->Ima[i];
        // --
        prm.Pa[i] = prm.Vmv[i] * prm.Ima[i] * cos(pprm->anglPh[i] * M_PI / 180.0) / 1e+3;
        prm.Pr[i] = prm.Vmv[i] * prm.Ima[i] * sin(pprm->anglPh[i] * M_PI / 180.0) / 1e+3;
        prm.Ps[i] = sqrt(pow(prm.Pa[i], 2) + pow(prm.Pr[i], 2));
        // --
        prm.CosF[i] = cos(pprm->anglPh[i] * M_PI / 180.0) * 1e+3;
        // --
        prm.Pa[NMB_PHASE] += pPa[i];
        prm.Pr[NMB_PHASE] += pPr[i];
        prm.Ps[NMB_PHASE] += pPs[i];
        // --
        prm.Ps[i] = -1; // reset
    }
    if(prm.Ps[NMB_PHASE]) prm.CosF[NMB_PHASE] = (prm.Pa[NMB_PHASE] * 1000LL) / prm.Ps[NMB_PHASE];
    else prm.CosF[NMB_PHASE] = 0;
}
void Electrical::SetPrm(uint8_t i, int32_t* pprm_val, int32_t val, std::string mess_throw, bool fCalcl){
    if(i < NMB_PHASE) pprm_val[i] = val;
    else if(i == NMB_PHASE){
        for(int j = 0; j < NMB_PHASE; j++) pprm_val[j] = val;
    }else throw mess_throw; 
    if(fCalcl) CalclElectricalPrm(); 
}
mtr::POWER Electrical::GetPOWER()const{
    mtr::POWER pwr = {0, 0, 0, 0};
    
    if(prm.Pa[NMB_PHASE] > 0) pwr.Ap = labs(prm.Pa[NMB_PHASE]);
    else pwr.An = labs(prm.Pa[NMB_PHASE]);
    
    if(prm.Pr[NMB_PHASE] > 0) pwr.Rp = labs(prm.Pa[NMB_PHASE]);
    else pwr.Rn = labs(prm.Pa[NMB_PHASE]);
    
    return pwr;
}
int32_t Electrical::random_val(int32_t val, int32_t diviation){
    int32_t max_dix;
    
    max_dix = ((labs(val) * diviation) / 100.0);
    val += (rand() % max_dix) - (max_dix / 2);
    
    return val;
}

void Electrical::Generate(float diviation){
    if(!fRandom){
        fRandom = true;
        prm_random = prm;
    }
    for(int i = 0; i < NMB_PHASE; i++){
        prm.Vmv[i] = random_val(prm_random.Vmv[i], diviation);
        prm.Ima[i] = random_val(prm_random.Ima[i], diviation);
        prm.anglPh[i] = random_val(prm_random.anglPh[i], diviation);
    }
    CalclElectricalPrm();
}
