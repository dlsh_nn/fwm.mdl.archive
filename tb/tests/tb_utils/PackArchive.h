/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PackArchive.h
 * Author: User
 *
 * Created on 3 июля 2017 г., 17:38
 */

#ifndef PACKARCHIVE_H
#define PACKARCHIVE_H

#include <time.h>
#include <string.h>
#include <vector>
#include <exception>

#include "define.h"

#include "Energy.h"

struct RCRD_ANSW_ARCH{
    mtr::DATE_t ds;
    vENERGY enrg;
};
typedef std::vector<RCRD_ANSW_ARCH> vARCH;
typedef std::vector<mtr::PACK_ANSW_PWR_PRFL_V0> vPPR;
/*struct PACK_ADAY{
    enum enStatusPrtcl
    PACK_ANSW_ARCH_HEAD head;
    std::vector<RCRD_ANSW_ARCH> rcrd;
};*/

class PackArchive {
public:
    enum enMarkPeriod{enMark_Begin, enMark_End};
    enum enTypeInitArchive{enInit_1_year};
protected:
    mtr::enTypeGetArch TypeAnswer;
    time_t tmt_bgn, tmt_end;
private:
    mtr::enStatusPrtcl sts;
    mtr::PACK_ANSW_ARCH_HEAD head;
    std::vector<RCRD_ANSW_ARCH> rcrds;
public:
    PackArchive(){reset();};
    PackArchive(mtr::LPCPACK_GET_ARCH pack_in);
    
    void Init(enTypeInitArchive typeInit, time_t tmt_bgn);
    virtual void Init(const void* ppack_in_raw);
    virtual void add_corrupt_rcrd(time_t tmt);
    virtual void add_rcrd(time_t tmt, const Energy& enrg);
    virtual void add_rcrd(time_t tmt, const Energy& enrg, const Electrical& electrical){
        add_rcrd(tmt, enrg);
    }
    virtual void serializ(LPPAY_LOAD_RAW pload);
    void deserializ(const void* pData, uint32_t size_data){
        PAY_LOAD_RAW pload[2] = {{(void*)pData, size_data}, {0, -1}};
        deserializ(pload);
    }
    virtual void deserializ(LPCPAY_LOAD_RAW pload);
    virtual time_t next_step(time_t tmt_crnt){
        throw "ERROR!!! PackArchive::next_step(time_t tmt_crnt) not relized." \
              "Need relize in children class.";
    };
    virtual time_t next_step(time_t tmt_crnt, time_t tmt_direction){
        return (tmt_crnt == tmt_direction) ? 0 :
               ((tmt_crnt < tmt_direction) ? next_step(tmt_crnt) : -1 * next_step(tmt_crnt));
    };
    virtual enMarkPeriod GetTypeMark(){return enMark_End;}
    mtr::enTypeGetArch GetTypeAnsw(){return TypeAnswer;}
    virtual mtr::ENERGY sum(const mtr::DATE_t& bgn, const mtr::DATE_t& end)const;
    time_t Get_tmt_bgn(){return tmt_bgn;}
    time_t Get_tmt_end(){return tmt_end;}
    // --
    virtual void OutputFile(std::string sFileName);
    // --
    const std::vector<RCRD_ANSW_ARCH>& get_rcrds()const{return rcrds;}
    bool operator!=(const PackArchive& val);
    bool operator==(const PackArchive& val);
private:
    virtual int32_t getSize();
    virtual void reset();
    void reset_rcrds();
    virtual void serializ_children(LPPAY_LOAD_RAW pload);
    
    //std::ofstream operator<<(mtr::DATE_t& ds);
    //std::ofstream operator<<(vENERGY& enrg);
};

//extern bool operator==(const RCRD_ANSW_ARCH& left, const RCRD_ANSW_ARCH& right);
extern bool operator==(const mtr::DATE_t& left, const mtr::DATE_t& right);
extern bool operator!=(const mtr::DATE_t& left, const mtr::DATE_t& right);
extern bool operator==(const mtr::DATE_TIME& left, const mtr::DATE_t& right);
extern bool operator==(const mtr::DATE_t& left, const mtr::DATE_TIME& right);
extern bool operator==(const mtr::DATE_TIME& left, const mtr::DATE_TIME& right);

#endif /* PACKARCHIVE_H */

