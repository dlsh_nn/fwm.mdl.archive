/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   GeneratePack.h
 * Author: User
 *
 * Created on 1 РёСЋР»СЏ 2017 Рі., 18:16
 */

#ifndef GENERATEPACK_H
#define GENERATEPACK_H

#include <time.h>
#include <string.h>
#include "define.h"
#include <vector>
#include <list>

#include "archive_scenario.h"
#include "Electrical.h"
#include "Energy.h"
#include "PackADAY.h"
#include "PackAMONTH.h"
#include "PackPPR.h"

class GeneratePack {
private:
    PAY_LOAD_RAW pay_load_generate[2];
    
public:
    GeneratePack();
    ~GeneratePack();
    
    LPCPAY_LOAD_RAW PackConstPQP(LPCTB_SCENARIO_ARCHIVE pscnr);
    LPCPAY_LOAD_RAW PackConstPQPSparseData(LPCTB_SCENARIO_ARCHIVE pscnr, std::list<time_t> tmt_of_shifts, time_t tmt_shift);
private:
    void free_pay_load();
    LPPAY_LOAD_RAW GenPack(LPPAY_LOAD_RAW pload, PackArchive* packa, LPCTB_SCENARIO_ARCHIVE pscnr);
    LPPAY_LOAD_RAW GenSparsePack(LPPAY_LOAD_RAW pload, PackArchive* packa, LPCTB_SCENARIO_ARCHIVE pscnr, std::list<time_t> tmt_of_shifts, time_t tmt_shift);
};

#endif /* GENERATEPACK_H */

