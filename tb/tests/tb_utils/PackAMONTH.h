/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PackAMONTH.h
 * Author: User
 *
 * Created on 4 ���� 2017 �., 7:30
 */

#ifndef PACKAMONTH_H
#define PACKAMONTH_H

#include "PackArchive.h"

class PackAMONTH : public PackArchive {
public:
    PackAMONTH() : PackArchive::PackArchive(){};
    PackAMONTH(mtr::LPCPACK_GET_ARCH pack_in) : PackArchive::PackArchive(pack_in){};
    
    void Init(enTypeInitArchive typeInit, time_t tmt_bgn){PackArchive::Init(typeInit, tmt_bgn);}
    void deserializ(const void* pData, uint32_t size_data){
        PackArchive::deserializ(pData, size_data);
    }
    time_t next_step(time_t tmt_crnt){
        time_t tmt_step;
        mtr::DATE_TIME dts;
        
        mtr::GetDts_tmt(tmt_crnt, &dts);
        // -- Next Month
        dts.sec = dts.min = dts.hour = 0;
        dts.date = 1;
        if(++dts.month > 12) dts.month = 1, dts.year++;
        // -- Calculated step
        tmt_step = mtr::GetTmt_dts(&dts) - tmt_crnt;
        
        return tmt_step;
    };
    time_t next_step(time_t tmt_crnt, time_t tmt_direction){
        return PackArchive::next_step(tmt_crnt, tmt_direction);
    }
};

#endif /* PACKAMONTH_H */

