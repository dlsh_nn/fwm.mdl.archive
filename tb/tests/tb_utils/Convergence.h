/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Convergence.h
 * Author: User
 *
 * Created on 6 ���� 2017 �., 10:12
 */

#ifndef CONVERGENCE_H
#define CONVERGENCE_H

#include <stdint.h>
#include <string.h>
#include <string>
#include <stdlib.h>
#include <math.h>

#include "define.h"
#include "archive_scenario.h"


#include "PackPPR.h"
#include "PackADAY.h"
#include "PackAMONTH.h"

class Convergence {
public:
    Convergence();
    bool CheckArchive(const PackPPR& appr, const PackADAY& aday, const PackAMONTH& amonth);
private:
    bool check_convergence(const PackPPR& appr, const PackADAY& aday);
    bool check_convergence(const PackADAY& aday, const PackAMONTH& amonth);
    
};

#endif /* CONVERGENCE_H */

