/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PackPPR.h
 * Author: User
 *
 * Created on 6 июля 2017 г., 11:33
 */

#ifndef PACKPPR_H
#define PACKPPR_H

#include "PackArchive.h"

class PackPPR : public PackArchive{
private:
    vPPR rcrds;
    
public:
    PackPPR() : PackArchive::PackArchive(){  };
    PackPPR(mtr::LPCPACK_GET_ARCH pack_in) : PackArchive::PackArchive(pack_in){};
    PackPPR(const mtr::PACK_ANSW_BGN_DTS_PPR &bgn_ppr);
    
    void Init(enTypeInitArchive typeInit, time_t tmt_bgn){PackArchive::Init(typeInit, tmt_bgn);}
    void Init(const void* ppack_in_raw);
    void add_rcrd(time_t tmt, const Energy& enrg){
        throw "ERROR!!! PackPPR::add_rcrd(time_t tmt, const Energy& enrg) impossible.";
    };
    void add_rcrd(time_t tmt, const Energy& enrg, const Electrical& electrical);
    void deserializ(const void* pData, uint32_t size_data){
        PackArchive::deserializ(pData, size_data);
    }
    void deserializ(LPCPAY_LOAD_RAW pload);
    enMarkPeriod GetTypeMark(){return enMark_Begin;}
    
    time_t next_step(time_t tmt_crnt){return 30 * 60L;};
    time_t next_step(time_t tmt_crnt, time_t tmt_direction){
        return PackArchive::next_step(tmt_crnt, tmt_direction);
    }
    // --
    void OutputFile(std::string sFileName);
    // --
    const std::vector<mtr::PACK_ANSW_PWR_PRFL_V0>& get_rcrds()const{return rcrds;}
    mtr::ENERGY sum(const mtr::DATE_t& bgn, const mtr::DATE_t& end)const;
    bool operator!=(const PackPPR& val);
private:
    void serializ_children(LPPAY_LOAD_RAW pload);
    int32_t getSize(){
        return (1 + rcrds.size() * sizeof(*rcrds.begin().base()));
    }
};

#endif /* PACKPPR_H */

