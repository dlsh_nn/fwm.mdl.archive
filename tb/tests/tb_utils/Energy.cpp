/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Energy.cpp
 * Author: User
 * 
 * Created on 27 ���� 2017 �., 8:01
 */

#include "Energy.h"

Energy::Energy() {
    reset();
}
Energy::Energy(const mtr::CE_MDL &ce_mdl){
    reset();
    memcpy(&enrg_int, &ce_mdl.enrg_int, sizeof(enrg_int));
}
Energy::Energy(const mtr::ENERGYS_MTR_TRFS &enrg_trfs){
    reset();
    memcpy(&ETarif, &enrg_trfs, sizeof(ETarif));
}
mtr::ENERGY Energy::Calculate(time_t Time, const Electrical& electro){
    uint64_t enrgy;
    int32_t *pE_int = &enrg_int.Ap;
    int32_t *pE_fract = &enrg_fract.Ap;
    int32_t P;
    
    Time = labs(Time);
    // -- Active Energy
    P = electro.GetPa(NMB_PHASE);
    if(P > 0) CalclEnrg(Time, P, enrg_int.Ap, enrg_fract.Ap);
    else CalclEnrg(Time, P, enrg_int.An, enrg_fract.An);
    // -- Reactive Energy
    P = electro.GetPr(NMB_PHASE);
    if(P > 0) CalclEnrg(Time, P, enrg_int.Rp, enrg_fract.Rp);
    else CalclEnrg(Time, P, enrg_int.Rn, enrg_fract.Rn);
    // -- Energy by Tarifs
    ETarif.enrg[0] += enrg_int;
    
    return enrg_int;
}
void Energy::CalclEnrg(const time_t &Tperiod, const int32_t P, int32_t &E_int, int32_t &E_fract){
    int64_t E;
    
    E = (1LL * mtr::abs_my(P) * Tperiod) + E_fract;
    E_fract = (E % (3600L * 1000));
    E_int += E / (3600L * 1000);
}
mtr::ENERGY Energy::Get(uint8_t indxTarif) const{
    if(indxTarif < NMB_TARIF) return ETarif.enrg[indxTarif];
    else throw "ERROR!!! indxTarif > mtr::NMB_TARIF in Energy::Get(uint8_t indxTarif)";
}
bool operator==(const mtr::ENERGY &left, const mtr::ENERGY &rigth){
    return !(left != rigth);
}
bool operator!=(const mtr::ENERGY &left, const mtr::ENERGY &rigth){
    return memcmp(&left, &rigth, sizeof(left));
}
mtr::ENERGY& operator+=(mtr::ENERGY& left, const mtr::ENERGY& rigth){
    int32_t* pEl = (int32_t*)&left;
    int32_t* pEr = (int32_t*)&rigth;
    for(int i = 0; i < 4; i++, pEl++, pEr++){
        *pEl += *pEr;
    }
    return left;
}
mtr::ENERGY& operator+=(mtr::ENERGY& left, const vENERGY& right){
    left += sum(right);
    return left;
}
mtr::ENERGY sum(const vENERGY& venrgy){
    vENERGY::const_iterator it;
    mtr::ENERGY enrgy = {0};
    
    for(it = venrgy.begin(); it != venrgy.end(); it++){
        if(it->Ap != -1) {
            enrgy += *it;
        }
    }
    return enrgy;
}
mtr::ENERGY operator-(const vENERGY& left, const vENERGY& right){
    mtr::ENERGY sum_left = sum(left);
    mtr::ENERGY sum_right = sum(right);
    return sum_left - sum_right;
}
mtr::ENERGY operator-(const mtr::ENERGY& left, const mtr::ENERGY& right){
    int32_t* pEl = (int32_t*)&left;
    int32_t* pEr = (int32_t*)&right;
    mtr::ENERGY enrgy = {0};
    int32_t* pEout = (int32_t*)&enrgy;
    for(int i = 0; i < 4; i++, pEl++, pEr++, pEout++){
        *pEout = *pEl - *pEr;
    }
    return enrgy;
}
