/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PackPPR.cpp
 * Author: User
 * 
 * Created on 6 июля 2017 г., 11:33
 */
#include <fstream>
#include "PackPPR.h"

PackPPR::PackPPR(const mtr::PACK_ANSW_BGN_DTS_PPR &bgn_ppr) : PackArchive::PackArchive(){
    
}
void PackPPR::Init(const void* ppack_in_raw){
    mtr::LPCPACK_GET_PWR_PRFL pack_in = (mtr::LPCPACK_GET_PWR_PRFL)ppack_in_raw;
    // --
    if(pack_in->Type == mtr::enTPPR_NoSkip_PPRv0){
        TypeAnswer = mtr::enType_NoPass;
    }
    tmt_bgn = mtr::GetTmt_dts(&pack_in->dts_bgn); 
    tmt_end = mtr::GetTmt_dts(&pack_in->dts_end);
}
void PackPPR::add_rcrd(time_t tmt, const Energy& enrg, const Electrical& electrical){
    mtr::PACK_ANSW_PWR_PRFL_V0 rcrd_ppr_v0;
    mtr::DATE_TIME dts_rcrd;
    
    mtr::GetDts_tmt(tmt, &dts_rcrd);
    memcpy(&rcrd_ppr_v0.dts, &dts_rcrd, sizeof(rcrd_ppr_v0.dts));
    rcrd_ppr_v0.prfl.enrg = enrg.Get();
    rcrd_ppr_v0.prfl.pwr_peak = electrical.GetPOWER();
    rcrd_ppr_v0.fValid = mtr::ePrflValid;
    rcrds.push_back(rcrd_ppr_v0);
}
void PackPPR::deserializ(LPCPAY_LOAD_RAW pload){
    mtr::PACK_ANSW_PWR_PRFL_V0 rcrd_ppr_v0;
    int32_t size = pload->size_data - 1;
    uint8_t* pData = (uint8_t*)pload->pData + 1;
    
    if(!pload->pData) throw "ERROR!!! pload->pData == 0 in PackPPR::deserializ.";
    
    rcrds.clear();
    while(size > 0){
        memcpy(&rcrd_ppr_v0, pData, sizeof(rcrd_ppr_v0)), pData += sizeof(rcrd_ppr_v0);
        rcrds.push_back(rcrd_ppr_v0);
        size -= sizeof(rcrd_ppr_v0);
    }
}
void PackPPR::serializ_children(LPPAY_LOAD_RAW pload){
    uint8_t *pData = (uint8_t *)pload->pData;
    pData[0] = mtr::STS_OK, pData += 1;
    {
        std::vector<mtr::PACK_ANSW_PWR_PRFL_V0>::iterator it_rcrd;
        
        for(it_rcrd = rcrds.begin(); it_rcrd != rcrds.end(); it_rcrd++){
            memcpy(pData, it_rcrd.base(), sizeof(*it_rcrd.base())), pData += sizeof(*it_rcrd.base());
        }
    }
}
mtr::ENERGY PackPPR::sum(const mtr::DATE_t& bgn, const mtr::DATE_t& end)const{
    std::vector<mtr::PACK_ANSW_PWR_PRFL_V0>::const_iterator it, it_bgn = rcrds.end(), it_end = rcrds.end();
    mtr::ENERGY sumE = {0};
    
    // -- find bgn and end iterators
    for(it = rcrds.begin(); it != rcrds.end(); it++){
        if(it->dts == bgn) it_bgn = it;
        if(it->dts == end) it_end = it;
        if((it_bgn != rcrds.end()) && (it_end != rcrds.end())) break;
    }
    // --
    while(it_bgn != it_end){
        if(it_bgn->fValid <= 3) {
            sumE += it_bgn->prfl.enrg;
        }
        it_bgn++;

    }
    return sumE;
}
bool PackPPR::operator!=(const PackPPR& val){
    vPPR::const_iterator itA, itVA = val.rcrds.begin();
    
    if(rcrds.size() != val.rcrds.size()) return true;
    for(itA = rcrds.begin(); itA != rcrds.end(); itA++, itVA++){
        if(memcmp(itA.base(), itVA.base(), sizeof(*itA)))return true;
    }
    return false;
}
void PackPPR::OutputFile(std::string sFileName){
    using namespace std;
    ofstream outputFile;
    string SP = ";";
    
    outputFile.open(sFileName.c_str(), ios_base::out);
    
    // write the file headers
    outputFile << "DD-MM-YY_T_h:m:s" << SP << "E-Ap" << SP  << "E-An" << SP << "E-Rp" << SP << std::endl;
    vPPR::iterator it;
    vENERGY::iterator ite;
    
    for(it = rcrds.begin(); it < rcrds.end(); it++){
        outputFile << (int)it->dts.date << "-" << (int)it->dts.month << "-" << (int)it->dts.year <<"_T_";
        outputFile << (int)it->dts.hour << ":" << (int)it->dts.min << ":" << (int)it->dts.sec << SP;
        outputFile << it->prfl.enrg.Ap << SP << it->prfl.enrg.An << SP << it->prfl.enrg.Rp << SP << it->prfl.enrg.Rn << SP;
        outputFile << it->prfl.pwr_peak.Ap << SP << it->prfl.pwr_peak.An << SP << it->prfl.pwr_peak.Rp << SP << it->prfl.pwr_peak.Rn << SP;
        outputFile << (int)it->fValid << SP << std::endl;;
    }
    
    outputFile.close();
}