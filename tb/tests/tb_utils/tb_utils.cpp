/* 
 * File:   pc_interface_utils.cpp
 * Author: ShaginDL
 * 
 * Created on 21 Март 2016 г., 13:57
 */

#include <list>

#include "tb_utils.h"

mtr::DRIVER tb_utils_cls::driver = {
    UartWrByte_wrapper,
    UartManagerRec_wrapper,
    mtr::enInterface_Optoport,
    UartStatus_wrapper
};
mtr::UART_DATA tb_utils_cls::ud_tested;
mtr::PROTOCOL_DATA tb_utils_cls::prtcl_dt;

tb_utils_cls::tb_utils_cls(){
    Init();
}   
tb_utils_cls::~tb_utils_cls(){
    if(prtcl_dt.pHead){
        free(prtcl_dt.pHead);
        prtcl_dt.pHead = 0;
    }
}
void tb_utils_cls::Init(){
    mess_err_tb = "OK";
    indx_line_tab_scnr = 0;
    // -- Init memory
    mtr::ers_ExtMemory();
    mtr::ers_at25_all();
    // -- UART_DATA init
    ud_tested.size_buf = sizeof(mtr::HeadPack) + 500 + sizeof(mtr::ARCH_ANSW_VAR);
    // -- RTC
    memset(&mtr::rtc_tbl.rtc_save, 0, sizeof(mtr::rtc_tbl.rtc_save));
    // --
    memset(&prtcl_dt, 0, sizeof(prtcl_dt));
    prtcl_dt.pDriver = &driver;
    prtcl_dt.pHead = (mtr::LPHeadPack)malloc(ud_tested.size_buf);
    prtcl_dt.psize_buf = &ud_tested.size_buf;
}
void tb_utils_cls::reInit_prtcl_dt(){
    free(prtcl_dt.pHead);
    prtcl_dt.pHead = (mtr::LPHeadPack)malloc(ud_tested.size_buf);
}
std::string tb_utils_cls::ArrayToStr(LPCPAY_LOAD_RAW ppack_pload){
    std::ostringstream oss;
    LPCPAY_LOAD_RAW pload = ppack_pload;
    
    while(pload->size_data != -1){
        oss << ArrayToStr(pload->pData, pload->size_data) << " ";
        pload++;
    }
    
    return oss.str();
}
std::string tb_utils_cls::ArrayToStr(mtr::LPPROTOCOL_DATA prtcl){
    return ArrayToStr(prtcl->pHead->Data, prtcl->size_data);
}
std::string tb_utils_cls::ArrayToStr(const void *pSrc, uint32_t size){
    std::ostringstream oss;
    const uint8_t *psrc = (uint8_t *)pSrc;

    if(!size) return "";
    
    oss << std::hex;
    while(size--){ oss << (uint32_t)*psrc++ << " "; }

    return oss.str();
}

uint32_t tb_utils_cls::GetSize(LPCPAY_LOAD_RAW ppack_pload){
    uint32_t size = 0;
    LPCPAY_LOAD_RAW pload = ppack_pload;
    
    while(pload->size_data != -1){
        size += pload->size_data;
        pload++;
    }
    
    return size;
}
std::string tb_utils_cls::MessageErrTest(const TB_SCENARIO_ARCHIVE *pscnr, enTypeMessErr typeMErr){
    return MessageErrTest(pscnr, pscnr->pack_valid.pload, typeMErr);
}
std::string tb_utils_cls::MessageErrTest(const TB_SCENARIO_ARCHIVE *pscnr, LPCPAY_LOAD_RAW pload_valid, enTypeMessErr typeMErr = eTMErr_All){
    std::ostringstream oss;

    oss << " - indx_line_tab_scnr == " << indx_line_tab_scnr << "\r\n";
    oss << " - cnt_pass_Arch_exec == " << cnt_pass_Arch_exec << "\r\n";
    // -- Settings
    oss << " - Settings : \r\n         ";
    oss << "pscnr->HeadPack.cmd = " << (mtr::enCmdPrtcl)pscnr->pack_tested.cmd << ";  ";
    switch(pscnr->pack_tested.cmd){
    case mtr::GET_BGN_DS_ARCH: oss << (mtr::enTypeArch)prtcl_dt.pHead->Data[0]; break;
    default:
        oss << "pscnr->HeadPack.Data[" << GetSize(&pscnr->pack_tested) << "] = " << ArrayToStr(pscnr->pack_tested.pload);
    }
    oss << ";\r\n";
    
    
    // -- Expected
    if(typeMErr == eTMErr_All){
        oss << " - Expected : \r\n         ";
        oss << "pscnr->HeadPack.cmd = " << (mtr::enCmdPrtcl)pscnr->pack_valid.cmd << ";\r\n";
        oss << "         " << "pscnr->HeadPack.Data[" << GetSize(pload_valid) << "] = " << ArrayToStr(pload_valid) << ";\r\n         ";
        // -- There is
        oss << "\r\n - There is : \r\n         ";
        oss << "prtcl_dt.pHead->Data["  << prtcl_dt.size_data << "] = ";
        if(prtcl_dt.size_data == 1) oss << (mtr::enStatusPrtcl)prtcl_dt.pHead->Data[0];
        else oss << ArrayToStr(prtcl_dt.pHead->Data, prtcl_dt.size_data);
    }
    return oss.str();
}

bool operator!=(const mtr::HeadPack &left, const mtr::HeadPack &right){
    return (0 != memcmp((void *)&left, (void *)&right, sizeof(mtr::HeadPack)));
}
void tb_utils_cls::test_action(const TB_SCENARIO_ARCHIVE *pscnr){
    RunArchExec(pscnr, 1000);
    
    copy_pload(prtcl_dt.pHead->Data, pscnr->pack_tested.pload);
    // -- Selected cmd
    switch(pscnr->pack_tested.cmd){
    case mtr::GET_BGN_DTS_PPR:
        mtr::get_bgn_dts_ppr(&prtcl_dt);
        break;
    case mtr::GET_PWR_PROFILE:
        mtr::get_pwr_profile(&prtcl_dt);
        break;
    case mtr::CMD_MNG_PPR:
        mtr::cmd_mng_ppr(&prtcl_dt);
        break;
    case mtr::GET_BGN_DS_ARCH:
        mtr::get_bgn_ds_arch(&prtcl_dt);
        break;
    case mtr::GET_ARCH_MTR:
        mtr::get_arch_mtr(&prtcl_dt);
        break;
    default:
        std::ostringstream oss;
        
        oss << "Error in test_action. pscnr->pack_tested.cmd = " \
            << (mtr::enCmdPrtcl)pscnr->pack_tested.cmd << " not determinde!!!";
        throw oss.str();
    }
//<<<<<<< HEAD
//    int Interface;
//    for(cnt_pass_Arch_exec = 0; cnt_pass_Arch_exec < 1e+6; cnt_pass_Arch_exec++){
//        mtr::Arch_exec();
//        // -- Check END Arch_exec
//        for(Interface = 0; Interface < mtr::NMB_Interface; Interface++){
//            if(mtr::arch_var.pAnswVar[Interface] != NULL) break;
//        }
//        if(Interface >= mtr::NMB_Interface) break;
//    }
//    CPPUNIT_NS::stdCOut() << "cnt_pass_Arch_exec = " << cnt_pass_Arch_exec << ";\n";
//=======
    RunArchExec(pscnr);
}
time_t tb_utils_cls::CalclTmtStep_Action(time_t* ptmt_work, time_t* ptmt_step, const TB_SCENARIO_ARCHIVE *pscnr){
    time_t tmt_max_stemp;
    time_t tmt_align_crnt, tmt_align_next;
    
    tmt_max_stemp = Min(*ptmt_work, pscnr->tmt_step_work);
    tmt_align_crnt = mtr::GetTmtArchPPR_align(mtr::rtc_tbl.crnt_tmt);
    tmt_align_next = mtr::GetTmtArchPPR_align(mtr::rtc_tbl.crnt_tmt + tmt_max_stemp);
    if(tmt_align_crnt != tmt_align_next){
        if(!mtr::arch_var.fSaveArch){
            *ptmt_step = tmt_align_next - mtr::rtc_tbl.crnt_tmt;
        }else{
            *ptmt_step = (tmt_align_next - 1) - mtr::rtc_tbl.crnt_tmt;
        }
    }else *ptmt_step = tmt_max_stemp;
    // -- Check transion season
    if(mtr::rtc_tbl.rtc_save.fSwitchSummer){
        if(mtr::CheckSummerWinter_tmt(*ptmt_work) != mtr::CheckSummerWinter_tmt(*ptmt_work + *ptmt_step)){
            *ptmt_step = 1;
        }
    }
    
    return *ptmt_step;
}
void tb_utils_cls::test_action_work(const TB_SCENARIO_ARCHIVE *pscnr){
    time_t tmt_work = pscnr->tmt_work, tmt_step = pscnr->tmt_step_work;
    Electrical electro(pscnr->prm_ElNet);

    switch(pscnr->TypeTest){
    case scnrWork: case scnrWorkDiviation: break;
    default:
        throw std::string("Error \"pscnr->TypeTest != scnrWork\" in tb_utils_cls::test_action_work!");
    }
    
    mtr::ce_mdl = electro.GetCE();
    if(pscnr->prm_Other.MagicUSE == UseOtherPrm){
        mtr::ce_mdl.Temp = pscnr->prm_Other.Temp;
    }
    while(tmt_work > 0){
        // -- Update curent time
        mtr::rtc_tbl.crnt_tmt += CalclTmtStep_Action(&tmt_work, &tmt_step, pscnr);
        mtr::GetDts_tmt(mtr::rtc_tbl.crnt_tmt, &mtr::rtc_tbl.crnt_dts);
        mtr::arch_var.fUpdateRTC = true;
        mtr::ce_mdl.ce_energy_update = true;
        // -- Calculated energy
        if(pscnr->TypeTest == scnrWorkDiviation) electro.Generate(pscnr->prm_ElNet.diviation);
        mtr::ce_mdl.enrg_int = energy.Calculate(tmt_step, electro);
        // -- Maneger Switch Summer/Winter
        if(mtr::rtc_tbl.rtc_save.fSwitchSummer) mtr::CheckValidWinterTime(true, true);
        // -- Run and update Archive module
        mtr::Energy_exec();
        for(int i = 0; i < 500; i++) mtr::Arch_exec();
        // --
        energy.rst_eint();
        tmt_work -= Min(tmt_work, tmt_step);
        if(!(tmt_work % 100)){
            //CPPUNIT_NS::stdCOut() << ".";
        }
    }
    mtr::ce_mdl.enrg_int = energy.Get();
}

void tb_utils_cls::test_action_make_break(time_t break_time) {
    //test_action_work(pscnr);
    
    mtr::DATE_TIME crnt_dts;
    mtr::GetDts_tmt(mtr::rtc_tbl.crnt_tmt, &crnt_dts);
    
    time_t new_time = mtr::rtc_tbl.crnt_tmt - break_time;
    mtr::DATE_TIME new_dts;
    mtr::GetDts_tmt(new_time, &new_dts);
    
    mtr::StartKillArch_foNewDTS(&crnt_dts, &new_dts);
    while(mtr::Arch_exec());
}
void tb_utils_cls::test_action_work_with_time_shift(const TB_SCENARIO_ARCHIVE *pscnr, time_t tmt_shift){
    time_t tmt_work = pscnr->tmt_work, tmt_step = pscnr->tmt_step_work;
    Electrical electro(pscnr->prm_ElNet);

    if(pscnr->TypeTest != scnrWorkWithShift) {
        throw std::string("Error \"pscnr->TypeTest != scnrWork\" in tb_utils_cls::test_action_work!");
    }
    
    mtr::ce_mdl = electro.GetCE();
    const uint8_t max_records_shift = 100;
    uint8_t records_before_shift = max_records_shift;
    tmt_of_shifts.clear();
    while(tmt_work > 0){
        // -- Update curent time
        //time_t tmt_shift = 6 * 24 * 60 * 60;//5 * 24 * 60 * 60;
        if((records_before_shift != 0) && (tmt_shift < tmt_work)) {
            mtr::rtc_tbl.crnt_tmt += CalclTmtStep_Action(&tmt_work, &tmt_step, pscnr);
            
        } else {
            tmt_of_shifts.push_back(mtr::rtc_tbl.crnt_tmt);
            mtr::rtc_tbl.crnt_tmt += tmt_shift;
        } 
        
        mtr::GetDts_tmt(mtr::rtc_tbl.crnt_tmt, &mtr::rtc_tbl.crnt_dts);
        mtr::arch_var.fUpdateRTC = true;
        mtr::ce_mdl.ce_energy_update = true;
        // -- Calculated energy
        if(pscnr->TypeTest == scnrWorkDiviation) electro.Generate(pscnr->prm_ElNet.diviation);
        mtr::ce_mdl.enrg_int = energy.Calculate(tmt_step, electro);
        // -- Run and update Archive module
        mtr::Energy_exec();
        for(int i = 0; i < 500; i++) mtr::Arch_exec();
        // --
        energy.rst_eint();
        tmt_work -= Min(tmt_work, tmt_step);
        
        records_before_shift--;

    }
    mtr::ce_mdl.enrg_int = energy.Get();
}

void tb_utils_cls::test_action_remove(const time_t &seconds_to_remove) {
    
    time_t new_time = mtr::rtc_tbl.crnt_tmt - seconds_to_remove;
    mtr::DATE_TIME new_dts;
    mtr::GetDts_tmt(new_time, &new_dts);
    
    time_t crnt_time = mtr::rtc_tbl.crnt_tmt;
    mtr::DATE_TIME crnt_dts;
    mtr::GetDts_tmt(crnt_time, &crnt_dts);
    
    mtr::rtc_tbl.crnt_tmt = new_time;
    mtr::GetDts_tmt(new_time, &mtr::rtc_tbl.crnt_dts);

    
    mtr::StartKillArch_foNewDTS(&crnt_dts, &new_dts);
    
    while(mtr::Arch_exec());
}

uint32_t tb_utils_cls::copy_pload(void *dst, LPCPAY_LOAD_RAW pload){
    uint32_t size_pload = 0;
    
    while( (pload->size_data != -1) && (pload->size_data != 0) ){
        memcpy(dst, pload->pData, pload->size_data);
        
        size_pload += pload->size_data;
        dst = (uint8_t*)dst + pload->size_data;
        pload++;
    }
    
    return size_pload;
}
//bool tb_utils_cls::check_pload(mtr::LPHeadPack pHead, LPCPAY_LOAD_RAW pload){
bool tb_utils_cls::check_pload(LPCTB_SCENARIO_ARCHIVE pscnr, mtr::LPHeadPack pHead, LPCPAY_LOAD_RAW pload){
    uint32_t size_pload = 0;
    uint8_t *psrc = pHead->Data;

    if((pload->size_data <= 0) && prtcl_dt.size_data) goto __error;
    if((pload->size_data > 0) && !prtcl_dt.size_data) goto __error;
    
    while( (pload->size_data != -1) && (pload->size_data != 0) ){
        if(memcmp(psrc, pload->pData, pload->size_data)) goto __error;
        
        size_pload += pload->size_data;
        psrc += pload->size_data;
        pload++;
    }
    /// TODO: Необходимо подумать каким образом добавить текущую запись в генерируемый пакет.
    ///       Н данный момент просто проигнорируем отсутствие в сгенерируемом пакете 
    ///       текущей записи профиля нагрузки.
    if(pscnr->TypeTest != scnrNormal_Check_ConstPQP){
        if(size_pload != prtcl_dt.size_data) goto __error;
    }

    return true;
    
__error:
    return false;
}
bool tb_utils_cls::TestScenario(const TB_SCENARIO_ARCHIVE *pscnr, std::string name_scnr){
    bool r = false;
    LPCPAY_LOAD_RAW pload = pscnr->pack_valid.pload;
    
    if(pscnr->TypeTest == scnrIgnor) goto __exit;
    
    try {
        time_t tmt_shift;
        time_t tmt_break;
        while(pscnr->TypeTest != scnrEND){
            pload = pscnr->pack_valid.pload;
            // -- Actions
            switch(pscnr->TypeTest){
            case scnrNormal:
            case scnrNormal_Check_ConstPQP:
            case scnrNormal_NoArchExec:
                test_action(pscnr);
                if(pscnr->TypeTest == scnrNormal_Check_ConstPQP){
                    pload = gen_pack.PackConstPQP(pscnr);
                }
                break;
            case scnrWork:
            case scnrWorkDiviation:
                test_action_work(pscnr);
                goto __next;
            case scnrWorkWithShift:
                tmt_shift = 6 * 24 * 60 * 60;
                test_action_work_with_time_shift(pscnr, tmt_shift);
                goto __next;
            case scnrCheckSparseData:
                tmt_shift = 6 * 24 * 60 * 60;
                pload = gen_pack.PackConstPQPSparseData(pscnr,tmt_of_shifts, tmt_shift);
                goto __next;        
            case scnrMakeRemove:
                test_action_remove(pscnr->tmt_work);
                goto __next;
            case scnrCheckArchive_Convergence:
                if(!CheckArchive_Convergence(pscnr)) goto __error;
                goto __next;
            case scnrAction_OffOn_Mtr:
                mtr::InitArch();
                goto __next;
            case scnrCheckArchiveExecCount:
                if(!CheckArchiveExecCount(pscnr, pload)) goto __error;
                goto __next;
//            case scnrCheckArchiveExecCountSparseData:
//                test_action_work(pscnr);
//                goto __next;
            case scnrAction_Off_Mtr:
                //if(mtr::CheckArchMech_busy()) mtr::save_ArchMeachVar();
                mtr::save_ArchMeachVar();
                mtr::EventSavePwrProfile(mtr::ePrflIncompl);
                goto __next;
            case scnrAction_On_Mtr:
                mtr::rtc_tbl.crnt_tmt += pscnr->tmt_work;
                mtr::GetDts_tmt(mtr::rtc_tbl.crnt_tmt, &mtr::rtc_tbl.crnt_dts);
                // --
                mtr::InitArch();
                goto __next;            default:
                throw std::string("pscnr->TypeTest - Not Finde in tb_utils_cls::TestScenario.");
            }
            // -- Check
            if(!check_pload(pscnr, prtcl_dt.pHead, pload)) goto __error;

            // -- NEXT action
        __next:
            pscnr++;
            indx_line_tab_scnr++;
            prtcl_dt.size_data = 0;
            // --
            reInit_prtcl_dt();
        }
    }
    catch(std::string mess_err){
        mess_err_tb = name_scnr + ";    !!! " + mess_err + "\r\n" + MessageErrTest(pscnr, eTMErr_OnlSett);
        goto __finally;
    }
    catch(const char* str_mess_err){
        mess_err_tb = name_scnr + ";    !!! " + std::string(str_mess_err) + "\r\n" + MessageErrTest(pscnr, eTMErr_OnlSett);
        goto __finally;
    }
    catch(...){
        goto __finally;
    }
    
__exit:
    r = true;
    goto __finally;

__error:
    mess_err_tb = name_scnr + ";    !!! " + "\r\n" + MessageErrTest(pscnr, pload);
    goto __finally;
    
__finally:
    reInit_prtcl_dt();
    /*if(pload != pscnr->pack_valid.pload){
        delete[] (uint8_t* )pload->pData;
        pload = pscnr->pack_valid.pload;
    }*/
    return r;
}
