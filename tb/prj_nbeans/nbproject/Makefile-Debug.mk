#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=Cygwin-Windows
CND_DLIB_EXT=dll
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/56252444/archive.o \
	${OBJECTDIR}/_ext/56252444/archive_answer.o \
	${OBJECTDIR}/_ext/56252444/archive_answer_Filter.o \
	${OBJECTDIR}/_ext/56252444/archive_cb.o \
	${OBJECTDIR}/_ext/56252444/archive_cmd.o \
	${OBJECTDIR}/_ext/56252444/archive_finde_by_index.o \
	${OBJECTDIR}/_ext/56252444/archive_ind.o \
	${OBJECTDIR}/_ext/56252444/archive_mech.o \
	${OBJECTDIR}/_ext/56252444/archive_utils.o \
	${OBJECTDIR}/_ext/56252444/profile_power.o \
	${OBJECTDIR}/_ext/56252444/profile_season_finde.o \
	${OBJECTDIR}/_ext/56252444/spodes_finde_records.o \
	${OBJECTDIR}/_ext/511e4115/archive_tb.o \
	${OBJECTDIR}/_ext/ebdb60c2/energy.o \
	${OBJECTDIR}/_ext/d6f73a19/ext_memory_tb.o \
	${OBJECTDIR}/_ext/f929bdc1/ctime.o \
	${OBJECTDIR}/_ext/f929bdc1/library.o \
	${OBJECTDIR}/_ext/f929bdc1/mktime.o \
	${OBJECTDIR}/_ext/f929bdc1/mmath.o \
	${OBJECTDIR}/_ext/2754c667/rtc_lib.o \
	${OBJECTDIR}/_ext/3183fefa/security.o \
	${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem.o \
	${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem_EEPROM.o \
	${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem_FLASH.o \
	${OBJECTDIR}/_ext/d52023fd/Clock.o \
	${OBJECTDIR}/_ext/d52023fd/Convergence.o \
	${OBJECTDIR}/_ext/d52023fd/ConversionType_to_Name.o \
	${OBJECTDIR}/_ext/d52023fd/Electrical.o \
	${OBJECTDIR}/_ext/d52023fd/Energy.o \
	${OBJECTDIR}/_ext/d52023fd/GeneratePack.o \
	${OBJECTDIR}/_ext/d52023fd/PackADAY.o \
	${OBJECTDIR}/_ext/d52023fd/PackAMONTH.o \
	${OBJECTDIR}/_ext/d52023fd/PackArchive.o \
	${OBJECTDIR}/_ext/d52023fd/PackPPR.o \
	${OBJECTDIR}/_ext/d52023fd/tb_utils.o \
	${OBJECTDIR}/_ext/d52023fd/tbu_Archive.o \
	${OBJECTDIR}/_ext/d52023fd/tbu_Protocol.o \
	${OBJECTDIR}/main.o

# Test Directory
TESTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}/tests

# Test Files
TESTFILES= \
	${TESTDIR}/TestFiles/f3/f8 \
	${TESTDIR}/TestFiles/f3/f7 \
	${TESTDIR}/TestFiles/f3/f1 \
	${TESTDIR}/TestFiles/f3/f2 \
	${TESTDIR}/TestFiles/f3/f3 \
	${TESTDIR}/TestFiles/f3/f4 \
	${TESTDIR}/TestFiles/f3/f5 \
	${TESTDIR}/TestFiles/f3/f6 \
	${TESTDIR}/TestFiles/f4/f1 \
	${TESTDIR}/TestFiles/f4/f3 \
	${TESTDIR}/TestFiles/f4/f2 \
	${TESTDIR}/TestFiles/f9/f1 \
	${TESTDIR}/TestFiles/f2 \
	${TESTDIR}/TestFiles/f1 \
	${TESTDIR}/TestFiles/f8 \
	${TESTDIR}/TestFiles/f7 \
	${TESTDIR}/TestFiles/f6 \
	${TESTDIR}/TestFiles/f5

# Test Object Files
TESTOBJECTFILES= \
	${TESTDIR}/_ext/355cc484/test_CnvrtDumpToExtMem.o \
	${TESTDIR}/_ext/3116a957/CmdMngPPR_scenario.o \
	${TESTDIR}/_ext/3116a957/test_CmdMngPpr_tcls.o \
	${TESTDIR}/_ext/3116a957/test_CmdMngPpr_trun.o \
	${TESTDIR}/_ext/4ea9c99d/CmdReadArch_NmbRcrd_scenario.o \
	${TESTDIR}/_ext/4ea9c99d/CmdReadArch_scenario.o \
	${TESTDIR}/_ext/4ea9c99d/test_CmdReadArch.o \
	${TESTDIR}/_ext/4ea9c99d/test_CmdReadArch_run.o \
	${TESTDIR}/_ext/130df44b/CmdReadPPR_NmbRcrd_scenario.o \
	${TESTDIR}/_ext/130df44b/CmdReadPPR_NmbRcrd_season_StW_scenario.o \
	${TESTDIR}/_ext/130df44b/CmdReadPPR_NmbRcrd_season_WtS_scenario.o \
	${TESTDIR}/_ext/130df44b/CmdReadPPR_Season_D60s_scenario.o \
	${TESTDIR}/_ext/130df44b/CmdReadPPR_Season_scenario.o \
	${TESTDIR}/_ext/130df44b/CmdReadPPR_scenario.o \
	${TESTDIR}/_ext/130df44b/test_CmdReadPPR.o \
	${TESTDIR}/_ext/130df44b/test_CmdReadPPR_run.o \
	${TESTDIR}/_ext/6174ab3b/CmdReadPPR_NmbTotal_scenario.o \
	${TESTDIR}/_ext/cb330a3f/Test_dump_05_09_17_scenario.o \
	${TESTDIR}/_ext/8fa8fa12/test_dump_tcls.o \
	${TESTDIR}/_ext/8fa8fa12/test_dump_trun.o \
	${TESTDIR}/_ext/cb3c2336/convergence_scenario.o \
	${TESTDIR}/_ext/cb3c2336/convergence_tcls.o \
	${TESTDIR}/_ext/cb3c2336/convergence_trun.o \
	${TESTDIR}/_ext/a8917848/arch_cnt_work_scenario.o \
	${TESTDIR}/_ext/a8917848/arch_depth_tcls.o \
	${TESTDIR}/_ext/a8917848/arch_depth_trun.o \
	${TESTDIR}/_ext/e88b830d/arch_exec_count_scenario.o \
	${TESTDIR}/_ext/e88b830d/arch_exec_count_tcls.o \
	${TESTDIR}/_ext/e88b830d/arch_exec_count_trun.o \
	${TESTDIR}/_ext/57272eb/arch_init_scenario.o \
	${TESTDIR}/_ext/57272eb/arch_init_tcls.o \
	${TESTDIR}/_ext/57272eb/arch_init_trun.o \
	${TESTDIR}/_ext/817ffb9f/arch_remove_scenario.o \
	${TESTDIR}/_ext/817ffb9f/arch_remove_tcls.o \
	${TESTDIR}/_ext/817ffb9f/arch_remove_trun.o \
	${TESTDIR}/_ext/832f68c3/arch_search_scenario.o \
	${TESTDIR}/_ext/832f68c3/arch_search_tcls.o \
	${TESTDIR}/_ext/832f68c3/arch_search_trun.o \
	${TESTDIR}/_ext/9588858f/test_Convergence.o \
	${TESTDIR}/_ext/9588858f/test_Electrical.o \
	${TESTDIR}/_ext/9588858f/test_Energy.o \
	${TESTDIR}/_ext/9588858f/test_PackADAY.o \
	${TESTDIR}/_ext/9588858f/test_PackAMONTH.o \
	${TESTDIR}/_ext/9588858f/test_PackPPR.o \
	${TESTDIR}/_ext/9588858f/test_tb_utils.o

# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=`cppunit-config --cflags`-fopenmp 
CXXFLAGS=`cppunit-config --cflags`-fopenmp 

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/prj_nbeans.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/prj_nbeans.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/prj_nbeans ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/56252444/archive.o: ../../src/archive.c
	${MKDIR} -p ${OBJECTDIR}/_ext/56252444
	${RM} "$@.d"
	$(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/56252444/archive.o ../../src/archive.c

${OBJECTDIR}/_ext/56252444/archive_answer.o: ../../src/archive_answer.c
	${MKDIR} -p ${OBJECTDIR}/_ext/56252444
	${RM} "$@.d"
	$(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/56252444/archive_answer.o ../../src/archive_answer.c

${OBJECTDIR}/_ext/56252444/archive_answer_Filter.o: ../../src/archive_answer_Filter.c
	${MKDIR} -p ${OBJECTDIR}/_ext/56252444
	${RM} "$@.d"
	$(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/56252444/archive_answer_Filter.o ../../src/archive_answer_Filter.c

${OBJECTDIR}/_ext/56252444/archive_cb.o: ../../src/archive_cb.c
	${MKDIR} -p ${OBJECTDIR}/_ext/56252444
	${RM} "$@.d"
	$(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/56252444/archive_cb.o ../../src/archive_cb.c

${OBJECTDIR}/_ext/56252444/archive_cmd.o: ../../src/archive_cmd.c
	${MKDIR} -p ${OBJECTDIR}/_ext/56252444
	${RM} "$@.d"
	$(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/56252444/archive_cmd.o ../../src/archive_cmd.c

${OBJECTDIR}/_ext/56252444/archive_finde_by_index.o: ../../src/archive_finde_by_index.c
	${MKDIR} -p ${OBJECTDIR}/_ext/56252444
	${RM} "$@.d"
	$(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/56252444/archive_finde_by_index.o ../../src/archive_finde_by_index.c

${OBJECTDIR}/_ext/56252444/archive_ind.o: ../../src/archive_ind.c
	${MKDIR} -p ${OBJECTDIR}/_ext/56252444
	${RM} "$@.d"
	$(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/56252444/archive_ind.o ../../src/archive_ind.c

${OBJECTDIR}/_ext/56252444/archive_mech.o: ../../src/archive_mech.c
	${MKDIR} -p ${OBJECTDIR}/_ext/56252444
	${RM} "$@.d"
	$(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/56252444/archive_mech.o ../../src/archive_mech.c

${OBJECTDIR}/_ext/56252444/archive_utils.o: ../../src/archive_utils.c
	${MKDIR} -p ${OBJECTDIR}/_ext/56252444
	${RM} "$@.d"
	$(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/56252444/archive_utils.o ../../src/archive_utils.c

${OBJECTDIR}/_ext/56252444/profile_power.o: ../../src/profile_power.c
	${MKDIR} -p ${OBJECTDIR}/_ext/56252444
	${RM} "$@.d"
	$(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/56252444/profile_power.o ../../src/profile_power.c

${OBJECTDIR}/_ext/56252444/profile_season_finde.o: ../../src/profile_season_finde.c
	${MKDIR} -p ${OBJECTDIR}/_ext/56252444
	${RM} "$@.d"
	$(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/56252444/profile_season_finde.o ../../src/profile_season_finde.c

${OBJECTDIR}/_ext/56252444/spodes_finde_records.o: ../../src/spodes_finde_records.c
	${MKDIR} -p ${OBJECTDIR}/_ext/56252444
	${RM} "$@.d"
	$(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/56252444/spodes_finde_records.o ../../src/spodes_finde_records.c

${OBJECTDIR}/_ext/511e4115/archive_tb.o: ../src/archive_tb.c
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	${RM} "$@.d"
	$(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/archive_tb.o ../src/archive_tb.c

${OBJECTDIR}/_ext/ebdb60c2/energy.o: ../src/energy/energy.c
	${MKDIR} -p ${OBJECTDIR}/_ext/ebdb60c2
	${RM} "$@.d"
	$(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/ebdb60c2/energy.o ../src/energy/energy.c

${OBJECTDIR}/_ext/d6f73a19/ext_memory_tb.o: ../src/ext_memory/ext_memory_tb.c
	${MKDIR} -p ${OBJECTDIR}/_ext/d6f73a19
	${RM} "$@.d"
	$(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d6f73a19/ext_memory_tb.o ../src/ext_memory/ext_memory_tb.c

${OBJECTDIR}/_ext/f929bdc1/ctime.o: ../src/library/ctime.c
	${MKDIR} -p ${OBJECTDIR}/_ext/f929bdc1
	${RM} "$@.d"
	$(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/f929bdc1/ctime.o ../src/library/ctime.c

${OBJECTDIR}/_ext/f929bdc1/library.o: ../src/library/library.c
	${MKDIR} -p ${OBJECTDIR}/_ext/f929bdc1
	${RM} "$@.d"
	$(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/f929bdc1/library.o ../src/library/library.c

${OBJECTDIR}/_ext/f929bdc1/mktime.o: ../src/library/mktime.c
	${MKDIR} -p ${OBJECTDIR}/_ext/f929bdc1
	${RM} "$@.d"
	$(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/f929bdc1/mktime.o ../src/library/mktime.c

${OBJECTDIR}/_ext/f929bdc1/mmath.o: ../src/library/mmath.c
	${MKDIR} -p ${OBJECTDIR}/_ext/f929bdc1
	${RM} "$@.d"
	$(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/f929bdc1/mmath.o ../src/library/mmath.c

${OBJECTDIR}/_ext/2754c667/rtc_lib.o: ../src/rtc/rtc_lib.c
	${MKDIR} -p ${OBJECTDIR}/_ext/2754c667
	${RM} "$@.d"
	$(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/2754c667/rtc_lib.o ../src/rtc/rtc_lib.c

${OBJECTDIR}/_ext/3183fefa/security.o: ../src/security/security.c
	${MKDIR} -p ${OBJECTDIR}/_ext/3183fefa
	${RM} "$@.d"
	$(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/3183fefa/security.o ../src/security/security.c

${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem.o: ../tests/Converting\ Dumps\ to\ ExtMemory/CnvrtDumpToExtMem.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/7cf7feb2
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem.o ../tests/Converting\ Dumps\ to\ ExtMemory/CnvrtDumpToExtMem.cpp

${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem_EEPROM.o: ../tests/Converting\ Dumps\ to\ ExtMemory/CnvrtDumpToExtMem_EEPROM.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/7cf7feb2
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem_EEPROM.o ../tests/Converting\ Dumps\ to\ ExtMemory/CnvrtDumpToExtMem_EEPROM.cpp

${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem_FLASH.o: ../tests/Converting\ Dumps\ to\ ExtMemory/CnvrtDumpToExtMem_FLASH.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/7cf7feb2
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem_FLASH.o ../tests/Converting\ Dumps\ to\ ExtMemory/CnvrtDumpToExtMem_FLASH.cpp

${OBJECTDIR}/_ext/d52023fd/Clock.o: ../tests/tb_utils/Clock.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/Clock.o ../tests/tb_utils/Clock.cpp

${OBJECTDIR}/_ext/d52023fd/Convergence.o: ../tests/tb_utils/Convergence.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/Convergence.o ../tests/tb_utils/Convergence.cpp

${OBJECTDIR}/_ext/d52023fd/ConversionType_to_Name.o: ../tests/tb_utils/ConversionType_to_Name.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/ConversionType_to_Name.o ../tests/tb_utils/ConversionType_to_Name.cpp

${OBJECTDIR}/_ext/d52023fd/Electrical.o: ../tests/tb_utils/Electrical.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/Electrical.o ../tests/tb_utils/Electrical.cpp

${OBJECTDIR}/_ext/d52023fd/Energy.o: ../tests/tb_utils/Energy.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/Energy.o ../tests/tb_utils/Energy.cpp

${OBJECTDIR}/_ext/d52023fd/GeneratePack.o: ../tests/tb_utils/GeneratePack.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/GeneratePack.o ../tests/tb_utils/GeneratePack.cpp

${OBJECTDIR}/_ext/d52023fd/PackADAY.o: ../tests/tb_utils/PackADAY.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/PackADAY.o ../tests/tb_utils/PackADAY.cpp

${OBJECTDIR}/_ext/d52023fd/PackAMONTH.o: ../tests/tb_utils/PackAMONTH.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/PackAMONTH.o ../tests/tb_utils/PackAMONTH.cpp

${OBJECTDIR}/_ext/d52023fd/PackArchive.o: ../tests/tb_utils/PackArchive.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/PackArchive.o ../tests/tb_utils/PackArchive.cpp

${OBJECTDIR}/_ext/d52023fd/PackPPR.o: ../tests/tb_utils/PackPPR.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/PackPPR.o ../tests/tb_utils/PackPPR.cpp

${OBJECTDIR}/_ext/d52023fd/tb_utils.o: ../tests/tb_utils/tb_utils.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/tb_utils.o ../tests/tb_utils/tb_utils.cpp

${OBJECTDIR}/_ext/d52023fd/tbu_Archive.o: ../tests/tb_utils/tbu_Archive.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/tbu_Archive.o ../tests/tb_utils/tbu_Archive.cpp

${OBJECTDIR}/_ext/d52023fd/tbu_Protocol.o: ../tests/tb_utils/tbu_Protocol.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/tbu_Protocol.o ../tests/tb_utils/tbu_Protocol.cpp

${OBJECTDIR}/main.o: main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Build Test Targets
.build-tests-conf: .build-tests-subprojects .build-conf ${TESTFILES}
.build-tests-subprojects:

${TESTDIR}/TestFiles/f3/f8: ${TESTDIR}/_ext/355cc484/test_CnvrtDumpToExtMem.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f3
	${LINK.cc} -o ${TESTDIR}/TestFiles/f3/f8 $^ ${LDLIBSOPTIONS}    

${TESTDIR}/TestFiles/f3/f7: ${TESTDIR}/_ext/9588858f/test_Convergence.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f3
	${LINK.cc} -o ${TESTDIR}/TestFiles/f3/f7 $^ ${LDLIBSOPTIONS}    

${TESTDIR}/TestFiles/f3/f1: ${TESTDIR}/_ext/9588858f/test_Electrical.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f3
	${LINK.cc} -o ${TESTDIR}/TestFiles/f3/f1 $^ ${LDLIBSOPTIONS}    

${TESTDIR}/TestFiles/f3/f2: ${TESTDIR}/_ext/9588858f/test_Energy.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f3
	${LINK.cc} -o ${TESTDIR}/TestFiles/f3/f2 $^ ${LDLIBSOPTIONS}    

${TESTDIR}/TestFiles/f3/f3: ${TESTDIR}/_ext/9588858f/test_PackADAY.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f3
	${LINK.cc} -o ${TESTDIR}/TestFiles/f3/f3 $^ ${LDLIBSOPTIONS}    

${TESTDIR}/TestFiles/f3/f4: ${TESTDIR}/_ext/9588858f/test_PackAMONTH.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f3
	${LINK.cc} -o ${TESTDIR}/TestFiles/f3/f4 $^ ${LDLIBSOPTIONS}    

${TESTDIR}/TestFiles/f3/f5: ${TESTDIR}/_ext/9588858f/test_PackPPR.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f3
	${LINK.cc} -o ${TESTDIR}/TestFiles/f3/f5 $^ ${LDLIBSOPTIONS}    

${TESTDIR}/TestFiles/f3/f6: ${TESTDIR}/_ext/9588858f/test_tb_utils.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f3
	${LINK.cc} -o ${TESTDIR}/TestFiles/f3/f6 $^ ${LDLIBSOPTIONS}    

${TESTDIR}/TestFiles/f4/f1: ${TESTDIR}/_ext/3116a957/CmdMngPPR_scenario.o ${TESTDIR}/_ext/3116a957/test_CmdMngPpr_tcls.o ${TESTDIR}/_ext/3116a957/test_CmdMngPpr_trun.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f4
	${LINK.cc} -o ${TESTDIR}/TestFiles/f4/f1 $^ ${LDLIBSOPTIONS}    `cppunit-config --libs`   

${TESTDIR}/TestFiles/f4/f3: ${TESTDIR}/_ext/4ea9c99d/CmdReadArch_NmbRcrd_scenario.o ${TESTDIR}/_ext/4ea9c99d/CmdReadArch_scenario.o ${TESTDIR}/_ext/4ea9c99d/test_CmdReadArch.o ${TESTDIR}/_ext/4ea9c99d/test_CmdReadArch_run.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f4
	${LINK.cc} -o ${TESTDIR}/TestFiles/f4/f3 $^ ${LDLIBSOPTIONS}    `cppunit-config --libs`   

${TESTDIR}/TestFiles/f4/f2: ${TESTDIR}/_ext/130df44b/CmdReadPPR_NmbRcrd_scenario.o ${TESTDIR}/_ext/130df44b/CmdReadPPR_NmbRcrd_season_StW_scenario.o ${TESTDIR}/_ext/130df44b/CmdReadPPR_NmbRcrd_season_WtS_scenario.o ${TESTDIR}/_ext/130df44b/CmdReadPPR_Season_D60s_scenario.o ${TESTDIR}/_ext/130df44b/CmdReadPPR_Season_scenario.o ${TESTDIR}/_ext/130df44b/CmdReadPPR_scenario.o ${TESTDIR}/_ext/130df44b/test_CmdReadPPR.o ${TESTDIR}/_ext/130df44b/test_CmdReadPPR_run.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f4
	${LINK.cc} -o ${TESTDIR}/TestFiles/f4/f2 $^ ${LDLIBSOPTIONS}    `cppunit-config --libs`   

${TESTDIR}/TestFiles/f9/f1: ${TESTDIR}/_ext/6174ab3b/CmdReadPPR_NmbTotal_scenario.o ${TESTDIR}/_ext/cb330a3f/Test_dump_05_09_17_scenario.o ${TESTDIR}/_ext/8fa8fa12/test_dump_tcls.o ${TESTDIR}/_ext/8fa8fa12/test_dump_trun.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles/f9
	${LINK.cc} -o ${TESTDIR}/TestFiles/f9/f1 $^ ${LDLIBSOPTIONS}   `cppunit-config --libs` 

${TESTDIR}/TestFiles/f2: ${TESTDIR}/_ext/a8917848/arch_cnt_work_scenario.o ${TESTDIR}/_ext/a8917848/arch_depth_tcls.o ${TESTDIR}/_ext/a8917848/arch_depth_trun.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f2 $^ ${LDLIBSOPTIONS}   `cppunit-config --libs`   

${TESTDIR}/TestFiles/f1: ${TESTDIR}/_ext/57272eb/arch_init_scenario.o ${TESTDIR}/_ext/57272eb/arch_init_tcls.o ${TESTDIR}/_ext/57272eb/arch_init_trun.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f1 $^ ${LDLIBSOPTIONS}   `cppunit-config --libs`   

${TESTDIR}/TestFiles/f8: ${TESTDIR}/_ext/e88b830d/arch_exec_count_scenario.o ${TESTDIR}/_ext/e88b830d/arch_exec_count_tcls.o ${TESTDIR}/_ext/e88b830d/arch_exec_count_trun.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f8 $^ ${LDLIBSOPTIONS}   `cppunit-config --libs`   

${TESTDIR}/TestFiles/f7: ${TESTDIR}/_ext/832f68c3/arch_search_scenario.o ${TESTDIR}/_ext/832f68c3/arch_search_tcls.o ${TESTDIR}/_ext/832f68c3/arch_search_trun.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f7 $^ ${LDLIBSOPTIONS}   `cppunit-config --libs`   

${TESTDIR}/TestFiles/f6: ${TESTDIR}/_ext/817ffb9f/arch_remove_scenario.o ${TESTDIR}/_ext/817ffb9f/arch_remove_tcls.o ${TESTDIR}/_ext/817ffb9f/arch_remove_trun.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f6 $^ ${LDLIBSOPTIONS}   `cppunit-config --libs`   

${TESTDIR}/TestFiles/f5: ${TESTDIR}/_ext/cb3c2336/convergence_scenario.o ${TESTDIR}/_ext/cb3c2336/convergence_tcls.o ${TESTDIR}/_ext/cb3c2336/convergence_trun.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f5 $^ ${LDLIBSOPTIONS}   `cppunit-config --libs`   


${TESTDIR}/_ext/355cc484/test_CnvrtDumpToExtMem.o: ../tests/Converting\ Dumps\ to\ ExtMemory/tests/test_CnvrtDumpToExtMem.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/355cc484
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/355cc484/test_CnvrtDumpToExtMem.o ../tests/Converting\ Dumps\ to\ ExtMemory/tests/test_CnvrtDumpToExtMem.cpp


${TESTDIR}/_ext/9588858f/test_Convergence.o: ../tests/tb_utils/tests/test_Convergence.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/9588858f
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/9588858f/test_Convergence.o ../tests/tb_utils/tests/test_Convergence.cpp


${TESTDIR}/_ext/9588858f/test_Electrical.o: ../tests/tb_utils/tests/test_Electrical.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/9588858f
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/9588858f/test_Electrical.o ../tests/tb_utils/tests/test_Electrical.cpp


${TESTDIR}/_ext/9588858f/test_Energy.o: ../tests/tb_utils/tests/test_Energy.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/9588858f
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/9588858f/test_Energy.o ../tests/tb_utils/tests/test_Energy.cpp


${TESTDIR}/_ext/9588858f/test_PackADAY.o: ../tests/tb_utils/tests/test_PackADAY.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/9588858f
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/9588858f/test_PackADAY.o ../tests/tb_utils/tests/test_PackADAY.cpp


${TESTDIR}/_ext/9588858f/test_PackAMONTH.o: ../tests/tb_utils/tests/test_PackAMONTH.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/9588858f
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/9588858f/test_PackAMONTH.o ../tests/tb_utils/tests/test_PackAMONTH.cpp


${TESTDIR}/_ext/9588858f/test_PackPPR.o: ../tests/tb_utils/tests/test_PackPPR.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/9588858f
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/9588858f/test_PackPPR.o ../tests/tb_utils/tests/test_PackPPR.cpp


${TESTDIR}/_ext/9588858f/test_tb_utils.o: ../tests/tb_utils/tests/test_tb_utils.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/9588858f
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/9588858f/test_tb_utils.o ../tests/tb_utils/tests/test_tb_utils.cpp


${TESTDIR}/_ext/3116a957/CmdMngPPR_scenario.o: ../tests/Tests.cmd/test.cmd_mng_ppr/CmdMngPPR_scenario.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/3116a957
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/3116a957/CmdMngPPR_scenario.o ../tests/Tests.cmd/test.cmd_mng_ppr/CmdMngPPR_scenario.cpp


${TESTDIR}/_ext/3116a957/test_CmdMngPpr_tcls.o: ../tests/Tests.cmd/test.cmd_mng_ppr/test_CmdMngPpr_tcls.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/3116a957
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/3116a957/test_CmdMngPpr_tcls.o ../tests/Tests.cmd/test.cmd_mng_ppr/test_CmdMngPpr_tcls.cpp


${TESTDIR}/_ext/3116a957/test_CmdMngPpr_trun.o: ../tests/Tests.cmd/test.cmd_mng_ppr/test_CmdMngPpr_trun.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/3116a957
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/3116a957/test_CmdMngPpr_trun.o ../tests/Tests.cmd/test.cmd_mng_ppr/test_CmdMngPpr_trun.cpp


${TESTDIR}/_ext/4ea9c99d/CmdReadArch_NmbRcrd_scenario.o: ../tests/Tests.cmd/test.cmd_read_arch/CmdReadArch_NmbRcrd_scenario.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/4ea9c99d
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/4ea9c99d/CmdReadArch_NmbRcrd_scenario.o ../tests/Tests.cmd/test.cmd_read_arch/CmdReadArch_NmbRcrd_scenario.cpp


${TESTDIR}/_ext/4ea9c99d/CmdReadArch_scenario.o: ../tests/Tests.cmd/test.cmd_read_arch/CmdReadArch_scenario.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/4ea9c99d
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/4ea9c99d/CmdReadArch_scenario.o ../tests/Tests.cmd/test.cmd_read_arch/CmdReadArch_scenario.cpp


${TESTDIR}/_ext/4ea9c99d/test_CmdReadArch.o: ../tests/Tests.cmd/test.cmd_read_arch/test_CmdReadArch.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/4ea9c99d
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/4ea9c99d/test_CmdReadArch.o ../tests/Tests.cmd/test.cmd_read_arch/test_CmdReadArch.cpp


${TESTDIR}/_ext/4ea9c99d/test_CmdReadArch_run.o: ../tests/Tests.cmd/test.cmd_read_arch/test_CmdReadArch_run.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/4ea9c99d
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/4ea9c99d/test_CmdReadArch_run.o ../tests/Tests.cmd/test.cmd_read_arch/test_CmdReadArch_run.cpp


${TESTDIR}/_ext/130df44b/CmdReadPPR_NmbRcrd_scenario.o: ../tests/Tests.cmd/test.cmd_read_ppr/CmdReadPPR_NmbRcrd_scenario.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/130df44b
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/130df44b/CmdReadPPR_NmbRcrd_scenario.o ../tests/Tests.cmd/test.cmd_read_ppr/CmdReadPPR_NmbRcrd_scenario.cpp


${TESTDIR}/_ext/130df44b/CmdReadPPR_NmbRcrd_season_StW_scenario.o: ../tests/Tests.cmd/test.cmd_read_ppr/CmdReadPPR_NmbRcrd_season_StW_scenario.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/130df44b
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/130df44b/CmdReadPPR_NmbRcrd_season_StW_scenario.o ../tests/Tests.cmd/test.cmd_read_ppr/CmdReadPPR_NmbRcrd_season_StW_scenario.cpp


${TESTDIR}/_ext/130df44b/CmdReadPPR_NmbRcrd_season_WtS_scenario.o: ../tests/Tests.cmd/test.cmd_read_ppr/CmdReadPPR_NmbRcrd_season_WtS_scenario.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/130df44b
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/130df44b/CmdReadPPR_NmbRcrd_season_WtS_scenario.o ../tests/Tests.cmd/test.cmd_read_ppr/CmdReadPPR_NmbRcrd_season_WtS_scenario.cpp


${TESTDIR}/_ext/130df44b/CmdReadPPR_Season_D60s_scenario.o: ../tests/Tests.cmd/test.cmd_read_ppr/CmdReadPPR_Season_D60s_scenario.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/130df44b
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/130df44b/CmdReadPPR_Season_D60s_scenario.o ../tests/Tests.cmd/test.cmd_read_ppr/CmdReadPPR_Season_D60s_scenario.cpp


${TESTDIR}/_ext/130df44b/CmdReadPPR_Season_scenario.o: ../tests/Tests.cmd/test.cmd_read_ppr/CmdReadPPR_Season_scenario.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/130df44b
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/130df44b/CmdReadPPR_Season_scenario.o ../tests/Tests.cmd/test.cmd_read_ppr/CmdReadPPR_Season_scenario.cpp


${TESTDIR}/_ext/130df44b/CmdReadPPR_scenario.o: ../tests/Tests.cmd/test.cmd_read_ppr/CmdReadPPR_scenario.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/130df44b
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/130df44b/CmdReadPPR_scenario.o ../tests/Tests.cmd/test.cmd_read_ppr/CmdReadPPR_scenario.cpp


${TESTDIR}/_ext/130df44b/test_CmdReadPPR.o: ../tests/Tests.cmd/test.cmd_read_ppr/test_CmdReadPPR.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/130df44b
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/130df44b/test_CmdReadPPR.o ../tests/Tests.cmd/test.cmd_read_ppr/test_CmdReadPPR.cpp


${TESTDIR}/_ext/130df44b/test_CmdReadPPR_run.o: ../tests/Tests.cmd/test.cmd_read_ppr/test_CmdReadPPR_run.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/130df44b
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/130df44b/test_CmdReadPPR_run.o ../tests/Tests.cmd/test.cmd_read_ppr/test_CmdReadPPR_run.cpp


${TESTDIR}/_ext/6174ab3b/CmdReadPPR_NmbTotal_scenario.o: ../tests/Tests.of.dumps/2.1.122/CmdReadPPR_NmbTotal_scenario.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/6174ab3b
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/6174ab3b/CmdReadPPR_NmbTotal_scenario.o ../tests/Tests.of.dumps/2.1.122/CmdReadPPR_NmbTotal_scenario.cpp


${TESTDIR}/_ext/cb330a3f/Test_dump_05_09_17_scenario.o: ../tests/Tests.of.dumps/4.0.125/Test_dump_05_09_17_scenario.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/cb330a3f
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/cb330a3f/Test_dump_05_09_17_scenario.o ../tests/Tests.of.dumps/4.0.125/Test_dump_05_09_17_scenario.cpp


${TESTDIR}/_ext/8fa8fa12/test_dump_tcls.o: ../tests/Tests.of.dumps/test_dump_tcls.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/8fa8fa12
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/8fa8fa12/test_dump_tcls.o ../tests/Tests.of.dumps/test_dump_tcls.cpp


${TESTDIR}/_ext/8fa8fa12/test_dump_trun.o: ../tests/Tests.of.dumps/test_dump_trun.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/8fa8fa12
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -I. `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/8fa8fa12/test_dump_trun.o ../tests/Tests.of.dumps/test_dump_trun.cpp


${TESTDIR}/_ext/a8917848/arch_cnt_work_scenario.o: ../tests/arch.depth/arch_cnt_work_scenario.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/a8917848
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/a8917848/arch_cnt_work_scenario.o ../tests/arch.depth/arch_cnt_work_scenario.cpp


${TESTDIR}/_ext/a8917848/arch_depth_tcls.o: ../tests/arch.depth/arch_depth_tcls.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/a8917848
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/a8917848/arch_depth_tcls.o ../tests/arch.depth/arch_depth_tcls.cpp


${TESTDIR}/_ext/a8917848/arch_depth_trun.o: ../tests/arch.depth/arch_depth_trun.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/a8917848
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/a8917848/arch_depth_trun.o ../tests/arch.depth/arch_depth_trun.cpp


${TESTDIR}/_ext/57272eb/arch_init_scenario.o: ../tests/arch.init/arch_init_scenario.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/57272eb
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/57272eb/arch_init_scenario.o ../tests/arch.init/arch_init_scenario.cpp


${TESTDIR}/_ext/57272eb/arch_init_tcls.o: ../tests/arch.init/arch_init_tcls.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/57272eb
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/57272eb/arch_init_tcls.o ../tests/arch.init/arch_init_tcls.cpp


${TESTDIR}/_ext/57272eb/arch_init_trun.o: ../tests/arch.init/arch_init_trun.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/57272eb
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/57272eb/arch_init_trun.o ../tests/arch.init/arch_init_trun.cpp


${TESTDIR}/_ext/e88b830d/arch_exec_count_scenario.o: ../tests/arch.exec.count/arch_exec_count_scenario.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/e88b830d
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/e88b830d/arch_exec_count_scenario.o ../tests/arch.exec.count/arch_exec_count_scenario.cpp


${TESTDIR}/_ext/e88b830d/arch_exec_count_tcls.o: ../tests/arch.exec.count/arch_exec_count_tcls.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/e88b830d
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/e88b830d/arch_exec_count_tcls.o ../tests/arch.exec.count/arch_exec_count_tcls.cpp


${TESTDIR}/_ext/e88b830d/arch_exec_count_trun.o: ../tests/arch.exec.count/arch_exec_count_trun.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/e88b830d
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/e88b830d/arch_exec_count_trun.o ../tests/arch.exec.count/arch_exec_count_trun.cpp


${TESTDIR}/_ext/832f68c3/arch_search_scenario.o: ../tests/arch.search/arch_search_scenario.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/832f68c3
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/832f68c3/arch_search_scenario.o ../tests/arch.search/arch_search_scenario.cpp


${TESTDIR}/_ext/832f68c3/arch_search_tcls.o: ../tests/arch.search/arch_search_tcls.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/832f68c3
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/832f68c3/arch_search_tcls.o ../tests/arch.search/arch_search_tcls.cpp


${TESTDIR}/_ext/832f68c3/arch_search_trun.o: ../tests/arch.search/arch_search_trun.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/832f68c3
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/832f68c3/arch_search_trun.o ../tests/arch.search/arch_search_trun.cpp


${TESTDIR}/_ext/817ffb9f/arch_remove_scenario.o: ../tests/arch.remove/arch_remove_scenario.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/817ffb9f
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/817ffb9f/arch_remove_scenario.o ../tests/arch.remove/arch_remove_scenario.cpp


${TESTDIR}/_ext/817ffb9f/arch_remove_tcls.o: ../tests/arch.remove/arch_remove_tcls.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/817ffb9f
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/817ffb9f/arch_remove_tcls.o ../tests/arch.remove/arch_remove_tcls.cpp


${TESTDIR}/_ext/817ffb9f/arch_remove_trun.o: ../tests/arch.remove/arch_remove_trun.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/817ffb9f
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/817ffb9f/arch_remove_trun.o ../tests/arch.remove/arch_remove_trun.cpp


${TESTDIR}/_ext/cb3c2336/convergence_scenario.o: ../tests/arch.convergence/convergence_scenario.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/cb3c2336
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/cb3c2336/convergence_scenario.o ../tests/arch.convergence/convergence_scenario.cpp


${TESTDIR}/_ext/cb3c2336/convergence_tcls.o: ../tests/arch.convergence/convergence_tcls.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/cb3c2336
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/cb3c2336/convergence_tcls.o ../tests/arch.convergence/convergence_tcls.cpp


${TESTDIR}/_ext/cb3c2336/convergence_trun.o: ../tests/arch.convergence/convergence_trun.cpp 
	${MKDIR} -p ${TESTDIR}/_ext/cb3c2336
	${RM} "$@.d"
	$(COMPILE.cc) -g -DPATH_ARCHIVE_tests=\"./../\" -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/_ext/cb3c2336/convergence_trun.o ../tests/arch.convergence/convergence_trun.cpp


${OBJECTDIR}/_ext/56252444/archive_nomain.o: ${OBJECTDIR}/_ext/56252444/archive.o ../../src/archive.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/56252444
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/56252444/archive.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/56252444/archive_nomain.o ../../src/archive.c;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/56252444/archive.o ${OBJECTDIR}/_ext/56252444/archive_nomain.o;\
	fi

${OBJECTDIR}/_ext/56252444/archive_answer_nomain.o: ${OBJECTDIR}/_ext/56252444/archive_answer.o ../../src/archive_answer.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/56252444
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/56252444/archive_answer.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/56252444/archive_answer_nomain.o ../../src/archive_answer.c;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/56252444/archive_answer.o ${OBJECTDIR}/_ext/56252444/archive_answer_nomain.o;\
	fi

${OBJECTDIR}/_ext/56252444/archive_answer_Filter_nomain.o: ${OBJECTDIR}/_ext/56252444/archive_answer_Filter.o ../../src/archive_answer_Filter.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/56252444
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/56252444/archive_answer_Filter.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/56252444/archive_answer_Filter_nomain.o ../../src/archive_answer_Filter.c;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/56252444/archive_answer_Filter.o ${OBJECTDIR}/_ext/56252444/archive_answer_Filter_nomain.o;\
	fi

${OBJECTDIR}/_ext/56252444/archive_cb_nomain.o: ${OBJECTDIR}/_ext/56252444/archive_cb.o ../../src/archive_cb.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/56252444
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/56252444/archive_cb.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/56252444/archive_cb_nomain.o ../../src/archive_cb.c;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/56252444/archive_cb.o ${OBJECTDIR}/_ext/56252444/archive_cb_nomain.o;\
	fi

${OBJECTDIR}/_ext/56252444/archive_cmd_nomain.o: ${OBJECTDIR}/_ext/56252444/archive_cmd.o ../../src/archive_cmd.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/56252444
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/56252444/archive_cmd.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/56252444/archive_cmd_nomain.o ../../src/archive_cmd.c;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/56252444/archive_cmd.o ${OBJECTDIR}/_ext/56252444/archive_cmd_nomain.o;\
	fi

${OBJECTDIR}/_ext/56252444/archive_finde_by_index_nomain.o: ${OBJECTDIR}/_ext/56252444/archive_finde_by_index.o ../../src/archive_finde_by_index.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/56252444
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/56252444/archive_finde_by_index.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/56252444/archive_finde_by_index_nomain.o ../../src/archive_finde_by_index.c;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/56252444/archive_finde_by_index.o ${OBJECTDIR}/_ext/56252444/archive_finde_by_index_nomain.o;\
	fi

${OBJECTDIR}/_ext/56252444/archive_ind_nomain.o: ${OBJECTDIR}/_ext/56252444/archive_ind.o ../../src/archive_ind.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/56252444
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/56252444/archive_ind.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/56252444/archive_ind_nomain.o ../../src/archive_ind.c;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/56252444/archive_ind.o ${OBJECTDIR}/_ext/56252444/archive_ind_nomain.o;\
	fi

${OBJECTDIR}/_ext/56252444/archive_mech_nomain.o: ${OBJECTDIR}/_ext/56252444/archive_mech.o ../../src/archive_mech.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/56252444
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/56252444/archive_mech.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/56252444/archive_mech_nomain.o ../../src/archive_mech.c;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/56252444/archive_mech.o ${OBJECTDIR}/_ext/56252444/archive_mech_nomain.o;\
	fi

${OBJECTDIR}/_ext/56252444/archive_utils_nomain.o: ${OBJECTDIR}/_ext/56252444/archive_utils.o ../../src/archive_utils.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/56252444
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/56252444/archive_utils.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/56252444/archive_utils_nomain.o ../../src/archive_utils.c;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/56252444/archive_utils.o ${OBJECTDIR}/_ext/56252444/archive_utils_nomain.o;\
	fi

${OBJECTDIR}/_ext/56252444/profile_power_nomain.o: ${OBJECTDIR}/_ext/56252444/profile_power.o ../../src/profile_power.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/56252444
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/56252444/profile_power.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/56252444/profile_power_nomain.o ../../src/profile_power.c;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/56252444/profile_power.o ${OBJECTDIR}/_ext/56252444/profile_power_nomain.o;\
	fi

${OBJECTDIR}/_ext/56252444/profile_season_finde_nomain.o: ${OBJECTDIR}/_ext/56252444/profile_season_finde.o ../../src/profile_season_finde.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/56252444
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/56252444/profile_season_finde.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/56252444/profile_season_finde_nomain.o ../../src/profile_season_finde.c;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/56252444/profile_season_finde.o ${OBJECTDIR}/_ext/56252444/profile_season_finde_nomain.o;\
	fi

${OBJECTDIR}/_ext/56252444/spodes_finde_records_nomain.o: ${OBJECTDIR}/_ext/56252444/spodes_finde_records.o ../../src/spodes_finde_records.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/56252444
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/56252444/spodes_finde_records.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/56252444/spodes_finde_records_nomain.o ../../src/spodes_finde_records.c;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/56252444/spodes_finde_records.o ${OBJECTDIR}/_ext/56252444/spodes_finde_records_nomain.o;\
	fi

${OBJECTDIR}/_ext/511e4115/archive_tb_nomain.o: ${OBJECTDIR}/_ext/511e4115/archive_tb.o ../src/archive_tb.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/511e4115
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/511e4115/archive_tb.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/511e4115/archive_tb_nomain.o ../src/archive_tb.c;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/511e4115/archive_tb.o ${OBJECTDIR}/_ext/511e4115/archive_tb_nomain.o;\
	fi

${OBJECTDIR}/_ext/ebdb60c2/energy_nomain.o: ${OBJECTDIR}/_ext/ebdb60c2/energy.o ../src/energy/energy.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/ebdb60c2
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/ebdb60c2/energy.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/ebdb60c2/energy_nomain.o ../src/energy/energy.c;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/ebdb60c2/energy.o ${OBJECTDIR}/_ext/ebdb60c2/energy_nomain.o;\
	fi

${OBJECTDIR}/_ext/d6f73a19/ext_memory_tb_nomain.o: ${OBJECTDIR}/_ext/d6f73a19/ext_memory_tb.o ../src/ext_memory/ext_memory_tb.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/d6f73a19
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/d6f73a19/ext_memory_tb.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d6f73a19/ext_memory_tb_nomain.o ../src/ext_memory/ext_memory_tb.c;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/d6f73a19/ext_memory_tb.o ${OBJECTDIR}/_ext/d6f73a19/ext_memory_tb_nomain.o;\
	fi

${OBJECTDIR}/_ext/f929bdc1/ctime_nomain.o: ${OBJECTDIR}/_ext/f929bdc1/ctime.o ../src/library/ctime.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/f929bdc1
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/f929bdc1/ctime.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/f929bdc1/ctime_nomain.o ../src/library/ctime.c;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/f929bdc1/ctime.o ${OBJECTDIR}/_ext/f929bdc1/ctime_nomain.o;\
	fi

${OBJECTDIR}/_ext/f929bdc1/library_nomain.o: ${OBJECTDIR}/_ext/f929bdc1/library.o ../src/library/library.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/f929bdc1
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/f929bdc1/library.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/f929bdc1/library_nomain.o ../src/library/library.c;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/f929bdc1/library.o ${OBJECTDIR}/_ext/f929bdc1/library_nomain.o;\
	fi

${OBJECTDIR}/_ext/f929bdc1/mktime_nomain.o: ${OBJECTDIR}/_ext/f929bdc1/mktime.o ../src/library/mktime.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/f929bdc1
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/f929bdc1/mktime.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/f929bdc1/mktime_nomain.o ../src/library/mktime.c;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/f929bdc1/mktime.o ${OBJECTDIR}/_ext/f929bdc1/mktime_nomain.o;\
	fi

${OBJECTDIR}/_ext/f929bdc1/mmath_nomain.o: ${OBJECTDIR}/_ext/f929bdc1/mmath.o ../src/library/mmath.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/f929bdc1
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/f929bdc1/mmath.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/f929bdc1/mmath_nomain.o ../src/library/mmath.c;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/f929bdc1/mmath.o ${OBJECTDIR}/_ext/f929bdc1/mmath_nomain.o;\
	fi

${OBJECTDIR}/_ext/2754c667/rtc_lib_nomain.o: ${OBJECTDIR}/_ext/2754c667/rtc_lib.o ../src/rtc/rtc_lib.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/2754c667
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/2754c667/rtc_lib.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/2754c667/rtc_lib_nomain.o ../src/rtc/rtc_lib.c;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/2754c667/rtc_lib.o ${OBJECTDIR}/_ext/2754c667/rtc_lib_nomain.o;\
	fi

${OBJECTDIR}/_ext/3183fefa/security_nomain.o: ${OBJECTDIR}/_ext/3183fefa/security.o ../src/security/security.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/3183fefa
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/3183fefa/security.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -D__UNIT_TEST__=1 -I../../inc -I../inc -I../../ -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/3183fefa/security_nomain.o ../src/security/security.c;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/3183fefa/security.o ${OBJECTDIR}/_ext/3183fefa/security_nomain.o;\
	fi

${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem_nomain.o: ${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem.o ../tests/Converting\ Dumps\ to\ ExtMemory/CnvrtDumpToExtMem.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/7cf7feb2
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem_nomain.o ../tests/Converting\ Dumps\ to\ ExtMemory/CnvrtDumpToExtMem.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem.o ${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem_nomain.o;\
	fi

${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem_EEPROM_nomain.o: ${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem_EEPROM.o ../tests/Converting\ Dumps\ to\ ExtMemory/CnvrtDumpToExtMem_EEPROM.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/7cf7feb2
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem_EEPROM.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem_EEPROM_nomain.o ../tests/Converting\ Dumps\ to\ ExtMemory/CnvrtDumpToExtMem_EEPROM.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem_EEPROM.o ${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem_EEPROM_nomain.o;\
	fi

${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem_FLASH_nomain.o: ${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem_FLASH.o ../tests/Converting\ Dumps\ to\ ExtMemory/CnvrtDumpToExtMem_FLASH.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/7cf7feb2
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem_FLASH.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem_FLASH_nomain.o ../tests/Converting\ Dumps\ to\ ExtMemory/CnvrtDumpToExtMem_FLASH.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem_FLASH.o ${OBJECTDIR}/_ext/7cf7feb2/CnvrtDumpToExtMem_FLASH_nomain.o;\
	fi

${OBJECTDIR}/_ext/d52023fd/Clock_nomain.o: ${OBJECTDIR}/_ext/d52023fd/Clock.o ../tests/tb_utils/Clock.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/d52023fd/Clock.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/Clock_nomain.o ../tests/tb_utils/Clock.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/d52023fd/Clock.o ${OBJECTDIR}/_ext/d52023fd/Clock_nomain.o;\
	fi

${OBJECTDIR}/_ext/d52023fd/Convergence_nomain.o: ${OBJECTDIR}/_ext/d52023fd/Convergence.o ../tests/tb_utils/Convergence.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/d52023fd/Convergence.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/Convergence_nomain.o ../tests/tb_utils/Convergence.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/d52023fd/Convergence.o ${OBJECTDIR}/_ext/d52023fd/Convergence_nomain.o;\
	fi

${OBJECTDIR}/_ext/d52023fd/ConversionType_to_Name_nomain.o: ${OBJECTDIR}/_ext/d52023fd/ConversionType_to_Name.o ../tests/tb_utils/ConversionType_to_Name.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/d52023fd/ConversionType_to_Name.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/ConversionType_to_Name_nomain.o ../tests/tb_utils/ConversionType_to_Name.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/d52023fd/ConversionType_to_Name.o ${OBJECTDIR}/_ext/d52023fd/ConversionType_to_Name_nomain.o;\
	fi

${OBJECTDIR}/_ext/d52023fd/Electrical_nomain.o: ${OBJECTDIR}/_ext/d52023fd/Electrical.o ../tests/tb_utils/Electrical.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/d52023fd/Electrical.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/Electrical_nomain.o ../tests/tb_utils/Electrical.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/d52023fd/Electrical.o ${OBJECTDIR}/_ext/d52023fd/Electrical_nomain.o;\
	fi

${OBJECTDIR}/_ext/d52023fd/Energy_nomain.o: ${OBJECTDIR}/_ext/d52023fd/Energy.o ../tests/tb_utils/Energy.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/d52023fd/Energy.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/Energy_nomain.o ../tests/tb_utils/Energy.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/d52023fd/Energy.o ${OBJECTDIR}/_ext/d52023fd/Energy_nomain.o;\
	fi

${OBJECTDIR}/_ext/d52023fd/GeneratePack_nomain.o: ${OBJECTDIR}/_ext/d52023fd/GeneratePack.o ../tests/tb_utils/GeneratePack.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/d52023fd/GeneratePack.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/GeneratePack_nomain.o ../tests/tb_utils/GeneratePack.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/d52023fd/GeneratePack.o ${OBJECTDIR}/_ext/d52023fd/GeneratePack_nomain.o;\
	fi

${OBJECTDIR}/_ext/d52023fd/PackADAY_nomain.o: ${OBJECTDIR}/_ext/d52023fd/PackADAY.o ../tests/tb_utils/PackADAY.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/d52023fd/PackADAY.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/PackADAY_nomain.o ../tests/tb_utils/PackADAY.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/d52023fd/PackADAY.o ${OBJECTDIR}/_ext/d52023fd/PackADAY_nomain.o;\
	fi

${OBJECTDIR}/_ext/d52023fd/PackAMONTH_nomain.o: ${OBJECTDIR}/_ext/d52023fd/PackAMONTH.o ../tests/tb_utils/PackAMONTH.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/d52023fd/PackAMONTH.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/PackAMONTH_nomain.o ../tests/tb_utils/PackAMONTH.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/d52023fd/PackAMONTH.o ${OBJECTDIR}/_ext/d52023fd/PackAMONTH_nomain.o;\
	fi

${OBJECTDIR}/_ext/d52023fd/PackArchive_nomain.o: ${OBJECTDIR}/_ext/d52023fd/PackArchive.o ../tests/tb_utils/PackArchive.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/d52023fd/PackArchive.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/PackArchive_nomain.o ../tests/tb_utils/PackArchive.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/d52023fd/PackArchive.o ${OBJECTDIR}/_ext/d52023fd/PackArchive_nomain.o;\
	fi

${OBJECTDIR}/_ext/d52023fd/PackPPR_nomain.o: ${OBJECTDIR}/_ext/d52023fd/PackPPR.o ../tests/tb_utils/PackPPR.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/d52023fd/PackPPR.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/PackPPR_nomain.o ../tests/tb_utils/PackPPR.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/d52023fd/PackPPR.o ${OBJECTDIR}/_ext/d52023fd/PackPPR_nomain.o;\
	fi

${OBJECTDIR}/_ext/d52023fd/tb_utils_nomain.o: ${OBJECTDIR}/_ext/d52023fd/tb_utils.o ../tests/tb_utils/tb_utils.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/d52023fd/tb_utils.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/tb_utils_nomain.o ../tests/tb_utils/tb_utils.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/d52023fd/tb_utils.o ${OBJECTDIR}/_ext/d52023fd/tb_utils_nomain.o;\
	fi

${OBJECTDIR}/_ext/d52023fd/tbu_Archive_nomain.o: ${OBJECTDIR}/_ext/d52023fd/tbu_Archive.o ../tests/tb_utils/tbu_Archive.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/d52023fd/tbu_Archive.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/tbu_Archive_nomain.o ../tests/tb_utils/tbu_Archive.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/d52023fd/tbu_Archive.o ${OBJECTDIR}/_ext/d52023fd/tbu_Archive_nomain.o;\
	fi

${OBJECTDIR}/_ext/d52023fd/tbu_Protocol_nomain.o: ${OBJECTDIR}/_ext/d52023fd/tbu_Protocol.o ../tests/tb_utils/tbu_Protocol.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d52023fd
	@NMOUTPUT=`${NM} ${OBJECTDIR}/_ext/d52023fd/tbu_Protocol.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d52023fd/tbu_Protocol_nomain.o ../tests/tb_utils/tbu_Protocol.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/_ext/d52023fd/tbu_Protocol.o ${OBJECTDIR}/_ext/d52023fd/tbu_Protocol_nomain.o;\
	fi

${OBJECTDIR}/main_nomain.o: ${OBJECTDIR}/main.o main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/main.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -I../../inc -I../inc -I../../ -I../tests/teamcity-cpp/cppunit -I../tests/teamcity-cpp/common -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main_nomain.o main.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/main.o ${OBJECTDIR}/main_nomain.o;\
	fi

# Run Test Targets
.test-conf:
	@if [ "${TEST}" = "" ]; \
	then  \
	    ${TESTDIR}/TestFiles/f3/f8 || true; \
	    ${TESTDIR}/TestFiles/f3/f7 || true; \
	    ${TESTDIR}/TestFiles/f3/f1 || true; \
	    ${TESTDIR}/TestFiles/f3/f2 || true; \
	    ${TESTDIR}/TestFiles/f3/f3 || true; \
	    ${TESTDIR}/TestFiles/f3/f4 || true; \
	    ${TESTDIR}/TestFiles/f3/f5 || true; \
	    ${TESTDIR}/TestFiles/f3/f6 || true; \
	    ${TESTDIR}/TestFiles/f4/f1 || true; \
	    ${TESTDIR}/TestFiles/f4/f3 || true; \
	    ${TESTDIR}/TestFiles/f4/f2 || true; \
	    ${TESTDIR}/TestFiles/f9/f1 || true; \
	    ${TESTDIR}/TestFiles/f2 || true; \
	    ${TESTDIR}/TestFiles/f1 || true; \
	    ${TESTDIR}/TestFiles/f8 || true; \
	    ${TESTDIR}/TestFiles/f7 || true; \
	    ${TESTDIR}/TestFiles/f6 || true; \
	    ${TESTDIR}/TestFiles/f5 || true; \
	else  \
	    ./${TEST} || true; \
	fi

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
