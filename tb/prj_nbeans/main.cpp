/* 
 * File:   main.c
 * Author: ShaginDL
 *
 * Created on 25 Декабрь 2014 г., 17:10
 */
/**
 * URL_SRC: http://www.c-cpp.ru/books/virtualnye-funkcii
 */
#include <iostream>

#include "define.h"

int main(int argc, char** argv) {
    
    for(int i = 60; i < 3600; i++){
        if(!(3600 % i )){
            std::cout << "Time = " << i << " (" << i / 60 << "min, " << i % 60 <<  "sec);\r\n";
        }
    }
    
    return 0;
}
#if 0
int main(int argc, char** argv) {
    int t;
    
    
#if (__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)
    t = 1;
#elif (__BYTE_ORDER__ == __ORDER_BIG_ENDIAN__)
    t = 2;
#endif
    
    return 0;
}
#endif

