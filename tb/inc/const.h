
#ifndef CONST_H
#define CONST_H

#define PSW_SIZE            6
#define SIZE_FLASH_CODE     0xFFFF

#define NMB_PHASE           3

struct _ENERGY{
    int32_t Ap, An;
    int32_t Rp, Rn;
};
typedef struct _ENERGY ENERGY, *LPENERGY;
typedef struct _ENERGY POWER, *LPPOWER;

struct _HEAD_LIST{
    uint16_t nmb_rcrds, indx_rcrd;
    AEMem_t Addr_rcrd;
};
typedef struct _HEAD_LIST HEAD_LIST, *LPHEAD_LIST;

#endif 