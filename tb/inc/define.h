#ifndef defineH
#define defineH

#ifdef	__cplusplus
namespace mtr{
    extern "C" {
#endif

#define NMB_PHASE 3
        
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include "fwm-common/fwm-common.h"


#include "library/mtime.h"
#include "library/library.h"
#include "library/mmath.h"


#include "ext_memory/flash_at25/flash_at25.h"
#include "pc_interfase/protocol.h"
#include "rtc/rtc.h"
#include "security/security.h"
#include "tarif/tarif.h"
#include "meter/meter.h"

#include "archive.h"
#include "archive_tb.h"


#include "energy/energy.h"
#include "modem/modem.h"
#include "CE/CE.h"
#include "ext_memory/memory.h"
        
#ifdef	__cplusplus
    }
}
#endif
    
#endif 