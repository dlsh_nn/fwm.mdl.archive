#ifndef modemH
#define modemH

#define ENBL_MODEM          ( ENBL_PIM_ISM2400x | ENBL_PIM_PLC | LCD_TERMINAL |         \
                              ENBL_GSM_EHS5     | ENBL_RS485   | ENBL_GSM_MG323 | ENBL_GSM_M660)
#define ENBL_MULT_MODEM     ( (ENBL_PIM_ISM2400x + ENBL_PIM_PLC +                               \
                               ENBL_GSM_EHS5 + ENBL_RS485 + ENBL_GSM_MG323 + ENBL_GSM_M660 +    \
                               ENBL_GSM_DRV_EGS5 + ENBL_GSM_DRV_EHS5) > 1 )
#define ENBL_DEBUG_MACH     (ENBL_PIM_PLC | ENBL_GSM_EHS5 | ENBL_GSM_MG323 | ENBL_GSM_M660)

#define __MODEM_UART()  /* -- debug -- */                               \
                        uint16_t mto_cnt_tx_dbg, mto_cnt_rx_dbg;    /* modem to optoport */ \
                        /* optoport to modem */                         \
                        uint16_t otm_cnt_tx_dbg, otm_cnt_rx_dbg;

#define TIME_OUT_DIRECT_ACSS    10    // 1 min
#define TIME_OUT_DBG_MDM_ACSS   5     // 1 min

enum enTypeMngAcss{
    enDrvrAcss = -1, 
    enDirectAcss = 0, 
    enIncapsAcss,
    enDebugModem,
    NMB_TYPE_MNG_ACSS
};
enum enModemStatus{mdm_READY_sts, mdm_BASY_sts, mdm_DONT_WORK_sts};

#define SIZE_KEY_ACSS 8

struct _MODEM_UART{
    __MODEM_UART()
};
typedef struct _MODEM_UART MODEM_UART, *LPMODEM_UART;

struct _MODEM_CONFIG{
    uint8_t raw[256];
};
typedef struct _MODEM_CONFIG MODEM_CONFIG, *LPMODEM_CONFIG;

typedef uint8_t (*fun_cmd_modem)(void); 
struct _TBL_INPUT_CMD{
    uint8_t cmd;
    uint8_t state;
    fun_cmd_modem fun_cmd;
};
typedef struct _TBL_INPUT_CMD TBL_INPUT_CMD, *LPTBL_INPUT_CMD;

#if ENBL_MODEM
#define ADDR_MODEM_CONFIG   GetAddr(modem_cnfg)

#define CHK_TimeOutLed_PLC              10
#if LCD_TERMINAL
    #define CHK_TimeOut_LcdRF           20

    #define CHK_TimeOut_LcdRF_RX_SMS    2
    #define CHK_TimeOut_LcdRF_TX_SMS    10
    #define CHK_TimeOut_LcdRF_ALL_SMS   20
    // --
    #define CHK_TimeOut_LcdRF_RX_GPRS   2
    #define CHK_TimeOut_LcdRF_TX_GPRS   5
#else
    #define CHK_TimeOut_LcdRF           2
    #define CHK_TimeOut_LcdRF_RX_SMS    2
    #define CHK_TimeOut_LcdRF_TX_SMS    10
    #define CHK_TimeOut_LcdRF_ALL_SMS   20
    // --
    #define CHK_TimeOut_LcdRF_RX_GPRS   2
    #define CHK_TimeOut_LcdRF_TX_GPRS   5
#endif

//----------------------------------------------------
// -- PACK
//----------------------------------------------------
struct _MNG_ACS{
    uint8_t psw[PSW_SIZE];
    uint8_t Type;
    uint8_t key[SIZE_KEY_ACSS];
};
typedef struct _MNG_ACS MNG_ACS, *LPMNG_ACS;
struct _ANSW_MNG_ACS{
    uint8_t Status;
};
typedef struct _ANSW_MNG_ACS ANSW_MNG_ACS, *LPANSW_MNG_ACS;
struct _INC_ACS{
    uint8_t Data[1];
};
typedef struct _INC_ACS INC_ACS, *LPINC_ACS;
struct _ANSW_INC_ACS{
    uint8_t Status;
};
typedef struct _ANSW_INC_ACS ANSW_INC_ACS, *LPANSW_INC_ACS;
struct _CNFG_MODEM{
    uint8_t psw[PSW_SIZE];
    union{
        CNFG_ISM_240x ism;
        CNFG_EGS5 cgf_egs5;
        uint8_t raw_data[1];    //
    }mdm;
};
typedef struct _CNFG_MODEM CNFG_MODEM, *LPCNFG_MODEM;

union _SIZE_Comm_Buff{
    // -- COMM modem variable
    #if ENBL_MULT_MODEM
        UART_DATA uart0_data;
    #endif
    // -- PLC MODEM
    #if ENBL_PIM_PLC
        struct{
            VAR_DRV_PLC vdp;
            VAR_PLC     vp;
        }PLC_TOTAL_VAR;
    #endif
    // -- RS485 MODEM
    #if ENBL_RS485
        struct{
            VAR_DRV_RS485 vdp;
        }RS485_TOTAL_VAR;
    #endif
    // -- MG323, M660, DRV_EGS5, DRV_EHS5 MODEM
    #if (ENBL_GSM_MG323 || ENBL_GSM_M660 || ENBL_GSM_DRV_EGS5 || ENBL_GSM_DRV_EHS5)
        struct{
            VAR_DRV_MG323 vdp;
            VAR_MG323     vp;
        }MG323_TOTAL_VAR;
    #endif
    // -- conversion_btwn_fmw
    CNV_BTWN_FMW CnvBtwnFmw;
    // -- raw
    uint8_t raw[1];
};
typedef union _SIZE_Comm_Buff SIZE_Comm_Buff;

struct _MODEM_TBL{
    enum enTypeMngAcss acss;
    uint8_t TimeOut_Acss, CHK_TimeOut_Acss;
    // --
    #if ENBL_MULT_MODEM
        SIZE_Comm_Buff Comm_Buff;
    #endif
};
typedef struct _MODEM_TBL MODEM_TBL, *LPMODEM_TBL;

extern MODEM_TBL mdm;
extern LPPROTOCOL_DATA prtcl_data_modem;

#if ENBL_MULT_MODEM
    extern const DRIVER code fmdm_NULL_driver;
    #define GetStateUart0()         uart0_data.state
#endif

#define GetMdmDirctAcss()    ((mdm.acss == enDirectAcss) || (DBG_UART == 1))
#define GetMdmDbgMdmAcss()   (mdm.acss == enDebugModem)
void InitModem(bool fBat);
void modem_exec(void);
void isr_modem_100ms(void) small reentrant;

void cmd_modem_cfg(LPPROTOCOL_DATA prtcl_data);

void cmd_mng_acs(LPPROTOCOL_DATA prtcl_data);
void cmd_inc_acs(LPPROTOCOL_DATA prtcl_data);
#if ENBL_DEBUG_MACH
    void isr_disconect_direct_acss_mech(uint8_t byte_rx) small reentrant;
#endif
void isr_OptoportToModem_dirAcss(uint8_t byte) small reentrant;

void InstallationModem(void);
#if ENBL_MULT_MODEM
    void fmdm_NULL_sr(void) small reentrant;
    void fmdm_NULL_sr_IRB(void);
    void fmdm_NULL(void);
    void fmdm_NULL_cmd_rx_prtcl(LPPROTOCOL_DATA prtcl_data);
    void fmdm_NULL_OptToMode_dirAcss(uint8_t byte) small reentrant;
    uint32_t fmdm_NULL_Build_prtcl_dt(LPPROTOCOL_DATA prtcl_data);
#else
    #define fmdm_NULL()
    #define fmdm_NULL_cmd_rx_prtcl(prtcl_data)
    #define fmdm_NULL_OptToMode_dirAcss(byte)
#endif
#if ENBL_PIM_ISM2400x
    void RF_ProtoTxCmd(unsigned char Cmd, unsigned char Status, unsigned char *pData, unsigned char size_data);
#else
    #define RF_ProtoTxCmd(Cmd, Status, pData, size_data)
#endif
#if ENBL_DEBUG_MACH
    void modem_DebugMach_exec(LPMODEM_UART pm_uart);
    void isr_modem_DebugMach_isr(LPMODEM_UART pm_uart, uint8_t byte_rx) small reentrant;
#endif
#if defined(__UNIT_TEST__)
    void putSBUF(uint8_t byte);
    void es0_isr(void) small reentrant interrupt ES0_IV using INTERFACE_REG_BANK;
#else
    #define putSBUF(byte)   SBUF = byte
#endif


#else

    #define InitModem(fBat)
    #define modem_exec()
    #define modem_100ms()

    #define GetMdmDirctAcss() false
    #define cmd_modem_cfg  DefaultCmd
    #define cmd_mng_acs    DefaultCmd
    #define cmd_inc_acs    DefaultCmd
    #define RF_ProtoTxCmd(Cmd, Status, pData, size_data)

#endif

#endif