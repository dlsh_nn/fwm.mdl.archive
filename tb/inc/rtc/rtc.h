#ifndef rtcH
#define rtcH

enum enTypeCorrectDTS{
    enCrctSOFT =            0x00,
    enCrctHARD =            0x01,
    enCrctOffSW_Summer =    0x02,
    enCrctOnSW_Summer =     0x03,
    enCrctErrValDTS =       0x04,
    enCrctBAT_OK =          0x05,
    enCrctBAT_fault =       0x06,
    enCrctFailureDTS =      0x07,
};
#define NO_DTS  {0, 0, 0, 0, 0, 0, 0}

struct _SAVE_RTC_PWROFF{
    uint8_t fSwitchSummer, fSummerTime;
    time_t tmt_BlockCorrectWinter;
};
typedef struct _SAVE_RTC_PWROFF SAVE_RTC_PWROFF, *LPSAVE_RTC_PWROFF;
typedef const SAVE_RTC_PWROFF *LPCSAVE_RTC_PWROFF;

enum enModeSoftCorect{eNO_CRCT, eSTART_SFT_CRCT, eBEGIN_CRCT};
struct _RTC_TBL{
    // -- Soft correction
    //enum enModeSoftCorect MdSftCrct;
    //time_t tmt_crct;
    //int8_t sgn_crct;
    //uint16_t sec_crct;
    // --
    //uint8_t fTimer_1s;
    // -- 
    DATE_TIME   crnt_dts;
    //DATE        crnt_ds;
    //TIME        crnt_tm;
    time_t      crnt_tmt;
    // --
    SAVE_RTC_PWROFF rtc_save;
    // --
    uint8_t cntTime_PrtcRstRTC;
};
typedef struct _RTC_TBL RTC_TBL, *LPRTC_TBL;
typedef const RTC_TBL *LPCRTC_TBL;

extern RTC_TBL rtc_tbl;

time_t GetTmtBgnSummerWinter_tmt(time_t tmt, bool fSummer);
time_t GetTmtBgnSummerWinter(LPDATE_TIME pdst, bool fSummer);
void CheckValidWinterTime(bool fNeedCorectTime, bool fNeedSavePPR);
void UpdateRtcTbl(void);
void UpdateRtcTbl_dts(LPDATE_TIME pdts);
uint8_t SetSummerWinter(void);
uint8_t SetSummerWinter_Check(LPDATE_TIME pdts);
uint8_t CheckSummerWinter_tmt(time_t tmt);

#include "rtc/rtc_lib.h"

#endif 