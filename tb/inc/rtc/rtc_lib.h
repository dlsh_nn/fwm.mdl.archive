#ifndef rtc_libH
#define rtc_libH

#define real_time_SEC   pDts_raw[0]
#define real_time_MIN   pDts_raw[1]
#define real_time_HR    pDts_raw[2]
#define real_time_DAY   pDts_raw[3]
#define real_time_DATE  pDts_raw[4]
#define real_time_MO    pDts_raw[5]
#define real_time_YER   pDts_raw[6]

//#define TMT_30MIN       1800ul
#define TMT_1HOUR       (2ul * TMT_30MIN)
#define TMT_1DAY        86400ul
#define TMT_1MONTH      (31 * TMT_1DAY)
#define TMT_NEW_MONTH(tm)   ((tm.sec == 0) && (tm.min == 0) && (tm.hour == 0))

#define MSK_SUMMER_TIME 0x80
#define MSK_WINTER_TIME 0x80

typedef time_t (*FUN_NexBgn_tmt)(time_t tmt);

#define dts_to_ds(dts, ds) memcpy(&ds, &dts.date, sizeof(ds))
#define dts_to_tm(dts, tm) memcpy(&tm, &dts.sec, sizeof(tm))

time_t GetTmt_dts(LPCDATE_TIME pdts);
void GetDs_tmt(time_t tmt, LPDATE pds);
time_t GetTmt_ds(LPCDATE pds);
time_t GetTmt_ts(LPTIME pts);
void GetDts_tmt(time_t tmt, LPDATE_TIME pdts);

bool check_ts(LPTIME ts);
bool check_dts(LPDATE_TIME dts);
void check_dts_correct(LPDATE_TIME pdts);

#endif