#ifndef PROTOCOL_H
#define PROTOCOL_H

#include "fwm-common/modules/mpro_interface/mpro_protocol.h"

#define MAX_DATA_SIZE	 600

enum enDrvMngRec{ enDrvMng_STOP_REC, enDrvMng_START_REC };

typedef void (*FunDrv_Wr)(uint8_t dt); 
typedef void (*FunDrv_ManagerRec)(enum enDrvMngRec flg);
typedef uint8_t (*FunDrv_Status)(void);

typedef void (*_pFunBuild_prtcl_dt)(void);

struct _Head{
    uint32_t    addr;
    uint16_t    cmd;
    uint8_t     Data[MAX_DATA_SIZE];
};
typedef struct _Head HeadPack, *LPHeadPack;
 
enum enTypeInterface{enInterface_MODEM, enInterface_Optoport, NMB_Interface};

struct _DRIVER{
    FunDrv_Wr               Wr;
    FunDrv_ManagerRec       ManagerRec;
    enum enTypeInterface    TypeInterface;
    FunDrv_Status           Status;
};
typedef struct _DRIVER DRIVER, *LPDRIVER;
typedef const DRIVER*  LPCDRIVER;

enum enUART_STATE{ust_IDLE, ust_GET_DATA, ust_ANLS_PACK, ust_ANLS_PACK_END, ust_STOP_RECV, ust_WAIT_OUT_DATA};

struct _UART_DATA{
    uint32_t size_buf;
    /*union{
        HeadPack head;
        uint8_t  buf[MAX_DATA_SIZE];
    }Dt;*/
};
typedef struct _UART_DATA  UART_DATA, *LPUART_DATA;

struct _PROTOCOL_DATA{
    LPCDRIVER pDriver;
    LPHeadPack pHead;
    enum enTypeAddr TypeAddr;
    uint32_t size_data;
    uint8_t crc;
    // -- Driver size_buf
    uint32_t *psize_buf;
};
typedef struct _PROTOCOL_DATA PROTOCOL_DATA, *LPPROTOCOL_DATA;

typedef void (*FunPack_exec)(LPPROTOCOL_DATA prtcl_data); 

void AnswPrtcl(LPPROTOCOL_DATA prtcl_data, uint16_t size_data);
void AnswPrtcl_begin(LPPROTOCOL_DATA prtcl_data, uint16_t size_data);
void AnswPrtcl_data(LPPROTOCOL_DATA prtcl_data, uint16_t size_data);
void AnswPrtcl_end(LPPROTOCOL_DATA prtcl_data);


#endif 