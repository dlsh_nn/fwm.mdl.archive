#ifndef SECURITY_H
#define SECURITY_H

#define pinAllAccess    (pin_INIT)

enum enPinInitMd{
    pinINIT_Normal, pinINIT_InitMtr
};

extern bool pin_INIT;

bool CheckPsw(uint8_t *chk_psw, enum enTypePsw TypePsw);
void SetPsw(uint8_t *new_psw, enum enTypePsw type);
void ClrPsw(void);

#endif 