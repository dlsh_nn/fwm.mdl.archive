/***************************************************************************
 * This code and information is provided "as is" without warranty of any
 * kind, either expressed or implied, including but not limited to the
 * implied warranties of merchantability and/or fitness for a particular
 * purpose.
 *
 * Copyright (C) 2008 Maxim Integrated Products, Inc. All Rights Reserved.
 * DESCRIPTION: This file has math functions that are not present in
 * the standard library.
 * 
 * AUTHOR:  RGV
 *
 * HISTORY: See end of file.
 **************************************************************************/
#ifndef MMATH_H
#define MMATH_H 1

// standard C99 library function not provided by Keil
// converts a float to the nearest long integer.
uint32_t abs_my(int32_t val) small reentrant;

/***************************************************************************
 * History:
 * $Log: mmath.h,v $
 * Revision 1.2  2010/05/24 23:24:00  Ray.Vandewalker
 * *** empty log message ***
 *
 * Revision 1.1  2008/10/07 22:39:45  tvander
 * Integrated global RAM, error reporting, real time clock.
 *
 *
 * Copyright (C) 2008 Maxim Integrated Products, Inc. All Rights Reserved.
 * this program is fully protected by the United States copyright
 * laws and is the property of Teridian Semiconductor Corporation.
 ***************************************************************************/
#endif
