#ifndef	_TIME_MTRH
#define _TIME_MTRH

struct tm {
        int	tm_sec;
        int	tm_min;
        int	tm_hour;
        int	tm_mday;
        int	tm_mon;
        int	tm_year;
        int	tm_wday;
        int	tm_yday;
        int	tm_isdst;
};

void mgmtime(const time_t * tp, struct tm *ptim);	/* Universal time */
extern time_t		mmktime(struct tm *);


#endif	/* _TIME */