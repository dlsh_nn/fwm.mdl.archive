#ifndef LIBRARY_H
#define LIBRARY_H

struct _LIST_FLASH_MEM{
    AEMem_t ADDR_BGN, SIZE_SECT;
    uint32_t NMB_RCRD_MAX;
    uint32_t NMB_RCRD_SECT;
    uint32_t SIZE_RECORD;
};
typedef struct _LIST_FLASH_MEM LIST_FLASH_MEM, *LPLIST_FLASH_MEM;
#define LPCLIST_FLASH_MEM const LIST_FLASH_MEM*

void NextRecord(LPHEAD_LIST pHead, uint16_t NmbRcrd_Cnfg, uint16_t NmbRcrd_Capacity);

uint32_t Addr_to_Indx(AEMem_t Addr, LPCLIST_FLASH_MEM plist);
AEMem_t Indx_to_Addr(uint32_t indx, LPCLIST_FLASH_MEM plist);
AEMem_t GetAddrRecord(int32_t indx, AEMem_t Addr_rcrd, LPCLIST_FLASH_MEM plist);
#define ClearArchRecord(tmt_check, type_clr, addr_head, plist)
uint8_t crc_calc(uint8_t crc, void *pData, uint16_t len);

#endif