#ifndef CE_H
#define CE_H

#define WHPMAX              1000000000L // Turn over a pulse counter at this number.  6.3(5/100); 5.4(5(10,60)) 

struct _CE_MDL{
    // --
    int32_t Vmv[NMB_PHASE], Ima[NMB_PHASE], CosF[NMB_PHASE + 1];
    // -- 
    int32_t Pa[NMB_PHASE + 1], Pr[NMB_PHASE + 1], Ps[NMB_PHASE + 1];
    //int32_t sumPa, sumPr, sumPs;
    ENERGY  enrg_int;
    // --
    int16_t Temp;
    
    
    uint8_t ce_energy_update;
};
typedef struct _CE_MDL CE_MDL, *LPCE_MDL;
typedef const CE_MDL* LPCCE_MDL;

extern CE_MDL ce_mdl;

#endif