#ifndef tarifH
#define tarifH

#define NMB_TARIF       8
#define NMB_TP          32
#define NMB_STT         32
#define NMB_TYPE_WTT    4
#define NMB_WTT         32
#define NMB_MD          40
#define NMB_TBL_TARIF   2

#define GetAddrTarifTbl(name)       (GetAddr(tarif_tbl) + offsetof(TARIF_TBL, name))
#define GetAddrTarif(indx, name)    (GetAddrTarifTbl(tarif[indx]) + offsetof(TARIF, name))

// -- 
struct _TARIF{
    SW_TBL_TMPL     stt[NMB_STT];
    uint8_t         NmbStt;
    WEEK_TBL_TMPL   wtt[NMB_WTT];
    uint8_t         NmbWtt;
    MONTH_TBL_TMPL  mtt;
    LWJ             lwj;
};
typedef struct _TARIF TARIF, *LPTARIF;
struct _TARIF_TBL{
    TARIF tarif[NMB_TBL_TARIF];
    uint8_t crnt_tarif;
};
typedef struct _TARIF_TBL TARIF_TBL, *LPTARIF_TBL;
//-----------------------------------------------------------------------
// JURNAL STS_MTR->eStsMtr_ChangeTarif
//-----------------------------------------------------------------------
struct _RCRD_JRNL_CHNG_TRF{
    uint8_t sts;
};
typedef struct _RCRD_JRNL_CHNG_TRF RCRD_JRNL_CHNG_TRF, *LPRCRD_JRNL_CHNG_TRF;
//-------------------------------------------------------------------------
// -- TABLE_VARIABLE
//-------------------------------------------------------------------------
enum enMdTARIF{enMdTrf_IDLE, enMdTrf_WR};
struct _TARIF_VAR{
    enum enMdTARIF Mode;
    uint8_t nmb_stt, nmb_wtt, nmb_lwj;
    uint8_t indx_wr_trf;
    // --
    uint8_t crnt_indx;
    uint8_t fUpdateRTC;
    // -- 
};
typedef struct _TARIF_VAR TARIF_VAR, *LPTARIF_VAR;

extern TARIF_VAR trf_var;

void InitTarif(bool fBat);
void isr_Tarif_500ms(void) small reentrant;
void Tarif_exec(void);
void get_nmb_stt(LPPROTOCOL_DATA prtcl_data);
void get_stt(LPPROTOCOL_DATA prtcl_data);
void get_nmb_wtt(LPPROTOCOL_DATA prtcl_data);
void get_wtt(LPPROTOCOL_DATA prtcl_data);
void get_mtt(LPPROTOCOL_DATA prtcl_data);
void get_lwj(LPPROTOCOL_DATA prtcl_data);
void start_wr_tarif(LPPROTOCOL_DATA prtcl_data);
void end_wr_tarif(LPPROTOCOL_DATA prtcl_data);
void write_stt(LPPROTOCOL_DATA prtcl_data);
void write_wtt(LPPROTOCOL_DATA prtcl_data);
void write_mtt(LPPROTOCOL_DATA prtcl_data);
void write_lwj(LPPROTOCOL_DATA prtcl_data);
// --
uint8_t GetTarifIndx(LPDATE_TIME pdts);

#if ENBL_TESTING
    void get_indx_tarif(LPPROTOCOL_DATA prtcl_data);
#else
    #define get_indx_tarif      DefaultCmd
#endif

#endif