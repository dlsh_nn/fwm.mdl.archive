#ifndef energyH
#define energyH

struct _ENERGY_DATA{
    ENERGYS_MTR_TRFS ETarif;      //ENERGY ETarif[NMB_TARIF];     // {EAp, EAn, ERp, ERn}[NMB_TARIF]
    ENERGYS_MTR_TRFS EpnTarif;    //ENERGY EpnTarif[NMB_TARIF];   // {EApn, ERpn}[NMB_TARIF] for EApn == EAp, ERpn == ERp
    int32_t EsumT_Ap, EsumT_An, EsumT_Apn;
    int32_t EsumT_Rp, EsumT_Rn, EsumT_Rpn;
};
typedef struct _ENERGY_DATA ENERGY_DATA, *LPENERGY_DATA;
struct _ENERGY_MNG{
    uint8_t rsrv;
};
typedef struct _ENERGY_MNG ENERGY_MNG, *LPENERGY_MNG;

extern ENERGY_DATA enrgDt;

void ClrEnergy(void);
void InitEnergy(void);
void isr_enrgy_100ms(void) small reentrant;
void Energy_exec(void);
#define SaveEnergy()    wr_ext_mem(GetAddr(enrg_save), &enrgDt, sizeof(enrgDt));
void overflow_check(void);
void Check_enrgDt_valid(void);

#endif
