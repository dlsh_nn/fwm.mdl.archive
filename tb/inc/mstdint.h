/***************************************************************************
 * This code and information is provided "as is" without warranty of any   *
 * kind, either expressed or implied, including but not limited to the     *
 * implied warranties of merchantability and/or fitness for a particular   *
 * purpose.                                                                *
 *                                                                         *
 * Copyright (C) 2005 Maxim Integrated Products Inc. All Rights Reserved.    *
 ***************************************************************************/
//**************************************************************************
//    
// DESCRIPTION: 71M652x POWER METER- standard integer definitions
// This include file is close to the POSIX and C99 standard.
// It doesn't have 64-bit definitions, because KEIL C doesn't.
// It doesn't have signal definitions.
// 
//  AUTHOR:  RGV
//
//  HISTORY: See end of file
//
//**************************************************************************
//               
// File: stdint.h
//               
//**************************************************************************

#ifndef STDINT_MTR_H
#define STDINT_MTR_H
#define HAVE_STDINT_H

#ifdef __KEIL__
    #define uchar   unsigned char 
    #define uint    unsigned short
    #define ulong   unsigned long
    #define schar   signed char 
    #define sint    signed short
    #define slong   signed long
#else
    #include <stdint.h>

    #define uchar   uint8_t 
    #define uint    uint16_t
    #define ulong   uint32_t
    #define schar   int8_t 
    #define sint    int16_t
    #define slong   int32_t
#endif

/* See your compiler manual for what this actually means.  Many
 * 8051 compilers, notably Keil, do not put local variables on
 * the stack! Instead, the linker manages a set of nesting overlays
 * in the type of memory selected by the memory model. So,
 * many compilers generate nonreentrant code by default. (I.e. unlike
 * other C compilers, functions can't just be called from interrupt code). */
#ifdef __KEIL__
    typedef uchar   uint8_t;
    typedef uint    uint16_t;
    typedef ulong   uint32_t;
    typedef schar   int8_t;
    typedef sint    int16_t;
    typedef slong   int32_t;
#endif

typedef int32_t time_t;		/* for representing times in seconds */

/* These are defined to make it easy to port to other 8051 compilers. 
 * Compared to other CPUs, the 8051 has a zoo of memory types. */

/* internal data, lower 128 bytes, addressed directly
 * Fastest available memory (except registers), not battery-backed-up,
 * competes with stack, registers, bools, and idata.
 * For portability, see uint_fast8_t and its sisters which are POSIX standard.
 * Obviously there's not much.  Don't waste it.
 * */
#ifdef __KEIL__
    typedef unsigned char data      uint8d_t;
    typedef unsigned short data     uint16d_t;
    typedef unsigned long data      uint32d_t;
    typedef signed char data        int8d_t;
    typedef signed short data       int16d_t;
    typedef signed long data        int32d_t;
#else
    typedef uchar   uint8d_t;
    typedef uint    uint16d_t;
    typedef ulong   uint32d_t;
    typedef schar   int8d_t;
    typedef sint    int16d_t;
    typedef slong   int32d_t;
#endif
/* internal data, 16 bytes (20h-2Fh), addressed directly, and bit addressable!
 * Fastest available memory, not battery-backed-up on meter chips,  
 * competes with stack, registers, bools, data, and idata. 
 * The space is valuable for bool globals. Don't waste it. */
#ifdef __KEIL__
    typedef unsigned char bdata      uint8b_t;
    typedef unsigned short bdata     uint16b_t;
    typedef unsigned long bdata      uint32b_t;
    typedef signed char bdata        int8b_t;
    typedef signed short bdata       int16b_t;
    typedef signed long bdata        int32b_t;
#else
    typedef uchar   uint8b_t;
    typedef uint    uint16b_t;
    typedef ulong   uint32b_t;
    typedef schar   int8b_t;
    typedef sint    int16b_t;
    typedef slong   int32b_t;
#endif

/* booleans are not a normal part of stdint.h, but pretty portable.
 * In Keil these go in 20h-2Fh.  They're fast, not battery-backed-up on meters,
 * and compete with bdata. Keil functions return bools in the carry bit, 
 * which makes code that's amusingly fast and small. */
#ifdef __KEIL__
    #undef bool
    typedef bit bool;
#else
    #ifndef	__cplusplus
        #define bool uint8_t
    #endif
#endif

#ifndef TRUE
    #define TRUE ((bool)1)
    #define FALSE ((bool)0)
#endif    
#define ON   ((bool)1)
#define OFF  ((bool)0)

#define true ((bool)1)
#define false ((bool)0)

/* internal data, 256 bytes, upper 128 addressed indirectly
 * pretty fast, not battery-backed-up on meter chips, 
 * ugh slower than data. Competes with data for space. */
#ifdef __KEIL__
    typedef unsigned char idata      uint8i_t;
    typedef unsigned short idata     uint16i_t;
    typedef unsigned long idata      uint32i_t;
    typedef signed char idata        int8i_t;
    typedef signed short idata       int16i_t;
    typedef signed long idata        int32i_t;
#else
    typedef uchar   uint8i_t;
    typedef uint    uint16i_t;
    typedef ulong   uint32i_t;
    typedef schar   int8i_t;
    typedef sint    int16i_t;
    typedef slong   int32i_t;
#endif

/* external data, 256 bytes of 2K of CMOS RAM,
 * The upper byte of the XDATA address is supplied by the sfr 0xBf, ADRMSB,
 * on meter chips.  On other 8051s, P2 filled this purpose.
 * Accessed indirectly. kinda fast, battery backed-up on 651X meter chips.
 * This is a great place for nonvolatile globals like power registers 
 * and configuration data. */
#ifdef __KEIL__
    typedef unsigned char pdata      uint8p_t;
    typedef unsigned short pdata     uint16p_t;
    typedef unsigned long pdata      uint32p_t;
    typedef signed char pdata        int8p_t;
    typedef signed short pdata       int16p_t;
    typedef signed long pdata        int32p_t;
#else
    typedef uchar   uint8p_t;
    typedef uint    uint16p_t;
    typedef ulong   uint32p_t;
    typedef schar   int8p_t;
    typedef sint    int16p_t;
    typedef slong   int32p_t;
#endif

/* external data,  2k of CMOS RAM, accessed indirectly via a 16-bit register. 
 * slowest but largest memory, battery backed-up on meter chips
 * Use it for everything possible.  On Keil's large model, this 
 * is the default! */
#ifdef __KEIL__
    typedef unsigned char xdata      uint8x_t;
    typedef unsigned short xdata     uint16x_t;
    typedef unsigned long xdata      uint32x_t;
    typedef signed char xdata        int8x_t;
    typedef signed short xdata       int16x_t;
    typedef signed long xdata        int32x_t;
#else
    typedef uchar   uint8x_t;
    typedef uint    uint16x_t;
    typedef ulong   uint32x_t;
    typedef schar   int8x_t;
    typedef sint    int16x_t;
    typedef slong   int32x_t;
#endif

/* external read-only data, in code space, 
 * accessed indirectly via a 16-bit register. 
 * slowest but largest place, nonvolatile programmable flash on meter chips
 * Use it for constants and tables. */
#ifdef __KEIL__
    typedef unsigned char code      uint8r_t;
    typedef unsigned short code     uint16r_t;
    typedef unsigned long code      uint32r_t;
    typedef signed char code        int8r_t;
    typedef signed short code       int16r_t;
    typedef signed long code        int32r_t;
#else
    #define code
    
    typedef uchar   uint8r_t;
    typedef uint    uint16r_t;
    typedef ulong   uint32r_t;
    typedef schar   int8r_t;
    typedef sint    int16r_t;
    typedef slong   int32r_t;
#endif
    
/* macros to define constants */
#ifdef __KEIL__
    #define INT8_C(_v_) _v_
    #define INT16_C(_v_) _v_
    #define INT32_C(_v_) _v_L
    #define INTMAX_C(_v_) _v_L
    #define UINT8_C(_v_) _v_
    #define UINT16_C(_v_) _v_
    #define UINT32_C(_v_) _v_UL
    #define UINTMAX_C(_v_) _v_UL

    #define UINT8_MAX  255
    #define UINT16_MAX 65535
    #define UINT32_MAX 4294967295L

    #define INT8_MIN  -128
    #define INT16_MIN -32768
    #define INT32_MIN -2147483648L

    #define INT8_MAX  127
    #define INT16_MAX 32767
    #define INT32_MAX 2147483647L

    /* Keil implements void pointers as 24-bit values, so
     * a 32-bit value is the smallest integer that can hold
     * a void pointer.  Typed pointers are always better.  */
    typedef unsigned long      uintptr_t;
    typedef signed long        intptr_t;

    #define UINTPTR_MAX 65535
    #define INTPTR_MIN -32768
    #define INTPTR_MAX 32767
    #define INTPTRDIFF_MIN -32768
    #define INTPTRDIFF_MAX 32767
    #define SIZE_MAX 65535

    #define WCHAR_MIN -128
    #define WCHAR_MAX 127
    #define WINT_MIN -32768
    #define WINT_MAX 32767


    typedef unsigned long      uintmax_t;
    typedef signed long        intmax_t;

    #define UINTMAX_MAX 4294967295L
    #define INTMAX_MIN -2147483648L
    #define INTMAX_MAX 2147483647L


    /* define a type that is at least the desired bit width.  See
     * your compiler manual for what this really means. */
    typedef unsigned char      uint_least8_t;
    typedef unsigned short     uint_least16_t;
    typedef unsigned long      uint_least32_t;

    typedef signed char        int_least8_t;
    typedef signed short       int_least16_t;
    typedef signed long        int_least32_t;

    #define INT_LEAST8_MIN -128
    #define INT_LEAST16_MIN -32768
    #define INT_LEAST32_MIN -2147483648L

    #define INT_LEAST8_MAX 127
    #define INT_LEAST16_MAX 32767
    #define INT_LEAST32_MAX 2147483647L

    #define UINT_LEAST8_MAX 255
    #define UINT_LEAST16_MAX 65535
    #define UINT_LEAST32_MAX 4294967295L

    /* define the fastest memory. */
    /* See uint8d_t and its brothers, above for a description of the memory. */
    typedef unsigned char data      uint_fast8_t;
    typedef unsigned short data     uint_fast16_t;
    typedef unsigned long  data     uint_fast32_t;

    typedef signed char data        int_fast8_t;
    typedef signed short data       int_fast16_t;
    typedef signed long data        int_fast32_t;

    #define INT_FAST8_MIN -128
    #define INT_FAST16_MIN -32768
    #define INT_FAST32_MIN -2147483648L

    #define INT_FAST8_MAX 127
    #define INT_FAST16_MAX 32767
    #define INT_FAST32_MAX 2147483647L

    #define UINT_FAST8_MAX 255
    #define UINT_FAST16_MAX 65535
    #define UINT_FAST32_MAX 4294967295L

#endif
    
/* These are not part of the standard, but they're very useful */
typedef union Uint8_16_t  { uint8_t c[2]; uint16_t i; } uint8_16_t;
typedef union Uint16_32_t  { uint16_t i[2]; uint32_t l; } uint16_32_t;
typedef union Uint8_16_32_t \
   { uint8_t c[4]; uint16_t i[2]; uint32_t l; } uint8_16_32_t;

typedef union Sint8_16_t  { int8_t c[2]; int16_t i; } int8_16_t;
typedef union Sint16_32_t  { int16_t i[2]; int32_t l; } int16_32_t;
typedef union Sint8_16_32_t \
   { int8_t c[4]; int16_t i[2]; int32_t l; } int8_16_32_t;

typedef struct Uint8_16_8_t { uint8_t f; uint16_t i; uint8_t c; } uint8_16_8_t;
typedef union  Uint8_16_8_32_t { uint8_16_8_t c; uint32_t l; } uint8_16_8_32_t;

#define get_size(val)       (sizeof(val) / sizeof(val[0]))
#ifndef __GNUC__
    #define offsetof(s,m)       ((size_t)&(((s *)0ul)->m))
    #define max(_v0_,_v1_) ((_v0_ > _v1_) ? _v0_ : _v1_)
#endif
#define Max(_v0_,_v1_) ((_v0_ > _v1_) ? _v0_ : _v1_)
#define Min(_v0_,_v1_) ((_v0_ < _v1_) ? _v0_ : _v1_)
#define sign(a)     (((a) > 0) - ((a) < 0))

#if 1 
    // BIG_ENDIAN
    #define HI 0
    #define LO 1

    #define HI_HI 0
    #define HI_LO 1
    #define LO_HI 2
    #define LO_LO 3
#else
    // LITTLE_ENDIAN
    #define HI 1
    #define LO 0

    #define HI_HI 3
    #define HI_LO 2
    #define LO_HI 1
    #define LO_LO 0
#endif

#define NO_BITS 0x00
#define BIT0    0x01
#define BIT1    0x02
#define BIT2    0x04
#define BIT3    0x08
#define BIT4    0x10
#define BIT5    0x20
#define BIT6    0x40
#define BIT7    0x80
#define BIT8    0x0100
#define BIT9    0x0200
#define BIT10   0x0400
#define BIT11   0x0800
#define BIT12   0x1000
#define BIT13   0x2000
#define BIT14   0x4000
#define BIT15   0x8000
#define BIT16   0x010000
#define BIT17   0x020000
#define BIT18   0x040000
#define BIT19   0x080000
#define BIT20   0x100000
#define BIT21   0x200000
#define BIT22   0x400000
#define BIT23   0x800000
#define BIT24   0x01000000
#define BIT25   0x02000000
#define BIT26   0x04000000
#define BIT27   0x08000000
#define BIT28   0x10000000
#define BIT29   0x20000000
#define BIT30   0x40000000
#define BIT31   0x80000000

#if (defined(__KEIL__))// && !defined(_SIZE_T))
    //typedef unsigned long size_t;		// type yielded by sizeof
    #define _SIZE_T						// to ignore warning from string.h
#endif
        
typedef uint32_t AEMem_t;
//typedef uint16_t AEMemEE_t;
//typedef uint32_t AEMemFLSH_t;

#define ASSERT_CONCAT_(a, b) a##b
#define ASSERT_CONCAT(a, b) ASSERT_CONCAT_(a, b)
#define ct_assert(e) enum { ASSERT_CONCAT(assert_line_, __LINE__) = (1 / (e)) }

#ifndef __KEIL__
    #define small 
    #define reentrant
    #define interrupt
    #define _at_(address)
    #define xdata

    #define sfr     uint8_t
    #define sbit    uint8_t

    #pragma pack(push,1)
#endif

#endif /* STDINT_H */
