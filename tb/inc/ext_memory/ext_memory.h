#ifndef ext_memoryH
#define ext_memoryH

#define ADDR_EEPROM             0xA0
#define SIZE_EEPROM             16384ul

// макрос получения смещения члена структуры
#define GetAddr(name)     (offsetof(EXT_MEMORY, name))
#define GetFullName(name)   (EXT_MEMORY.name)
#define GetAddrArray(name, strct, indx)     (GetAddr(name) + sizeof(strct) * (AEMem_t)indx)
#define GetAddrFLASH(name)  (offsetof(EXT_MEMORY_FLASH, name))

#define VERSION_EEPROM VERSION(0, 0, 1)
struct _EXT_MEMORY{
    //-----------------------------------------------------------------------------
    // -- EEPROM
    //-----------------------------------------------------------------------------
    struct _EEPROM{
        union _SECT{
            struct _NAME{
                uint32_t Version;
                
                uint32_t      _AddrMeter;
                uint8_t       _Password[NMB_TYPE_PSW][PSW_SIZE];

                ENERGY_DATA   _enrg_save;
                ARCHIV_HEAD   _arch_head;
                HEAD_LIST     _pwr_prfl_head;
                // -- archiv_mech
                EE_ARCH_SAVE  _save_Arch;
                ARCH_MECH_VAR _save_ArchMechVar;
            }name;
            uint8_t raw[SIZE_EEPROM];
        }sect;
    }EEPROM;
};
typedef struct _EXT_MEMORY EXT_MEMORY, *LPEXT_MEMORY;

#define NMB_RCRD_SECT_ARCH          (SIZE_SECTOR / sizeof(RCRD_ARCH_WITH_SHIFT))
#define NMB_RCRD_SECT_PwrPrfl       (SIZE_SECTOR / sizeof(RCRD_PWR_PRFL_WITH_SHIFT))

#define NMB_RCRD_SECT_for_DAY       (NMB_RCRD_DAY / NMB_RCRD_SECT_ARCH)
#define SIZE_SECT_ARCH_DAY          ((NMB_RCRD_SECT_for_DAY + 2) * SIZE_SECTOR)//(6 * SIZE_SECTOR)

#define NMB_RCRD_SECT_for_MONTH     (NMB_RCRD_MNTH / NMB_RCRD_SECT_ARCH)
#define SIZE_SECT_ARCH_MONTH        ((NMB_RCRD_SECT_for_MONTH + 2) * SIZE_SECTOR)//(3 * SIZE_SECTOR)

#define NMB_RCRD_SECT_for_PRFL      (NMB_RCRD_PWR_PRFL / NMB_RCRD_SECT_PwrPrfl)
#define SIZE_SECT_PWR_PRFLv1        ((NMB_RCRD_SECT_for_PRFL + 2) * SIZE_SECTOR)    // ((6000 / (4096 / 58)) + 2) * 4096 = 356352
//-----------------------------------------------------------------------------
// -- Address space flash
//-----------------------------------------------------------------------------
// -- archive
#define ADDR_FLASH_VerMEM               SIZE_EEPROM
#define ADDR_FLASH_sect_arch_bgn_day    (ADDR_FLASH_VerMEM + SIZE_SECTOR)         // 3444
#define ADDR_FLASH_sect_arch_bgn_mnth   (ADDR_FLASH_sect_arch_bgn_day + SIZE_SECT_ARCH_DAY)     // 677
#define ADDR_FLASH_sect_pwr_prfl        (ADDR_FLASH_sect_arch_bgn_mnth + SIZE_SECT_ARCH_MONTH)    // 262144 = 64 * 4096

#define ADDR_FLASH_sect_open_intf_case  (ADDR_FLASH_sect_open_kolod_mtr + SIZE_SECT_JRNL_OPEN_MTR)
#define ADDR_FLASH_sect_on_of_mtr       (ADDR_FLASH_sect_open_intf_case + SIZE_SECT_JRNL_OPEN_MTR)

// -- for test and debug
#define ADDR_FLASH_END                  (ADDR_FLASH_sect_renew_bgn + SIZE_SECT_RENEW)

//-----------------------------------------------------------------------------
#define Version             EEPROM.sect.name.Version
#define AddrMeter           EEPROM.sect.name._AddrMeter
#define Password            EEPROM.sect.name._Password
#define pwr_prfl            EEPROM.sect.name._pwr_prfl

#define enrg_save           EEPROM.sect.name._enrg_save
#define arch_head           EEPROM.sect.name._arch_head
#define pwr_prfl_head       EEPROM.sect.name._pwr_prfl_head
#define save_ArchMechVar    EEPROM.sect.name._save_ArchMechVar
#define save_Arch           EEPROM.sect.name._save_Arch

#define SIZE_BUFF_set_ext_mem   256

void rd_ext_mem(AEMem_t addr, void *dst, uint16_t size);
void wr_ext_mem(AEMem_t addr, const void *src, uint16_t size);
    
#include "ext_memory_tb.h"

#endif
