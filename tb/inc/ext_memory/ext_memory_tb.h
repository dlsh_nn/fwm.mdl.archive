/* 
 * File:   ext_memory.h
 * Author: ShaginDL
 *
 * Created on 25 Декабрь 2014 г., 19:13
 */

#ifndef EXT_MEMORY_TB_H
#define	EXT_MEMORY_TB_H

#ifdef	__cplusplus
extern "C" {
#endif

    #ifdef EXT_MEMORY_TB_H//__UNIT_TEST__
    
        #define NMB_RCRD_STS_MTR_SECT       (SIZE_SECTOR / sizeof(RCRD_JRNL_STS_MTR))
        #define RSRFV_STS_MTR               (SIZE_SECTOR - NMB_RCRD_STS_MTR_SECT * sizeof(RCRD_JRNL_STS_MTR))
        #define NMB_RCRD_PWR_MNG_SECT       (SIZE_SECTOR / sizeof(RCRD_JRNL_PWR_MNG))
        #define RSRFV_PWR_MNG               (SIZE_SECTOR - NMB_RCRD_PWR_MNG_SECT * sizeof(RCRD_JRNL_PWR_MNG))
        #define NMB_RCRD_PQP_MNG_SECT       (SIZE_SECTOR / sizeof(RCRD_JRNL_PQP_MTR))
        #define RSRFV_PQP_MNG               (SIZE_SECTOR - NMB_RCRD_PQP_MNG_SECT * sizeof(RCRD_JRNL_PQP_MTR))
        
        #define VERSION_FLASH VERSION(1, 0, 1)
        struct _EXT_FLASH{
            //------------------------------------------------------------------
            // -- EEPROM
            //------------------------------------------------------------------
            struct _FLASH{
                union _SECT_FLASH{
                    struct _NAME_FLASH{
                        uint8_t reserv_for_version[SIZE_SECTOR];
                        struct _DAY_ARCH{
                            struct _DAY_SECTL{
                                RCRD_ARCH_WITH_SHIFT rcrds[NMB_RCRD_SECT_ARCH];
                                uint8_t rsrv[SIZE_SECTOR - NMB_RCRD_SECT_ARCH * sizeof(RCRD_ARCH_WITH_SHIFT)];
                            }sect[SIZE_SECT_ARCH_DAY / SIZE_SECTOR];
                        }ArchDay;
                        
                        struct _MONTH_ARCH{
                            struct _MONTH_SECTL{
                                RCRD_ARCH_WITH_SHIFT rcrds[NMB_RCRD_SECT_ARCH];
                                uint8_t rsrv[SIZE_SECTOR - NMB_RCRD_SECT_ARCH * sizeof(RCRD_ARCH_WITH_SHIFT)];
                            }sect[SIZE_SECT_ARCH_MONTH / SIZE_SECTOR];
                        }ArchMonth;
                        //uint8_t reserv[ADDR_FLASH_sect_pwr_prfl - ADDR_FLASH_sect_arch_bgn_mnth - SIZE_EEPROM];
                        
                        struct _PWR_PRFL{
                            struct _PPR_SECT{
                                RCRD_PWR_PRFL_WITH_SHIFT rcrds[NMB_RCRD_SECT_PwrPrfl];
                                uint8_t rsrv[SIZE_SECTOR - NMB_RCRD_SECT_PwrPrfl * sizeof(RCRD_PWR_PRFL_WITH_SHIFT)];
                            }sect[SIZE_SECT_PWR_PRFLv1 / SIZE_SECTOR];
                        }PwrPrfl;
                    }name;
                    uint8_t raw[SIZE_FLASH];
                }sect;
            }FLASH;
        };
        typedef struct _EXT_FLASH EXT_FLASH, *LPEXT_FLASH;
        
        extern EXT_MEMORY ExtMemory;
        extern EXT_FLASH ExtFlash;

    #endif
    void ers_ExtMemory(void);
    void set_ext_mem(AEMem_t addr, uint8_t val, uint16_t size);
#ifdef	__cplusplus
}
#endif

#endif	/* EXT_MEMORY_H */

