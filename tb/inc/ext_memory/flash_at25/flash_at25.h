#ifndef flash_at25
#define flash_at25

#define SIZE_SECTOR     0x1000ul
#define SIZE_PAGE_PAGE  256ul
#define mskSIZE_SECTOR  (~(SIZE_SECTOR - 1))
#define SIZE_FLASH      0x80000ul
#define MAX_ADDR	    (SIZE_FLASH - 1)

#include "flash_at25_tb.h"

extern bool fErrorFlash;

void InitFlash_at25(void);
void ers_at25_all(void);
void ers_at25(uint32_t addr, uint32_t size);
void wr_at25(uint32_t addr, void *src, uint16_t size);
void rd_at25(uint32_t addr, void *dst, uint16_t size);
void unpr_at25_all(void);
void unpr_at25(uint32_t addr, uint32_t size);
//void pr_at25(uint32_t addr, uint32_t size);
#ifndef MDL_RENEW_STATIC
    void TestFlashValid(void);
#else
    #define TestFlashValid() {}
#endif

#else
        
#define fErrorFlash     false
        
#endif
