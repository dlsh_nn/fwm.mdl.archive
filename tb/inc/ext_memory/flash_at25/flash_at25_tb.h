#ifndef flash_at25_tb
#define flash_at25_tb

#define AT25_TB 0

#if AT25_TB
    void at25_tb(void);
#else
    #define at25_tb()
#endif

#endif