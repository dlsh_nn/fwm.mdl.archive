#ifndef fwm_commonH
#define fwm_commonH

#ifdef	__cplusplus
extern "C" {
#endif

#include "inc/default_defines.h"

#include "inc/types.h"
#include "inc/const.h"
#include "modules/rtc/rtc_comm.h"
#include "modules/mpro_interface/mpro_protocol.h"
#include "modules\memory\memory_comm.h"
	
#ifdef	__cplusplus
}
#endif

#endif // !fwm-commonH

