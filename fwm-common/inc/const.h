
#ifndef CONST_H
#define CONST_H

enum enTypeMeter{
    MAYAK_103ATD
};

#define PSW_SIZE            6
#define SIZE_FLASH_CODE     0xFFFF

#define MAX_NMB_CYCLES_IND  6
#define MAX_MODE_METER    254
#define NMB_PHASE          3

struct _ENERGY{
    int32_t Ap, An;
    int32_t Rp, Rn;
};
typedef struct _ENERGY ENERGY, *LPENERGY;
typedef struct _ENERGY POWER, *LPPOWER;

struct _HEAD_LIST{
    uint16_t nmb_rcrds, indx_rcrd;
    AEMem_t Addr_rcrd;
};
typedef struct _HEAD_LIST HEAD_LIST, *LPHEAD_LIST;

#define _BigEnd_u16(val)   ((val >> 8) | (val << 8))
//#define _BigEnd_u32(val)   ((val >> 8) | (val << 8))

#endif 
