## README ##

**fwm-common** - репозиторий для размещения общих файлов необходимых для нескольких проектов.

----------

## Проверка перед push into develop ##
1. **Цель:** обеспечить стабильность всех связанных проектов. Ниже описан набор действий гарантирующие выполнения цели. 
2. Выкладываем ветку в которой производились изменения на *origin*.
1. Проверка для проекта *fwm-intf-module*.
    1. Клонируем [fwm-intf-module](https://bitbucket.org/cart-team/fwm-intf-module.git) ветка *develop*, переключаем поддерево *fwm-common*  на ветку где производились изменения. Забираем актуальные изменения как ПКМ по *fwm-common*, *Получить fwm-common*. При получении возникают конфликты которые необходимо разрешить вручную выбрав *Использовать их*.
    1. Запускаем *Visual Studio 2017*, открываем решение *fwm-intf-module\Projects\vs_project\fwm-intf-module.sln*.
        1. Запускаем тесты *Тест->Выполнить->Все тесты* или *Cntrl+R,A*. Все тесты должны пройти **успешно**.
    2. Запускаем *IAR for ARM*, открываем workspace *fwm-intf-module\Projects\IAR_EWARM\project.eww*.
        1. Собираем проект *F7*. Проект должен **собраться** без ошибок.
2. Проверка для проекта *fwm.mdl.archive*
    1. Клонируем проект [fwm.mdl.archive](https://bitbucket.org/dlsh_nn/fwm.mdl.archive.git) ветка *develop*, переключаем поддерево *fwm-common*  на ветку где производились изменения. Забираем актуальные изменения как ПКМ по *fwm-common*, *Получить fwm-common*. 
        1. При получении возникают конфликты которые необходимо разрешить вручную выбрав *Использовать их*.
        1. Комитим изменения в develop связанный с слиянием.
    2. Открываем проект *fwm.mdl.archive\tb\prj_nbeans* в *NetBeans*.
    1. Запускаем все тесты *Alt+F6*. Все тесты **должны** пройти. 
3. Проверка для проекта *fwm-meter*
    1. Клонируем проект [fwm-meter](https://bitbucket.org/cart-team/fwm-meter.git) ветка *develop*, переключаем поддерево *fwm-common*  на ветку где производились изменения. Забираем актуальные изменения как ПКМ по *fwm-common*, *Получить fwm-common*.
    2. Открываем проект *fwm-meter\prj_NetBeans\mayak_meters* в *NetBeans*.
    3. Запускаем все тесты *Alt+F6*. Всен тесты **должны** пройти.
4. **В случае возникновение ошибок**, правим ошибки в проекте, комитим в *develop* не отправляя в *origin*.
    1.  В случае изменения в проекте *fwm-common*, отправляем *fwm-common* в *origin*.
    2.  В случае изменений в проекте из ветки *develop*, переключаем поддерево *fwm-common* на ветку *master*. Отпрвляем изменения в *origin/develop*.
3.  После проверки, что все проекты с данными изменениями стабильны. Необходимо открыть репозиторий *fwm-common* и ветку созданную в п.1 перенести в мастер согласно спецификации [GitFlow](https://habrahabr.ru/post/106912/).

## Применяемость ##
Здесь описывается проекты в которых применяется данный репозиторий/проект.


1. [fwm-meter](https://bitbucket.org/cart-team/fwm-meter.git) - проект для ПП ПУ, описание [здесь](https://bitbucket.org/cart-team/fwm-meter/src/6e4b053c6b5b2993ce0f0e84ed432f854bc89801/README.md?at=develop&fileviewer=file-view-default).  
    2. [fwm.mdl.archive](https://bitbucket.org/dlsh_nn/fwm.mdl.archive.git) - проект модуля архивов, описание [здесь](https://bitbucket.org/dlsh_nn/fwm.mdl.archive/src/9a8b710e56623a26580792a7ddeadd154d4270a2/README.md?at=develop&fileviewer=file-view-default).  
3. [fwm-intf-module](https://bitbucket.org/cart-team/fwm-intf-module.git) - проект ПП МИ, описание [здесь](https://bitbucket.org/cart-team/fwm-intf-module/src/a3f060c9337301b9a6dc2daccd068c590219108b/README.md?at=develop&fileviewer=file-view-default). 

## Использование ##
В проекте где используются модули из каталога modules, необходимо подключить
в качестве каталога включения места поиска заголовочных файлов компилятора расположение директории *fwm-common*, но не его самого.  
*Например:* Для проекта `fwm.mdl.archive\tb\prj_nbeans\` в проект *prj_nbeans* подключен данный модуль как `../../`.   
