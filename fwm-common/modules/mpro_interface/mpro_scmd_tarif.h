#ifndef mpro_struct_command_tarifH
#define mpro_struct_command_tarifH

#ifndef tarifH
    #define NMB_TARIF       8
    #define NMB_TP          32
    #define NMB_STT         32
    #define NMB_TYPE_WTT    4
    #define NMB_WTT         32
    #define NMB_MD          40
    #define NMB_TBL_TARIF   2
#endif

//=============================================================================
// TARIF
//=============================================================================
// -- TP
struct _TIME_POINT {
    TIME    ts;
    uint8_t NmbTrfSw;
};
typedef struct _TIME_POINT TIME_POINT, *LPTIME_POINT;
// -- STT
struct _SW_TBL_TMPL {
    uint8_t NmbTP;
    TIME_POINT tp[NMB_TP];
};
typedef struct _SW_TBL_TMPL SW_TBL_TMPL, *LPSW_TBL_TMPL;
// -- WTT
struct _WEEK_TBL_TMPL {
    uint8_t IndxSTT[NMB_TYPE_WTT];
};
typedef struct _WEEK_TBL_TMPL WEEK_TBL_TMPL, *LPWEEK_TBL_TMPL;
// -- MTT
struct _MONTH_TBL_TMPL {
    uint8_t IndxWTT[12];
};
typedef struct _MONTH_TBL_TMPL MONTH_TBL_TMPL, *LPMONTH_TBL_TMPL;
// -- MD
struct _MOVED_DAY {
    uint8_t day, month;
    uint8_t TypeDay;
};
typedef struct _MOVED_DAY MOVED_DAY, *LPMOVED_DAY;
// -- LWJ
struct _LWJ {
    uint8_t NmbMD;
    MOVED_DAY md[NMB_MD];
};
typedef struct _LWJ LWJ, *LPLWJ;
//-----------------------------------------------------------------------
// PACK
//-----------------------------------------------------------------------
// -- STT
struct _PACK_ANSW_NMB_STT {
    uint8_t Status;
    uint8_t NmbSTT;
};
typedef struct _PACK_ANSW_NMB_STT PACK_ANSW_NMB_STT, *LPPACK_ANSW_NMB_STT;
struct _PACK_GET_GET_STT {
    uint8_t indx_stt;
};
typedef struct _PACK_GET_GET_STT PACK_GET_GET_STT, *LPPACK_GET_GET_STT;
struct _PACK_ANSW_STT {
    uint8_t Status;
    uint8_t NmbTP;
    TIME_POINT tp[1];
};
typedef struct _PACK_ANSW_STT PACK_ANSW_STT, *LPPACK_ANSW_STT;
// -- WTT
struct _PACK_ANSW_NMB_WTT {
    uint8_t Status;
    uint8_t NmbWTT;
};
typedef struct _PACK_ANSW_NMB_WTT PACK_ANSW_NMB_WTT, *LPPACK_ANSW_NMB_WTT;
struct _PACK_GET_WTT {
    uint8_t indx_wtt;
};
typedef struct _PACK_GET_WTT PACK_GET_WTT, *LPPACK_GET_WTT;
struct _PACK_ANSW_WTT {
    uint8_t Status;
    WEEK_TBL_TMPL wtt;
};
typedef struct _PACK_ANSW_WTT PACK_ANSW_WTT, *LPPACK_ANSW_WTT;
// -- MTT
struct _PACK_ANSW_MTT {
    uint8_t Status;
    MONTH_TBL_TMPL mtt;
};
typedef struct _PACK_ANSW_MTT PACK_ANSW_MTT, *LPPACK_ANSW_MTT;
// -- LWJ
struct _PACK_GET_LWJ {
    uint8_t indx, nmb;
};
typedef struct _PACK_GET_LWJ PACK_GET_LWJ, *LPPACK_GET_LWJ;
struct _PACK_ANSW_LWJ {
    uint8_t Status;
    LWJ lwj;
};
typedef struct _PACK_ANSW_LWJ PACK_ANSW_LWJ, *LPPACK_ANSW_LWJ;
// -- START_WR_TARIF
struct _PACK_BGN_WR_TRF {
    uint8_t psw[PSW_SIZE];
};
typedef struct _PACK_BGN_WR_TRF PACK_BGN_WR_TRF, *LPPACK_BGN_WR_TRF;
// -- END_WR_TARIF
struct _PACK_END_WR_TRF {
    uint8_t psw[PSW_SIZE];
};
typedef struct _PACK_END_WR_TRF PACK_END_WR_TRF, *LPPACK_END_WR_TRF;
// -- WR_STT
struct _PACK_WR_STT {
    uint8_t     psw[PSW_SIZE];
    uint8_t     indx_stt;
    SW_TBL_TMPL stt;
};
typedef struct _PACK_WR_STT PACK_WR_STT, *LPPACK_WR_STT;
struct _PACK_WR_STT_for_DLMS {
    uint8_t     indx_stt;
    SW_TBL_TMPL stt;
};
typedef struct _PACK_WR_STT_for_DLMS PACK_WR_STT_for_DLMS, *LPPACK_WR_STT_for_DLMS;
// -- WR_WTT
struct _PACK_WR_WTT {
    uint8_t     psw[PSW_SIZE];
    int8_t      indx_wtt;
    WEEK_TBL_TMPL wtt;
};
typedef struct _PACK_WR_WTT PACK_WR_WTT, *LPPACK_WR_WTT;
struct _PACK_WR_WTT_for_DLMS {
    int8_t      indx_wtt;
    WEEK_TBL_TMPL wtt;
};
typedef struct _PACK_WR_WTT_for_DLMS PACK_WR_WTT_for_DLMS, *LPPACK_WR_WTT_for_DLMS;
// -- WR_MTT
struct _PACK_WR_MTT {
    uint8_t         psw[PSW_SIZE];
    MONTH_TBL_TMPL  mtt;
};
typedef struct _PACK_WR_MTT PACK_WR_MTT, *LPPACK_WR_MTT;
struct _PACK_WR_MTT_for_DLMS {
    MONTH_TBL_TMPL  mtt;
};
typedef struct _PACK_WR_MTT_for_DLMS PACK_WR_MTT_for_DLMS, *LPPACK_WR_MTT_for_DLMS;
// -- WR_LWJ
struct _PACK_WR_LWJ {
    uint8_t psw[PSW_SIZE];
    uint8_t Index;
    LWJ     lwj;
};
typedef struct _PACK_WR_LWJ PACK_WR_LWJ, *LPPACK_WR_LWJ;
struct _PACK_WR_LWJ_for_DLMS {
    uint8_t Index;
    LWJ     lwj;
};
typedef struct _PACK_WR_LWJ_for_DLMS PACK_WR_LWJ_for_DLMS, *LPPACK_WR_LWJ_for_DLMS;
// -- GET_INDX_TARIF
struct _PACK_GET_INDX_TRF {
    DATE_TIME dts;
};
typedef struct _PACK_GET_INDX_TRF PACK_GET_INDX_TRF, *LPPACK_GET_INDX_TRF;
struct _PACK_ANSW_INDX_TRF {
    uint8_t Status;
    uint8_t IndxTrf;
};
typedef struct _PACK_ANSW_INDX_TRF PACK_ANSW_INDX_TRF, *LPPACK_ANSW_INDX_TRF;

#endif
