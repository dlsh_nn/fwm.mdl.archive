#ifndef FWM_COMMON_PROTOCOL_H
#define FWM_COMMON_PROTOCOL_H

#define PACKET_FLG	        0x7E
#define STAFF_BYTE	        0x5D
#define STAFF_MASK		    0x20
#define CMD_ANSW_MSK        0x8000

enum enTypeAddr{
    TypeAddr_NotMine,
    TypeAddr_Mine,
    TypeAddr_PU,
    TypeAddr_Group1,
    TypeAddr_Group2,
    TypeAddr_Retrans,
    TypeAddr_Another
};
enum enAddrPrtcl{
	ADDR_GRPP_1 			= 0x0000,
	ADDR_GRPP_2 			= 0x0001
};
#define ADDR_METER_GROUP1   0
#define ADDR_METER_GROUP2   1
#define ADDR_METER_PULT		2
#define ADDR_METER_IDCH     3

enum enCmdPrtcl{
    SET_PASSWORD_COMMAND    = 0x0010, 	  //код команды 2 байта - данные хексом
    READ_VER_COMMAND        = 0x0011,
    SET_ADDR_COMMAND        = 0x0012,
    GET_ADDR_COMMAND        = 0x0250,
    READ_TIME_COMMAND       = 0x0013,
    SET_TIME_SOFT_COMMAND   = 0x0014,    
    SET_TIME_HARD_COMMAND   = 0x0015,
    SET_SWITCH_SUMMER       = 0x0033,
    GET_SWITCH_SUMMER       = 0x0034,
    // -- TARIF
    GET_NMB_STT             = 0x0016,
    GET_STT                 = 0x0017,
    GET_NMB_WTT             = 0x0018,
    GET_WTT                 = 0x0019,
    GET_MTT                 = 0x001A,
    GET_LWJ                 = 0x001B,
    START_WR_TARIF          = 0x001C,
    END_WR_TARIF            = 0x001D,
    WRITE_STT               = 0x001E,
    WRITE_WTT               = 0x001F,
    WRITE_MTT               = 0x0020,
    WRITE_LWJ               = 0x0021,
    GET_INDX_TARIF          = 0x0031,
    // -- MODE IND
    CMD_SET_MODE_IND        = 0x0027,
    CMD_GET_MODE_IND        = 0x0028,
    // -- MODE_IMP_OUT
    CMD_SET_IMP_MODE        = 0x002E,
    CMD_GET_IMP_MODE        = 0x002F,
    // -- RELE
    CMD_GET_MNG_PWR         = 0x002B,
    CMD_SET_MNG_PWR         = 0x002C,
    CMD_MNG_PWR             = 0x0202,
    CMD_GET_PWR             = 0x0203,
    // -- PQP
    CMD_GET_PQP             = 0x002D,
    // -- JOURNAL
    GET_NMB_RECORDS_JRNL    = 0x0025,
    GET_RECORD_JRNL         = 0x0026,
    CLR_JURNAL              = 0x0030,
    GET_ENERGY_METER        = 0x0022,
    GET_ARCH_MTR            = 0x0023,
    GET_END_DTS_JRNL        = 0x0035,
    // -- 
    CMD_GET_PRM_PQP         = 0x0037,
    CMD_SET_PRM_PQP         = 0x0038,
    // -- PROFILE
    GET_PWR_PROFILE         = 0x0024,
    CMD_MNG_PPR             = 0x0124,
    GET_BGN_DTS_PPR         = 0x0040,
    // -- Arch
    GET_BGN_DS_ARCH         = 0x0041,
    // --
    CMD_SET_TYPE            = 0x0200,
    CMD_GET_TYPE            = 0x0201,
    // -- modem_config
    CMD_MNG_ACS             = 0x0042,
    CMD_INC_ACS             = 0x0043,
    CMD_MODEM_CNFG          = 0x0044,
    // -- Memory
    GET_MEMORY              = 0x0300,
    SET_MEMORY              = 0x0301,
    // -- renew
    CMD_BGN_RENEW           = 0x0050,
    CMD_DATA_RENEW          = 0x0051,
    CMD_END_RENEW           = 0x0052,
    // -- crypto
    CMD_AUTH_CRYPTO         = 0x0060,
    CMD_GET_Ci              = 0x0061,
    // -- Testing meter
    CMD_SET_PQP             = 0x0070,
    CMD_SET_EVENT           = 0x0071,
    // -- Modem Terminal
    CMD_SET_DATA_TERMINAL   = 0x0080,
    CMD_SET_DATA_METR       = 0x0081,
    CMD_DATA_PIM	        = 0x0082,
    // -- Module Intf
    CMD_MNG_MODULE_INTF     = 0x0085,
    // -- Other
    CMD_RST_METER           = 0x0320,
    CMD_IDCH                = 0x00A0,
    CMD_PRIVATE_ACCES       = 0x0302,
    
    CMD_NON                 = 0x7FFF
};
// -- STATUS
enum enStatusPrtcl{
    STS_OK                  = 0x00,
    STS_UNKNOWN_CMD         = 0x01,
    STS_ERR_FRMT            = 0x02,
    STS_NOT_FOUND           = 0x03,
    STS_ERR_PSW             = 0x04,
    STS_ERR_OTHER           = 0x05,
    STS_ERR_TIME            = 0x06,
    STS_ERR_CORECT_TIME     = 0x07,
    STS_ERR_LEN             = 0x08,
    STS_ERR_PRM             = 0x09,
    STS_ERR_pin_INIT        = 0x0A,
    STS_ERR_PROHIBITED      = 0x0B,
    STS_ERR_METR_RELOAD     = 0x0C,
    ANSW_METER_WAIT_TIMEOUT = 0x0D,
    ANSW_METER_BUSY         = 0x0E,
    STS_ERR_MODEM           = 0x0F,
    
    // -- Service STATUS
    STS_NEED_CHECKED_EXTERN = -2,
    
    STS_NOT_DEFINED = -1,
    
    DEFAULT_CMD = -1,

    STS_SIZE = INT8_MAX,
    enStatusPrtcl_sizeof = sizeof(uint8_t)
};  

enum enTypeDataPIM {
	enSetMacAddr_PIM = 0,
	enGetMacAddr_PIM = 1,
	enSet_TimeOut_Link_pimRF = 2,
	enGet_Level_pimRF = 3,

	enTypeDataPIM_SIZE = INT16_MAX
};
enum enTypeDataMetr {
	enType_Data_FromTerminal_ToMeter = 0,
	enType_Data_KeyPush = 1,
	enType_MngLcdData = 2,
	enType_SetAddrTermnl = 3,
	enType_GetAddrTermnl = 4,
	enType_Data_SetTimeOut_Link_User = 5,
	enType_GetLevel_pimRF_Terminal = 6,

	enTypeDataMetr_SIZE = INT16_MAX
};
enum enTypeDataTerminal {
	enType_Data_FromMeter_ToTerminal = 0,
	enType_SetAddrTerminal = 1,
	enType_SetAddrMeter = 2,
	enType_SetTimeOut_Link_Terminal = 3,
	enType_GetAddrMeter = 4,
	enType_SetTimeOut_Link_Meter = 5,
	enType_GetLevel_pimRF = 7,

	enTypeDataTerminal_SIZE = INT16_MAX
};

#define msk_MAC_ADDR_PIM_RF     0xF0000000
#define bitMAC_ADDR_PIM_RF      0x10
#define bitMAC_ADDR_TRMNL_RF    0x20
#define bitADDR_LINK_TOKEN      0x30

#define mskADDR_MI				0x40000000

#include "mpro_struct_command.h"

#endif 