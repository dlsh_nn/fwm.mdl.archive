#ifndef mpro_struct_commandH
#define mpro_struct_commandH

#if !defined(__KEIL__)
	#pragma pack(push, 1)
#endif

#ifndef VER_PRTCL
	#define VER_PRTCL 0x0E // -- Last on 22.07.17
#endif
#ifndef NMB_TARIF
    #define NMB_TARIF       8
#endif
//=============================================================================
// NULL COMMAND
//=============================================================================
struct _PACK_NULL_CMD {
	uint8_t null;
};
typedef struct _PACK_NULL_CMD PACK_NULL_CMD, *LPPACK_NULL_CMD;
typedef const PACK_NULL_CMD* LPCPACK_NULL_CMD;
struct _PACK_INSER_PSW_CMD {
    uint8_t null;
};
typedef struct _PACK_INSER_PSW_CMD PACK_INSER_PSW_CMD, *LPPACK_INSER_PSW_CMD;
typedef const PACK_INSER_PSW_CMD* LPCPACK_INSER_PSW_CMD;
//=============================================================================
// READ_VER_COMMAND
//=============================================================================
// -- READ_VER_COMMAND
struct _PACK_READ_VER_COMMAND {
	uint8_t Status;
	uint32_t ver_prtcl;
	uint32_t type_meter;
	uint32_t ver_firm;
	uint32_t ver_tech_firm;
	uint32_t crc_frm;
	uint8_t NameFrm[1];
};
typedef struct _PACK_READ_VER_COMMAND PACK_READ_VER_COMMAND, *LPPACK_READ_VER_COMMAND;
//=============================================================================
// CMD_MNG_MODULE_INTF
//=============================================================================
enum enTypeDataMI{
	enSetAddr_MI = 0,
	enGetAddr_MI = 1,

    TypeDataMI_NOT_DEFINED = -1,
};
struct _PACK_DATA_MI {
	uint16_t Type;		// enum enTypeDataMI
	uint8_t Data[1];
};
typedef struct _PACK_DATA_MI PACK_DATA_MI, *LPPACK_DATA_MI;
struct _PACK_DATA_MI__PUtoMI{
	uint16_t Type;		// enum enTypeDataMI
	uint32_t AddrMI;
};
typedef struct _PACK_DATA_MI__PUtoMI PACK_DATA_MI__PUtoMI, *LPPACK_DATA_MI__PUtoMI;
struct _PACK_DATA_MI__AnswMItoPU{
    enum enStatusPrtcl Status;
    uint16_t Type;		// enum enTypeDataMI
    uint32_t AddrMI;
};
typedef struct _PACK_DATA_MI__AnswMItoPU PACK_DATA_MI__AnswMItoPU, *LPPACK_DATA_MI__AnswMItoPU;
//=============================================================================
// IMP_MODE
//=============================================================================
enum enNameImpOut{enNameImpA, enNameImpR, enNMB_NameImp};
// -- SET_IMP_MODE
struct _PACK_SET_IMP_MODE {
	uint8_t psw[PSW_SIZE];
	uint8_t     imp[enNMB_NameImp];
};
typedef struct _PACK_SET_IMP_MODE   PACK_SET_IMP_MODE, *LPPACK_SET_IMP_MODE;
// -- GET_IMP_MODE
struct _PACK_GET_IMP_MODE {
	uint8_t     imp[enNMB_NameImp];
};
typedef struct _PACK_GET_IMP_MODE   PACK_GET_IMP_MODE, *LPPACK_GET_IMP_MODE;
//=============================================================================
// SET/GET_ADDR_COMMAND
//=============================================================================
// -- SET_ADDR_COMMAND
struct _PACK_SET_ADDR_COMMAND {
	uint8_t        RxPsw[PSW_SIZE];
	uint32_t       RxAddr;
};
typedef struct _PACK_SET_ADDR_COMMAND   PACK_SET_ADDR_COMMAND, *LPPACK_SET_ADDR_COMMAND;
// -- GET_ADDR_COMMAND
struct _PACK_GET_ADDR_COMMAND {
	uint32_t    Addr[1];
};
typedef struct _PACK_GET_ADDR_COMMAND   PACK_GET_ADDR_COMMAND, *LPPACK_GET_ADDR_COMMAND;
// -- ANSW_GET_ADDR_COMMAND
struct _PACK_ANSW_GET_ADDR_COMMAND {
	uint8_t     Status;
	uint32_t    Addr;
};
typedef struct _PACK_ANSW_GET_ADDR_COMMAND PACK_ANSW_GET_ADDR_COMMAND, *LPPACK_ANSW_GET_ADDR_COMMAND;
//=============================================================================
// SET/GET_TYPE
//=============================================================================
// -- SET_TYPE
struct _PACK_SET_TYPE_COMMAND {
	uint32_t    Type;
	DATE_TIME   dts_config;
	uint32_t    slc_imp;
};
typedef struct _PACK_SET_TYPE_COMMAND   PACK_SET_TYPE_COMMAND, *LPPACK_SET_TYPE_COMMAND;
// -- GET_TYPE
struct _PACK_GET_TYPE_COMMAND {
	uint32_t    Type;
	DATE_TIME   dts_config;
	uint32_t    slc_imp;
	uint8_t     fSwitchSummer;
};
typedef struct _PACK_GET_TYPE_COMMAND   PACK_GET_TYPE_COMMAND, *LPPACK_GET_TYPE_COMMAND;
struct _PACK_ANSW_GET_TYPE_COMMAND {
    enum enStatusPrtcl Status;
    PACK_GET_TYPE_COMMAND fld;
};
typedef struct _PACK_ANSW_GET_TYPE_COMMAND   PACK_ANSW_GET_TYPE_COMMAND, *LPPACK_ANSW_GET_TYPE_COMMAND;
//=============================================================================
// CMD_SET_EVENT
//=============================================================================
enum enTestingType { enKeyEvent, enSetTimeEvent, enSetALL_memory_FF };
#define msk_key_event_up          0x01
#define msk_key_event_OpenCase    0x02

struct _PACK_SET_EVENT {
	uint8_t type;				// enum enTestingType
};
typedef struct _PACK_SET_EVENT PACK_SET_EVENT, *LPPACK_SET_EVENT;
struct _PACK_ANSWR_EVENT {
	uint8_t Status;				// enum enStatusPrtcl
};
typedef struct _PACK_ANSWR_EVENT PACK_ANSWR_EVENT, *LPPACK_ANSWR_EVENT;
// -- enSetTimeEvent
struct _PACK_SET_EVENT_KEY {
	uint8_t type;				// enum enTestingType
	uint8_t key_event;
};
typedef struct _PACK_SET_EVENT_KEY PACK_SET_EVENT_KEY, *LPPACK_SET_EVENT_KEY;
// -- enEvent_SetTime
struct _PACK_SET_EVENT_SET_TIME {
	uint8_t type;				// enum enTestingType
	DATE_TIME dts;
};
typedef struct _PACK_SET_EVENT_SET_TIME PACK_SET_EVENT_SET_TIME, *LPPACK_SET_EVENT_SET_TIME;
//=============================================================================
// READ_TIME_COMMAND
//=============================================================================
// -- READ_TIME_COMMAND
struct _PACK_READ_TIME_COMMAND{
    uint8_t      Status;
    DATE_TIME    DateTime;
};
typedef struct _PACK_READ_TIME_COMMAND PACK_READ_TIME_COMMAND, *LPPACK_READ_TIME_COMMAND;
//=============================================================================
// SET_TIME_SOFT_COMMAND / SET_TIME_HARD_COMMAND
//=============================================================================
// -- SET_TIME_COMMAND
struct _PACK_SET_TIME_COMMAND{
    uint8_t      RxPsw[PSW_SIZE];
    DATE_TIME    DateTime;
};
typedef struct _PACK_SET_TIME_COMMAND   PACK_SET_TIME_COMMAND,   *LPPACK_SET_TIME_COMMAND;
//=============================================================================
// SET_SWITCH_SUMMER / GET_SWITCH_SUMMER
//=============================================================================
// -- PACK_SW_SUMMER
struct _SET_SW_SUMMER{
    uint8_t psw[PSW_SIZE];
    uint8_t fSwitchSummer;
};
typedef struct _SET_SW_SUMMER SET_SW_SUMMER, *LPSET_SW_SUMMER;
struct _ANSW_GET_SW_SUMMER{
    uint8_t Status;
    uint8_t fSwitchSummer;
};
typedef struct _ANSW_GET_SW_SUMMER ANSW_GET_SW_SUMMER, *LPANSW_GET_SW_SUMMER;
//=============================================================================
// GET_MEM
//=============================================================================
// -- GET_MEM
struct _PACK_GET_MEM{
    uint8_t TypeMem;
    uint32_t Addr;
    uint8_t size_data;
};
typedef struct _PACK_GET_MEM PACK_GET_MEM, *LPPACK_GET_MEM;
// -- ANSW_GET_MEM
struct _PACK_ANSW_GET_MEM{
    uint8_t Status;
    uint8_t Data[1];
};
typedef struct _PACK_ANSW_GET_MEM PACK_ANSW_GET_MEM, *LPPACK_ANSW_GET_MEM;
//=============================================================================
// SET_MEM
//=============================================================================
// -- SET_MEM
struct _PACK_SET_MEM{
    uint8_t TypeMem;
    uint32_t Addr;
    uint8_t size_data;
    uint8_t Data[1];
};
typedef struct _PACK_SET_MEM PACK_SET_MEM, *LPPACK_SET_MEM;
// -- SET_MEM
struct _PACK_SET_MEM_pData {
	uint8_t TypeMem;
	uint32_t Addr;
	uint8_t size_data;
	uint8_t *pData;
};
typedef struct _PACK_SET_MEM_pData PACK_SET_MEM_pData, *LPPACK_SET_MEM_pData;
// -- ANSW_SET_MEM
struct _PACK_ANSW_SET_MEM{
    uint8_t Status;
};
typedef struct _PACK_ANSW_SET_MEM PACK_ANSW_SET_MEM, *LPPACK_ANSW_SET_MEM;
//=============================================================================
// CMD_GET_PQP
//=============================================================================
enum enTypePQP {
	enPQP_U, enPQP_I, enPQP_P, enPQP_F, enPQP_cos, enPQP_others, enPQP_NMB,
	enPQP_ALL = 0xFF,		// -1,
	enPQP_ClrTesting = 0xFE // -2
};
// -- PQP
struct _PACK_GET_PQP {
	uint8_t indx_pqp;
};
typedef struct _PACK_GET_PQP PACK_GET_PQP, *LPPACK_GET_PQP;
typedef const PACK_GET_PQP* LPCPACK_GET_PQP;

union _PACK_FLD_PQP {
	// --- Total
	struct _TOTAL {
		// -- U
		int32_t U[3];
		int32_t I[3];
		// -- P
		struct {
			int32_t P[4], Q[4];
			int32_t S[4];
		}p;
		// -- F
		int32_t F;
		// -- coef
		struct {
			int32_t cos[3], tan[3];
		}coeff;
		// -- others
		struct {
			int32_t Temp, VBat;
		}others;
	}total;
	// -- U
	int32_t U[3];
	int32_t I[3];
	// -- P
	struct {
		int32_t P[4], Q[4], S[4];
	}p;
	// -- F
	int32_t F;
	// -- coef
	struct {
		int32_t cos[3], tan[3];
	}coeff;
	// -- others
	struct {
		int32_t Temp, VBat;
	}others;
};
typedef union _PACK_FLD_PQP PACK_FLD_PQP, *LPPACK_FLD_PQP;

struct _PACK_ANSW_PQP {
	uint8_t Status;
	uint8_t indx_pqp;
	PACK_FLD_PQP fld;
};
typedef struct _PACK_ANSW_PQP PACK_ANSW_PQP, *LPPACK_ANSW_PQP;
//=============================================================================
// CMD_SET_PQP
//=============================================================================
struct _PACK_SET_PQP {
	uint8_t indx_pqp;
	PACK_FLD_PQP fld;
};
typedef struct _PACK_SET_PQP PACK_SET_PQP, *LPPACK_SET_PQP;
//=============================================================================
// GET_ENERGY_METER
//=============================================================================
struct _PACK_GET_ENRG {
	uint8_t MskTarif;
};
typedef struct _PACK_GET_ENRG PACK_GET_ENRG, *LPPACK_GET_ENRG;
typedef const PACK_GET_ENRG* LPCPACK_GET_ENRG;
struct _PACK_ANSW_ENRG {
	uint8_t Status;
	uint8_t MskTarif;
};
typedef struct _PACK_ANSW_ENRG PACK_ANSW_ENRG, *LPPACK_ANSW_ENRG;
struct _RCRD_ANSW_ENRG {
	ENERGY enrg;
};
typedef struct _RCRD_ANSW_ENRG RCRD_ANSW_ENRG, *LPRCRD_ANSW_ENRG;
//=============================================================================
// CMD_SET_MODE_IND / CMD_GET_MODE_IND
//=============================================================================
#if (VER_PRTCL <= 0x00000001)
	struct _PACK_SET_MODE_IND_HEAD {
		uint8_t psw[PSW_SIZE];
		uint8_t TimeCycl;
		uint8_t indx;
		uint8_t nmb_cycles;
		uint8_t len_cycles[MAX_NMB_CYCLES_IND];
	};
	typedef struct _PACK_SET_MODE_IND_HEAD PACK_SET_MODE_IND_HEAD, *LPPACK_SET_MODE_IND_HEAD;
#elif (VER_PRTCL <= 0x0000000E)
	struct _PACK_SET_MODE_IND_HEAD {
		uint8_t psw[PSW_SIZE];
		uint8_t indx;
		uint8_t TimeCycl;
		uint8_t nmb_cycles;
		uint8_t len_cycles[MAX_NMB_CYCLES_IND];
	};
	typedef struct _PACK_SET_MODE_IND_HEAD PACK_SET_MODE_IND_HEAD, *LPPACK_SET_MODE_IND_HEAD;
#else
	#error "No support VER_PRTCL"
#endif
struct _PACK_SET_MODE_IND {
	uint8_t psw[PSW_SIZE];
	uint8_t indx;
	uint8_t mode[1];
};
typedef struct _PACK_SET_MODE_IND PACK_SET_MODE_IND, *LPPACK_SET_MODE_IND;
struct _PACK_GET_MODE_IND {
	uint8_t indx, size_mode;
};
typedef struct _PACK_GET_MODE_IND PACK_GET_MODE_IND, *LPPACK_GET_MODE_IND;
struct _PACK_ANSW_MODE_IND_HEAD {
	uint8_t TimeCycl;
	uint8_t nmb_cycles;
	uint8_t len_cycles[1];
};
typedef struct _PACK_ANSW_MODE_IND_HEAD PACK_ANSW_MODE_IND_HEAD, *LPPACK_ANSW_MODE_IND_HEAD;
struct _PACK_ANSW_MODE_IND {
	uint8_t mode[1];
};
typedef struct _PACK_ANSW_MODE_IND PACK_ANSW_MODE_IND, *LPPACK_ANSW_MODE_IND;
//=============================================================================
// PWR_PROFILE COMMONs
//=============================================================================
typedef int16_t             Type_PPR_Umxx;
#define MAX_PPR_Umxx        INT16_MAX
#define SCALE_PPR_Umxx      100L

enum enPrflValid{
    ePrflValid   = 0,
    ePrflIncompl = 1,
    ePrflCurrent = 2,
    ePrflSumm    = 3,
    
    ePrflTypeEnd,
    
    ePrflNoValid_NoFinde_v02 =  0x7E, // -2,
    ePrflNoValid_NoFinde =      0xFE, // -2,
    //ePrflNoValid =            0xFF, // -1,

    ePrfl_SIZE = INT8_MAX
};
struct _PWR_PRFL_DATA_v0{
    ENERGY      enrg;
    POWER       pwr_peak;
};
typedef struct _PWR_PRFL_DATA_v0 PWR_PRFL_DATA_v0, *LPPWR_PRFL_DATA_v0;
struct _PWR_PRFL_DATA_v2{
    Type_PPR_Umxx   Umax[3];
    Type_PPR_Umxx   Umin[3];
};
typedef struct _PWR_PRFL_DATA_v2 PWR_PRFL_DATA_v2, *LPPWR_PRFL_DATA_v2;
struct _PWR_PRFL_DATA_vs4{
    Type_PPR_Umxx   Uph_avrg[NMB_PHASE];
    int8_t Temp;
};
typedef struct _PWR_PRFL_DATA_vs4 PWR_PRFL_DATA_vs4, *LPPWR_PRFL_DATA_vs4;

struct _PWR_PRFL_DATA{
    PWR_PRFL_DATA_v0 v0;
    PWR_PRFL_DATA_v2 v2;
    PWR_PRFL_DATA_vs4 vs4;
};
typedef struct _PWR_PRFL_DATA PWR_PRFL_DATA, *LPPWR_PRFL_DATA;

union _PWR_PRFL_STS{
    struct{
        uint8_t ePrfl_sts   : 7;
        uint8_t fSW         : 1; // fSummerWinter
    }fild;
    uint8_t raw;
};
typedef union _PWR_PRFL_STS PWR_PRFL_STS, *LPPWR_PRFL_STS;

struct _RCRD_PWR_PRFL{
    time_t          tmt;
    PWR_PRFL_DATA   dt;
    PWR_PRFL_STS    fValid;
};
typedef struct _RCRD_PWR_PRFL RCRD_PWR_PRFL, *LPRCRD_PWR_PRFL;      // 61

struct _RCRD_PWR_PRFL_WITH_SHIFT{
    uint16_t shift;
    RCRD_PWR_PRFL prfl;
};
typedef struct _RCRD_PWR_PRFL_WITH_SHIFT RCRD_PWR_PRFL_WITH_SHIFT, *LPRCRD_PWR_PRFL_WITH_SHIFT;
//=============================================================================
// GET_PWR_PROFILE
//=============================================================================
#define MSK_bitArch_Skip    0x01
#define MSK_MatchTimeStamp  0x80
#define MSK_SeasonRcrds     0x10
#define MSK_TypeV           (~(MSK_MatchTimeStamp | MSK_bitArch_Skip | MSK_SeasonRcrds))

#define TypePPR_v00      0x00
#define TypePPR_v02      0x20
#define TypePPR_vs04     0x44
#define TypePPR_getNmb   0x40
#define TypePPR_getNmb_DTS   0x42
#define TypePPR_forINDX  0x46

#define NMB_TypeV        11
#define def_SupportTypeV    {                             \
    TypePPR_v00,  (TypePPR_v00  | MSK_bitArch_Skip),      \
    TypePPR_v02,  (TypePPR_v02  | MSK_bitArch_Skip),      \
    TypePPR_vs04, (TypePPR_vs04 | MSK_SeasonRcrds),       \
    (TypePPR_vs04 | MSK_SeasonRcrds),                     \
    TypePPR_getNmb_DTS, TypePPR_getNmb, TypePPR_forINDX,  \
    -1   \
}

enum enTypePPR{
    enTPPR_NoSkip_PPRv0 =   TypePPR_v00,
    enTPPR_Skip_PPRv0  =    (TypePPR_v00 | MSK_bitArch_Skip),
    enTPPR_NoSkip_PPRv2 =   TypePPR_v02,
    enTPPR_Skip_PPRv2 =   (TypePPR_v02 | MSK_bitArch_Skip),
    enTPPR_NoSkip_PPR_vs4 =  TypePPR_vs04,
    enTPPR_NoSkipSeason_PPR_vs4 = (TypePPR_vs04 | MSK_SeasonRcrds), // 0x54
    
    enTPPR_getNmb_from_DTS =  TypePPR_getNmb_DTS,
    enTPPR_getNmb = TypePPR_getNmb,
    enTPPR_PPRvs4_by_INDX = TypePPR_forINDX,

    TPPR_NOT_DEFINED = -1,
    enTPPR_SIZE = INT8_MAX
};
struct _PACK_GET_PWR_PRFL{
    enum enTypePPR Type;   // archive.h -> MSK_Type
    DATE_TIME dts_bgn, dts_end;
};
typedef struct _PACK_GET_PWR_PRFL PACK_GET_PWR_PRFL, *LPPACK_GET_PWR_PRFL;
typedef const PACK_GET_PWR_PRFL* LPCPACK_GET_PWR_PRFL;
struct _PACK_GET_PPR_by_INDX{
    enum enTypePPR Type;   // archive.h -> MSK_Type
    uint16_t indx_bgn, indx_end;
};
typedef struct _PACK_GET_PPR_by_INDX PACK_GET_PPR_by_INDX, *LPPACK_GET_PPR_by_INDX;
struct _PACK_ANSW_PWR_PRFL{
    DATE_TIME     dts;
    PWR_PRFL_DATA prfl;
    uint8_t       fValid;
};
typedef struct _PACK_ANSW_PWR_PRFL PACK_ANSW_PWR_PRFL, *LPPACK_ANSW_PWR_PRFL;
struct _PACK_ANSW_PWR_PRFL_V0{
    DATE_TIME        dts;
    PWR_PRFL_DATA_v0 prfl;
    uint8_t          fValid;
};
typedef struct _PACK_ANSW_PWR_PRFL_V0 PACK_ANSW_PWR_PRFL_V0, *LPPACK_ANSW_PWR_PRFL_V0;
struct _PACK_ANSW_PWR_PRFL_V2{
    DATE_TIME        dts;
    PWR_PRFL_DATA_v0 v0;
    PWR_PRFL_DATA_v2 v2;
    uint8_t          fValid;
};
typedef struct _PACK_ANSW_PWR_PRFL_V2 PACK_ANSW_PWR_PRFL_V2, *LPPACK_ANSW_PWR_PRFL_V2;
struct _PACK_ANSW_PWR_PRFL_VS4 {
    DATE_TIME           dts;
    ENERGY              enrg;
    PWR_PRFL_DATA_vs4   vs4;
    uint8_t             fValid;  // enum enPrflValid
};
typedef struct _PACK_ANSW_PWR_PRFL_VS4 PACK_ANSW_PWR_PRFL_VS4, *LPPACK_ANSW_PWR_PRFL_VS4;
typedef const PACK_ANSW_PWR_PRFL_VS4* LPCPACK_ANSW_PWR_PRFL_VS4;
struct _PACK_ANSW_PPR_NMB_RCRD {
    uint16_t Nmb;
};
typedef struct _PACK_ANSW_PPR_NMB_RCRD PACK_ANSW_PPR_NMB_RCRD, *LPPACK_ANSW_PPR_NMB_RCRD;
typedef const PACK_ANSW_PPR_NMB_RCRD* LPCPACK_ANSW_PPR_NMB_RCRD;
//=============================================================================
// GET_BGN_DTS_PPR
//=============================================================================
struct _PACK_ANSW_BGN_DTS_PPR{
    enum enStatusPrtcl Status;
    DATE_TIME dts_bgn;
};
typedef struct _PACK_ANSW_BGN_DTS_PPR PACK_ANSW_BGN_DTS_PPR, *LPPACK_ANSW_BGN_DTS_PPR;
//=============================================================================
// CMD_MNG_PPR
//=============================================================================
enum enMngPprType { 
    enMPPR_rdDuration = 0x01, enMPPR_wrDuration = 0x02, 
    PprType_NOT_DEFINED = -1,
    enMPPR_SIZE = INT8_MAX
};

struct _PACK_CMD_MNG_PPR {
    uint8_t psw[PSW_SIZE];
    enum enMngPprType type;
    uint16_t Duration;
};
typedef struct _PACK_CMD_MNG_PPR PACK_CMD_MNG_PPR, *LPPACK_CMD_MNG_PPR;
typedef const PACK_CMD_MNG_PPR* LPCPACK_CMD_MNG_PPR;

struct _PACK_ANSW_MNG_PPR {
    enum enStatusPrtcl Status;
    uint16_t Duration;
};
typedef struct _PACK_ANSW_MNG_PPR PACK_ANSW_MNG_PPR, *LPPACK_ANSW_MNG_PPR;
//=============================================================================
// CMD_READ_ARCH
//=============================================================================
enum enTypeArch{ eARCH_DAY, eARCH_MNTH, eARCH_PPR, NMB_ACRH, 
    enTypeArch_SIZE = INT8_MAX, eARCH_NOT_DEFINED = -1
};
#define TypeMR_v00      0x00
#define def_SupportTypeV_ARCH {     \
    TypeMR_v00, (TypeMR_v00 | MSK_bitArch_Skip), \
    TypePPR_getNmb_DTS,             \
    TypePPR_getNmb, TypePPR_forINDX, -1      \
}
enum enTypeGetArch{
    enType_NoPass =     TypeMR_v00, 
    enType_Pass =       (TypeMR_v00 | MSK_bitArch_Skip), 
    enType_ArchGetNmb_DS = TypePPR_getNmb_DTS,
    enType_ArchGetNmb = TypePPR_getNmb,
    enType_Pass_by_INDX = TypePPR_forINDX,
    
    enTypeARCH_SIZE = INT8_MAX,
    NMB_TYPE_GET_ARCH
};

struct _PACK_GET_BGN_DS_ARCH{
    enum enTypeArch TypeArch;
};
typedef struct _PACK_GET_BGN_DS_ARCH PACK_GET_BGN_DS_ARCH, *LPPACK_GET_BGN_DS_ARCH;
struct _PACK_ANSW_BGN_DS_ARCH{
    enum enStatusPrtcl Status;
    DATE_t ds;
};
typedef struct _PACK_ANSW_BGN_DS_ARCH PACK_ANSW_BGN_DS_ARCH, *LPPACK_ANSW_BGN_DS_ARCH;

struct _PACK_GET_ARCH{
    enum enTypeGetArch Type;
    enum enTypeArch TypeArch;
    uint8_t MskTarif;
    DATE_t ds_bgn, ds_end;
};
typedef struct _PACK_GET_ARCH PACK_GET_ARCH, *LPPACK_GET_ARCH;
typedef const PACK_GET_ARCH* LPCPACK_GET_ARCH;
struct _PACK_GET_ARCH_by_INDX{
    enum enTypeGetArch Type;
    enum enTypeArch TypeArch;
    uint8_t MskTarif;
    uint16_t indx_bgn, indx_end;
};
typedef struct _PACK_GET_ARCH_by_INDX PACK_GET_ARCH_by_INDX, *LPPACK_GET_ARCH_by_INDX;
struct _RCRD_ANSW_ARCH{
    uint8_t Data[1];
};
typedef struct _RCRD_ANSW_ARCH RCRD_ANSW_ARCH, *LPRCRD_ANSW_ARCH;
struct _TB_RCRD_ANSW_ARCH_ALLT{
    DATE_t ds;
    ENERGY enrg[NMB_TARIF];        
};
typedef struct _TB_RCRD_ANSW_ARCH_ALLT TB_RCRD_ANSW_ARCH_ALLT, *LPTB_RCRD_ANSW_ARCH_ALLT;
struct _PACK_ANSW_ARCH_HEAD{
    enum enTypeArch TypeArch;
    uint8_t MskTarif;
};
typedef struct _PACK_ANSW_ARCH_HEAD PACK_ANSW_ARCH_HEAD, *LPPACK_ANSW_ARCH_HEAD;
struct _PACK_ANSW_ARCH_NMB_RCRD {
    enum enStatusPrtcl Status;
    uint16_t Nmb;
};
typedef struct _PACK_ANSW_ARCH_NMB_RCRD PACK_ANSW_ARCH_NMB_RCRD, *LPPACK_ANSW_ARCH_NMB_RCRD;
typedef const PACK_ANSW_ARCH_NMB_RCRD* LPCPACK_ANSW_ARCH_NMB_RCRD;
struct _PACK_GET_NMB_ARCH{
    enum enTypeGetArch Type;
    enum enTypeArch TypeArch;
};
typedef struct _PACK_GET_NMB_ARCH PACK_GET_NMB_ARCH, *LPPACK_GET_NMB_ARCH;
//=============================================================================
// SET_PASSWORD_COMMAND
//=============================================================================
enum enTypePsw {
    PswGroup = 1,
    PswInd,
    // --
    NMB_TYPE_PSW,
    PswFirstDefined = PswGroup,
    
    PSW_NOT_DEFINED = -1,
    Psw_SIZE = INT8_MAX,
    enTypePsw_sizeof = sizeof(uint8_t)
};
struct _PACK_SET_PASSWORD_COMMAND {
    uint8_t CurentPsw[PSW_SIZE];
    uint8_t TypePsw;
    uint8_t NewPsw[PSW_SIZE];
};
typedef struct _PACK_SET_PASSWORD_COMMAND PACK_SET_PASSWORD_COMMAND, *LPPACK_SET_PASSWORD_COMMAND;
//=============================================================================
// CMD_PRIVATE_ACCES
//=============================================================================
enum enPrivateAccessType { enPA_GetPSW = 0x0001, enPA_SIZE = INT16_MAX, enPA_sizeof = sizeof(uint16_t) };

struct _PACK_CMD_PRIV_ACCESS {
    enum enPrivateAccessType Type;
    enum enTypePsw TypePsw;
};
typedef struct _PACK_CMD_PRIV_ACCESS PACK_CMD_PRIV_ACCESS, *LPPACK_CMD_PRIV_ACCESS;

struct _PACK_ANSW_CMD_PACCESS {
    enum enStatusPrtcl Status;
    enum enPrivateAccessType Type;
    enum enTypePsw TypePsw;
    uint8_t psw_crnt[PSW_SIZE];
};
typedef struct _PACK_ANSW_CMD_PACCESS PACK_ANSW_CMD_PACCESS, *LPPACK_ANSW_CMD_PACCESS;

#include "mpro_scmd_tarif.h"

#if !defined(__KEIL__)
	#pragma pack(pop)
#endif

#endif
