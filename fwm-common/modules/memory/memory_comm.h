#ifndef memory_commH
#define memory_commH

#define VERSION(major, minor, patch)    ((0x00 << 24) | (major << 16) | (minor << 8) | patch)
#define MEM_RSRV(val)   ASSERT_CONCAT(uint8_t rsrv_, __LINE__[val])

enum enTypeMem {
    enMem_EEPROM, enMem_RAM, eMem_FLASH, eMem_NVM, eMem_romFLASH, enMem_SFR, eMem_FLASH_ERASE_SECTOR,
    enMem_RmtSens, enMem_EEPROM_Total, enMem_CC1101
};

struct _VERSION_MEMORY{
    union{
        uint32_t u32;
        struct{
            uint8_t RSRV;
            // -- 
            uint8_t major;
            uint8_t minor;
            uint8_t patch;
        }fld;
    }represent;
};
typedef struct _VERSION_MEMORY VERSION_MEMORY, *LPVERSION_MEMORY;

#endif // !memory_comm


