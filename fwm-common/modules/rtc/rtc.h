#ifndef comm_rtcH
#define comm_rtcH

#ifdef DATE
	#undef DATE
#endif

enum enDayWeek{
    Sunday = 1,
    Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
};
enum enMonth{
    January = 1,
    February,
    March,
    April,
    May,
    June,
    July,
    August,
    September,
    October,
    November,
    December
};

// -- DATA_TIME template
struct _DATE_TIME{
    uint8_t sec;
    uint8_t min;
    uint8_t hour;
    uint8_t day;
    uint8_t date;
    uint8_t month;
    uint8_t year;
};
typedef struct _DATE_TIME DATE_TIME, *LPDATE_TIME;
// -- TIME template
struct __TIME{
    uint8_t sec;
    uint8_t min;
    uint8_t hour;
};
typedef struct __TIME TIME, *LPTIME;
typedef const DATE_TIME* LPCDATE_TIME;
// -- DATE template
struct __DATE{
    uint8_t date;
    uint8_t month;
    uint8_t year;
};
typedef struct __DATE DATE_t, *LPDATE;
typedef const DATE_t* LPCDATE;
typedef uint32_t DTS_RAW, *LPDTS_RAW;

#endif
