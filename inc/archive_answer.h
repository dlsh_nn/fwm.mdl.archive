#ifndef archive_answerH
#define archive_answerH

enum enMode_AnswArch{
    enMdAns_IDLE, enMdAns_AnswBgn, 
    enMdAns_Finde_tmt, enMdAns_FindeDbl_tmt,
    enMdAns_Next_tmt,
    enMdAns_CheckCurrentRecord,
    enMdAns_OutArchiveRecord, enMdAns_OutArchiveRecord_Dbl, 
    enMdAns_OutNullRecord, enMdAns_OutCurrentRecord,
    // --
    enMdAns_FindeByIndex_Begin,
    enMdAns_FindeByIndex_NmbRcrd,
    enMdAns_FindeByIndex_CheckIndexValid,
    enMdAns_FindeByIndex_NmbRcrd_Archive,   ///< For Archive Day and Month
    enMdAns_Answ_STS_ERR_PRM,
    // --
    enMdAns_Filter_SpodesRcrds,
    
    enMdAns_ResetMech,
    // -
    enMdAns_NO_DEF = -1
};

struct _BY_INDEX_FINDE{
    uint8_t fValidRecordDay, cnt_rcrds_day, chk_rcrds_day;
    int16_t cnt_index_spodes;
    uint8_t fFinde_bgn, fFinde_end;
    time_t tmt_index_spodes;
};
typedef struct _BY_INDEX_FINDE BY_INDEX_FINDE, *LPBY_INDEX_FINDE;

struct _PPR_SEASON_FINDE{
    int8_t fSeason_of_index;
    int8_t fssn, fSeasonBgn, fSeasonEnd, fssn_crnt_tmt;
    time_t tmtBgn_Uncertainty, tmtEnd_Uncertainty;
    //int8_t cntOutRcrds;
};
typedef struct _PPR_SEASON_FINDE PPR_SEASON_FINDE, *LPPPR_SEASON_FINDE;
typedef const PPR_SEASON_FINDE *LPCPPR_SEASON_FINDE;

struct _SPODES_FINDE{
    uint8_t fFixCnt, fEnd;
    time_t tBgn, tEnd;
    uint16_t cnt_remove, cnt_add, NmbRcrds;
};
typedef struct _SPODES_FINDE SPODES_FINDE, *LPSPODES_FINDE;

struct _ARCH_ANSW_INIT{
    enum enTypeArch TypeArch;
    uint8_t Type, MskTarif;
    time_t tmt_bgn, tmt_end;
    int16_t indx_bgn, indx_end;
    LPPROTOCOL_DATA prtcl_data;
    uint8_t fAnsweBegin;
    PPR_SEASON_FINDE season;
};
typedef struct _ARCH_ANSW_INIT ARCH_ANSW_INIT, *LPARCH_ANSW_INIT;
typedef const ARCH_ANSW_INIT *LPCARCH_ANSW_INIT;

struct _ARCH_ANSW_VAR{
    enum enMode_AnswArch mode, mode_filter;
    ARCH_ANSW_INIT init_vars;

    uint8_t fAnsweBegin, fResetMech;
    uint8_t *pout;
    uint32_t driver_size_buf_save;
    int8_t DirectFinde, Direction;
    time_t tmt, tmt_of_indx;
    int16_t indx, end_indx, bgn_indx, indx_restore;
    uint8_t cnt_FindeDbl;
    // -- crnt rcrd
    time_t tmt_crnt;
    PWR_PRFL_DATA rcrd;
    // -- Algorithm SPODES Finde Records
    SPODES_FINDE spFinde;
    // -- For Out records at season
    PPR_SEASON_FINDE season;
    // -- For Finde By Index
    BY_INDEX_FINDE byIndex;
};
typedef struct _ARCH_ANSW_VAR ARCH_ANSW_VAR, *LPARCH_ANSW_VAR;
typedef const ARCH_ANSW_VAR *LPCARCH_ANSW_VAR;

enum enStatusPrtcl InitMechAnsw_Arch(LPARCH_ANSW_INIT pVarInit);
void MechAnswerArch(void);
void ResetArchivAnswer(enum enTypeInterface type_interface) small reentrant;
void AnswPrtcl_ARCH(uint16_t size_pack);

time_t NexBgnPPR(void);
time_t NexBgnDay(void);
time_t NexBgnMonth(void);

int16_t BinSearch_index(time_t tmt, int16_t bgn_index, int16_t end_index, enum enTypeArch TypeArch);

#endif