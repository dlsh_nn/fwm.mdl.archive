#ifndef profile_powerH
#define profile_powerH

#define PPR_ALIGN_END           0
#define SHFT_CRNT_TMT_for_SAVE  60

#define NMB_RCRD_PWR_PRFL   (48ul * 125ul)

struct _SHIFT_TIME {
    int16_t shift;
    time_t   tmt;
};
typedef struct _SHIFT_TIME SHIFT_TIME, *LPSHIFT_TIME;

//enum enPrflValid{
//    ePrflNoValid_NoFinde = -2,
//    //ePrflNoValid = -1,
//    ePrflValid   = 0,
//    ePrflIncompl = 1,
//    ePrflCurrent = 2,
//    ePrflSumm    = 3,
//};
//struct _PWR_PRFL_DATA_v0{
//    ENERGY      enrg;
//    POWER       pwr_peak;
//};
//typedef struct _PWR_PRFL_DATA_v0 PWR_PRFL_DATA_v0, *LPPWR_PRFL_DATA_v0;
//struct _PWR_PRFL_DATA_v2{
//    Type_PPR_Umxx   Umax[3];
//    Type_PPR_Umxx   Umin[3];
//};
//typedef struct _PWR_PRFL_DATA_v2 PWR_PRFL_DATA_v2, *LPPWR_PRFL_DATA_v2;
//struct _PWR_PRFL_DATA_vs4{
//    Type_PPR_Umxx   Uph_avrg[3];
//    Type_PPR_Umxx   Uln_avrg[2];
//    uint8_t Temp;
//    uint32_t Duration;
//};
//typedef struct _PWR_PRFL_DATA_vs4 PWR_PRFL_DATA_vs4, *LPPWR_PRFL_DATA_vs4;
//
//struct _PWR_PRFL_DATA{
//    PWR_PRFL_DATA_v0 v0;
//    PWR_PRFL_DATA_v2 v2;
//    PWR_PRFL_DATA_vs4 vs4;
//};
//typedef struct _PWR_PRFL_DATA PWR_PRFL_DATA, *LPPWR_PRFL_DATA;
//
//union _PWR_PRFL_STS{
//    struct{
//        uint8_t ePrfl_sts   : 7;
//        uint8_t fSW         : 1; // fSummerWinter
//    }fild;
//    uint8_t raw;
//};
//typedef union _PWR_PRFL_STS PWR_PRFL_STS, *LPPWR_PRFL_STS;
//
//struct _RCRD_PWR_PRFL{
//    time_t          tmt;
//    PWR_PRFL_DATA   dt;
//    PWR_PRFL_STS    fValid;
//};
//typedef struct _RCRD_PWR_PRFL RCRD_PWR_PRFL, *LPRCRD_PWR_PRFL;      // 61

//struct _RCRD_PWR_PRFL_WITH_SHIFT{
//    uint16_t shift;
//    RCRD_PWR_PRFL prfl;
//};
//typedef struct _RCRD_PWR_PRFL_WITH_SHIFT RCRD_PWR_PRFL_WITH_SHIFT, *LPRCRD_PWR_PRFL_WITH_SHIFT;
//--------------------------------------------------------------------------------------
// PACK
//--------------------------------------------------------------------------------------
//enum enTypePPR{
//    enTPPR_no_skip  = 0x00,
//    enTPPR_skip_PPR_v2 = 0x20
//};
//struct _PACK_GET_PWR_PRFL{
//    enum enTypePPR Type;   // archive.h -> MSK_Type
//    DATE_TIME dts_bgn, dts_end;
//};
//typedef struct _PACK_GET_PWR_PRFL PACK_GET_PWR_PRFL, *LPPACK_GET_PWR_PRFL;
//typedef const PACK_GET_PWR_PRFL* LPCPACK_GET_PWR_PRFL;
//struct _PACK_ANSW_PWR_PRFL{
//    DATE_TIME     dts;
//    PWR_PRFL_DATA prfl;
//    uint8_t       fValid;
//};
//typedef struct _PACK_ANSW_PWR_PRFL PACK_ANSW_PWR_PRFL, *LPPACK_ANSW_PWR_PRFL;
//struct _PACK_ANSW_PWR_PRFL_V0{
//    DATE_TIME        dts;
//    PWR_PRFL_DATA_v0 prfl;
//    uint8_t          fValid;
//};
//typedef struct _PACK_ANSW_PWR_PRFL_V0 PACK_ANSW_PWR_PRFL_V0, *LPPACK_ANSW_PWR_PRFL_V0;
//struct _PACK_ANSW_PWR_PRFL_V2{
//    DATE_TIME        dts;
//    PWR_PRFL_DATA_v2 prfl;
//    uint8_t          fValid;
//};
//typedef struct _PACK_ANSW_PWR_PRFL_V2 PACK_ANSW_PWR_PRFL_V2, *LPPACK_ANSW_PWR_PRFL_V2;
//struct _PACK_ANSW_BGN_DTS_PPR{
//    enum enStatusPrtcl Status;
//    DATE_TIME dts_bgn;
//};
//typedef struct _PACK_ANSW_BGN_DTS_PPR PACK_ANSW_BGN_DTS_PPR, *LPPACK_ANSW_BGN_DTS_PPR;

enum enModePPR{
    enMdPpr_IDLE, enMdPpr_AnswPPR, 
    enMdPpr_AnswPPR_FindeBgnRcrdPPR, enMdPpr_AnswPPR_1
};
struct _PWR_PROFILE{
    int32_t Apn, Rpn;
    POWER pwr_avg;
    int32_t Uph_avrg[NMB_PHASE];
    int16_t cnt_avg;
    
    PWR_PRFL_DATA rcrd;
    PWR_PRFL_STS fValid_rcrd;
    AEMem_t Addr_NeedDel_PwrOff;
};
typedef struct _PWR_PROFILE PWR_PROFILE, *LPPWR_PROFILE;

extern PWR_PROFILE ppr;

void profilePwrPeak(void);
void PwrProfile_meter_off(void);
void PreSavePPR(LPPWR_PRFL_DATA prcrd);
void PwrProfile_update(void);
void EventSavePwrProfile(enum enPrflValid fValid);
void EventSavePwrProfile_tmt(enum enPrflValid fValid, time_t crnt_tmt);
void PwrProfilePwrOn(void);
void RecordPPR_NeedDel_PwrOn(void);
#if (_71M6533 || _71M6543)
    #define GetDltTmtPwrPrfl_tmt(tmt)    (tmt % TMT_30MIN)
    int32_t AvgPPR_for_ind(int32_t enrg);
#endif

#endif