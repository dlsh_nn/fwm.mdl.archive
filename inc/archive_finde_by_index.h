/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   archive_finde_by_index.h
 * Author: User
 *
 * Created on 8 августа 2017 г., 22:37
 */

#ifndef ARCHIVE_FINDE_BY_INDEX_H
#define ARCHIVE_FINDE_BY_INDEX_H

enum enStatusPrtcl CheckFindeByIndex(LPARCH_ANSW_INIT pivars);
void InitFindeByIndex(LPARCH_ANSW_INIT pivars, LPARCH_ANSW_VAR pvars);
enum enMode_AnswArch FindeByIndex_exec(LPARCH_ANSW_INIT pivars, LPARCH_ANSW_VAR pvars, uint8_t cnt_read_tmt);

#endif /* ARCHIVE_FINDE_BY_INDEX_H */

