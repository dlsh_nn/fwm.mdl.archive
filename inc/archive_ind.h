#ifndef archive_indH
#define archive_indH

#if (_71M6533 || _71M6543)
    void IndLCD_Arch(enum enModeMeter mode);

    void IndLCD_TypeEnrg(uint8_t indx_TypeEnrg);
    void IndLCD_Data(uint8_t indx_month);
    void IndLCD_Enrg(uint8_t indx_month, uint8_t indx_TypeEnrg);

    void energy_month_lcd(uint8_t month, uint8_t en_type);
#else
    #define IndLCD_Arch(mode)
#endif

#endif