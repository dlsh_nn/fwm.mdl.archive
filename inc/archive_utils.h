#ifndef archive_utilsH
#define archive_utilsH

time_t GetTmtArchDay_align(time_t tmt);
time_t GetTmtArchMonth_align(time_t tmt);
time_t GetTmtArchPPR_align(time_t tmt);

void fun_Fill_DateRecord_Arch(time_t tmt_align, void *pds);
void fun_Fill_DateRecord_PPR(time_t tmt_align, void *pds);

void fun_AfterSave_Arch(void);
void fun_AfterSave_PPR(void);

#if !ENBL_KILL_ARCH_MECH
    void KillArchRecord(time_t tmt_new, time_t tmt_crnt, LPCLIST_ARCHIV_FLASH_MEM plist);
#endif
enum enStatusPrtcl get_arch_tmt(enum enTypeArch TypeArch, time_t *ptmt, uint8_t fBgn);
void save_lbl_time_arch(LPDATE_TIME pdts);
uint8_t MaxDblRecords(enum enTypeArch TypeArch, time_t tmt);

#endif