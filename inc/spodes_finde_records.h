/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   spodes_finde_records.h
 * Author: User
 *
 * Created on 1 августа 2017 г., 19:28
 */

#ifndef SPODES_FINDE_RECORDS_H
#define SPODES_FINDE_RECORDS_H

void InitSPODES_Finde(LPARCH_ANSW_INIT pivars, LPARCH_ANSW_VAR pvars);
void SPODES_Finde_PreNewTmt(time_t tmt, uint8_t Type, LPARCH_ANSW_VAR pvars);
uint8_t SPODES_Finde_ArchRcrd(LPCARCH_ANSW_INIT pivars, LPARCH_ANSW_VAR pvars);
uint8_t SPODES_Finde_NullRcrd(LPCARCH_ANSW_INIT pivars, LPARCH_ANSW_VAR pvars);
void SPODES_Finde_ArchRcrd_Dbl(uint8_t Type, LPARCH_ANSW_VAR pvars);
void SPODES_Finde_OutNmbRecord(uint8_t Type, LPARCH_ANSW_VAR pvars);

#endif /* SPODES_FINDE_RECORDS_H */

