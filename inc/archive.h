#ifndef archiveH
#define archiveH

#include "profile_power.h"
#include "archive_ind.h"

#define NMB_RCRD_DAY    125L
#define NMB_RCRD_MNTH   36L

#define GetAddrArch(arch)       (GetAddr(arch_bgn_day) + offsetof(ARCHIV, arch))
#define GetAddrArchHead(arch)   (GetAddr(arch_bgn_day) + offsetof(ARCHIV, arch))

//-----------------------------------------------------------------------------
// CONFIG
//-----------------------------------------------------------------------------
typedef time_t (*FUN_Align_tmt)(time_t tmt);
typedef void (*FUN_Fill_DateRecord)(time_t tmt_align, void *pds);
typedef void (*FUN_AfterSave)(void);
typedef time_t (*FUN_Next_tmt)(void);

struct _LIST_ARCHIV_FLASH_MEM{
    AEMem_t     AddrArchHead;
    uint16_t    nmb_record_declared;
    uint16_t    Offset_DtArch, size_DtArch;
    uint8_t     SupportTypeV[NMB_TypeV];
    // --
    LIST_FLASH_MEM lfm;
    // --
    FUN_Align_tmt fun_Align_tmt;
    FUN_Fill_DateRecord fun_Fill_DateRecord;
    FUN_AfterSave fun_AfterSave;
    // -- functions MechAnswer
    FUN_Next_tmt fun_Next_tmt;
};
typedef struct _LIST_ARCHIV_FLASH_MEM LIST_ARCHIV_FLASH_MEM, *LPLIST_ARCHIV_FLASH_MEM;
typedef const LIST_ARCHIV_FLASH_MEM* LPCLIST_ARCHIV_FLASH_MEM;
//-----------------------------------------------------------------------------
// RECORD
//-----------------------------------------------------------------------------
// -- 28 byte
struct _RCRD_ARCH{
    time_t  tmt;
    DATE_t  ds;
    ENERGYS_MTR_TRFS  enrg_trf;
};
typedef struct _RCRD_ARCH RCRD_ARCH, *LPRCRD_ARCH;

struct _RCRD_ARCH_WITH_SHIFT{
    uint16_t shift;
    RCRD_ARCH arch;
};

typedef struct _RCRD_ARCH_WITH_SHIFT RCRD_ARCH_WITH_SHIFT, *LPRCRD_ARCH_WITH_SHIFT;
// -- Begin Day
struct _ARCH_DAY{
    RCRD_ARCH rcrd[NMB_RCRD_DAY];
};
typedef struct _ARCH_DAY ARCH_DAY, *LPARCH_DAY;
// -- Begin Month
struct _ARCH_MNTH{
    RCRD_ARCH   rcrd_mnth[NMB_RCRD_MNTH];
};
typedef struct _ARCH_MNTH ARCH_MNTH, *LPARCH_MNTH;
// -- Arch
struct _ARCHIV_HEAD{
    HEAD_LIST   head_day, head_mnth;
};
typedef struct _ARCHIV_HEAD ARCHIV_HEAD, *LPARCHIV_HEAD;
struct _ARCHIV{
    ARCH_DAY    bgn_day;
    ARCH_MNTH   bgn_mnth;
};
typedef struct _ARCHIV ARCHIV, *LPARCHIV;
//-----------------------------------------------------------------------------
// ANSWER Type
//-----------------------------------------------------------------------------
#include "archive_answer.h"

struct _ARCH_SAVE{
    uint16_t tmt_Duration;
};
typedef struct _ARCH_SAVE ARCH_SAVE, *LPARCH_SAVE;
struct _EE_ARCH_SAVE{
    ARCH_SAVE save;
    // --
    uint8_t crc;
    uint8_t reserv[64];
};
typedef struct _EE_ARCH_SAVE EE_ARCH_SAVE, *LPEE_ARCH_SAVE;

struct _ARCH_VAR{
    uint8_t fUpdateRTC, fSaveArch;
    
    enum enTypeInterface indx_Interface;
    LPARCH_ANSW_VAR pAnswVar[NMB_Interface];
    uint8_t fIgnorePPR;
    time_t tmt_bgn[NMB_ACRH];
    ARCH_SAVE save;
};
typedef struct _ARCH_VAR ARCH_VAR, *LPARCH_VAR;

extern ARCH_VAR arch_var;
extern const LIST_ARCHIV_FLASH_MEM code ListArch[NMB_ACRH];

void InitArch(void);
void InitArch_save(void);
void save_ArchVarSave(void);
void isr_Arch_100ms(void) small reentrant;
uint8_t Arch_exec(void);
void reset_arch(void);

void get_bgn_ds_arch(LPPROTOCOL_DATA prtcl_data);
void get_arch_mtr(LPPROTOCOL_DATA prtcl_data);
void get_pwr_profile(LPPROTOCOL_DATA prtcl_data);
void get_bgn_dts_ppr(LPPROTOCOL_DATA prtcl_data);

void AddArch_record(enum enTypeArch Type, time_t tmt_not_align, void *pDataArch);
void AddArch_record_DtArch(enum enTypeArch Type, time_t tmt_not_align, void *pDtArch);
void StartKillArch_foNewDTS(LPDATE_TIME pCrnt_dts, LPDATE_TIME pNew_dts);
void StartKillArch_foNewDTS_TypeArch(LPDATE_TIME pCrnt_dts, LPDATE_TIME pNew_dts, enum enTypeArch TypeArch);

#if (PWR_MNG_Eday || PWR_MNG_Emonth)
    void ReadRecordArch(int8_t indxTarif, enum enTypeArch TypeArch, LPENERGY dst);
#endif
#if (_71M6533 || _71M6543)
    void ReadRecordArch_ds(LPDATE pds, enum enTypeArch TypeArch, LPRCRD_ARCH dst);
#endif

#include "archive_mech.h"
#include "archive_utils.h"
#include "archive_cmd.h"
#include "spodes_finde_records.h" 
#include "profile_season_finde.h"
#include "archive_finde_by_index.h"
#include "archive_answer_Filter.h"

#endif