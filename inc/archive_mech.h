#ifndef archive_mechH
#define archive_mechH

#if !defined(ENBL_KILL_ARCH_MECH)
    #define ENBL_KILL_ARCH_MECH     1
    #define ENBL_FINDE_BGN_DTS      1
#else
    #define ENBL_FINDE_BGN_DTS      0
#endif

enum enArchMechMode{
    enIdleAM,
    // -- KillArch modes
    enKillArch_Bgn,
    enKillArch_foNewDTS, enKillArch_foNewDTS_Next,
    enAM_KillArchRecord, enAM_KillArchRecord_work,
    enKillArch_End,
    // -- finde bgn dts PPR
    enFindeArchBgnDTS_Bgn,
    enFindeArchBgnDTS_work,
    enFindeArchBgnDTS_End,
    
    enArchMechMode_SIZE = INT8_MAX
};

struct _ARCH_MECH_VAR{
    enum enArchMechMode Mode;
    
    uint8_t fNeedUpdatePPR;
    time_t tmt_crnt_raw, tmt_new_raw;
    time_t tmt_crnt, tmt_new, tmt;
    enum enTypeArch TypeArch;
    HEAD_LIST head;
    LPCLIST_ARCHIV_FLASH_MEM plist;
    uint16_t cnt_rcrd;
    int32_t indx_rcrd;
    PWR_PRFL_DATA prfl_summ;
    
    uint8_t crc;
    
    uint8_t reserv[64];
};
typedef struct _ARCH_MECH_VAR ARCH_MECH_VAR, *LPARCH_MECH_VAR;

extern ARCH_MECH_VAR arch_mech;

void InitArch_mech(void);
void arch_mech_exec(void);

#define CheckArchMech_busy()        (arch_mech.Mode != enIdleAM)
void save_ArchMeachVar(void);
#if ENBL_FINDE_BGN_DTS
    void arch_mech_FindArchBgnDTS(void);
#else
    #define arch_mech_FindArchBgnDTS()  arch_mech.Mode = enIdleAM
#endif
#if ENBL_KILL_ARCH_MECH
    void arch_mech_KillArch(void);

    enum enArchMechMode funAM_KillArchRecord_work(int16_t *shift);
    void funAM_SummPPR(LPPWR_PRFL_DATA pprfl);
#else
    #define arch_mech_KillArch()    arch_mech.Mode = enIdleAM
#endif

#endif