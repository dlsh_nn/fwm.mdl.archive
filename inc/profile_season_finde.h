/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   profile_season_finde.h
 * Author: User
 *
 * Created on 3 августа 2017 г., 21:57
 */

#ifndef PROFILE_SEASON_FINDE_H
#define PROFILE_SEASON_FINDE_H

/// public
bool getEnableSeason ();
enum enStatusPrtcl CheckValidPrm(LPCARCH_ANSW_INIT pivars);
void InitPPrSeasonFinde(LPARCH_ANSW_INIT pivars, LPARCH_ANSW_VAR pvars);
time_t NextBgnPPR_considering_Season(time_t next_tmt, LPARCH_ANSW_VAR pvars, LPPPR_SEASON_FINDE pssn);
bool OutRcrdPPR_consider_Season(LPCPPR_SEASON_FINDE pssn);
bool OutCurrentRcrdPPR_consider_SeasonPPR(LPARCH_ANSW_VAR pvars);
uint8_t SetSeason_for_NullRcrd(uint8_t day, LPCPPR_SEASON_FINDE pssn);
bool OutRcrdPPR_Forbidden_consider_Season(LPCPPR_SEASON_FINDE pssn);
bool Check_season_of_index(LPCPPR_SEASON_FINDE pssn);
time_t dlt_tmt_season(time_t tmt_min, int8_t fmin, time_t tmt_sub, int8_t fsub, LPCPPR_SEASON_FINDE pssn);
/// private
bool CheckUncertainty_tmt(time_t tmt, LPCPPR_SEASON_FINDE pssn);
int8_t dlt_Season(LPCARCH_ANSW_VAR pvars);
uint8_t FindeDbl_considerind_Season(uint8_t cnt_FindeDbl);
        
//bool OutRcrdPPR_Forbidden_consider_Season(LPCPPR_SEASONCheckNeedOutNullRecord_consider_SeasonutNullRecord_season(time_t tmt, LPCARCH_ANSW_INIT pivars, LPPPR_SEASON_FINDE pssn);

#endif /* PROFILE_SEASON_FINDE_H */

