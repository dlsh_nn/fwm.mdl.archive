/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "define.h"

extern void OutNullRecord_PPR(void);

bool fEnableFindeSeason = false;

bool getEnableSeason () {
    return fEnableFindeSeason;
}

enum enStatusPrtcl CheckValidPrm(LPCARCH_ANSW_INIT pivars){
    enum enStatusPrtcl sts = STS_OK;
    time_t tmt_bgn = pivars->tmt_bgn - (time_t)pivars->season.fSeasonBgn * 60 * 60L;
    time_t tmt_end = pivars->tmt_end - (time_t)pivars->season.fSeasonEnd * 60 * 60L;
    
    if(!(pivars->Type & MSK_SeasonRcrds)) return sts;
    // --
    if(tmt_bgn > tmt_end){
        sts = STS_ERR_PRM;
    }else if( ((CheckSummerWinter_tmt(pivars->tmt_bgn) == 0) && pivars->season.fSeasonBgn) ||
              ((CheckSummerWinter_tmt(pivars->tmt_end) == 0) && pivars->season.fSeasonEnd) ){
        sts = STS_ERR_PRM;
    }
    
    return sts;
}

void InitPPrSeasonFinde(LPARCH_ANSW_INIT pivars, LPARCH_ANSW_VAR pvars){
    LPPPR_SEASON_FINDE pssn = &pvars->season;
    // -- Check Enable Module Finde Season
    fEnableFindeSeason = false;
    if(!(pivars->Type & MSK_SeasonRcrds) || (pivars->TypeArch != eARCH_PPR) ){
        pivars->season.fSeasonBgn = pivars->season.fSeasonEnd = 0;
        return;
    }
    fEnableFindeSeason = true;
    // -- Init Season Finde
    memcpy(pssn, &pivars->season, sizeof(*pssn));
    pssn->fssn = pvars->season.fSeasonBgn;
    pssn->fssn_crnt_tmt = rtc_tbl.rtc_save.fSwitchSummer & rtc_tbl.rtc_save.fSummerTime;
    pssn->tmtEnd_Uncertainty = GetTmtBgnSummerWinter_tmt(pivars->tmt_bgn, false);
    pssn->tmtBgn_Uncertainty = pvars->season.tmtEnd_Uncertainty - 1L * 60 * 60;
}
/* Описание for Doxygen
 * @brief Функция предназначена для разруливания перехода времени через границы 
 *          сезонов. Last Sun March   with 2h to 3h сдвиг времени с Winter to Summer
 *                   Last Sun October with 3h to 2h сдвиг времени с Summer to Winter
 *          [Diagram work read_ppr of season](https://drive.google.com/file/d/0B1FNWfEB2SqIbVlycnI2YmpfeDQ/view?usp=sharing)
 * @return Возвращает штамп времени для поисковой машины с целью продолжения поиска.
 *          При fEnableFindeSeason == false возвращает tmt_next буз изменения.
 *              Иначе возвращает время с учетом механизьма перевода часов.
 */
time_t NextBgnPPR_considering_Season(time_t tmt_next, LPARCH_ANSW_VAR pvars, LPPPR_SEASON_FINDE pssn){
    if(fEnableFindeSeason){
        if(CheckUncertainty_tmt(tmt_next, pssn) || CheckUncertainty_tmt(pvars->tmt, pssn)){
            time_t tmtBS, tmtBW;
            
            tmtBS = GetTmtBgnSummerWinter_tmt(tmt_next, true);
            tmtBW = GetTmtBgnSummerWinter_tmt(tmt_next, false);
            // -- Check Need sheeft Winter <--> Summer
            if( (tmtBS <= tmt_next) && ((tmt_next < tmt_next)) && !pssn->fssn){
                // -- Last Sun March   with 2h to 3h сдвиг времени с Winter to Summer
                pssn->fssn = true;
                tmt_next += 60 * 60L;
            }else if( ((tmt_next < tmtBS) || (tmtBW <= tmt_next)) && pssn->fssn){
                // -- Last Sun October with 3h to 2h сдвиг времени с Summer to Winter
                pssn->fssn = false;
                tmt_next -= 60 * 60L;
            }
        }else{
             pssn->fssn = ( (pssn->fSeasonBgn | pssn->fSeasonEnd) & (CheckSummerWinter_tmt(tmt_next) != 0) );
        }
    }
    return tmt_next;
}
/**
 * \brief Проверка текущего получаса на соответсии запрашиваемого сезона времени.
 * @return true - в случае соответствие сезона текущего получаса и запрашиваемого
 *                сезона или проверка не нужна.
 *         false - сезон текущего получаса не соответствует
 */
bool OutCurrentRcrdPPR_consider_SeasonPPR(LPARCH_ANSW_VAR pvars){
    LPPPR_SEASON_FINDE pssn = &pvars->season;
    
    if(!fEnableFindeSeason) return true;
    
    return (pssn->fssn_crnt_tmt == pssn->fssn);
}
uint8_t SetSeason_for_NullRcrd(uint8_t day, LPCPPR_SEASON_FINDE pssn){
    if(!fEnableFindeSeason) return day;
    
    return day = (pssn->fssn) ? (day | MSK_SUMMER_TIME) : (day & ~MSK_SUMMER_TIME);
}
/** Описание
 * @brief Функция предназначена для запрещения вывода найденной записи в профиле 
 *          в зависимости от сезона по сравнению с сезоном текущей записи профиля.
  * @return true - сезоны найденной записи в архиве и текущей совпадают
 *          false - сезоны найденной записи в архиве и текущей НЕ совпадают  или
 *                  проверка не нужна.
 */
bool OutRcrdPPR_Forbidden_consider_Season(LPCPPR_SEASON_FINDE pssn){
    if(!fEnableFindeSeason) return true;
    
    return (pssn->fssn == pssn->fssn_crnt_tmt);
}
/** Описание 
 * @brief Функция проверяет найденую получасовую запись на соответствие требуемому 
 *          сезонну
  * @return true - запись соответствует текущему сезону или учет сезона отключен.
 *          false - запись не соответствует текущему сезону
 */
bool OutRcrdPPR_consider_Season(LPCPPR_SEASON_FINDE pssn){
    if(!fEnableFindeSeason) return true;

    return (pssn->fSeason_of_index == pssn->fssn);
}
/** Описание
 * \brief Функция предназначена для проверки соответствия сезона, найденной записи
   * @return true - в случае равенства сезонов или отсутствия необходимости проверки
 *           false - в случае НЕ равенства сезонов
 */
bool Check_season_of_index(LPCPPR_SEASON_FINDE pssn){
    if(!fEnableFindeSeason) return true;
    
    return (pssn->fSeason_of_index == pssn->fssn);
}
/** Описание
 * @brief Функция предназначена для получении дельты сезонов (с учетом направления поиска)
  * @return <= 0 в случе если сезон записи по текущему индеку (pvars->indx) 
 *               смещен на -1 час относительно сезона искомого штампа времени (pvars->tmt).
 *          > 0  в случае если сезон записи по текущему индеку (pvars->indx) 
 *               смещен на +1 час относительно сезона искомого штампа времени (pvars->tmt).
 */
int8_t dlt_Season(LPCARCH_ANSW_VAR pvars){
    if(!fEnableFindeSeason) return 0;
    if(!CheckUncertainty_tmt(pvars->tmt, &pvars->season)) return 0;
    
    return (-1 * (pvars->season.fSeason_of_index - pvars->season.fssn) * pvars->Direction);
}
/** Описание
 * @brief Функция предназначена для вычисления разницы двух времен в зависимости от
 *          сезона в промежутке неопределенности.
 * @param tmt_min       уменьшаемое (вычитаемое должно находится в pvars->tmt)
 * @param fssn_tmt_min  флаг сезона уменьшаемого
* @return разнизу двух времен в зависимости от сезона (приводится оба времени к 
 *        сезону зима). В случае если сезон учитывать не треба, выводится дельта
 *        без учета сезона.
 */
time_t dlt_tmt_season(time_t tmt_min, int8_t fmin, time_t tmt_sub, int8_t fsub, LPCPPR_SEASON_FINDE pssn){
    time_t tmt_norm = tmt_min - tmt_sub;
    
    if(!fEnableFindeSeason) return tmt_norm;
    if(!CheckUncertainty_tmt(tmt_min, pssn) &&
        !CheckUncertainty_tmt(tmt_sub, pssn) ) return tmt_norm;
    {
        time_t TSmin = (time_t)fmin * 60 * 60L;
        time_t TSsub = (time_t)fsub * 60 * 60L;
        
        return (tmt_norm - TSmin + TSsub); ///< [(tmt_min - TSmin) - (tmt_sub - TSsub)];
    }
}
uint8_t FindeDbl_considerind_Season(uint8_t cnt_FindeDbl){
    if(!fEnableFindeSeason) return cnt_FindeDbl;
    else return 0;
}
//////////////////////////////////////////
////////////////  PRIVATE ////////////////
//////////////////////////////////////////
bool CheckUncertainty_tmt(time_t tmt, LPCPPR_SEASON_FINDE pssn){
    return ((pssn->tmtBgn_Uncertainty <= tmt) && (tmt < pssn->tmtEnd_Uncertainty));
} 
