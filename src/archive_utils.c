#include "define.h"

time_t GetTmtArch_align(enum enTypeArch TypeArch, time_t tmt){
    DATE_TIME dts;
    
    if(TypeArch == eARCH_PPR){
        tmt /= arch_var.save.tmt_Duration;
        tmt *= arch_var.save.tmt_Duration;
        #if PPR_ALIGN_END
            tmt += TMT_30MIN;
        #endif
    }else{
        // -- get tmt crnt
        GetDts_tmt(tmt, &dts);
        dts.sec = 0;
        dts.min = 0;
        dts.hour = 0;
        if(TypeArch == eARCH_MNTH) dts.date = 1;
        tmt = GetTmt_dts(&dts);
    }
    // --
    return tmt;
}
time_t GetTmtArchDay_align(time_t tmt){
    return GetTmtArch_align(eARCH_DAY, tmt);
}
time_t GetTmtArchMonth_align(time_t tmt){
    return GetTmtArch_align(eARCH_MNTH, tmt);
}
time_t GetTmtArchPPR_align(time_t tmt){
    return GetTmtArch_align(eARCH_PPR, tmt);
}
void fun_Fill_DateRecord_Arch(time_t tmt_align, void *pds){
    LPRCRD_ARCH prcrd = &((LPRCRD_ARCH_WITH_SHIFT)pds)->arch;
    
    prcrd->tmt = tmt_align;
    GetDs_tmt(tmt_align, &prcrd->ds);
}
void fun_Fill_DateRecord_PPR(time_t tmt_align, void *pds){
    LPRCRD_PWR_PRFL prcrd = &((LPRCRD_PWR_PRFL_WITH_SHIFT)pds)->prfl;
    
    prcrd->tmt = tmt_align;
    // -- Summer/Winter
    prcrd->fValid.raw = ppr.fValid_rcrd.raw;
    prcrd->fValid.fild.fSW = (SetSummerWinter() == MSK_SUMMER_TIME);
}
void fun_AfterSave_Arch(void){}
void fun_AfterSave_PPR(void){
    uint8_t i;
    
    ppr.cnt_avg = 0;
    memset(&ppr.pwr_avg, 0, sizeof(ppr.pwr_avg));
    memset(&ppr.Uph_avrg, 0, sizeof(ppr.Uph_avrg));
    memset(&ppr.rcrd, 0, sizeof(ppr.rcrd));
    for(i = 0; i < 3; i++) ppr.rcrd.v2.Umin[i] = MAX_PPR_Umxx;
}
#if !ENBL_KILL_ARCH_MECH
void KillArchRecord(time_t tmt_new, time_t tmt_crnt, LPCLIST_ARCHIV_FLASH_MEM plist){
    HEAD_LIST head;
    AEMem_t addr;
    int32_t indx_rcrd = 0;
    time_t tmt;
    uint16_t i;
    
    // -- 
    if(tmt_new < tmt_crnt){
        if(tmt_crnt != plist->fun_Align_tmt(tmt_crnt - tmt_new)){
            // -- Get head
            rd_ext_mem(plist->AddrArchHead, &head, sizeof(head));
            // -- fined DTS begin
            for(i = 0; i < head.nmb_rcrds; i++){
                // -- read head record
                addr = GetAddrRecord(indx_rcrd, head.Addr_rcrd, &plist->lfm);
                rd_ext_mem(addr, &tmt, sizeof(tmt));
                if((tmt_new <= tmt) && (tmt != -1)){
                    // -- kill record
                    tmt = 0;
                    wr_ext_mem(addr, &tmt, sizeof(tmt));
                }
                indx_rcrd--;
            }
        }
    }
}
#endif
// -- if(fBgn = true) get_bgn_tmt else get_end_tmt
enum enStatusPrtcl get_arch_tmt(enum enTypeArch TypeArch, time_t *ptmt, uint8_t fBgn){
    enum enStatusPrtcl status = STS_OK;
    time_t tmt = 0;
    HEAD_LIST head;
    AEMem_t addr_arch;
    uint16_t i = 0, indx = 0;
    int32_t indx_rcrd;
    
    // --
    rd_ext_mem(ListArch[TypeArch].AddrArchHead, &head, sizeof(head));
    if(!head.nmb_rcrds || (head.nmb_rcrds > ListArch[TypeArch].nmb_record_declared)){
        status = STS_NOT_FOUND;
    }else{
        #if ENBL_FINDE_BGN_DTS
        if(fBgn && (TypeArch == eARCH_PPR)){
            tmt = arch_var.tmt_bgn[TypeArch];
        }else
        #endif
        {
            // -- fined DTS begin
            indx_rcrd = fBgn ? (-1 * (int32_t)head.nmb_rcrds) : 0;
            for(i = 0; i <= head.nmb_rcrds; i++){
                // -- read head record
                addr_arch = GetAddrRecord(indx_rcrd, head.Addr_rcrd, &ListArch[TypeArch].lfm);
                addr_arch += offsetof(RCRD_PWR_PRFL_WITH_SHIFT, prfl.tmt);
                rd_ext_mem(addr_arch, &tmt, sizeof(tmt));
                if(tmt && (tmt != -1)) break;
                // --
                if(fBgn) indx_rcrd++;
                else indx_rcrd--;
            }
        }
        // -- 
        if((i > head.nmb_rcrds) || !tmt){
            status = STS_NOT_FOUND;
        }else{
            *ptmt = tmt;
        }
    }
    
    return status;
}
void save_lbl_time_arch(LPDATE_TIME pdts){
    time_t tmt, tmt_end_arch;
    enum enTypeArch TypeArch;
    enum enStatusPrtcl sts;

    for(TypeArch = eARCH_DAY; TypeArch <= eARCH_MNTH; TypeArch++){
        // -- get tmt crnt, new
        tmt = ListArch[TypeArch].fun_Align_tmt(GetTmt_dts(pdts));
        // -- Check exist label in archive
        sts = get_arch_tmt(TypeArch, &tmt_end_arch, false);
        if( (sts != STS_OK) || (tmt_end_arch != tmt) ){
            // -- Creation label in archive
            AddArch_record_DtArch(TypeArch, tmt, &enrgDt.ETarif);
        }
    }
}
void InitArch_save(void){
    rd_ext_mem(GetAddr(save_Arch), &arch_var.save, sizeof(arch_var.save));
}
void save_ArchVarSave(void){
    uint8_t crc;
    crc = crc_calc(0, &arch_var.save, sizeof(arch_var.save));
    wr_ext_mem(GetAddr(save_Arch), &arch_var.save, sizeof(arch_var.save));
    wr_ext_mem(GetAddr(save_Arch) + sizeof(arch_var.save), &crc, sizeof(crc));
}
uint8_t MaxDblRecords(enum enTypeArch TypeArch, time_t tmt){
    if(TypeArch == eARCH_PPR){
        DATE_TIME dts;
        time_t tmtBgnWinter;
        
        GetDts_tmt(tmt, &dts);
        tmtBgnWinter = GetTmtBgnSummerWinter(&dts, false);
        if( ((tmtBgnWinter - 1 * 60L) <= tmt) || (tmt < tmtBgnWinter) ){
            return (60 * 60L / arch_var.save.tmt_Duration);
        }
    }
    return 2;
}