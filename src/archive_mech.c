#include "define.h"

ARCH_MECH_VAR arch_mech;

void InitArch_mech(void){
    rd_ext_mem(GetAddr(save_ArchMechVar), &arch_mech, sizeof(arch_mech));
    if(arch_mech.crc != crc_calc(0, &arch_mech, offsetof(ARCH_MECH_VAR, crc))){
        memset(&arch_mech, 0, sizeof(arch_var));
    }
    if(!CheckArchMech_busy()){
        arch_mech.Mode = enFindeArchBgnDTS_Bgn;
    }
}
void arch_mech_exec(void){
    if(arch_mech.Mode != enIdleAM){
        if( (enKillArch_Bgn <= arch_mech.Mode) && (arch_mech.Mode <= enKillArch_End) ){
            arch_mech_KillArch();
        }else if( (enFindeArchBgnDTS_Bgn <= arch_mech.Mode) && (arch_mech.Mode <= enFindeArchBgnDTS_End) ){
            arch_mech_FindArchBgnDTS();
        }
    }
}
void save_ArchMeachVar(void){
    arch_mech.crc = crc_calc(0, &arch_mech, offsetof(ARCH_MECH_VAR, crc));
    wr_ext_mem(GetAddr(save_ArchMechVar), &arch_mech, sizeof(arch_mech));
}
#if ENBL_KILL_ARCH_MECH
void arch_mech_KillArch(void){
    static int16_t shift = 0;
    switch(arch_mech.Mode){
    case enKillArch_Bgn:
        arch_mech.Mode = enKillArch_foNewDTS;
        break;
    case enKillArch_foNewDTS:
        if(arch_mech.TypeArch < NMB_ACRH){
            int16_t crnt_index;
            time_t crnt_tmt;
            int16_t new_index;
            
            arch_mech.Mode = enAM_KillArchRecord;
            // -- Init vars
            arch_mech.plist = &ListArch[arch_mech.TypeArch];
            arch_mech.fNeedUpdatePPR = false;
            // -- Get head
            rd_ext_mem(arch_mech.plist->AddrArchHead, &arch_mech.head, sizeof(arch_mech.head));
            // -- get tmt crnt, new
            arch_mech.tmt_crnt = arch_mech.plist->fun_Align_tmt(arch_mech.tmt_crnt_raw);
            arch_mech.tmt_new = arch_mech.plist->fun_Align_tmt(arch_mech.tmt_new_raw);
            crnt_index = 0;
            crnt_tmt = arch_mech.tmt_crnt;
            if(arch_mech.TypeArch == eARCH_PPR) {
                crnt_tmt -= 30 * 60; //Because the latest ppr has time stamp less than arch_mech.tmt_crnt on 30 min
                shift += 1;
            }
            crnt_index = BinSearch_index(/*arch_mech.tmt_crnt*/crnt_tmt, 0, 1 - arch_mech.head.nmb_rcrds, arch_mech.TypeArch);
            
            new_index = BinSearch_index(arch_mech.tmt_new, 0, 1 - arch_mech.head.nmb_rcrds, arch_mech.TypeArch);
            if( (crnt_index > 0) || (new_index > 0) ) {
                shift = -1;
            } else {
                shift += crnt_index - new_index;
                if(shift < 0) {
                    shift = 0;
                }
            }
        }else{
            arch_mech.Mode = enKillArch_End;
            save_ArchMeachVar();
            // -- Update if need PPR
            arch_var.fSaveArch = true;
            if(arch_mech.fNeedUpdatePPR){
                funAM_SummPPR(&ppr.rcrd);
                memcpy(&ppr.rcrd, &arch_mech.prfl_summ, sizeof(ppr.rcrd));
                ppr.fValid_rcrd.fild.ePrfl_sts = ePrflSumm;
            }else{
                if(rtc_tbl.crnt_tmt % arch_var.save.tmt_Duration) ppr.fValid_rcrd.fild.ePrfl_sts = ePrflIncompl;
                else ppr.fValid_rcrd.fild.ePrfl_sts = ePrflValid;
            }
            // -- Check need save label
            save_lbl_time_arch(&rtc_tbl.crnt_dts);
        }
        break;
    case enAM_KillArchRecord:
        // -- Check Need kill arch
        if(arch_mech.tmt_new < arch_mech.tmt_crnt){
            // -- init var
            arch_mech.Mode = enAM_KillArchRecord_work;
            arch_mech.cnt_rcrd = 0;
            arch_mech.indx_rcrd = 0;
            // -- mech PPR befor kill
            if(arch_mech.TypeArch == eARCH_PPR){
                arch_mech.fNeedUpdatePPR = true;
            }
            break;
        }else{
            // -- mech PPR if do not need kill
            if(arch_mech.TypeArch == eARCH_PPR){
                if(arch_mech.tmt_new > arch_mech.tmt_crnt){
                    EventSavePwrProfile_tmt(ePrflIncompl, (arch_mech.tmt_crnt + SHFT_CRNT_TMT_for_SAVE));
                    // -- Check need save label
                    save_lbl_time_arch(&rtc_tbl.crnt_dts);
                }
            }
        }
        arch_mech.Mode = enKillArch_foNewDTS_Next;
        break;
    case enAM_KillArchRecord_work:
        arch_mech.Mode = funAM_KillArchRecord_work(&shift);
        break;
    case enKillArch_foNewDTS_Next:
        arch_mech.Mode = enKillArch_foNewDTS;
        // -- Next TypeArch
        arch_mech.TypeArch++;
        break;
    case enKillArch_End:
        arch_mech.Mode = enFindeArchBgnDTS_Bgn;
        shift = 0;
        break;
    default:
        arch_mech.Mode = enIdleAM;
    }
}
enum enArchMechMode funAM_KillArchRecord_work(int16_t *shift){
    enum enArchMechMode mode = enAM_KillArchRecord_work;
    AEMem_t addr;
    time_t tmt;
    uint8_t cnt_read_tmt = 5;
    PWR_PRFL_DATA   profile;
    bool fNeedKill;
    
    while(cnt_read_tmt--){
        // -- fined DTS begin
        if(arch_mech.cnt_rcrd < arch_mech.head.nmb_rcrds){
            // -- read head record
            addr = GetAddrRecord(arch_mech.indx_rcrd, arch_mech.head.Addr_rcrd, &arch_mech.plist->lfm);
            rd_ext_mem(addr + offsetof(RCRD_PWR_PRFL_WITH_SHIFT, prfl.tmt), &tmt, sizeof(tmt));
            // --
            fNeedKill = false;
            if(arch_mech.TypeArch == eARCH_PPR){
                if((arch_mech.tmt_new <= tmt) && (tmt != -1)){
                    fNeedKill = true;
                    // -- mech PPR Before kill
                    if(arch_mech.TypeArch == eARCH_PPR){
                        rd_ext_mem(addr + offsetof(RCRD_PWR_PRFL_WITH_SHIFT, prfl.dt), &profile, sizeof(profile));
                        funAM_SummPPR(&profile);
                    }
                }
            }else if((arch_mech.tmt_new < tmt) && (tmt != -1)) fNeedKill = true;
            // -- kill record
            if(fNeedKill){
                SHIFT_TIME st;
                
                tmt = 0;
                st.tmt = 0;
                st.shift = *shift;
                if(*shift > 1) {
                    *shift -= 1;                    
                } else if(*shift == 1){
                    *shift = -1;
                } else {
                    *shift = 0;
                }
                //wr_ext_mem(addr + offsetof(RCRD_PWR_PRFL_WITH_SHIFT, prfl.tmt), &tmt, sizeof(tmt));
                wr_ext_mem(addr, &st, sizeof(SHIFT_TIME));
            }
            // -- Next
            arch_mech.indx_rcrd--;
            arch_mech.cnt_rcrd++;
        }else{
            mode = enKillArch_foNewDTS_Next;
            break;
        }
    }
    
    return mode;
}
void funAM_SummPPR(LPPWR_PRFL_DATA pprfl){
    int32_t *psrc, *pdst;
    int8_t i;

    // -- Summ Energy
    psrc = (int32_t *)&pprfl->v0.enrg;
    pdst = (int32_t *)&arch_mech.prfl_summ.v0.enrg;
    for(i = 0; i < 4; i++) pdst[i] += psrc[i];
    // -- Summ Power
    psrc = (int32_t *)&pprfl->v0.pwr_peak;
    pdst = (int32_t *)&arch_mech.prfl_summ.v0.pwr_peak;
    for(i = 0; i < 4; i++){
        if(abs_my(pdst[i]) < abs_my(psrc[i])) pdst[i] = psrc[i];
    }
    // -- Summ Umax
    psrc = (int32_t *)&pprfl->v2.Umax;
    pdst = (int32_t *)&arch_mech.prfl_summ.v2.Umax;
    for(i = 0; i < 3; i++){
        if(abs_my(pdst[i]) < abs_my(psrc[i])) pdst[i] = psrc[i];
    }
    // -- Summ Umin
    psrc = (int32_t *)&pprfl->v2.Umin;
    pdst = (int32_t *)&arch_mech.prfl_summ.v2.Umin;
    for(i = 0; i < 3; i++){
        if(abs_my(pdst[i]) > abs_my(psrc[i])) pdst[i] = psrc[i];
    }
}
#endif
#if ENBL_FINDE_BGN_DTS
void arch_mech_FindArchBgnDTS(void){
    uint8_t cnt_read_tmt = 6;
    AEMem_t addr_arch;
    
    switch(arch_mech.Mode){
    case enFindeArchBgnDTS_Bgn:
        arch_mech.TypeArch = eARCH_PPR;
        // --
        arch_var.tmt_bgn[arch_mech.TypeArch] = 0;
        rd_ext_mem(ListArch[arch_mech.TypeArch].AddrArchHead, &arch_mech.head, sizeof(arch_mech.head));
        if(!arch_mech.head.nmb_rcrds || (arch_mech.head.nmb_rcrds > ListArch[arch_mech.TypeArch].nmb_record_declared)){
            arch_mech.Mode = enIdleAM;
        }else{
            arch_mech.Mode = enFindeArchBgnDTS_work;
            arch_mech.indx_rcrd = -1 * (int32_t)(arch_mech.head.nmb_rcrds - 1);
            arch_mech.cnt_rcrd = 0;
        }
        break;
    case enFindeArchBgnDTS_work:
        while(cnt_read_tmt--){
            if(arch_mech.cnt_rcrd < arch_mech.head.nmb_rcrds){
                // -- read head record
                addr_arch = GetAddrRecord(arch_mech.indx_rcrd, arch_mech.head.Addr_rcrd, &ListArch[arch_mech.TypeArch].lfm);
                addr_arch += offsetof(RCRD_PWR_PRFL_WITH_SHIFT, prfl);
                rd_ext_mem(addr_arch, &arch_mech.tmt, sizeof(arch_mech.tmt));
                if(arch_mech.tmt && (arch_mech.tmt != -1)){
                    arch_mech.Mode = enFindeArchBgnDTS_End;
                    break;
                }
                // --
                arch_mech.indx_rcrd++;
                arch_mech.cnt_rcrd++;
            }else{
                arch_mech.Mode = enFindeArchBgnDTS_End;
                break;
            }
        }
        break;
    case enFindeArchBgnDTS_End:
        if(arch_mech.cnt_rcrd != arch_mech.head.nmb_rcrds){
            arch_var.tmt_bgn[arch_mech.TypeArch] = arch_mech.tmt;
        }
        arch_mech.Mode = enIdleAM;
        break;
    default:
        arch_mech.Mode = enIdleAM;
    }
}
#endif