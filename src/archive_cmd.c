/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "define.h"

void cmd_mng_ppr(LPPROTOCOL_DATA prtcl_data){
    LPPACK_CMD_MNG_PPR p_in = (LPPACK_CMD_MNG_PPR)prtcl_data->pHead->Data;
    LPPACK_ANSW_MNG_PPR p_out = (LPPACK_ANSW_MNG_PPR)prtcl_data->pHead->Data;
    
    // -- Check PSW
    if(!CheckPsw(p_in->psw, PswInd)){
        p_out->Status = STS_ERR_PSW;
        goto __error;
    }
    if(p_in->type == enMPPR_rdDuration){
        p_out->Status = STS_OK;
        p_out->Duration = arch_var.save.tmt_Duration;
    }else if(p_in->type == enMPPR_wrDuration){
        if(CheckArchMech_busy()){
            p_out->Status = ANSW_METER_BUSY;
            goto __error;
        }
        // --
        if(!((60L * 60) % p_in->Duration) && 
            ((60 <= p_in->Duration) && (p_in->Duration <= 60L * 60))){
            StartKillArch_foNewDTS_TypeArch(&rtc_tbl.crnt_dts, 0, eARCH_PPR);
            // --
            arch_var.save.tmt_Duration = p_in->Duration;
            save_ArchVarSave();
            // --
            p_out->Status = STS_OK;
            goto __exit;
        }else{
            p_out->Status = STS_ERR_PRM;
            goto __error;
        }
    }
    
    AnswPrtcl(prtcl_data, sizeof(*p_out));
    return;
    
__error:
__exit:
    AnswPrtcl(prtcl_data, 1);
}