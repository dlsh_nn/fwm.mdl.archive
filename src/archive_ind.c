#include "define.h"

#if (_71M6533 || _71M6543)

int8r_t CodeDirEnrg[4] = {1, -1, 1, -1};
int8r_t CodeDimEnrg[2][4] = {
    {eSymKWH, eSymKWH, eSymCLR, eSymCLR},         // LcdKWA_h
    {eSymCLR, eSymCLR, eSymKWH, eSymKWH},         // LcdKWR_h
};

void IndLCD_Arch(enum enModeMeter mode){
    static uint8_t cntTimeOut_IndData = 0;
    static enum enModeMeter mode_crnt = -1;
    uint8_t indx_month, indx_TypeEnrg;

    // -- next archiv data
    if(mode_crnt != mode){
       cntTimeOut_IndData = 0;
    }
    mode_crnt = mode;
    // -- Get parametrs mode
    mode -= ind_arch_m0_AP;
    indx_TypeEnrg = mode / 12;
    indx_month = mode - (indx_TypeEnrg * 12);
    // -- View Date, Energy
    if(cntTimeOut_IndData < 3){
        cntTimeOut_IndData++;
        IndLCD_Data(indx_month);
    }else{
        IndLCD_Enrg(indx_month, indx_TypeEnrg);
    }
    // -- View TypeEnrg
    IndLCD_TypeEnrg(indx_TypeEnrg);    
}
void IndLCD_TypeEnrg(uint8_t indx_TypeEnrg){
    // -- Dimension
    LcdKWA_h(CodeDimEnrg[0][indx_TypeEnrg]);
    LcdKWR_h(CodeDimEnrg[1][indx_TypeEnrg]);
    // -- DIR
    LcdDirectEnrg(eSymOff);
    LcdReturnEnrg(eSymOff);
    if(CodeDirEnrg[indx_TypeEnrg] > 0) LcdDirectEnrg(eSymOn);
    else LcdReturnEnrg(eSymOn);
    // -- Type AP
    view_Type(en_viewType_AP, -1);
}
void GetDtsArchMonth_indx(LPDATE_TIME pdts, uint8_t indx_month){
    // -- calculated month, year
    if(indx_month >= pdts->month){
        pdts->year--;
        indx_month -= pdts->month;
        pdts->month = December;
    }
    pdts->month -= indx_month;    
}
void IndLCD_Data(uint8_t indx_month){
    DATE_TIME dts;
    
    dts = rtc_tbl.crnt_dts;
    dts.date = 1;
    GetDtsArchMonth_indx(&dts, indx_month);
    // -- Out LCD
    LcdDate(&dts);
}
void IndLCD_Enrg(uint8_t indx_month, uint8_t indx_TypeEnrg){
    uint8_t i;
    RCRD_ARCH arch_rec;
    DATE_TIME dts;
    uint32_t enrg, *pValEnrg;
    
    dts = rtc_tbl.crnt_dts;
    dts.date = 1;
    GetDtsArchMonth_indx(&dts, indx_month);
    ReadRecordArch_ds((LPDATE)&dts.date, eARCH_MNTH, &arch_rec);
    if(arch_rec.tmt != -1){
        enrg = 0;
        for(i = 0; i < NMB_TARIF; i++){
            pValEnrg = (uint32_t *)&(arch_rec.enrg_trf.enrg[i]);
            enrg += pValEnrg[indx_TypeEnrg];
        }
        view_lcd_num((enrg / 1000L), (enrg % 1000L) / 10L, enIND_5up_pnt);
    }else{
        LCD_NA();
    }
}

#endif