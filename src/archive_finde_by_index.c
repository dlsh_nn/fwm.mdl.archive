#include "define.h"

extern void InitModeAns_AnswBgn(void);
extern bool Next_indx(enum enMode_AnswArch Mode);
extern int16_t GetArch_NmbRcrd(void);
extern time_t GetArch_tmt_CheckIndx(int16_t indx);

bool fEnFindeByIndx = false;

/** Описание
 * @brief Функция предназначена для проверки на запрос поиска по индексам
 *          и в случае истины, преобразует индексы в штампы времени.
 * @return STS_OK - в случае если паарметры запроса валидны или поиск по индексу не 
 *                  требуется.
 *         STS_ERR_PRM - в случае если запрошены данные по индексам, но параметры
 *                  находятся не в допуске.
 */
enum enStatusPrtcl CheckFindeByIndex(LPARCH_ANSW_INIT pivars){
    enum enStatusPrtcl sts = STS_OK;
    
    if(pivars->Type != enTPPR_PPRvs4_by_INDX) return sts;
        
    if( ( (pivars->indx_bgn < 0) && (pivars->indx_bgn != -1) ) || 
        ( (pivars->indx_end < 0) && (pivars->indx_end != -1) ) ){
        sts = STS_ERR_PRM;
    }
    
    return sts;
}
/** Описание
 * Функция предназначена для иницилизации данных при запросе количества записей
 *  в рамках СПОДЭС или запрос записей по индексу.
*/
void InitFindeByIndex(LPARCH_ANSW_INIT pivars, LPARCH_ANSW_VAR pvars){
    enum enTypeArch TypeArchAlignBgn = pivars->TypeArch; 
    time_t tmt_least = rtc_tbl.crnt_tmt;

    fEnFindeByIndx = false;
    if(TypeArchAlignBgn == eARCH_PPR) TypeArchAlignBgn = eARCH_DAY;
    get_arch_tmt(pivars->TypeArch, &tmt_least, true);
    if(pivars->Type == enTPPR_getNmb){
        // -- Replacement enTPPR_getNmb to TypePPR_getNmb_DTS
        pivars->Type = TypePPR_getNmb_DTS;
        pivars->tmt_bgn = ListArch[TypeArchAlignBgn].fun_Align_tmt(tmt_least);
        pivars->tmt_end = ListArch[pivars->TypeArch].fun_Align_tmt(rtc_tbl.crnt_tmt);
    }else if(pivars->Type == enTPPR_PPRvs4_by_INDX){
        uint8_t *pfFinde = &pvars->byIndex.fFinde_bgn;
        
        /// -- Check by Predefined
        {
            int8_t i;
            time_t *ptmt = &pivars->tmt_bgn;
            int16_t *pIndex = &pivars->indx_bgn;
            
            for(i = 0; i < 2; i++, ptmt++, pIndex++){
                if(!*pIndex || (*pIndex == -1)){
                    pfFinde[i] = true;
                    if(!*pIndex){
                        /// Current tmt
                        *ptmt = ListArch[pivars->TypeArch].fun_Align_tmt(rtc_tbl.crnt_tmt);
                    }else{
                        /// Last tmt
                        *ptmt = ListArch[TypeArchAlignBgn].fun_Align_tmt(tmt_least);
                    }
                }
            }
        }
        if(pfFinde[0] && pfFinde[1]){
            pivars->Type = (pivars->TypeArch == eARCH_PPR) ? pivars->Type = TypePPR_vs04 :
                                enType_NoPass;
        }else{
            /// Finde NmbRecords by SPODES
            pvars->mode = enMdAns_FindeByIndex_Begin;
            
            pvars->byIndex.fValidRecordDay = true;
            pvars->byIndex.cnt_index_spodes = 0;
            pvars->byIndex.tmt_index_spodes = ListArch[pivars->TypeArch].fun_Align_tmt(rtc_tbl.crnt_tmt);
            pvars->byIndex.cnt_rcrds_day = (pvars->byIndex.tmt_index_spodes % TMT_1DAY) / arch_var.save.tmt_Duration;
            pvars->byIndex.chk_rcrds_day = (TMT_1DAY / arch_var.save.tmt_Duration);
            pvars->DirectFinde = 1;
            pvars->Direction = pvars->DirectFinde;
            pvars->indx = pvars->bgn_indx = 0;
            pvars->end_indx = -1 * GetArch_NmbRcrd();
        }
    }else return;

    fEnFindeByIndx = true;
}
void CheckSpodesIndex_in_request(LPARCH_ANSW_INIT pivars, LPARCH_ANSW_VAR pvars){
    int8_t i;
    uint8_t *pfFinde = &pvars->byIndex.fFinde_bgn;
    int16_t *pIndex = &pivars->indx_bgn;
    time_t *ptmt = &pivars->tmt_bgn;

    for(i = 0; i < 2; i++, ptmt++, pIndex++, pfFinde++){
        if(!*pfFinde){
            if(*pIndex == pvars->byIndex.cnt_index_spodes){
                *pfFinde = true;
                *ptmt = pvars->byIndex.tmt_index_spodes;
            }
        }
    }
}
bool CheckEndFindeIndexes(LPARCH_ANSW_INIT pivars, LPARCH_ANSW_VAR pvars){
    if(pvars->byIndex.fFinde_bgn && pvars->byIndex.fFinde_end){
        pivars->Type = (pivars->TypeArch == eARCH_PPR) ? pivars->Type = TypePPR_vs04 :
                        enType_Pass;
        InitModeAns_AnswBgn();
        // -- Init Filter
        pvars->mode_filter = enMdAns_Filter_SpodesRcrds;
        pvars->byIndex.fValidRecordDay = false;
        // --
        pvars->fAnsweBegin = false;
        pvars->pout[0] = STS_OK;
        AnswPrtcl_data(pivars->prtcl_data, 1);
        return true;
    }
    return false;
}
enum enMode_AnswArch FindeByIndex_exec(LPARCH_ANSW_INIT pivars, LPARCH_ANSW_VAR pvars, uint8_t cnt_read_tmt){
    enum enMode_AnswArch mode = pvars->mode;
    time_t dlt_tmt;
    
    switch(mode){
    case enMdAns_FindeByIndex_Begin:
        mode = (pivars->TypeArch == eARCH_PPR) ? enMdAns_FindeByIndex_NmbRcrd :
                                                 enMdAns_FindeByIndex_NmbRcrd_Archive;
        
        AnswPrtcl_begin(pivars->prtcl_data, 0);
        break;
    case enMdAns_FindeByIndex_NmbRcrd:
        while( (mode == enMdAns_FindeByIndex_NmbRcrd) && (cnt_read_tmt--) ){
            /// -- Get tmt of index archive
            pvars->tmt_of_indx = GetArch_tmt_CheckIndx(pvars->indx);
            /// -- Get tmt of index spodes
            if(pvars->byIndex.fValidRecordDay){
                pvars->byIndex.cnt_index_spodes++;
                /// pvars->byIndex.tmt_index_spodes -= arch_var.save.tmt_Duration;
                pvars->tmt = pvars->byIndex.tmt_index_spodes;
                pvars->DirectFinde = -1;
                pvars->byIndex.tmt_index_spodes = ListArch[pivars->TypeArch].fun_Next_tmt();
                pvars->DirectFinde = 1;
            }else{
                mode = enMdAns_FindeByIndex_CheckIndexValid;
                break;
            }
            // -- Check tmt of index archive to delta tmt of index SPODES
            if( (pvars->tmt_of_indx == -1) || !pvars->tmt_of_indx){
                dlt_tmt = 0;
            }else{
                dlt_tmt = pvars->tmt_of_indx - pvars->byIndex.tmt_index_spodes;
            }
            /// Check curent spodes index == bgn or end index request
            CheckSpodesIndex_in_request(pivars, pvars);
            /// Check End finde index
            if(CheckEndFindeIndexes(pivars, pvars)){
                mode = pvars->mode;
                break;
            }
            // -- Next index
            if(!dlt_tmt){
                if(!Next_indx(enMdAns_Answ_STS_ERR_PRM)){
                    if(!pvars->byIndex.fValidRecordDay){
                        ///< enMdAns_Answ_STS_ERR_PRM
                        mode = pvars->mode;
                        break;
                    }else{
                        /// -- Continue finde index
                        mode = enMdAns_FindeByIndex_NmbRcrd;
                    }
                }
            }
            // -- Update cnt_rcrds_day
            if(pvars->byIndex.cnt_rcrds_day) pvars->byIndex.cnt_rcrds_day--;
            if(!pvars->byIndex.cnt_rcrds_day){
                pvars->byIndex.fValidRecordDay = (pivars->TypeArch != eARCH_PPR);
                pvars->byIndex.cnt_rcrds_day = pvars->byIndex.chk_rcrds_day;
            }
        }
        break;
    case enMdAns_FindeByIndex_CheckIndexValid:
        while( (mode == enMdAns_FindeByIndex_CheckIndexValid) && (cnt_read_tmt--) ){
            if(Next_indx(enMdAns_Answ_STS_ERR_PRM)){
                // -- Finde Valid Tmt
                pvars->tmt_of_indx = GetArch_tmt_CheckIndx(pvars->indx);
                if( (pvars->tmt_of_indx != -1) && pvars->tmt_of_indx){
                    dlt_tmt = pvars->tmt_of_indx - pvars->byIndex.tmt_index_spodes;
                    if(dlt_tmt < 0){
                        mode = enMdAns_FindeByIndex_NmbRcrd;
                        pvars->byIndex.fValidRecordDay = true;
                        pvars->byIndex.cnt_rcrds_day = pvars->byIndex.chk_rcrds_day;
                        pvars->byIndex.tmt_index_spodes = GetTmtArchDay_align(pvars->tmt_of_indx + TMT_1DAY);
                    }
                }
            }else mode = pvars->mode;
        }
        break;
    case enMdAns_FindeByIndex_NmbRcrd_Archive:
        while( (mode == enMdAns_FindeByIndex_NmbRcrd_Archive) && (cnt_read_tmt--) ){
            pvars->tmt_of_indx = GetArch_tmt_CheckIndx(pvars->indx);
            // -- Check tmt by current Index
            if( (pvars->tmt_of_indx != -1) && pvars->tmt_of_indx){
                pvars->byIndex.tmt_index_spodes = pvars->tmt_of_indx;
                /// Check curent spodes index == bgn or end index request
                CheckSpodesIndex_in_request(pivars, pvars);
                if(CheckEndFindeIndexes(pivars, pvars)){
                    mode = pvars->mode;
                    break;
                }
                // -- Next counter SPODES
                pvars->byIndex.cnt_index_spodes++;
            }
            // -- Next Index Archive
            if(!Next_indx(enMdAns_Answ_STS_ERR_PRM)){
                ///< enMdAns_Answ_STS_ERR_PRM
                mode = pvars->mode;
                break;
            }
        }
        break;
    case enMdAns_Answ_STS_ERR_PRM:
        mode = enMdAns_ResetMech;
        // --
        pvars->pout[0] = STS_ERR_PRM;
        AnswPrtcl_data(pivars->prtcl_data, 1);
        AnswPrtcl_end(pivars->prtcl_data);
        break;
    default:
        mode = enMdAns_ResetMech;
    }
    
    return mode;
}