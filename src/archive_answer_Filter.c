#include "define.h"

extern bool Next_indx(enum enMode_AnswArch Mode);
extern time_t GetArch_tmt_CheckIndx(int16_t indx);

void FilterAnsw_PreNextIndex(LPARCH_ANSW_VAR pvars){
    if(pvars->mode_filter != enMdAns_Filter_SpodesRcrds) return;
    
    if(!pvars->byIndex.fValidRecordDay){
        if(GetTmtArchDay_align(pvars->tmt_of_indx) == GetTmtArchDay_align(pvars->tmt)){
            pvars->byIndex.fValidRecordDay = true;
        }
    }
}
bool FilterAnsw_CheckNeed_OutNullRecord(LPARCH_ANSW_VAR pvars){
    time_t dlt_tmt;
    
    if(pvars->mode_filter != enMdAns_Filter_SpodesRcrds) return true;
    
    if(!pvars->byIndex.fValidRecordDay){
        time_t tmt_of_indx;
        pvars->byIndex.cnt_rcrds_day = pvars->byIndex.chk_rcrds_day;
        do{
            tmt_of_indx = GetArch_tmt_CheckIndx(pvars->indx);
            if((tmt_of_indx == -1) || !tmt_of_indx){
                continue;
            }else{                
                dlt_tmt = GetTmtArchDay_align(tmt_of_indx) - GetTmtArchDay_align(pvars->tmt);

                if(!dlt_tmt){
                    pvars->byIndex.fValidRecordDay = true;
                    goto __exit_OutRecrod;
                }
            }
        }while(Next_indx(enMdAns_Next_tmt));

        goto __exit_NotOutRecord;
        
    }else{
        dlt_tmt = pvars->tmt - (GetTmtArchDay_align(pvars->tmt + TMT_1DAY) - arch_var.save.tmt_Duration);
        // -- Check position 00:00:00 or 23:30:00 for example Duration = 30 minute
        if((GetTmtArchDay_align(pvars->tmt) == pvars->tmt) || !dlt_tmt){
            pvars->byIndex.fValidRecordDay = false;
        }
    }
    
__exit_OutRecrod:
    return true;
__exit_NotOutRecord:
    return false;
}
