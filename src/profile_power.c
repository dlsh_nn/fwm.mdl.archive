#include "define.h"

PWR_PROFILE ppr;

void PwrProfilePwrOn(void){
    RCRD_PWR_PRFL rcrd;
    HEAD_LIST head;
    time_t tmt_crnt_align30min;
    
    // -- read record
    rd_ext_mem(GetAddr(pwr_prfl_head), &head, sizeof(head));
    rd_ext_mem(head.Addr_rcrd + offsetof(RCRD_PWR_PRFL_WITH_SHIFT, prfl), &rcrd, sizeof(rcrd));
    // -- align 30 min
    tmt_crnt_align30min = GetTmtArchPPR_align(rtc_tbl.crnt_tmt);
    if(rtc_tbl.crnt_tmt % arch_var.save.tmt_Duration){
        ppr.fValid_rcrd.fild.ePrfl_sts = ePrflIncompl;
    }
    // -- check flg
    ppr.Addr_NeedDel_PwrOff = 0;
    if(rcrd.fValid.fild.ePrfl_sts == ePrflIncompl){
        if(tmt_crnt_align30min == rcrd.tmt){
            ppr.Addr_NeedDel_PwrOff = head.Addr_rcrd;
            memcpy(&ppr.rcrd, &rcrd.dt, sizeof(ppr.rcrd));
        }
    }
}
void EventSavePwrProfile(enum enPrflValid fValid){
    EventSavePwrProfile_tmt(fValid, rtc_tbl.crnt_tmt);
}
void EventSavePwrProfile_tmt(enum enPrflValid fValid, time_t crnt_tmt){
    if( (ppr.fValid_rcrd.fild.ePrfl_sts != ePrflIncompl) && 
        (ppr.fValid_rcrd.fild.ePrfl_sts != ePrflSumm) ){
        ppr.fValid_rcrd.fild.ePrfl_sts = fValid;
    }
    #if !PPR_ALIGN_END
        if(fValid == ePrflValid) crnt_tmt -= SHFT_CRNT_TMT_for_SAVE;
    #endif
    // -- Add record
    if(!arch_var.fIgnorePPR){
        RecordPPR_NeedDel_PwrOn();
        PreSavePPR(&ppr.rcrd);
        AddArch_record_DtArch(eARCH_PPR, crnt_tmt, &ppr.rcrd);
        // -- Synchronization Energy
        SaveEnergy();
    }else arch_var.fIgnorePPR = false;
    // -- Check Start finde bgn DTS PPR again
    if(!CheckArchMech_busy()){
        arch_mech.Mode = enFindeArchBgnDTS_Bgn;
    }
    
    ppr.fValid_rcrd.fild.ePrfl_sts = ePrflValid;
}
void profilePwrPeak(void){
    uint8_t i;
    Type_PPR_Umxx Vscale;
    
    // -- active
    if(ce_mdl.Pa[NMB_PHASE] >= 0){
        if(ce_mdl.Pa[NMB_PHASE] > ppr.rcrd.v0.pwr_peak.Ap){
            ppr.rcrd.v0.pwr_peak.Ap = ce_mdl.Pa[NMB_PHASE];
        }
    }else{
        if(ce_mdl.Pa[NMB_PHASE] < ppr.rcrd.v0.pwr_peak.An){
            ppr.rcrd.v0.pwr_peak.An = ce_mdl.Pa[NMB_PHASE];
        }
    }
    // -- reactive
    if(ce_mdl.Pr[NMB_PHASE] >= 0){
        if(ce_mdl.Pr[NMB_PHASE] > ppr.rcrd.v0.pwr_peak.Rp){
            ppr.rcrd.v0.pwr_peak.Rp = ce_mdl.Pr[NMB_PHASE];
        }
    }else{
        if(ce_mdl.Pr[NMB_PHASE] < ppr.rcrd.v0.pwr_peak.Rn){
            ppr.rcrd.v0.pwr_peak.Rn = ce_mdl.Pr[NMB_PHASE];
        }
    }
    for(i = 0; i < NMB_PHASE; i++){
        Vscale = ce_mdl.Vmv[i] / SCALE_PPR_Umxx;
        // -- Umax
        if(Vscale > ppr.rcrd.v2.Umax[i]){
            ppr.rcrd.v2.Umax[i] = Vscale;
        }
        // -- Umin
        if(Vscale < ppr.rcrd.v2.Umin[i]){
            ppr.rcrd.v2.Umin[i] = Vscale;
        }
    }
}
void PreSavePPR(LPPWR_PRFL_DATA prcrd){
    uint8_t i;

    prcrd->vs4.Temp = ce_mdl.Temp / 100;
    for(i = 0; i < NMB_PHASE; i++){
        if(ppr.cnt_avg){
            prcrd->vs4.Uph_avrg[i] = ppr.Uph_avrg[i]  / ppr.cnt_avg;
        }
    }
}
void profilePwrAverage(void){
    uint8_t i;

    // -- average
    ppr.cnt_avg++;
    if(ce_mdl.Pa[NMB_PHASE] >= 0) ppr.pwr_avg.Ap += (ce_mdl.Pa[NMB_PHASE] / 1000);
    else ppr.pwr_avg.An += -1 * (ce_mdl.Pa[NMB_PHASE] / 1000);
    if(ce_mdl.Pr[NMB_PHASE] > 0) ppr.pwr_avg.Rp += (ce_mdl.Pr[NMB_PHASE] / 1000);
    else ppr.pwr_avg.Rn += -1 * (ce_mdl.Pr[NMB_PHASE] / 1000);
    // --
    for(i = 0; i < NMB_PHASE; i++){
        ppr.Uph_avrg[i] += ce_mdl.Vmv[i] / SCALE_PPR_Umxx;
    }    
}
void PwrProfile_update(void){
    uint8_t i;
    int32_t *pAx_src = &ce_mdl.enrg_int.Ap;
    int32_t *pAx_dst = &ppr.rcrd.v0.enrg.Ap;
    
    for(i = 0; i < 4; i++){
        pAx_dst[i] += pAx_src[i];
    }
    
    ppr.Apn = ppr.rcrd.v0.enrg.Ap + ppr.rcrd.v0.enrg.An;
    ppr.Rpn = ppr.rcrd.v0.enrg.Rp + ppr.rcrd.v0.enrg.Rn;
    // --
    profilePwrPeak();
    profilePwrAverage();
}
void RecordPPR_NeedDel_PwrOn(void){
    uint32_t tmt = 0;
    
    if(ppr.Addr_NeedDel_PwrOff != 0){
        wr_ext_mem(ppr.Addr_NeedDel_PwrOff + offsetof(RCRD_PWR_PRFL_WITH_SHIFT, prfl.tmt), &tmt, sizeof(tmt));
        // -- disable
        ppr.Addr_NeedDel_PwrOff = 0;
    }
}
#if (_71M6533 || _71M6543)
int32_t AvgPPR_for_ind(int32_t enrg){
    return EnrgAlign_Wh((enrg / ppr.cnt_avg) * 1000);
}
#endif