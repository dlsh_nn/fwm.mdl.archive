#include "define.h"

//const LIST_ARCHIV_FLASH_MEM code ListArch[NMB_ACRH] = {
//    // -- eARCH_DAY
//    { GetAddr(arch_head.head_day), NMB_RCRD_DAY,
//      offsetof(RCRD_ARCH, enrg_trf), sizeof(ENERGYS_MTR_TRFS),
//      def_SupportTypeV,
//      // --
//      { ADDR_FLASH_sect_arch_bgn_day, SIZE_SECT_ARCH_DAY,
//        ( (SIZE_SECT_ARCH_DAY / SIZE_SECTOR) * NMB_RCRD_SECT_ARCH ),
//        NMB_RCRD_SECT_ARCH,
//        sizeof(RCRD_ARCH)
//      },
//      // -- Function
//      GetTmtArchDay_align, fun_Fill_DateRecord_Arch, fun_AfterSave_Arch,
//      NexBgnDay
//    },
//    // -- eARCH_MNTH
//    { GetAddr(arch_head.head_mnth), NMB_RCRD_MNTH,
//      offsetof(RCRD_ARCH, enrg_trf), sizeof(ENERGYS_MTR_TRFS),
//      def_SupportTypeV,
//      // --
//      { ADDR_FLASH_sect_arch_bgn_mnth, SIZE_SECT_ARCH_MONTH,
//        ( (SIZE_SECT_ARCH_MONTH / SIZE_SECTOR) * NMB_RCRD_SECT_ARCH ),
//        NMB_RCRD_SECT_ARCH,
//        sizeof(RCRD_ARCH)
//      },
//      // -- Function
//      GetTmtArchMonth_align, fun_Fill_DateRecord_Arch, fun_AfterSave_Arch,
//      NexBgnMonth
//    },
//    // -- eARCH_PPR
//    { GetAddr(pwr_prfl_head), NMB_RCRD_PWR_PRFL,
//      offsetof(RCRD_PWR_PRFL, dt), sizeof(PWR_PRFL_DATA),
//      def_SupportTypeV,
//      // --
//      { ADDR_FLASH_sect_pwr_prfl, SIZE_SECT_PWR_PRFL,
//        ( (SIZE_SECT_PWR_PRFL / SIZE_SECTOR) * NMB_RCRD_SECT_PwrPrfl ),
//        NMB_RCRD_SECT_PwrPrfl,
//        sizeof(RCRD_PWR_PRFL)
//      },
//      // -- Function
//      GetTmtArchPPR_align, fun_Fill_DateRecord_PPR, fun_AfterSave_PPR,
//      NexBgnPPR
//    },
//};

const LIST_ARCHIV_FLASH_MEM code ListArch[NMB_ACRH] = {
    // -- eARCH_DAY
    { GetAddr(arch_head.head_day), NMB_RCRD_DAY,
      offsetof(RCRD_ARCH_WITH_SHIFT, arch.enrg_trf), sizeof(ENERGYS_MTR_TRFS),
      def_SupportTypeV_ARCH,
      // --
      { ADDR_FLASH_sect_arch_bgn_day, SIZE_SECT_ARCH_DAY,
        ( (SIZE_SECT_ARCH_DAY / SIZE_SECTOR) * NMB_RCRD_SECT_ARCH ),
        NMB_RCRD_SECT_ARCH,
        sizeof(RCRD_ARCH_WITH_SHIFT)
      },
      // -- Function
      GetTmtArchDay_align, fun_Fill_DateRecord_Arch, fun_AfterSave_Arch,
      NexBgnDay
    },
    // -- eARCH_MNTH
    { GetAddr(arch_head.head_mnth), NMB_RCRD_MNTH,
      offsetof(RCRD_ARCH_WITH_SHIFT, arch.enrg_trf), sizeof(ENERGYS_MTR_TRFS),
      def_SupportTypeV_ARCH,
      // --
      { ADDR_FLASH_sect_arch_bgn_mnth, SIZE_SECT_ARCH_MONTH,
        ( (SIZE_SECT_ARCH_MONTH / SIZE_SECTOR) * NMB_RCRD_SECT_ARCH ),
        NMB_RCRD_SECT_ARCH,
        sizeof(RCRD_ARCH_WITH_SHIFT)
      },
      // -- Function
      GetTmtArchMonth_align, fun_Fill_DateRecord_Arch, fun_AfterSave_Arch,
      NexBgnMonth
    },
    // -- eARCH_PPR
    { GetAddr(pwr_prfl_head), NMB_RCRD_PWR_PRFL,
      offsetof(RCRD_PWR_PRFL_WITH_SHIFT, prfl.dt), sizeof(PWR_PRFL_DATA),
      def_SupportTypeV,
      // --
      { ADDR_FLASH_sect_pwr_prfl, SIZE_SECT_PWR_PRFLv1,
        ( (SIZE_SECT_PWR_PRFLv1 / SIZE_SECTOR) * NMB_RCRD_SECT_PwrPrfl ),
        NMB_RCRD_SECT_PwrPrfl,
        sizeof(RCRD_PWR_PRFL_WITH_SHIFT)
      },
      // -- Function
      GetTmtArchPPR_align, fun_Fill_DateRecord_PPR, fun_AfterSave_PPR,
      NexBgnPPR
    },
};

ARCH_VAR arch_var;

void InitArch(void){
    memset(&arch_var, 0, sizeof(arch_var));
    InitArch_save();
    // -- PPR
    ListArch[eARCH_PPR].fun_AfterSave();
    PwrProfilePwrOn();
    // -- DAY, MONTH
    if(!fBatMode){
        save_lbl_time_arch(&rtc_tbl.crnt_dts);
    }
    InitArch_mech();
    // -- Block save arch
    arch_var.fSaveArch = true;
}
uint8_t Arch_exec(void){
    if(CheckArchMech_busy()){
        arch_mech_exec();
        return 1;
    }else{
        // -- Work
        if(arch_var.fUpdateRTC){
            uint16_t rem;
            arch_var.fUpdateRTC = false;
            
            rem = rtc_tbl.crnt_tmt % arch_var.save.tmt_Duration;
            if(!arch_var.fSaveArch){
                // -- ppr
                if(rem < 30){
                    arch_var.fSaveArch = true;
                    
                    EventSavePwrProfile(ePrflValid);
                    // -- Day
                    if(!rtc_tbl.crnt_dts.hour && !rtc_tbl.crnt_dts.min){
                        AddArch_record_DtArch(eARCH_DAY, rtc_tbl.crnt_tmt, &enrgDt.ETarif);
                        // -- Month
                        if((rtc_tbl.crnt_dts.date == 1) && !rtc_tbl.crnt_dts.hour){
                            AddArch_record_DtArch(eARCH_MNTH, rtc_tbl.crnt_tmt, &enrgDt.ETarif);
                        }
                    }
                }
            }else if(rem > 30){
                arch_var.fSaveArch = false;
            }
        }
        MechAnswerArch();
        return 0;
    }
    return 0;
}
void reset_arch(void){
    HEAD_LIST head;
    union{
        ENERGYS_MTR_TRFS arch;
        PWR_PRFL_DATA prfl;
    }Dt;
    enum enTypeArch TypeArch;
    
    memset(&head, 0, sizeof(head));
    memset(&Dt, 0, sizeof(Dt));
    // -- PPR
    arch_var.save.tmt_Duration = 30 * 60;
    save_ArchVarSave();

    for(TypeArch = 0; TypeArch < NMB_ACRH; TypeArch++){
        // -- head
        head.Addr_rcrd = ListArch[TypeArch].lfm.ADDR_BGN;
        wr_ext_mem(ListArch[TypeArch].AddrArchHead, &head, sizeof(head));
        // -- First record
        AddArch_record_DtArch(TypeArch, rtc_tbl.crnt_tmt, &Dt);
        // -- Begin tmt archiv
        arch_var.tmt_bgn[TypeArch] = ListArch[TypeArch].fun_Align_tmt(rtc_tbl.crnt_tmt);
    }
    // --
    arch_var.fIgnorePPR = true;
}
//-----------------------------------------------------------------------------
// -- get_bgn_dts
//-----------------------------------------------------------------------------
void get_bgn_dts_ppr(LPPROTOCOL_DATA prtcl_data){
    LPPACK_ANSW_BGN_DTS_PPR p_out = (LPPACK_ANSW_BGN_DTS_PPR)prtcl_data->pHead->Data;
    time_t tmt;
    
    if(CheckArchMech_busy()){
        p_out->Status = ANSW_METER_BUSY;
        goto __error;
    }
    p_out->Status = get_arch_tmt(eARCH_PPR, &tmt, true);
    if(p_out->Status == STS_OK) GetDts_tmt(tmt, &p_out->dts_bgn);
    else goto __error;
    
    AnswPrtcl(prtcl_data, sizeof(*p_out));
    return;
    
__error:
    AnswPrtcl(prtcl_data, 1);
}
void get_bgn_ds_arch(LPPROTOCOL_DATA prtcl_data){
    time_t tmt;
    LPPACK_GET_BGN_DS_ARCH p_in = (LPPACK_GET_BGN_DS_ARCH)prtcl_data->pHead->Data;
    LPPACK_ANSW_BGN_DS_ARCH p_out = (LPPACK_ANSW_BGN_DS_ARCH)prtcl_data->pHead->Data;

    if(CheckArchMech_busy()){
        p_out->Status = ANSW_METER_BUSY;
        goto __error;
    }
    // -- Check valid Type Arch
    if(p_in->TypeArch >= eARCH_PPR){
        p_out->Status = STS_ERR_PRM;
        goto __error;
    }
    p_out->Status = get_arch_tmt(p_in->TypeArch, &tmt, true);
    if(p_out->Status != STS_OK) goto __error;

    GetDs_tmt(tmt, &p_out->ds);    
    AnswPrtcl(prtcl_data, sizeof(*p_out));
    return;
    
__error:
    AnswPrtcl(prtcl_data, 1);
}
//-----------------------------------------------------------------------------
// -- Get archiv data
//-----------------------------------------------------------------------------
void get_pwr_profile(LPPROTOCOL_DATA prtcl_data){
    LPPACK_GET_PWR_PRFL p_in = (LPPACK_GET_PWR_PRFL)prtcl_data->pHead->Data;
    LPPACK_GET_PPR_by_INDX p_in_indx = (LPPACK_GET_PPR_by_INDX)prtcl_data->pHead->Data;
    uint8_t *psts = prtcl_data->pHead->Data;
    ARCH_ANSW_INIT arch_answ_init;
    
    if(CheckArchMech_busy()){
        *psts = ANSW_METER_BUSY;
        goto __error;
    }

    prtcl_data->pDriver->ManagerRec(enDrvMng_STOP_REC);
    // -- init vars
    memset(&arch_answ_init, 0, sizeof(arch_answ_init));
    arch_answ_init.TypeArch = eARCH_PPR;
    arch_answ_init.Type = p_in->Type;
    arch_answ_init.tmt_bgn = GetTmt_dts(&p_in->dts_bgn);
    arch_answ_init.tmt_end = GetTmt_dts(&p_in->dts_end);
    arch_answ_init.indx_bgn = p_in_indx->indx_bgn;
    arch_answ_init.indx_end = p_in_indx->indx_end;
    arch_answ_init.season.fSeasonBgn = ((p_in->dts_bgn.day & MSK_SUMMER_TIME) != 0);
    arch_answ_init.season.fSeasonEnd = ((p_in->dts_end.day & MSK_SUMMER_TIME) != 0);
    arch_answ_init.prtcl_data = prtcl_data;
    // -- Init mech answer
    if((*psts = InitMechAnsw_Arch(&arch_answ_init)) != STS_OK) goto __error;

   
    return;
__error:
    prtcl_data->pDriver->ManagerRec(enDrvMng_START_REC);
    AnswPrtcl(prtcl_data, 1);
}
void get_arch_mtr(LPPROTOCOL_DATA prtcl_data){
    LPPACK_GET_ARCH p_in = (LPPACK_GET_ARCH)prtcl_data->pHead->Data;
    LPPACK_GET_ARCH_by_INDX p_in_indx = (LPPACK_GET_ARCH_by_INDX)prtcl_data->pHead->Data;
    uint8_t *psts = prtcl_data->pHead->Data;
    ARCH_ANSW_INIT arch_answ_init;

    prtcl_data->pDriver->ManagerRec(enDrvMng_STOP_REC);
    if(CheckArchMech_busy()){
        *psts = ANSW_METER_BUSY;
        goto __error;
    }

    // -- init vars
    arch_var.indx_Interface = prtcl_data->pDriver->TypeInterface;
    arch_answ_init.TypeArch = p_in->TypeArch;
    arch_answ_init.Type = p_in->Type;
    arch_answ_init.MskTarif = p_in->MskTarif;
    arch_answ_init.tmt_bgn = GetTmt_ds(&p_in->ds_bgn);
    arch_answ_init.tmt_end = GetTmt_ds(&p_in->ds_end);
    arch_answ_init.indx_bgn = p_in_indx->indx_bgn;
    arch_answ_init.indx_end = p_in_indx->indx_end;
    arch_answ_init.prtcl_data = prtcl_data;
    // -- Init mech answer
    if((*psts = InitMechAnsw_Arch(&arch_answ_init)) != STS_OK) goto __error;
    
    return;
__error:
    prtcl_data->pDriver->ManagerRec(enDrvMng_START_REC);
    AnswPrtcl(prtcl_data, 1);
}
#if (PWR_MNG_Eday || PWR_MNG_Emonth)
void ReadRecordArch(int8_t indxTarif, enum enTypeArch TypeArch, LPENERGY dst){
    HEAD_LIST head;
    AEMem_t addr_rcrd_arch;
    time_t tmt;
    int8_t nmbTarif = indxTarif;
    ENERGY enrg;

    memset(dst, 0x00, sizeof(*dst));
    // -- Check if need SummT
    if(indxTarif >= NMB_TARIF){
        indxTarif = 0;
        nmbTarif = NMB_TARIF;
    }
    rd_ext_mem(ListArch[TypeArch].AddrArchHead, &head, sizeof(head));
    rd_ext_mem(head.Addr_rcrd, &tmt, sizeof(tmt));
    if((tmt != -1) && tmt){
        for( ;indxTarif < nmbTarif; indxTarif++){
            addr_rcrd_arch = head.Addr_rcrd + offsetof(RCRD_ARCH_WITH_SHIFT, arch.enrg_trf) + ((AEMem_t)indxTarif) * sizeof(ENERGY);
            rd_ext_mem(addr_rcrd_arch, &enrg, sizeof(enrg));
            // --
            dst->Ap += enrg.Ap;
            dst->An += enrg.An;
            dst->Rp += enrg.Rp;
            dst->Rn += enrg.Rn;
        }
    }else memset(dst, 0xFF, sizeof(*dst));
}
#endif
void ReadRecordArch_ds(LPDATE pds, enum enTypeArch TypeArch, LPRCRD_ARCH dst){
    HEAD_LIST head;
    AEMem_t addr_rcrd_arch;
    time_t tmt, tmt_finde;
    uint16_t indx = 0;
    
    tmt_finde = GetTmt_ds(pds);
    memset(dst, 0xFF, sizeof(*dst));
    rd_ext_mem(ListArch[TypeArch].AddrArchHead, &head, sizeof(head));
    for(indx = 0; indx < head.nmb_rcrds; indx++){
        addr_rcrd_arch = GetAddrRecord( (-1 * (int16_t)indx), head.Addr_rcrd, &ListArch[TypeArch].lfm);
        rd_ext_mem(addr_rcrd_arch + offsetof(RCRD_ARCH_WITH_SHIFT, arch), &tmt, sizeof(tmt));//offsetof(RCRD_ARCH, ds));
        if(tmt == tmt_finde){
            rd_ext_mem(addr_rcrd_arch + offsetof(RCRD_ARCH_WITH_SHIFT, arch), dst, sizeof(*dst));
            break;
        }
    }
}
void AddArch_record_DtArch(enum enTypeArch Type, time_t tmt_not_align, void *pDtArch){
//    union{
//        RCRD_ARCH       arch;
//        RCRD_PWR_PRFL   ppr;
//    }ArchRecord;

    union{
        RCRD_ARCH_WITH_SHIFT       arch;
        RCRD_PWR_PRFL_WITH_SHIFT   ppr;
    }ArchRecord;
    uint8_t *pRawArchRecord;

    //memset(&ArchRecord, 0, sizeof(ArchRecord));
    memset(&ArchRecord, 0xFF, sizeof(uint16_t));

    //MAKE IT CLEAR: pRawArchRecord - запись в бинарном виде? (указатель на данные)
    // -- Fill ArchRecord
    //MAKE IT CLEAR: Offset_DtArch - для чего нужен этот сдвиг? (чтобы перейти на место записи данных, пропустив место под метку времени)
    pRawArchRecord = ((uint8_t *)&ArchRecord) + ListArch[Type].Offset_DtArch;
    memcpy(pRawArchRecord, pDtArch, ListArch[Type].size_DtArch);
    // --
    AddArch_record(Type, tmt_not_align, &ArchRecord);
}
void AddArch_record(enum enTypeArch Type, time_t tmt_not_align, void *pDataArch){
    HEAD_LIST head;
    AEMem_t Addr_new;
    uint8_t *pds = (uint8_t *)pDataArch;
    time_t tmt_align;

    rd_ext_mem(ListArch[Type].AddrArchHead, &head, sizeof(head));
    //MAKE IT CLEAR: подготавливаем память для новой записи?
    NextRecord(&head, ListArch[Type].nmb_record_declared, ListArch[Type].lfm.NMB_RCRD_MAX);
    // -- Date
    tmt_align = ListArch[Type].fun_Align_tmt(tmt_not_align);
    ListArch[Type].fun_Fill_DateRecord(tmt_align, pds);
    // -- new addr
    Addr_new = GetAddrRecord(1, head.Addr_rcrd, &ListArch[Type].lfm);
    if((head.Addr_rcrd & ~(SIZE_SECTOR - 1)) != (Addr_new & ~(SIZE_SECTOR - 1))){
        ers_at25(Addr_new - SIZE_EEPROM, SIZE_SECTOR);
        unpr_at25(Addr_new - SIZE_EEPROM, SIZE_SECTOR);
    }
    head.Addr_rcrd = Addr_new;
    // -- write head/record
    //Записываем информацию о head
    wr_ext_mem(ListArch[Type].AddrArchHead, &head, sizeof(head));
    //Дописываем данные записи (pds) по адресу head.Addr_rcrd
    wr_ext_mem(head.Addr_rcrd, pds, ListArch[Type].lfm.SIZE_RECORD);
    // -- After Save Arch
    ListArch[Type].fun_AfterSave();
}
#if ENBL_KILL_ARCH_MECH
void StartKillArch_foNewDTS(LPDATE_TIME pCrnt_dts, LPDATE_TIME pNew_dts){
    StartKillArch_foNewDTS_TypeArch(pCrnt_dts, pNew_dts, eARCH_DAY);
}
void StartKillArch_foNewDTS_TypeArch(LPDATE_TIME pCrnt_dts, LPDATE_TIME pNew_dts, enum enTypeArch TypeArch){
    uint8_t i;
    
    // -- InitArchMech
    arch_mech.Mode = enKillArch_Bgn;
    arch_mech.TypeArch = TypeArch;
    // -- get tmt crnt, new
    arch_mech.tmt_crnt_raw = GetTmt_dts(pCrnt_dts);
    arch_mech.tmt_new_raw = (pNew_dts) ? GetTmt_dts(pNew_dts) : 0;
    // -- others
    memset(&arch_mech.prfl_summ, 0, sizeof(arch_mech.prfl_summ));
    for(i = 0; i < 3; i++) arch_mech.prfl_summ.v2.Umin[i] = MAX_PPR_Umxx;
    save_ArchMeachVar();
    // -- Reset if need MechAnswer
    for(i = 0; i < NMB_Interface; i++){
        ResetArchivAnswer(i);
    }
}
#else
void StartKillArch_foNewDTS(LPDATE_TIME pCrnt_dts, LPDATE_TIME pNew_dts){
    time_t tmt_crnt, tmt_new;
    enum enTypeArch TypeArch;

    for(TypeArch = eARCH_DAY; TypeArch < NMB_ACRH; TypeArch++){
        // -- get tmt crnt, new
        tmt_crnt = ListArch[TypeArch].fun_Align_tmt(GetTmt_dts(pCrnt_dts));
        tmt_new = ListArch[TypeArch].fun_Align_tmt(GetTmt_dts(pNew_dts));
        // -- Check Need kill arch
        KillArchRecord(tmt_new, tmt_crnt, &ListArch[TypeArch]);
    }
    // -- Update if need PPR
    arch_var.fSaveArch = true;
    if(rtc_tbl.crnt_tmt % TMT_30MIN) ppr.fValid_rcrd.fild.ePrfl_sts = ePrflIncompl;
    else ppr.fValid_rcrd.fild.ePrfl_sts = ePrflValid;
    // -- Save label time arch
    save_lbl_time_arch(pNew_dts);
}
#endif
