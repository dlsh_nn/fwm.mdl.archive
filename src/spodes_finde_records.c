#include "define.h"
/* ***************************************************
 * Algorithm Finde number records according SPODES
 * ***************************************************
 * For PPR
 * 1. If tmt == tBgn then cnt_remove = 0
 * 2. If fFix == 0 then cnt_remove++
 * 3. If tmt == tEnd then fEnd = true, fFix = true ==> cnt_remove == const
 * 4. If tmt -> Record_valid then cnt_remove = 0, fFix = true ==> cnt_remove == const
 * 5. If tmt -> Record_curent & !fEnd then cnt_remove = 0, fFix = false ==> cnt_remove must increment
 * 6. If tmt -> End day then Nmb = Nmb - cnt_remove + cnt_add, cnt_remove = 0, cnt_add = 0,
 *                           fFix = false ==> cnt_remove must increment
 * 7. If double detected && !fEnd then cnt_add++ 
 */

void InitSPODES_Finde(LPARCH_ANSW_INIT pivars, LPARCH_ANSW_VAR pvars){
    if((pivars->Type & MSK_TypeV) == TypePPR_getNmb_DTS){
        if(pivars->TypeArch == eARCH_PPR){
            // -- Calculated number records
            pvars->spFinde.NmbRcrds = (pivars->tmt_end - pivars->tmt_bgn) / 
                                        arch_var.save.tmt_Duration + 1;
            // -- Save actual tmtBgn, tmtEnd
            pvars->spFinde.tBgn = pivars->tmt_bgn;
            pvars->spFinde.tEnd = pivars->tmt_end;
            // -- Align tmt on days
            pivars->tmt_bgn = GetTmtArchDay_align(pivars->tmt_bgn);
            pivars->tmt_end = GetTmtArchDay_align(pivars->tmt_end + TMT_1DAY) - 
                                    arch_var.save.tmt_Duration;
        }else if(pivars->TypeArch == eARCH_DAY){
            pvars->spFinde.NmbRcrds = (pivars->tmt_end - pivars->tmt_bgn) / TMT_1DAY + 1;
        }
    }
}
void SPODES_Finde_PreNewTmt(time_t tmt, uint8_t Type, LPARCH_ANSW_VAR pvars){
    if((Type & MSK_TypeV) == TypePPR_getNmb_DTS){
        if(tmt == pvars->spFinde.tEnd){
            pvars->spFinde.fEnd = true;
            pvars->spFinde.fFixCnt = true;
        }
        if(tmt == GetTmtArchPPR_align(pvars->tmt_crnt)){
            if(!pvars->spFinde.fEnd){
                pvars->spFinde.cnt_remove = 0;
                pvars->spFinde.fFixCnt = false;
            }
        }
        if( !((tmt + arch_var.save.tmt_Duration) % TMT_1DAY)){
            pvars->spFinde.NmbRcrds -= Min(pvars->spFinde.NmbRcrds, pvars->spFinde.cnt_remove);
            pvars->spFinde.NmbRcrds += pvars->spFinde.cnt_add;
            // -- reset counters
            pvars->spFinde.cnt_add = 0;
            pvars->spFinde.cnt_remove = 0;
            pvars->spFinde.fFixCnt = false;
        }
    }
}
uint8_t SPODES_Finde_ArchRcrd(LPCARCH_ANSW_INIT pivars, LPARCH_ANSW_VAR pvars){
    if((pivars->Type & MSK_TypeV) == TypePPR_getNmb_DTS){
        pvars->spFinde.cnt_remove = 0;
        pvars->spFinde.fFixCnt = true;
        
        if(pivars->TypeArch == eARCH_MNTH){
            pvars->spFinde.NmbRcrds++;
        }    
        return true;
    }
    return false;
}
uint8_t SPODES_Finde_NullRcrd(LPCARCH_ANSW_INIT pivars, LPARCH_ANSW_VAR pvars){
    if((pivars->Type & MSK_TypeV) == TypePPR_getNmb_DTS){
        if(pvars->tmt == pvars->spFinde.tBgn){
            pvars->spFinde.cnt_remove = 0;
        }
        if(!pvars->spFinde.fFixCnt){
            pvars->spFinde.cnt_remove++;
        }
        if(pivars->TypeArch == eARCH_DAY){
            if(pvars->spFinde.NmbRcrds) pvars->spFinde.NmbRcrds--;
        }
        return true;
    }
    return false;
}
void SPODES_Finde_ArchRcrd_Dbl(uint8_t Type, LPARCH_ANSW_VAR pvars){
    if((Type & MSK_TypeV) == TypePPR_getNmb_DTS){
        if(!pvars->spFinde.fEnd){
            pvars->spFinde.cnt_add++;
        }
    }
}
void SPODES_Finde_OutNmbRecord(uint8_t Type, LPARCH_ANSW_VAR pvars){
    if((Type & MSK_TypeV) == TypePPR_getNmb_DTS){
        LPPACK_ANSW_PPR_NMB_RCRD p_out = (LPPACK_ANSW_PPR_NMB_RCRD)pvars->pout;
    
        p_out->Nmb = pvars->spFinde.NmbRcrds;
        // -- 
        AnswPrtcl_ARCH(sizeof(*p_out));
    }
}