#include "define.h"

// -- Need Init with use, after defined.
// Init: TMT_TypeArch[eARCH_PPR] = arch_var.save.tmt_Duration;
#define _TMT_Duration_Arch() uint32r_t TMT_TypeArch[NMB_ACRH] = {TMT_1DAY, TMT_1MONTH, -1}

LPARCH_ANSW_INIT    pivars;
LPARCH_ANSW_VAR     pvars;

enum enStatusPrtcl CheckValid_ArchTmt(void){
    _TMT_Duration_Arch();
    enum enStatusPrtcl sts = STS_OK;
    uint8_t TypeV = (pivars->Type & MSK_TypeV);
    
    if( (pivars->Type == enTPPR_getNmb) ||
        (pivars->Type == enTPPR_PPRvs4_by_INDX) ) return sts;
    
    TMT_TypeArch[eARCH_PPR] = arch_var.save.tmt_Duration;
    if(pivars->TypeArch == eARCH_MNTH){
        DATE_TIME dts;
        
        GetDts_tmt(pivars->tmt_bgn, &dts);
        if((dts.sec != 0) || (dts.min != 0) || (dts.hour != 0) || (dts.date != 1)){
            sts = STS_ERR_PRM;
        }
    }else if(pivars->TypeArch > NMB_ACRH){
        sts = STS_ERR_PRM;
    }else if( (pivars->tmt_bgn % TMT_TypeArch[pivars->TypeArch]) || 
              (pivars->tmt_end % TMT_TypeArch[pivars->TypeArch]) ){
        sts = STS_ERR_PRM;
    }else if(pivars->TypeArch == eARCH_PPR){
        if(TypeV == TypePPR_getNmb_DTS){
            if(pivars->tmt_bgn > pivars->tmt_end){
                sts = STS_ERR_PRM;
            }
        }else if(pivars->Type & MSK_SeasonRcrds){
            sts = CheckValidPrm(pivars);
        }
    }
    
    return sts;
}
enum enStatusPrtcl CheckRequestNmbRcrds(void){
    uint16_t NmbGetRcrd;
    enum enStatusPrtcl sts = STS_OK;
    _TMT_Duration_Arch();
    
    TMT_TypeArch[eARCH_PPR] = arch_var.save.tmt_Duration;
    
    if( (pivars->Type == enTPPR_getNmb) || 
        (pivars->Type == enTPPR_PPRvs4_by_INDX) ) return sts;
    
    NmbGetRcrd = abs_my((pivars->tmt_end - pivars->tmt_bgn)) / TMT_TypeArch[pivars->TypeArch];
    if( NmbGetRcrd > ListArch[pivars->TypeArch].nmb_record_declared){
        sts = STS_ERR_PRM;
    }
    
    return sts;
}
enum enStatusPrtcl find_on_arch(void){
    enum enStatusPrtcl sts = STS_NOT_FOUND;
    time_t tmt_arch, tmt;
    HEAD_LIST head;
    AEMem_t addr_rcrd_arch;
    uint16_t i, indx = 0;
    int32_t indx_rcrd = 0;
    
    rd_ext_mem(ListArch[pivars->TypeArch].AddrArchHead, &head, sizeof(head));
    // -- fined DTS in Arch
    for(i = 0; i <= head.nmb_rcrds; i++){
        // -- read head record
        addr_rcrd_arch = GetAddrRecord(indx_rcrd, head.Addr_rcrd, &ListArch[pivars->TypeArch].lfm);
        rd_ext_mem(addr_rcrd_arch + offsetof(RCRD_ARCH_WITH_SHIFT, arch.tmt), &tmt_arch, sizeof(tmt_arch));
        if((pivars->tmt_bgn <= tmt_arch) && (tmt_arch <= pivars->tmt_end)){
            sts = STS_OK;
            break;
        }
        indx_rcrd--;
    }
    if(pivars->TypeArch == eARCH_PPR){
        // -- Current time
        tmt = ListArch[pivars->TypeArch].fun_Align_tmt(pvars->tmt_crnt);
        if((pivars->tmt_bgn <= tmt) && (tmt <= pivars->tmt_end)){
            sts = STS_OK;
        }
    }
    
    return sts;
}
int16_t GetArch_NmbRcrd(void){
    HEAD_LIST head;
    
    rd_ext_mem(ListArch[pivars->TypeArch].AddrArchHead, &head, sizeof(head));
    
    if(head.nmb_rcrds > 1){
        return (pivars->TypeArch == eARCH_PPR) ? (head.nmb_rcrds - 1) : head.nmb_rcrds;
    }
    
    return 0;
}
void InitModeAns_AnswBgn(void){
    time_t tmt_bgn = pivars->tmt_bgn - (time_t)pivars->season.fSeasonBgn * 60 * 60L;
    time_t tmt_end = pivars->tmt_end - (time_t)pivars->season.fSeasonEnd * 60 * 60L;
    
    pvars->mode = enMdAns_AnswBgn;
    pvars->tmt = pivars->tmt_bgn;
    pvars->DirectFinde = (tmt_bgn <= tmt_end) ? 1 : -1;
    pvars->Direction = pvars->DirectFinde;
    if(pvars->DirectFinde < 0){
        pvars->indx = pvars->bgn_indx = -1 * GetArch_NmbRcrd();
        pvars->end_indx = 0;
    }else{
        pvars->indx = pvars->bgn_indx = 0;
        pvars->end_indx = -1 * GetArch_NmbRcrd();
    }
}
void InitMechAnswer_Vars(LPARCH_ANSW_INIT pivars, LPARCH_ANSW_VAR pvars){
    pvars->fAnsweBegin = true;
    pvars->pout = pivars->prtcl_data->pHead->Data;
    if(!pvars->mode){
        InitModeAns_AnswBgn();
    }
    // crnt rcrd PPR
    pvars->tmt_crnt = rtc_tbl.crnt_tmt;
    memcpy(&pvars->rcrd, &ppr.rcrd, sizeof(pvars->rcrd));
}
enum enStatusPrtcl InitMechAnsw_Arch(LPARCH_ANSW_INIT pVarInit){
    enum enStatusPrtcl sts = STS_OK;
    HEAD_LIST head;
    uint8_t i;
    enum enTypeInterface typeInterface;
    

    // -- Check TypeArch
    if(pVarInit->TypeArch >= NMB_ACRH){
        sts = STS_ERR_PRM;
        goto __error;
    }
    // -- Check valid records
    rd_ext_mem(ListArch[pVarInit->TypeArch].AddrArchHead, &head, sizeof(head));
    if(!head.nmb_rcrds){
        sts = STS_NOT_FOUND;
        goto __error;
    }
    // -- Init Vars
    typeInterface = pVarInit->prtcl_data->pDriver->TypeInterface;
    if(arch_var.pAnswVar[typeInterface]){
        sts = ANSW_METER_BUSY;
        goto __error;
    }
    arch_var.pAnswVar[typeInterface] = (LPARCH_ANSW_VAR)&
            pVarInit->prtcl_data->pHead->Data[(*pVarInit->prtcl_data->psize_buf - offsetof(HeadPack, Data)) - sizeof(*pvars)];
    pvars = arch_var.pAnswVar[typeInterface];
    pivars = &pvars->init_vars;
    memset(pvars, 0, sizeof(*pvars));
    memcpy(pivars, pVarInit, sizeof(*pivars));
    
    // -- Finde by Index
    if( (sts = CheckFindeByIndex(pivars)) != STS_OK ) goto __error;
    // -- Check Valid tmt
    if((sts = CheckValid_ArchTmt()) != STS_OK) goto __error;
    // -- Check Valid TypeV
    for(i = 0; i < NMB_TypeV; i++){
        if(ListArch[pivars->TypeArch].SupportTypeV[i] == pivars->Type) break;
    }
    if(i >= NMB_TypeV){
        sts = STS_ERR_PRM;
        goto __error;
    }
    // -- Check found if enType_Pass
    if((pivars->Type & MSK_bitArch_Skip) == enType_Pass){
        if((sts = find_on_arch()) != STS_OK) goto __error;
    }
    // -- Check NmbGetRcrd
    if( (sts = CheckRequestNmbRcrds()) != STS_OK) goto __error;
    // -- Init FindeByIndex
    InitFindeByIndex(pivars, pvars);
    // --Init SPODES Finde Mech
    InitSPODES_Finde(pivars, pvars);
    // -- Init PPR season finde
    InitPPrSeasonFinde(pivars, pvars);
    // -- InitMechAnswer Vars
    pvars->driver_size_buf_save = *pivars->prtcl_data->psize_buf;
    *pivars->prtcl_data->psize_buf -= sizeof(*pvars);
    InitMechAnswer_Vars(pivars, pvars);

    return sts;
__error:
    arch_var.pAnswVar[typeInterface] = 0;
    return sts;
}

//получаем время в записи по индексу
time_t GetArch_tmt_CheckIndx(int16_t indx){
    HEAD_LIST head;
    AEMem_t addr;
    time_t tmt = 0;

    SHIFT_TIME st;
    uint8_t fValid;

    if( ((pvars->end_indx - pvars->indx) * pvars->DirectFinde) <= 0 ){
        //считываем адрес первой записи? начала блока?
        //rd_ext_mem(ListArch[pivars->TypeArch].AddrArchHead, &head, sizeof(head));
        //получаем адрес(по индексу), по которому нужно взять метку времени
        //addr = GetAddrRecord(indx, head.Addr_rcrd, &ListArch[pivars->TypeArch].lfm);
        
        rd_ext_mem(ListArch[pivars->TypeArch].AddrArchHead, &head, sizeof(head));
        addr = GetAddrRecord(indx, head.Addr_rcrd, &ListArch[pivars->TypeArch].lfm);
        
//        addr += offsetof(RCRD_ARCH_WITH_SHIFT, arch);
//        //считываем метку времени
//        rd_ext_mem(addr, &tmt, sizeof(tmt));
        rd_ext_mem(addr, &st, sizeof(SHIFT_TIME));
        tmt = st.tmt;
        if(!tmt) tmt = -1;
        else{
            addr += offsetof(RCRD_PWR_PRFL_WITH_SHIFT, prfl.fValid);
            rd_ext_mem(addr, &fValid, sizeof(fValid));
            pvars->season.fSeason_of_index = ((fValid & MSK_SUMMER_TIME) != 0);
        }
    }    
    return tmt;
}


SHIFT_TIME GetArchiveSearchInfo(int16_t index, enum enTypeArch TypeArch){
    HEAD_LIST head;
    AEMem_t addr;
    time_t tmt = 0;

    SHIFT_TIME st;
        
    rd_ext_mem(ListArch[TypeArch].AddrArchHead, &head, sizeof(head));
    addr = GetAddrRecord(index, head.Addr_rcrd, &ListArch[TypeArch].lfm);

    rd_ext_mem(addr, &st, sizeof(SHIFT_TIME));
   
    return st;
}

//For temporary purpose 
void SetSummerWinterTimeChange(int16_t index, enum enTypeArch TypeArch) {
    HEAD_LIST head;
    AEMem_t addr;
    uint8_t fValid;

    rd_ext_mem(ListArch[TypeArch].AddrArchHead, &head, sizeof(head));
    addr = GetAddrRecord(index, head.Addr_rcrd, &ListArch[TypeArch].lfm);

    addr += offsetof(RCRD_PWR_PRFL_WITH_SHIFT, prfl.fValid);
    rd_ext_mem(addr, &fValid, sizeof(fValid));
    pvars->season.fSeason_of_index = ((fValid & MSK_SUMMER_TIME) != 0);
}

//bool BinNext_indx(enum enMode_AnswArch Mode){
//    int16_t shift;
//    
//    int16_t bgn_index = pvars->bgn_indx;
//    int16_t end_index = pvars->end_indx;
//    
//    int16_t index = (pvars->end_indx-pvars->indx)/2;    
//    
//    time_t tmt = pvars->tmt;
//    while(true) {
//        time_t tmt_of_index = GetArch_tmt_CheckIndx(index);
//        if((tmt - tmt_of_index)<0) {
//            bgn_index = index;
//            //shift = (bgn_index - end_index + 1)/2;
//            int16_t diff = bgn_index - end_index;
//            shift = diff == 1 ? 1: diff/2; 
//        } else if((tmt - tmt_of_index)>0){
//            end_index = index;
//            //shift = (end_index - bgn_index - 1)/2;
//            int16_t diff = end_index - bgn_index;
//            shift = diff == 1 ? 1: diff/2; 
//        } else {
//            pvars->indx = index;
//            pvars->tmt_of_indx = tmt_of_index;
//            return true;
//        }
//        if(!shift) {
//           pvars->mode = Mode;
//           return false;
//        }
//        index -= shift;//-1 * pvars->Direction;
//    }
//}

int16_t BinSearch_index(time_t tmt, int16_t bgn_index, int16_t end_index, enum enTypeArch TypeArch) {
  
    int16_t shift = 0;
    int16_t bgn_ind = bgn_index;
    int16_t end_ind = end_index;
    int16_t diff = 0;
    
    int16_t index = (end_index - bgn_index) / 2;
    
    time_t tmt_to_find = tmt;
	
     do {
        SHIFT_TIME st_of_index;
        
        st_of_index = GetArchiveSearchInfo(index, TypeArch);

        if(st_of_index.tmt == 0) {
            if (st_of_index.shift > 0) {
                st_of_index = GetArchiveSearchInfo(index - st_of_index.shift, TypeArch);
            } 
        }
        
        if( (tmt_to_find - st_of_index.tmt) < 0 ) {
            bgn_ind = index;
            diff = bgn_ind - end_ind; 
			
        } else if( (tmt_to_find - st_of_index.tmt) > 0 ) {
            end_ind = index;
            diff = end_ind - bgn_ind;
			
        } else {
            st_of_index = GetArchiveSearchInfo(index, TypeArch);
            if(st_of_index.shift > 0) {
                return index - st_of_index.shift;
            } else if(st_of_index.shift == -1) {
                return index;
            }else {
                return 1;
            }
        }

            shift = ((diff == 1) || (diff == -1)) && (shift != -diff) ? diff : diff/2; 
            index -= shift;

    } while(shift != 0);
    return 1; //because correct indexes in [-5999,0] range
}

bool Next_indx(enum enMode_AnswArch Mode){
    if((pvars->Direction * pvars->DirectFinde) > 0){
        if(pvars->indx == pvars->end_indx){
            pvars->mode = Mode;
            return false;
        }
    }else{
        if(pvars->indx == pvars->bgn_indx){
            pvars->mode = Mode;
            return false;
        }
    }
    pvars->indx += -1 * pvars->Direction;
    return true;
}

void AnswPrtcl_ARCH(uint16_t size_pack){
    if(pvars->fAnsweBegin){
        pvars->fAnsweBegin = false;
        // -- Answer BEGIN
        pvars->pout[0] = STS_OK;
        AnswPrtcl_begin(pivars->prtcl_data, 1);
    }
    
    AnswPrtcl_data(pivars->prtcl_data, size_pack);
}
void OutArch_HEAD(void){
    LPPACK_ANSW_ARCH_HEAD p_out = (LPPACK_ANSW_ARCH_HEAD)pvars->pout;
    
    /// -- Check Type
    if( (pivars->Type != enType_ArchGetNmb_DS) &&
        (pivars->Type != enType_ArchGetNmb) ){
        /// -- Check TypeArchive
        switch(pivars->TypeArch){
        case eARCH_DAY:
        case eARCH_MNTH:
            // -- Answer Archiv HEAD
            p_out->TypeArch = pivars->TypeArch;
            p_out->MskTarif = pivars->MskTarif;
            AnswPrtcl_ARCH(sizeof(*p_out));
            break;
        }
    }
}
//-----------------------------------------------------------------------------
// -- If no records
//-----------------------------------------------------------------------------
void OutNullRecord_Arch(void){
    LPRCRD_ANSW_ARCH pdata_out = (LPRCRD_ANSW_ARCH)pvars->pout;
    uint8_t j, msk_tarif = 0x01;
    LPDATE pDs = (LPDATE)pvars->pout;
    
    // -- Answer DS HEAD
    GetDs_tmt(pvars->tmt, pDs);
    AnswPrtcl_ARCH(sizeof(DATE_t));
    // -- out data
    memset(pdata_out->Data, 0xFF, sizeof(ENERGY));
    for(j = 0; j < NMB_TARIF; j++){
        if(pivars->MskTarif & msk_tarif){
            AnswPrtcl_ARCH(sizeof(ENERGY));
        }
        msk_tarif <<= 1;
    }
}
uint16_t SizePackPPRvxx(uint8_t TypePPRvxx, uint8_t fValid){
    LPPACK_ANSW_PWR_PRFL p_out = (LPPACK_ANSW_PWR_PRFL)pvars->pout;
    LPPACK_ANSW_PWR_PRFL_VS4 p_out_vs04 = (LPPACK_ANSW_PWR_PRFL_VS4)pvars->pout;
    uint16_t size_pack = sizeof(PACK_ANSW_PWR_PRFL);
    uint8_t fValid_Status = fValid;
    uint8_t *pfValid = &p_out->fValid;
    
    if(TypePPRvxx == TypePPR_v00){
        size_pack = offsetof(PACK_ANSW_PWR_PRFL, prfl.v2) + 1;
        pfValid = (uint8_t *)(&p_out->prfl.v2);
    }else if( TypePPRvxx == TypePPR_v02){
        size_pack = offsetof(PACK_ANSW_PWR_PRFL, prfl.vs4) + 1;
        pfValid = (uint8_t *)(&p_out->prfl.vs4);
        // -- 
        fValid_Status &= ~MSK_SUMMER_TIME;
    }else if( TypePPRvxx == TypePPR_vs04){
        memcpy(&p_out_vs04->vs4, &p_out->prfl.vs4, sizeof(p_out_vs04->vs4) + 1);
        size_pack = sizeof(PACK_ANSW_PWR_PRFL_VS4);
        pfValid = (uint8_t *)(&p_out_vs04->fValid);
    }
    // -- Clear Flag Summere in fValid
    if((fValid & ~MSK_SUMMER_TIME) < ePrflTypeEnd){
        fValid_Status &= ~MSK_SUMMER_TIME;
    }
    // -- Summer flasg
    if(fValid & MSK_SUMMER_TIME){
        p_out->dts.day |= MSK_SUMMER_TIME;
    }
    *pfValid = fValid_Status;
    
    return size_pack;
}
void OutNullRecord_PPR(void){
    LPPACK_ANSW_PWR_PRFL p_out = (LPPACK_ANSW_PWR_PRFL)pvars->pout;
    uint16_t size_pack;
    uint8_t TypeV = (pivars->Type & MSK_TypeV);

    memset(p_out, 0xFF, sizeof(PACK_ANSW_PWR_PRFL));
    GetDts_tmt(pvars->tmt, &p_out->dts);
    // --
    size_pack = SizePackPPRvxx(TypeV, ePrflNoValid_NoFinde);
    p_out->dts.day &= ~MSK_SUMMER_TIME;
    p_out->dts.day |= SetSummerWinter_Check(&p_out->dts);
    // -- If Season Finde Type
    p_out->dts.day = SetSeason_for_NullRcrd(p_out->dts.day, &pvars->season);
    // -- Out
    AnswPrtcl_ARCH(size_pack);
}
void CheckNeedOutNullRecord(void){
    if(SPODES_Finde_NullRcrd(pivars, pvars)) return;
    // -- If Need Out Passs record
    if((pivars->Type & MSK_bitArch_Skip) == enType_NoPass){
        switch(pivars->TypeArch){
        case eARCH_DAY:
        case eARCH_MNTH:
            OutNullRecord_Arch();
            break;
        case eARCH_PPR:
            OutNullRecord_PPR();
            break;
        }
    }
}
//-----------------------------------------------------------------------------
// -- OutArchiveRecord
//-----------------------------------------------------------------------------
void OutArchiveRecord_Arch(void){
    uint8_t j, msk_tarif = 0x01;
    HEAD_LIST head;
    AEMem_t addr;
    LPRCRD_ANSW_ARCH pdata_out = (LPRCRD_ANSW_ARCH)pvars->pout;
    LPDATE pDs = (LPDATE)pvars->pout;
    
    // -- Answer DS HEAD
    GetDs_tmt(pvars->tmt, pDs);
    AnswPrtcl_ARCH(sizeof(DATE_t));
    // -- Data
    rd_ext_mem(ListArch[pivars->TypeArch].AddrArchHead, &head, sizeof(head));
    addr = GetAddrRecord(pvars->indx, head.Addr_rcrd, &ListArch[pivars->TypeArch].lfm);
    for(j = 0; j < NMB_TARIF; j++){
        if(pivars->MskTarif & msk_tarif){
            rd_ext_mem(addr + offsetof(RCRD_ARCH_WITH_SHIFT, arch.enrg_trf.enrg[j]), pdata_out, sizeof(ENERGY));
            AnswPrtcl_ARCH(sizeof(ENERGY));
        }
        msk_tarif <<= 1;
    }
}
void PPR_RemoveSign(void){
    int32_t *pValPwr;
    uint8_t i;
    LPPACK_ANSW_PWR_PRFL p_out = (LPPACK_ANSW_PWR_PRFL)pvars->pout;
    
    pValPwr = (int32_t *)&(p_out->prfl.v0.pwr_peak);
    for(i = 0; i < 4; i++, pValPwr++){
        *pValPwr = abs_my(*pValPwr);
    }
}
void OutArchiveRecord_PPR(void){
    HEAD_LIST head;
    AEMem_t addr;
    LPPACK_ANSW_PWR_PRFL p_out = (LPPACK_ANSW_PWR_PRFL)pvars->pout;
    uint8_t TypeV = (pivars->Type & MSK_TypeV);
    uint16_t size_pack;
    
    if(!OutRcrdPPR_consider_Season(&pvars->season)) return;
    // -- Forbidden output current tmt
    if( (pvars->tmt != GetTmtArchPPR_align(pvars->tmt_crnt)) || 
        !OutRcrdPPR_Forbidden_consider_Season(&pvars->season) ){
        
        GetDts_tmt(pvars->tmt, &p_out->dts);
        // -- read head record
        rd_ext_mem(ListArch[pivars->TypeArch].AddrArchHead, &head, sizeof(head));
        addr = GetAddrRecord(pvars->indx, head.Addr_rcrd, &ListArch[pivars->TypeArch].lfm);
        addr += offsetof(RCRD_PWR_PRFL_WITH_SHIFT, prfl.dt);
        rd_ext_mem(addr, &p_out->prfl, sizeof(p_out->prfl) + 1);
        size_pack = SizePackPPRvxx(TypeV, p_out->fValid);
        if(p_out->fValid <= ePrflCurrent){
            PPR_RemoveSign();
        }
        // -- 
        AnswPrtcl_ARCH(size_pack);
    }
}
void OutArchiveRecord(void){
    if(SPODES_Finde_ArchRcrd(pivars, pvars)) return;
    switch(pivars->TypeArch){
    case eARCH_DAY:
    case eARCH_MNTH:
        OutArchiveRecord_Arch();
        break;
    case eARCH_PPR:
        OutArchiveRecord_PPR();
        break;
    }
}
//-----------------------------------------------------------------------------
// -- OutCurrentRecord
//-----------------------------------------------------------------------------
void OutCurrentRecord_PPR(void){
    LPPACK_ANSW_PWR_PRFL p_out = (LPPACK_ANSW_PWR_PRFL)pvars->pout;
    uint8_t TypeV = (pivars->Type & MSK_TypeV);
    uint16_t size_pack;
    
    GetDts_tmt(pvars->tmt, &p_out->dts);
    p_out->dts.day |= SetSummerWinter();
    size_pack = SizePackPPRvxx(TypeV, ePrflCurrent);
    if(TypeV == TypePPR_vs04){
        LPPACK_ANSW_PWR_PRFL_VS4 p_out_vs04 = (LPPACK_ANSW_PWR_PRFL_VS4)pvars->pout;
        PreSavePPR(&pvars->rcrd);
        memcpy(&p_out_vs04->enrg, &pvars->rcrd.v0.enrg, sizeof(p_out_vs04->enrg));
        memcpy(&p_out_vs04->vs4, &pvars->rcrd.vs4, sizeof(p_out_vs04->vs4));
    }else memcpy(&p_out->prfl.v0, &pvars->rcrd.v0, size_pack - sizeof(DATE_TIME) - 1);
    PPR_RemoveSign();
    // -- 
    AnswPrtcl_ARCH(size_pack);
}
/**
 * \brief Проверка на соответствие искомому штампу времени pvars->tmt,
 *         текущей записи в профиле нагрузке.
 * @return  0 - Текущий получас не соответстувет искомому штампу времени,
 *                возможно надо выдать нулевую запись.
 *          1 - Текущий получас выдан. 
 */
int8_t CheckNeedOutCurrentRecord(void){
    if(pivars->TypeArch == eARCH_PPR){
        if(pvars->tmt == GetTmtArchPPR_align(pvars->tmt_crnt)){
            if((pivars->Type & MSK_TypeV) == TypePPR_getNmb_DTS){

            }else{
                if(OutCurrentRcrdPPR_consider_SeasonPPR(pvars)){
                    OutCurrentRecord_PPR();
                }else return 0;
            }
            return 1;
        }
    }
    return 0;
}

//-----------------------------------------------------------------------------
// -- Mech Answer Record
//-----------------------------------------------------------------------------
void MechAnswerArch(void){
    time_t dlt_tmt;
    uint8_t cnt_read_tmt = 5;
    
    // -- Check Interface
    arch_var.indx_Interface++;
    if(arch_var.indx_Interface >= NMB_Interface) arch_var.indx_Interface = 0;
    // -- init Local Variable
    pvars = arch_var.pAnswVar[arch_var.indx_Interface];
    pivars = &pvars->init_vars;
    // -- Check
    if(!pvars) goto __end;
      
    // -- Check ready interface
    if(pivars->prtcl_data->pDriver->Status() == mdm_DONT_WORK_sts){
        goto __reset_mech;
    }else if(pivars->prtcl_data->pDriver->Status() != mdm_READY_sts) goto __end;
    // --
    switch(pvars->mode){
    case enMdAns_AnswBgn:
        // -- Answer Begin
        AnswPrtcl_ARCH(0);
        OutArch_HEAD();
        pvars->mode = enMdAns_Finde_tmt;
        break;
    case enMdAns_Finde_tmt:
        if(getEnableSeason()) {
            while(( pvars->mode == enMdAns_Finde_tmt) && (cnt_read_tmt--) ){
                pvars->tmt_of_indx = GetArch_tmt_CheckIndx(pvars->indx);
                // -- Check Valid
                if(pvars->tmt_of_indx == -1){
                    Next_indx(enMdAns_OutNullRecord);
                    continue;
                }else if(!pvars->tmt_of_indx) pvars->mode = enMdAns_OutNullRecord;
                // --
                dlt_tmt = (pvars->tmt_of_indx - pvars->tmt) * pvars->Direction;
                if(!dlt_tmt){
                    if(Check_season_of_index(&pvars->season)){
                        pvars->mode = enMdAns_OutArchiveRecord;
                    }else{
                        Next_indx(enMdAns_OutNullRecord);
                        continue;
                    }
                }else if(dlt_tmt > 0){
                    FilterAnsw_PreNextIndex(pvars);
                    Next_indx(enMdAns_OutNullRecord);
                }else if(dlt_tmt < 0){
                    if(dlt_Season(pvars) <= 0){
                        pvars->mode = enMdAns_OutNullRecord;
                    }else{
                        FilterAnsw_PreNextIndex(pvars);
                        Next_indx(enMdAns_OutNullRecord);
                    }
                }
            }
        } else {
            while( ( pvars->mode == enMdAns_Finde_tmt) && (cnt_read_tmt--) ){
                pvars->tmt_of_indx = GetArch_tmt_CheckIndx(pvars->indx);
                // -- Check Valid
                if(pvars->tmt_of_indx == -1){
                    Next_indx(enMdAns_OutNullRecord);
                    continue;
                }else if(!pvars->tmt_of_indx) pvars->mode = enMdAns_OutNullRecord;
                // --
                dlt_tmt = (pvars->tmt_of_indx - pvars->tmt);
                if(!dlt_tmt){
                    pvars->mode = enMdAns_OutArchiveRecord;
                } else if((pvars->Direction > 0) || (pvars->Direction * pvars->DirectFinde > 0) || (pvars->Direction < 0 && pvars->indx == 0)) {
                    int16_t end_indx, bgn_indx, index;
                
                    end_indx = (pvars->end_indx < pvars->bgn_indx ) ? pvars->end_indx : pvars->bgn_indx;
                    bgn_indx = (pvars->end_indx > pvars->bgn_indx ) ? pvars->end_indx : pvars->bgn_indx;;
                    index = BinSearch_index(pvars->tmt, bgn_indx, end_indx, pivars->TypeArch);
                    if(index <= 0) {
                        pvars->indx = index;
                        pvars->tmt_of_indx = GetArch_tmt_CheckIndx(pvars->indx);
                    } else {
                        pvars->mode = enMdAns_OutNullRecord;
                    }                                                                 
                } else {
                    FilterAnsw_PreNextIndex(pvars);
                    Next_indx(enMdAns_OutNullRecord);                
                } 
            }
        }
        break;
    case enMdAns_FindeDbl_tmt:
        while(( pvars->mode == enMdAns_FindeDbl_tmt) && (cnt_read_tmt--) ){
             pvars->tmt_of_indx = GetArch_tmt_CheckIndx(pvars->indx);
            // -- Check Valid
            if(pvars->tmt_of_indx == -1){
                Next_indx(enMdAns_CheckCurrentRecord);
                continue;
            }else if(!pvars->tmt_of_indx) pvars->mode = enMdAns_CheckCurrentRecord;
            // --
            if(pvars->tmt_of_indx == pvars->tmt){
                SPODES_Finde_ArchRcrd_Dbl(pivars->Type, pvars);
                pvars->mode = enMdAns_OutArchiveRecord_Dbl;
                break;
            }
            if(pvars->cnt_FindeDbl--){
                Next_indx(enMdAns_CheckCurrentRecord);
            }else{
                pvars->mode = enMdAns_CheckCurrentRecord;
            }
        }
        break;
    case enMdAns_OutArchiveRecord:
    case enMdAns_OutArchiveRecord_Dbl:
        OutArchiveRecord();
        // -- Save position
        if(pvars->mode == enMdAns_OutArchiveRecord) pvars->indx_restore = pvars->indx;
        // -- Finde Dbl
        if(Next_indx(enMdAns_CheckCurrentRecord)){
            pvars->cnt_FindeDbl = MaxDblRecords(pivars->TypeArch, pvars->tmt);
            pvars->cnt_FindeDbl = FindeDbl_considerind_Season(pvars->cnt_FindeDbl);
            if(pvars->cnt_FindeDbl) pvars->mode = enMdAns_FindeDbl_tmt;
            else pvars->mode = enMdAns_CheckCurrentRecord;
        }
        break;
    case enMdAns_CheckCurrentRecord:
        CheckNeedOutCurrentRecord();
    
        pvars->indx = pvars->indx_restore;
        pvars->Direction = pvars->DirectFinde;
        pvars->mode = enMdAns_Next_tmt;
        break;
    case enMdAns_OutNullRecord:
        // -- Check Current
        if(!CheckNeedOutCurrentRecord()){
            if(FilterAnsw_CheckNeed_OutNullRecord(pvars)){
                CheckNeedOutNullRecord();
            }
        }
        pvars->mode = enMdAns_Next_tmt;
        break;
    case enMdAns_Next_tmt:
    {
        time_t tmt_prev = pvars->tmt;
        int8_t fssn_prev = pvars->season.fssn;
        
        SPODES_Finde_PreNewTmt(pvars->tmt, pivars->Type, pvars);
        pvars->tmt = ListArch[pivars->TypeArch].fun_Next_tmt();
        /// dlt_tmt = (pivars->tmt_end - pvars->tmt) * pvars->DirectFinde;
        dlt_tmt = dlt_tmt_season(pivars->tmt_end, pvars->season.fSeasonEnd, 
                    pvars->tmt, pvars->season.fssn, &pvars->season) * (time_t)pvars->DirectFinde;
        if(dlt_tmt >= 0){
            pvars->mode = enMdAns_Finde_tmt;
            /// dlt_tmt = (pvars->tmt - tmt_prev) * pvars->DirectFinde;
            dlt_tmt = dlt_tmt_season(pvars->tmt, pvars->season.fssn, 
                        tmt_prev, fssn_prev, &pvars->season) * pvars->DirectFinde;
            if(dlt_tmt >= 0){
                pvars->Direction *= -1;
            }
        }else{
            SPODES_Finde_OutNmbRecord(pivars->Type, pvars);
            // EndProcess
            AnswPrtcl_end(pivars->prtcl_data);
            goto __reset_mech;
        }
        break;
    }
    case enMdAns_FindeByIndex_Begin:
    case enMdAns_FindeByIndex_CheckIndexValid:
    case enMdAns_FindeByIndex_NmbRcrd:
    case enMdAns_FindeByIndex_NmbRcrd_Archive:
    case enMdAns_Answ_STS_ERR_PRM:
        pvars->mode = FindeByIndex_exec(pivars, pvars, cnt_read_tmt);
        break;
    case enMdAns_ResetMech:
    default:
        // Abort interupted process
        if(!pvars->fAnsweBegin){
            AnswPrtcl_end(pivars->prtcl_data);
        }
        goto __reset_mech;
        break;
    }
__end:
    if(pvars){
        if(pvars->fResetMech) pvars->mode = enMdAns_ResetMech;
    }
    return;
    
__reset_mech:
    *pivars->prtcl_data->psize_buf = pvars->driver_size_buf_save;
    pivars->prtcl_data->pDriver->ManagerRec(enDrvMng_START_REC);
    arch_var.pAnswVar[arch_var.indx_Interface] = 0;
}
time_t NexBgnPPR(void){
    time_t next_tmt;
    
    next_tmt = (pvars->tmt + arch_var.save.tmt_Duration * pvars->DirectFinde);
    next_tmt = NextBgnPPR_considering_Season(next_tmt, pvars, &pvars->season);
    
    return next_tmt;
}
time_t NexBgnDay(void){
    return (pvars->tmt + TMT_1DAY * pvars->DirectFinde);
}
time_t NexBgnMonth(void){
    DATE_TIME dts;
    
    GetDts_tmt(pvars->tmt, &dts);
    dts.month += pvars->DirectFinde;
    // -- Check Valid Month
    if(dts.month > 12){
        dts.month = 1;
        dts.year++;
    }
    if(!dts.month){
        dts.month = 12;
        dts.year--;
    }
    dts.sec = 0;
    dts.min = 0;
    dts.hour = 0;
    dts.date = 1;
    
    return GetTmt_dts(&dts);
}
#pragma save
#pragma NOAREGS
void ResetArchivAnswer(enum enTypeInterface type_interface) small reentrant{
    if(arch_var.pAnswVar[type_interface]){
        arch_var.pAnswVar[type_interface]->fResetMech = true;
    }
}
#pragma restore